## Ejecucion

### Windows
Para poder ejecutar correctamente esta aplicación debe usar el archivo BASH  __proyectIP__, que abrirá las interfaces de Envío y Recepción de paquetes, en un subsistema Linux con WSL. 
### Linux
Para poder ejecutar correctamente esta aplicación debe usar el archivo BASH __proyectIP__, que abrirá las interfaces de Envío y Recepción de paquetes. 

```

$ ./proyectIP.sh
```


