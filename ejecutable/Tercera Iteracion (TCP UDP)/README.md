## Ejecucion

Para poder ejecutar correctamente esta aplicación, debe usar el archivo BASH __proyectIP__, que abrirá las interfaces de Envío y Recepción de paquetes. La aplicación de recepción realiza la obtención de paquetes durante 5 segundos en intervalos de 30 en 30 segundos.

```

$ ./proyectIP.sh
```



> Esta aplicacion funciona de la misma manera que en la iteracion anterior, con la diferencia que el boton "Payload" de ICMP abre la interfaz de dicho protocolo, en lugar de un lector hexadecimal. Además, se han incluido TCP y UDP como protocolos posibles a utilizar.
