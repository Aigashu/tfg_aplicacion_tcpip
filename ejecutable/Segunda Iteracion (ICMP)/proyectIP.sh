#!/bin/bash

sudo chmod 755 ./proyectIP-Envio
sudo chmod 755 ./proyectIP-Recepcion
sudo setcap cap_net_admin,cap_net_raw=pei ./proyectIP-Envio
sudo setcap cap_net_admin,cap_net_raw=pei ./proyectIP-Recepcion

./proyectIP-Envio &
./proyectIP-Recepcion &
