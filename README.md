
TFG del Grado de Ingenieria Informatica sobre la creación de un paquete de datos para enviarlo por la red, a partir de la edición de todos los protocolos que componen dicho paquete.

## Licencia

El contenido de este repositorio se distribuye bajo la licencia _CC0 1.0 Universal (CC0 1.0)_, incluida en el mismo. Esto significa esencialmente que puedes utilizarlo como quieras y para lo que quieras, sin condiciones pero también sin garantía alguna por mi parte.


## Codigo

El codigo de este programa está hecho en lenguaje  [__Rust__](https://www.rust-lang.org/es/), por lo que será necesaria su instalación para poder ejecutarlo. Para ejecutar una aplicación Rust se debe escribir en una línea de comandos de terminal las siguientes instrucciones:

- Para compilar: 
```
cargo build 
```
- Para ejecutar:

```
cargo run
```
- En caso de querer limpiar archivos innecesarios (se crearán de nuevo una vez se vuelva a compilar):
```
cargo clean 
```

Así mismo, es necesario instalar la librería __FLTK__ . Para esto, deberá ejecutarse esta línea de comandos ( o en su defecto instalar las librerías que en ella aparecen) : 

```
sudo apt-get install libx11-dev libxext-dev libxft-dev libxinerama-dev libxcursor-dev libxrender-dev libxfixes-dev libpango1.0-dev libgl1-mesa-dev libglu1-mesa-dev

```

## Instrucciones de ejecucion

Para poder ejecutar correctamente la aplicación, tanto desde código como con el ejecutable es **obligatorio** usar __root__/superusuario, ya que se crear paquetes a nivel bajo y se necesita para ello tener ciertos privilegios del sistema. Para ejecutarlo comodamente, navegue hacia la carpeta "ejecutables"


