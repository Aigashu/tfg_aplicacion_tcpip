#![allow(dead_code)]
#![allow(unused_variables)]
use fltk::{app, menu::*,text::*, button::Button,button::RadioRoundButton,table::*, frame::Frame,enums::Color, group::*, prelude::*, window::Window, input::Input};
use fltk_evented::Listener;
use fltk_grid::Grid;
use crossbeam::channel;
use itertools::Itertools;
use std::collections::HashMap;
use std::cell::Cell;

use crate::Interfaces::Protocols::IP::IP;
use std::sync::{Arc, Mutex};
use std::rc::Rc;
use std::cell::RefCell;
//Struct es donde se alojan las variables 
#[derive(Clone)]
pub struct InterfazPaquete {
    campoVersion : String, //Cuando se edite hay que poner let mut 
    campoIHL : String,
    campoTypeOfService : String,
    campoECN : String, 
    campoLength : String,
    campoID : String,
    campoFlags: String,
    campoFragmentOffSet : String,
    campoTTL : String,
    campoProtocol : String,
    campoHeaderChecksum : String,
    campoSourceAddress : String,
    campoDestinationAddress : String,
    campoOptions : String,
    campoPadding : String,
    campoHexadecimalPayload : String,
    packageIP : IP //Necesitamos el objeto IP para enviar, pedir ayuda, etc
}

//Constructor por defecto
pub fn new() -> InterfazPaquete {
    let ippac = IP::new();
    InterfazPaquete {
    campoVersion : "".to_string(),
    campoIHL : "".to_string(),
    campoTypeOfService : "".to_string(),
    campoECN : "".to_string(),
    campoLength : "".to_string(),
    campoID : "".to_string(),
    campoFlags: "".to_string(),
    campoFragmentOffSet : "".to_string(),
    campoTTL : "".to_string(),
    campoProtocol : "".to_string(),
    campoHeaderChecksum : "".to_string(),
    campoSourceAddress : "".to_string(),
    campoDestinationAddress : "".to_string(),
    campoOptions : "".to_string(),
    campoPadding : "".to_string(),
    campoHexadecimalPayload : "".to_string(),
    packageIP : ippac,
    }


}

//Los métodos se ubican en el impl
impl InterfazPaquete {
    //Metodos para Interfaz

   
    /*
    * @function : Esta funcion creará la interfaz base de la aplicación, que nos permitirá seleccionar la creación
                    o edición del protocolo de la capa que queramos, dentro de los limites que impusimos a la aplicación
    */
    pub fn createInterface(self)
    {
        let app = app::App::default();
        let mut wind = Window::new(100, 100, 300, 300, "Protocol Select"); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let frame = Frame::new(100, 2, 50, 50, "Seleccione un protocolo ");
        let mut butIP = Button::new(120, 210, 50, 50, "IP"); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let mut butTCP = Button::new(90, 140, 50, 50, "TCP");
        let mut butUDP = Button::new(150, 140, 50, 50, "UDP");
        let mut butHTTP = Button::new(120, 75, 50, 50, "HTTP");
        wind.end();
        wind.show();
        // the closure capture is mutable borrow to our button
        butIP.set_callback(move |b|{
            wind.hide();
            let p =  Arc::new(Mutex::new(self.clone()));
            p.lock().unwrap().clone().interfaceIP();
            wind.show();
        });
        butTCP.set_callback(|b| b.set_label("PRESSED")); //WIP
        butUDP.set_callback(|b| b.set_label("PRESSED")); //WIP
        butHTTP.set_callback(|b| b.set_label("PRESSED")); //WIP
        app.run().unwrap();
       
    }
    /**@in :
     * @out: 
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete IP
     */
    pub fn interfaceIP( mut self){
            let app = app::App::default();
            let mut wind = Window::default()
            .with_size(900, 350) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 3");
            //Definimos la interfaz
            let titulo = Frame::new(0, 0, 900, 50, "IP");

            //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
            //Los hacemos mutable para cambiar sus valores 
            //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
            //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

            self.refresh_Interface(1);
            let mut inputVersion = Input::new(65,50,75,25,"Version"); //X,Y,ANCHO, ALTO, TITULO
            inputVersion.set_value(self.campoVersion.as_str());
                let mut helpVersion: Listener<_>  = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpVersion.set_color(Color::Yellow);

            let mut inputIHL = Input::new(215,50,75,25,"IHL"); //X,Y,ANCHO, ALTO, TITULO
            inputIHL.set_value(self.campoIHL.as_str());
                let mut helpIHL : Listener<_> = Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpIHL.set_color(Color::Yellow);

            let mut inputTypeOfServ = Input::new(435,50,75,25,"TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
            inputTypeOfServ.set_value(self.campoTypeOfService.as_str());
                let mut helpTOS : Listener<_> = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpTOS.set_color(Color::Yellow);

            let mut inputECN = Input::new(595,50,75,25,"ECN"); //X,Y,ANCHO, ALTO, TITULO
            inputECN.set_value(self.campoECN.as_str());
                let mut helpECN : Listener<_> = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpECN.set_color(Color::Yellow);

            let mut inputLength = Input::new(775,50,75,25,"Length"); //X,Y,ANCHO, ALTO, TITULO
            inputLength.set_value(self.campoLength.as_str());
                let mut helpLen : Listener<_> = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpLen.set_color(Color::Yellow);


            let mut inputID = Input::new(65,100,175,25,"ID"); //X,Y,ANCHO, ALTO, TITULO
            inputID.set_value(self.campoID.as_str());
                let mut helpID : Listener<_> = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpID.set_color(Color::Yellow);

            let mut inputFlags =  Input::new(355,100,30,25,"Flags"); //X,Y,ANCHO, ALTO, TITULO
            inputFlags.set_value("0");
            inputFlags.set_readonly(true);
                let mut helpFlags : Listener<_> = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpFlags.set_color(Color::Yellow);
            
                let mut msb = Choice::new(400, 100, 40, 25, None); //1
                msb.add_choice("0");
                msb.add_choice("1");
                msb.set_value(1); //Posicion 1 de la lista

                let mut lsb = Choice::new(450, 100,  40, 25, None);//0
                lsb.add_choice("0");
                lsb.add_choice("1");
                lsb.set_value(0); //Posicion 0



            let mut inputFragmentOff = Input::new(685,100,130,25,"FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
            inputFragmentOff.set_value(self.campoFragmentOffSet.as_str());
                let mut helpOffSet : Listener<_> = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpOffSet.set_color(Color::Yellow);


            let mut inputTTL = Input::new(65,150,100,25,"TTL"); //X,Y,ANCHO, ALTO, TITULO
            inputTTL.set_value(self.campoTTL.as_str());
                let mut helpTTL : Listener<_> = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpTTL.set_color(Color::Yellow);

            let mut inputProtocol = Input::new(345,150,100,25,"Protocol"); //X,Y,ANCHO, ALTO, TITULO
            inputProtocol.set_value(self.campoProtocol.to_uppercase().as_str());
                let mut helpPro : Listener<_> = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpPro.set_color(Color::Yellow);

            let mut inputHeaderChecksum = Input::new(630,150,100,25,"Checksum"); //X,Y,ANCHO, ALTO, TITULO
            inputHeaderChecksum.set_value(self.campoHeaderChecksum.as_str());
                let mut helpChecksum: Listener<_>  = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpChecksum.set_color(Color::Yellow);
            
            let mut generaChecksum : Listener<_>= Button::new(785, 145, 80, 40, "Generar\nChecksum").into();


            let mut inputSourceAdd = Input::new(165,205,200,25,"Source Address"); //X,Y,ANCHO, ALTO, TITULO
            inputSourceAdd.set_value(self.campoSourceAddress.as_str());
                let mut helpSource : Listener<_> = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpSource.set_color(Color::Yellow);


            let mut inputDestAdd = Input::new(630,205,200,25,"Destination Address"); //X,Y,ANCHO, ALTO, TITULO
            inputDestAdd.set_value(self.campoDestinationAddress.as_str());
                let mut helpDest: Listener<_>  = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpDest.set_color(Color::Yellow);

/*
            let mut inputOptions = Input::new(100,305,300,25,"Options"); //X,Y,ANCHO, ALTO, TITULO
            inputOptions.set_value("1;");
                let mut helpOpt : Listener<_> = Button::new(415, 305, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpOpt.set_color(Color::Yellow);
                
            let mut inputPadding = Input::new(530,305,300,25,"Padding"); //X,Y,ANCHO, ALTO, TITULO
            inputPadding.set_value(self.campoPadding.as_str());
            inputPadding.set_readonly(true); //Como el padding está solo por mostrar que es un campo, no se rellena
                let mut helpPadding: Listener<_>  = Button::new(845, 305, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpPadding.set_color(Color::Yellow);
*/

            let mut hexaPayload = Input::new(250,255,400,25,"Payload"); //X,Y,ANCHO, ALTO, TITULO
            hexaPayload.set_value("0x05;0x32;0x00;0x17;0x00;0x00;0x00;0x01;0x00;0x00;0x00;0x00;0x50;0x02;0x07;0xFF;0x00;0x00;0x00;0x00;");
            hexaPayload.set_readonly(true); 
                let mut helphexPay: Listener<_>  = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helphexPay.set_color(Color::Yellow);


            let mut paylo: Listener<_> = Button::new(50, 265, 80, 40, "Payload").into();
                let mut helpPay: Listener<_>  = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpPay.set_color(Color::Yellow);
            
            let mut sendP : Listener<_> = Button::new(775, 265, 80, 40, "Send").into();
            wind.end();
            wind.show();
            let vent = wind.clone();
            //Modificamos la Interfaz con los datos que tenemos

            /*
            A la hora de pulsar un boton puede dar error porque le pasas el self y supera su tiempo de vida.
            Para solucionar esto debe pasarse Self como un Arc<Mutex<Self>>, esto es, un semaforo, que permita modificar self respetando
                tanto el tiempo de vida de los hilos como los datos de cada hilo
            */

            //BOTONES DE AYUDA
            // S : Sender y r: Receiver . Objetos que se ponen a la escucha para enviar y esperar recibir algo,
            // en este caso, un string que dirá sobre que campo buscamos información
            let (s, r) =  app::channel::<&str>();
            
            let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
            let clonPayload = Arc::new(Mutex::new(self.clone()));
            let clonObtencion = Arc::new(Mutex::new(self.clone()));

           
           //Se incluyen aqui los datos de la interfaz del Payload para crearla consistentemente ( cerrar dicha interfaz
            //  no produzca fallos en el comportamiento de la interfazcompleta )


           
            // El map que se editará se llama "but_input" y se usa en la edición del payload
            let mut but_input : HashMap<(i32,i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input> 
            //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
            let mut map_input : HashMap<(i32,i32), String> =  HashMap::new(); 
            
            //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
            //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
            let mut flex : Flex ;
            let mut gridMedio :Grid = Grid::default_fill();
            let mut columnaMedio : Flex ; 
            let mut casillaSignificado  =  TextEditor::default(); //Fuera para poder transformarla
            let mut columnaFinal : Flex;

            // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
            // Cuando la app lo recoge , lo muestra
            helpVersion.on_click(move |b|{
                s.send("Version"); 
            });

            helpIHL.on_click(move |b|{
                s.send("IHL");
            });
           

            helpTOS.on_click(move |b|{
                s.send("TypeOfService");
            });

            helpECN.on_click(move |b|{
                s.send("ECN");
            });

            helpLen.on_click(move |b|{
                s.send("Length");
            });


            helpID.on_click(move |b|{
                s.send("ID");
            });


            helpFlags.on_click(move |b|{
                s.send("Flags");
            });

            helpOffSet.on_click(move |b|{
                s.send("FragmentOffset");
            });

            helpTTL.on_click(move |b|{
                s.send("TTL");
            });


            helpPro.on_click(move |b|{
                s.send("Protocol");
            });


            helpChecksum.on_click(move |b|{
                s.send("HeaderChecksum");
            });
            
            
            helpSource.on_click(move |b|{
                s.send("Source");
            });



            helpDest.on_click(move |b|{
                s.send("Destination");
            });
          
            /*
            helpOpt.on_click(move |b|{
                b.emit(s,"Options");
            });
            helpOpt.on_hover(move |b|{
                b.emit(s,"Options");
            });
            helpPadding.on_click(move |b|{
                b.emit(s,"Padding");
            });
            helpPadding.on_hover(move |b|{
                b.emit(s,"Padding");
            });
            */

            helphexPay.on_click(move |b|{
                s.send("HexadecimalPayload");
            });

            helpPay.on_click(move |b|{
                s.send("Payload");
            });
   
            generaChecksum.set_callback(move |b|{
                s.send("0");
            });
            sendP.on_click(move |b|{
                s.send("1");
            });
           
            paylo.on_click(move |b|{
                s.send("ButonPayload");
            }); //WIP. Debe abrir una interfaz de edicion de payload
        
           
            //Esto recoge información del campo y la usa para construir el PopUp
            while app.wait(){
                match r.recv(){
                    Some(variableAux)=>{
                        let helpS = variableAux;
                        let stringAux =  clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                        let strHelp = stringAux.as_str();

                        let p = clonObtencion.clone();
                        let mut copiaMapa = but_input.clone();
                        
                        match helpS {
                            
                            "Version"=>{
                                helpVersion.set_tooltip(strHelp);
                                helpVersion.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "IHL"=>{
                                helpIHL.set_tooltip(strHelp);
                                helpIHL.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "TypeOfService"=> {
                                helpTOS.set_tooltip(strHelp);
                                helpTOS.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "ECN"=>{
                                helpECN.set_tooltip(strHelp);
                                helpECN.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Length"=>{
                                helpLen.set_tooltip(strHelp);
                                helpLen.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "ID"=>{
                                helpID.set_tooltip(strHelp);
                                helpID.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Flags"=>{
                                helpFlags.set_tooltip(strHelp);
                                helpFlags.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "FragmentOffset"=>{
                                helpOffSet.set_tooltip(strHelp);
                                helpOffSet.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "TTL"=>{
                                helpTTL.set_tooltip(strHelp);
                                helpTTL.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Protocol"=>{
                                helpPro.set_tooltip(strHelp);
                                helpPro.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "HeaderChecksum"=>{
                                helpChecksum.set_tooltip(strHelp);
                                helpChecksum.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Source"=>{
                                helpSource.set_tooltip(strHelp);
                                helpSource.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Destination"=>{
                                helpDest.set_tooltip(strHelp);
                                helpDest.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            //"Options"=>{},
                            //"Padding"=>{},
                            "HexadecimalPayload"=>{
                                helphexPay.set_tooltip(strHelp);
                                helphexPay.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "Payload"=>{
                                helpPay.set_tooltip(strHelp);
                                helpPay.tooltip();
                                wind.redraw();
                                app.redraw();
                            },
                            "0"=>{
                                //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                                self.save_Fields(inputVersion.clone().value(), inputIHL.clone().value(), inputTypeOfServ.clone().value(), inputECN.clone().value(), inputLength.clone().value(),inputID.clone().value(),
                                    inputFlags.clone().value(), msb.clone().value().to_string(), lsb.clone().value().to_string(), inputFragmentOff.clone().value(), inputTTL.clone().value(), inputProtocol.clone().value(), 
                                    "0".to_string(), inputSourceAdd.clone().value(), inputDestAdd.clone().value(), hexaPayload.clone().value() );
                                //Obtenemos el json
                                let paqueteString = self.json_To_IP(self.campoVersion.clone(), self.campoIHL.clone(), self.campoTypeOfService.clone(),
                                    self.campoECN.clone(), self.campoLength.clone(), self.campoID.clone(), self.campoFlags.clone(),self.campoFragmentOffSet.clone(),
                                    self.campoTTL.clone(),self.campoProtocol.clone(),  self.campoHeaderChecksum.clone(),
                                    self.campoSourceAddress.clone(), self.campoDestinationAddress.clone(), self.campoOptions.clone(),
                                    self.campoHexadecimalPayload.clone());
                                //Procedemos a calcular checksum


                                //let appo = app::App::default();
                                let mut windo = Window::default().with_size(400, 300).center_screen().with_label("Confirmacion");
                                let frame = Frame::new(150, 75, 50, 100, "Se generará un checksum a partir\n 
                                de los datos actuales de la interfaz.\n
                                ¿Quiere continuar?");
                                //let mut no = Button::new(150, 200, 50, 50, "Salir"); // Lugar de aparicion(x,y) , Tamaño (x,y) 
                                let mut ok :Listener<_>= Button::new(175, 200, 50, 50, "Si").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                //let mut nuevoValorStr ="";

                                windo.end();
                                windo.show();
                                let mut valAux :u16 = 0;

                                let j = paqueteString.clone().to_string();
                                let paquete = self.ip_from_json(paqueteString);
                                match paquete{
                                    Ok(ref resultadoIP) => { //Si el paquete está bien, buscamos como generar su checksum

                                        valAux = self.obtener_Checksum(); //Sacamos la IP con unwrap
                                    
                                    }
                                    Err(msg) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen,menText); // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                }
                                let mut co = inputHeaderChecksum.clone();
                                ok.on_click(move |b| { //Cuando pulsamos OK se cierra el POPUP
                                    let valorChecksum = valAux;
                                    //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                    let valorStr = valorChecksum.to_string();
                                    let nuevoValorStr = valorStr.as_str();
                                    co.set_value(nuevoValorStr);
                                });
                                
                                /*no.set_callback(move |_|{
                                    
                                    windo.hide();
                                });*/
                        
                                //appo.run().unwrap();
                                wind.redraw();
                                app.redraw();
                            },
                            "1"=>{
                                //Guardamos los datos
                                self.save_Fields(inputVersion.clone().value(), inputIHL.clone().value(), inputTypeOfServ.clone().value(), inputECN.clone().value(), inputLength.clone().value(),inputID.clone().value(),
                                    inputFlags.clone().value(), msb.clone().value().to_string(), lsb.clone().value().to_string(), inputFragmentOff.clone().value(), inputTTL.clone().value(), inputProtocol.clone().value(), 
                                    inputHeaderChecksum.clone().value(), inputSourceAdd.clone().value(), inputDestAdd.clone().value(), hexaPayload.clone().value() );
                                //Obtenemos el json
                                let paqueteString = self.json_To_IP(self.campoVersion.clone(), self.campoIHL.clone(), self.campoTypeOfService.clone(),
                                    self.campoECN.clone(), self.campoLength.clone(), self.campoID.clone(), self.campoFlags.clone(),self.campoFragmentOffSet.clone(),
                                    self.campoTTL.clone(),self.campoProtocol.clone(),  self.campoHeaderChecksum.clone(),
                                    self.campoSourceAddress.clone(), self.campoDestinationAddress.clone(), self.campoOptions.clone(),
                                    self.campoHexadecimalPayload.clone());
                                //Procedemos a enviar
                                let paquete = self.ip_from_json(paqueteString);
                                let clon = Arc::new(Mutex::new(self.clone()));
                                match paquete{
                                    Ok(ref resultadoIP) => {
                                        let pac = (*resultadoIP).clone();
                                        wind.redraw();
                                        app.redraw();
                                        clon.lock().unwrap().sendPackage( pac ); //Sacamos la IP con unwrap
                                        
                                    }
                                    Err(msg) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( msg.as_str()));
                                        
                                        wind.redraw();
                                        app.redraw();
                                        clon.lock().unwrap().clone().popup_Mensaje(tipoMen,menText); // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                }
                                
                            
                            },
                            
                            "ButonPayload" =>{ //Aqui se encuentra la creación de la interfaz de edición comoda del payload
                                // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                                let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                                let mut vectorMiembrosPayload = Vec::new();
                            
                                if copiaPayloadHexadecimal != "" {
                                    let separador : Vec<&str> = copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"
                                    
                                    //Teniendo ese vector, iteramos sobre el
                                    for var in separador{ //  0x95
                                        if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                            let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                            vectorMiembrosPayload.push(val[1]);
                                        }
                                    }
                                    //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                                }
                                
                                //let appPayl = app::App::default();
                                let mut windPayl = Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                
                                let mut butActualizar = Button::new(510, 630, 50, 50, "Actualiza\nTexto");
                                let mut butActInput =  Button::new(510, 630, 50, 50, "Actualiza\nHex");
                                
                                flex = Flex::new(0,0,70,700,None).column();
                                {
                                    let mut grid = Grid::default_fill();
                                    grid.debug(false);
                                    grid.set_layout(25, 1);

                                    //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                    let mut labelOffset = Frame::default(); labelOffset.set_label("Offset");
                                    grid.insert(&mut labelOffset, 0, 0); // Titulo Offset
                                    grid.insert_ext(&mut butActualizar,22,0,1,2);
                                    
                                    let mut i: i8=0;
                                    let mut varCol = 1;
                                    for mut i in 1..11
                                    {
                                        let mut  a = String::from("00");
                                        let ar : u8= i*10;
                                        let arg = ar.to_string();
                                        a.push_str(arg.as_str());
                                        let mut offsetCol = Frame::default(); offsetCol.set_label(a.as_str());
                                        grid.insert(&mut offsetCol, varCol, 0); // Titulo Offset

                                        varCol+=1;
                                    }
                                    
                                    
                                    flex.end();
                                }
                                columnaMedio = Flex::new(70,0,430,700,None).column();
                                {
                                    let mut tablaMitad  =  Flex::default_fill().row();
                                    {
                                       gridMedio = Grid::default_fill();
                                        gridMedio.debug(false);
                                        gridMedio.set_layout(25, 17);
                                        let mut labelHexadecimal = Frame::default(); labelHexadecimal.set_label("Texto Hexadecimal");
                                        gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 1); // Titulo Hexadecimal


                                        for y in 1..25 { //10 columnas * 20 de distancia
                                            for x in 0..17 { // 16 filas * 20 de distancia entre cada una
                                                if x != 8{

                                                    let mut valorInputHexa = "";
                                                    
                                                    //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload
                                                    
                                                    if !vectorMiembrosPayload.is_empty(){ //Hay elementos aun en el vector
                                                        let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                                        valorInputHexa=valorAux; 
                                                    }else{ //Ya se ha gastado el vector
                                                       valorInputHexa="00"; //0 como valor por defecto
                                                    }
                                                    // Primero creamos el input
                                                    let mut inputHexa =  Input::default(); inputHexa.set_value(valorInputHexa);
                                                    //Por ultimo lo metemos en la interfaz
                                                    gridMedio.insert(&mut inputHexa, y, x);
                                                    //Despues lo añadimos a la lista de inputs
                                                    but_input.insert((y, x) , inputHexa );
                                                    
                                                    &(map_input).insert((y, x), String::from(valorInputHexa));
                                                    
                                                    
                                                    
                                                    
                                                }
                                            }
                                        }

                                        
                                        
                                        tablaMitad.end();
                                    }
                                   
                                    columnaMedio.end();
                                }


                                


                                columnaFinal = Flex::new(500,0,200,700,None).column();
                                {
                                    let mut gridFinal = Grid::default_fill();
                                    gridFinal.debug(false);
                                    gridFinal.set_layout(25, 5);

                                    //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                    let mut labelSignificado = Frame::default(); labelSignificado.set_label("Significado");
                                    gridFinal.insert_ext(&mut labelSignificado, 0, 0,5,1); // Titulo Offset
                                                                        
                                    
                                    let wrap = WrapMode::AtBounds;
                                    let mut buff = TextBuffer::default();
                                    casillaSignificado.wrap_mode(wrap,0); //El limite de texto es 100, es decir, el limite de columna
                                    buff.set_text("Pulse actualizar para revelar el payload por defecto");
                                    casillaSignificado.set_buffer(buff);
                                    gridFinal.insert_ext(&mut casillaSignificado, 1, 0, 5, 21); //  Label de transformacion

                                    let mut save = Button::new(510, 630, 50, 50, "Save"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                    gridFinal.insert_ext(&mut save, 22, 3, 2, 2); //  Label de transformacion
                                    gridFinal.insert_ext(&mut butActInput,22,0,2,2);

                                    butActualizar.set_callback(move |_|{
                                       s.send("AC");
                                    });
                                    butActInput.set_callback(move |_|{
                                        s.send("TEXT");
                                    });
                                    save.set_callback(move |_|{
                                        s.send("SAVE");
                                        
                                    });
                                    columnaFinal.end();
                                }
                               
                                windPayl.end();
                                windPayl.show();
                                
                                // the closure capture is mutable borrow to our button
                                
                                //appPayl.run().unwrap();
                            },
                            // Y a partir de aqui se encuentra el comportamiento de los botones de la interfaz de edición del payload: 
                            "AC"=>{ //Actualiza Hexadecimal a Texto
                                                    
                                                    
                                for  (key,but) in copiaMapa {
                                    let varAux :String =  map_input.get_mut(&key).unwrap().to_string();
                                    let auxBut = but.value();
                                    if auxBut != varAux.as_str() { // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                        let insercion = auxBut.as_str();
                                        let mut insercion = insercion.to_string();

                                        map_input.insert(key, insercion);
                                        
                                        
                                    }
                                }
                                let string = p.lock().unwrap().clone().actualizaLenguajeNatural(map_input.clone());
                                
                                let mut buff = TextBuffer::default(); //Creamos un buffer para meterlo en el texto
                                buff.set_text(string.as_str()); //Metemos texto en buffer
                                casillaSignificado.set_buffer(buff); //Metemos buffer en la casilla que ocupa
                                
                            },
                            "SAVE"=>{ //Guarda datos en el input de la interfaz completa 
                                //Procedemos a guardar todo en el input Payload del paquete IP
                                //Para ello, guardaremos todos los datos de los inputs en un vector

                                let mut vecPayl = Vec::new(); 

                                //Debemos actualizar los datos en caso de que se hayan modificado
                                let copiaMapa = but_input.clone();
                                for  (key,but) in copiaMapa {
                                    let varAux :String =  map_input.get_mut(&key).unwrap().to_string();
                                    let auxBut = but.value();
                                    if auxBut != varAux.as_str() { // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                        let insercion = auxBut.as_str();
                                        let mut insercion = insercion.to_string();
                                    
                                        map_input.insert(key, insercion);
                                    
                                    }
                                }
                                //Una vez editados, se extraen al vector
                                let mut concatenacion= "";
                                let mut auxiStr = String::new();
                                let iterador =  map_input.iter().sorted(); //Necesitamos orden
                                //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                                for (key,val) in iterador {
                                    vecPayl.push(val);
                                }
                                //Una vez tenemos [00,00,00,...]
                            
                                for val in vecPayl{
                                    auxiStr.push_str("0x"); 
                                    auxiStr.push_str(val.as_str());
                                    auxiStr.push_str(";");
                                    //El resultado queda como "0x00;"  
                                }
                                concatenacion = auxiStr.as_str();
                                //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                                //Metemos dicho valor en el hexaPayload
                                hexaPayload.set_value(concatenacion);
                                
                            },
                            "TEXT"=>{ //Actualiza los inputs Hexadecimales a partir del texto ASCII
                                //Obtenemos el valor del texto en el momento
                                let mut textoBuf : TextBuffer = TextBuffer::default();
                                match casillaSignificado.buffer(){
                                     Some(opcion)=>{
                                         textoBuf=opcion;
                                     }
                                     None=>(),
                                };
                                let textoCasilla= textoBuf.text();
                                //Obtenido el texto, lo transformamos a hexadecimal
                                let textoHexa = textoCasilla.as_str().as_bytes().iter().map(|x| format!("{:02x}", x)).collect::<String>();
                                //Ese hexadecimal a vector de caracteres, para introducirlos en los inputs
                                let mut vectorChar : Vec<char> = textoHexa.chars().collect();

                                //Iteramos sobre el mapa de inputs para cambiar sus valores por los nuevos
                                let iterador =  map_input.iter().sorted(); //Necesitamos orden, asi que usamos el orden del mapa de Strings
        
                            
                                for (key, val) in iterador{ //Recorremos y modificamos
                                    if !vectorChar.is_empty(){
                                        //95
                                        let mostsb = vectorChar.remove(0); //9
                                        let lestsb = vectorChar.remove(0); //5
                                
                                        let mut duplaHexadecimal = String::from(mostsb);
                                        let copiales = String::from(lestsb);
                                        duplaHexadecimal.push_str(copiales.as_str());
                                
                                        //Obtenido el dato, lo introducimos en la casilla correspondiente
                                        let mut inputAux = Input::default(); inputAux.set_value(duplaHexadecimal.as_str());
                                        //Lo modificamos en el grid, para que se vea reflejado 
                                        gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs 
                                        gridMedio.insert(&mut inputAux, key.0,key.1);
                                        but_input.insert(*key, inputAux);  //Aunque usemos el mapa de strings nos interesan las key
                                    } else{
                                        //Una vez vaciado el vector, se llenan los inputs con 0
                                        let mut inputAux = Input::default(); inputAux.set_value("00");
                                        //Lo modificamos en el grid, para que se vea reflejado 
                                        gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs 
                                        gridMedio.insert(&mut inputAux, key.0,key.1);
                                        but_input.insert(*key, inputAux); 
                                    }
                                }
                            },
                            
                            _=>{},
                        }
                    },
                    None =>(),
                }
                wind.redraw();
                app.redraw();
            }

            //app.run().unwrap();
           
    }

    /**
     * @in: String "mensajeErr" : COntiene el error o mensaje que ha producido que se genere el PopUp
     *      String "tipoMensaje": Muestra en el titulo del PopUp si es un mensaje ERROR o un mensaje "Informacion Dato"
     * @out: 
     * @function: Esta funcion Muestra un mensaje PopUp que avisa de un posible error o de la ayuda solicitada
     *          
     */
    pub fn popup_Mensaje<'a>(self, tipoMensaje : String, mensajeErr : Arc<Mutex<&str>>){
        
        let mensajeTitulo = tipoMensaje.as_str();
        let mensajeError = mensajeErr.to_owned();
        let varAux = mensajeError.lock().unwrap();
        let auxi = varAux;

        let mut windo = Window::default().with_size(400, 300).center_screen().with_label(mensajeTitulo);
        let mut frame = Frame::new(170, 75, 50, 100, "");
        frame.set_label(&auxi); //La solucion era pasarlo como label editado en lugar de en la creación 

        //let mut ok = Button::new(170, 200, 50, 50, "OK"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        windo.end();
        windo.show();

        /*ok.set_callback(move |b|{ //Cuando pulsamos OK se cierra el POPUP
            windo.hide();
        });*/

        
    }

    

    // Metodos para IP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz 
     * @out: String creado en formato JSON para crear un paquete IP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete IP 
     */
    fn  json_To_IP(&mut self, version: String, IHL: String, typeofServ: String, ECN : String, len : String, 
        id : String, flags : String, fragmOffset : String, ttl : String, protcol : String,
    headerCheck : String, mut source : String, mut destination : String, options : String, payloadHexa : String  ) -> String {
        //Primero miramos el valor de las IP porque al llevar "." son casos especiales. En caso de ser campos vacios se los añadimos

        if source.as_str() == "" {
            source = String::from("...");
        }
        if destination.as_str() == "" {
            destination = String::from("...");
        }

        
        //Como son Strings necesitamos usar .clone()

        let json : String  = "Version:".to_owned() + &version.clone()+ "#" + "IHL:" + &IHL.clone() + "#" + 
                "TypeOfService:" + &typeofServ.clone() + "#" + "ECN:"+ &ECN.clone() + "#" + "Length:" + &len.clone() + "#" +
                "ID:" + &id.clone() + "#" + "Flags:" + &flags.clone() + "#" + "FragmentOffset:" + &fragmOffset.clone() + "#" +
                "TTL:" + &ttl.clone() + "#" + "Protocol:" + &protcol.clone() + "#" + "HeaderCheksum:" + &headerCheck.clone() + "#" + 
                "Source:" + &source.clone() + "#" + "Destination:" + &destination.clone() + "#" + 
                "Options:" + &options.clone() + "#" + "Payload:" + &payloadHexa.clone() + "#" ;

        return json;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz 
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_Fields<'a>( &mut self, inputVersion : String, inputIHL : String, inputTypeOfServ : String,  inputECN : String, inputLength : String, 
        inputID : String, inputFlags : String, msb :String ,lsb :String, inputFragmentOff : String, inputTTL : String, inputProtocol : String, inputHeaderChecksum : String,
        inputSourceAdd : String, inputDestAdd : String,  hexaPayload : String) 
     { 
        
        self.campoVersion = inputVersion;
        self.campoIHL = inputIHL;
        self.campoTypeOfService = inputTypeOfServ;
        self.campoECN = inputECN;
        self.campoLength = inputLength;
        self.campoID = inputID;
        //AQUI IRIA FLAGS
        self.campoFragmentOffSet = inputFragmentOff;
        self.campoTTL = inputTTL;
        self.campoProtocol = inputProtocol;
        self.campoHeaderChecksum = inputHeaderChecksum;
        self.campoSourceAddress = inputSourceAdd;
        self.campoDestinationAddress = inputDestAdd;
        self.campoOptions = "1;".to_string();
        self.campoPadding = "".to_string();
        self.campoHexadecimalPayload = hexaPayload;

        if msb == "0".to_string(){
            if lsb == "0".to_string(){
                self.campoFlags = String::from("000");
            } else{
                self.campoFlags = String::from("001");
            }
        } else{
            if lsb == "0".to_string(){
                self.campoFlags = String::from("010");
            } else{
                self.campoFlags = String::from("011");
            }
        }
    }

    
    fn refresh_Interface(&mut self, first_time: u8) -> () {
        //actualiza interfaz sin datos nuevos

        if first_time == 1 {
            // Actualiza la interfaz con los valores de IP
            self.campoVersion = self.packageIP.get_Version().to_string();
            self.campoIHL = self.packageIP.get_IHL().to_string();
            self.campoTypeOfService = self.packageIP.get_TypeOfService().to_string();
            self.campoECN = self.packageIP.get_ECN().to_string();
            self.campoLength = self.packageIP.get_Length().to_string();
            self.campoID = self.packageIP.get_ID().to_string();
            self.campoFlags = self.packageIP.get_Flags().to_string();
            self.campoFragmentOffSet = self.packageIP.get_FragmentOffset().to_string();
            self.campoTTL = self.packageIP.get_TTL().to_string();
            self.campoProtocol = self.packageIP.get_Protocol().to_string();
            self.campoHeaderChecksum = self.packageIP.get_HeaderChecksum().to_string();
            self.campoSourceAddress = self.packageIP.get_SourceAddress().to_string();
            self.campoDestinationAddress = self.packageIP.get_DestinationAddress().to_string();
            //Options y Payload Hexadecimal no se añaden aqui ya que son vectores, se añadirán en la interfaz
        } else {
        }
    }
    // Añade el nuevo protocolo y actualiza los datos. Crea los nuevos protocolos  y llama a refresh_Interface
    fn update_Interface(&self, idProtocol : String) -> () { 


    }

    /**
     * @in: ninguno
     * @out: Objeto de la clase IP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase IP.
     */
    pub fn ip(&self ) -> IP {
        let nuevo = IP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase IP
     * @function: Esta funcion creará un paquete de la clase IP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_IP
     */
    fn ip_from_json(&self, json : String) -> Result<IP,String> { 

        let ippaq = self.packageIP.ip_From_String(json);
        
        return ippaq;
    }

    /**
     * @in: objeto de la clase IP
     * @out: null
     * @function: Esta funcion enviará la clase IP con los datos introducidos por la Interfaz
     */
    fn sendPackage(&mut self,  pack :  IP )  {
        
        self.packageIP = pack; // actualizamos el paquete IP para guardarlo por si es necesario 
        let resultado = match self.packageIP.send_IP_Package() {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new("Se realizado satisfactoriamente\n 
                el envio del paquete"));                            
                clon.popup_Mensaje(tipMen.clone(),mensajeTexto.clone());
            },
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new("Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos"));                            
                clon.popup_Mensaje(tipMen.clone(),mensajeTexto.clone());
            },
        };

        println!("{:?}", resultado);

    }
    
    /**
     * @in: ->Int "protocolo", que nos dirá a que protocolo (y a que clase) va a pedir información
     *                          IP = 1, TCP = 2, UDP = 3 , HTTP = 5 , FTP = 5
     *      ->String "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, para mostrarlo
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    fn field_Help (&self, protocolo : u8, campo : String) -> String {
        let mut ayuda : String = String::new();
        match protocolo {
            1=> {ayuda=  self.packageIP.field_Help(campo);}, //Ayuda para IP
            2=> {ayuda = "".to_string();}//Ayuda para TCP. Work in Progress
            3 => {ayuda = "".to_string();}//Ayuda para UDP. Work in Progress
            4 => {ayuda = "".to_string();}//Ayuda para HTTP. Work in Progress
            5 => {ayuda = "".to_string();}//Ayuda para UDP. Work in Progress
            _ => {} // caso base
        }
        
        
        return ayuda;
    }

    /**
     * @in :
     * @out: Numero u16  que representa el checksum del paquete construido hasta ahora
     * @function: Funcion que llama a la funcion que genera checksum de paquete IP
     */

    pub fn obtener_Checksum(&self) -> u16 {

        let num = self.packageIP.generaChecksum();

        return num;
    }

    /**
     * @in : but_input: Mapa con todos los resultados hexadecimales de la interfaz
     * @out : String que devolverá el valor en lenguaje natural de ese conjunto de string hexadecimales
     * @function : La funcion transforma el conjunto completo de texto hexadecimal "00409A..." a un texto 
     * que el usuario puede entender y que le dice que significa todo el payload
     */
    pub fn actualizaLenguajeNatural(self, but_input : HashMap<(i32,i32),String> ) -> String{ //Map de Button, hay que coger los valores

        let mut concatenacion= "";
        let mut auxiStr = String::new();
    
        let iterador =  but_input.iter().sorted(); 
        //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
        for (key,val) in iterador {
            auxiStr.push_str(val.as_str()); 
        }	
        concatenacion = auxiStr.as_str();
        // Aqui concatenacion debe tener el string de todos losvalores hexadecimales 
        let mut vecHexa = Vec::new();
        vecHexa = match hex::decode(concatenacion){ //Vec<u8>
                                Ok(res) => {res},
                                Err(e) => return String::from(format!("Error al transformar.\n Siga editando el hexadecimal\n para obtener los datos correctos")),
                            };//Lo transformamos a Vec<u8>
                        
        //Vec u8 lo pasamos al string, ahora resuelto
                         
        let s = String::from_utf8_lossy(&vecHexa).into_owned(); // turns invalid UTF-8 bytes into � and so no error handling is required. 
                        
        return s;
    }
    
    

}
