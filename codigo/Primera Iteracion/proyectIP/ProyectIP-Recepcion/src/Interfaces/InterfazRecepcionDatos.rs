#![allow(dead_code)]
#![allow(unused_variables)]
use fltk::{
    app, browser::SelectBrowser, button::Button, enums::Color, frame::Frame, input::Input,
    prelude::*, window::Window,
};
use fltk::{ menu::*,text::*,  button::RadioRoundButton,table::*,  group::*, prelude::*};
use fltk_evented::Listener;
use fltk_grid::Grid;
use crossbeam::channel::unbounded;
use fltk::{prelude::*, *};
use std::{thread, time};
use std::time::{Duration,Instant};
extern crate timer; //Temporizador
extern crate chrono;

use pnet::datalink::Channel::Ethernet;
use pnet::packet::ethernet::EthernetPacket;
use pnet::packet::ethernet::EtherType;
use pnet::datalink::*;
use itertools::Itertools;
use std::collections::HashMap;
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::BTreeMap;
use std::sync::{Arc, Mutex};

use crate::Interfaces::Protocols::IP::IP;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::MutableIpv4Packet;
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::*;
use std::net::Ipv4Addr;


//La interfaz de recepción tendrá los mismos datos que la de envío
#[derive(Clone)]
pub struct InterfazDatos {
    campoVersion: String,
    campoIHL: String,
    campoTypeOfService: String,
    campoECN: String,
    campoLength: String,
    campoID: String,
    campoFlags: String,
    campoFragmentOffSet: String,
    campoTTL: String,
    campoProtocol: String,
    campoHeaderChecksum: String,
    campoSourceAddress: String,
    campoDestinationAddress: String,
    campoOptions: String,
    campoPadding: String,
    campoHexadecimalPayload: String,
    listaInterfaces: Vec<NetworkInterface>,
    listaPaquetes :SelectBrowser,
    paqueteIP : IP,
    
}

//Constructor por defecto
pub fn new() -> InterfazDatos {
    let listaInter = interfaces(); //Vec<NetworkInterfaces>
    let listaPaq = SelectBrowser::default();
    let pIP = IP::new();
    InterfazDatos {
        campoVersion: "".to_string(),
        campoIHL: "".to_string(),
        campoTypeOfService: "".to_string(),
        campoECN: "".to_string(),
        campoLength: "".to_string(),
        campoID: "".to_string(),
        campoFlags: "".to_string(),
        campoFragmentOffSet: "".to_string(),
        campoTTL: "".to_string(),
        campoProtocol: "".to_string(),
        campoHeaderChecksum: "".to_string(),
        campoSourceAddress: "127.0.0.1".to_string(),
        campoDestinationAddress: "".to_string(),
        campoOptions: "".to_string(),
        campoPadding: "".to_string(),
        campoHexadecimalPayload: "".to_string(),
        listaInterfaces: listaInter,
        listaPaquetes:listaPaq,
        paqueteIP: pIP,
    }
}

impl InterfazDatos {

    /**
     * @in: String con la IP en formato: "XXX.XXX.XX.XX"
     * @out: IP en formato Ipv4Addr
     * @function: La función transforma de manera general un string con la IP en una IP en formato Ipv4Addr, en caso de necesitarla
    */
    pub fn convertstring_to_IPV4(&self, ipString : String)-> Ipv4Addr {
        
        let vecIP : Vec<&str> = ipString.split(".").collect(); 
        //Conversion con cobertura de errores
        let bit1 : Result<u8,String>  = match vecIP[0].parse::<u8>() {
            Ok(bit1) => Ok(bit1),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
            Err(e) =>  Err(String::from("Error al convertir el bit")), 
        };
        let bit2 : Result<u8,String>  = match vecIP[1].parse::<u8>() {
            Ok(bit2) => Ok(bit2),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
            Err(e) =>  Err(String::from("Error al convertir el bit")), 
        };
        let bit3 : Result<u8,String>  = match vecIP[2].parse::<u8>() {
            Ok(bit3) => Ok(bit3),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
            Err(e) =>  Err(String::from("Error al convertir el bit")), 
        };
        let bit4 : Result<u8,String>  = match vecIP[3].parse::<u8>() {
            Ok(bit4) => Ok(bit4),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
            Err(e) =>  Err(String::from("Error al convertir el bit")), 
        };
        let valA = bit1.unwrap();
        let valB = bit2.unwrap();
        let valC = bit3.unwrap();
        let valD = bit4.unwrap();
        
        let ret = Ipv4Addr::new(valA,valB,valC,valD);
        
        return ret;
    }

    /**
     * @in: 
     * @out:
     * @function: La funcion crea la interfaz de Búsqueda y Recepción de paquetes, permitiendo abrirlos en la capa que el usuario elija
     */
    pub fn createInterface(&mut self) {
        let appInt = app::App::default();
        let mut windInt = Window::new(800, 100, 500, 650, "Busqueda de paquetes"); // Lugar de aparicion(x,y) , Tamaño (x,y)
        
        let frameInterfaz = Frame::new(275, 15, 75, 50, "Interfaz de red:");
        let frameBusqueda = Frame::new(25, 15, 75, 50, "Busqueda:");
        let mut frameEspera = Frame::new(220, 65, 75, 50, "En unos segundos empezará\nla búsqueda de paquetes...");
        let frameSeleccion = Frame::new(25, 100, 75, 50, "Seleccion: ");
        let frameProtocolo = Frame::new(60, 575, 100, 50, "Protocolo con el que abrir\nel paquete:");
        
        let mut selectorProtocolo = menu::Choice::new(210, 590, 100, 30, None);
        selectorProtocolo.add_choice("IP");
        selectorProtocolo.add_choice("ICMP");
        selectorProtocolo.add_choice("TCP");
        selectorProtocolo.add_choice("UDP");
        selectorProtocolo.add_choice("HTTP");
        selectorProtocolo.add_choice("FTP");
        selectorProtocolo.set_value(0); //Posicion 0 de la lista : IP por defecto

        //Chooser con interfaces
        let mut selectorInterfaces = menu::Choice::new(375, 25, 100, 30, None);
        for val in &self.listaInterfaces {
            // No sabemos que interfaces tenemos pero tenemos el vector de las obtenidas
            //Introducimos las opciones encontradas en el selector
            selectorInterfaces.add_choice(val.name.as_str());
        }
        selectorInterfaces.set_value(1); //Por defecto se pone la primera, así evitas que haya error por no seleccionar ninguna
        
        
        let mut seleccionPaquete : Listener<_> = Button::new(400, 585, 80, 40, "Seleccion").into();
        //Funcion de busqueda de paquetes
        //Recogida en lista de dichos paquetes
        let mut listaPaquetes = SelectBrowser::new(25, 140, 450, 400, "Paquetes encontrados"); //x,y,ancho, alto, titulo

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        
        //Para guardar los paquetes IP que recibamos . Tree para que sea mas rapida su busqueda
        let mut arbolPaquetes :BTreeMap<(String,String),Vec<u8>> = BTreeMap::new();
        

        //let (mut p, mut x)= unbounded::<&str>();
        let (mut s, mut r) = app::channel::<&str>();
        let timer = timer::Timer::new();
    
        let mut _guard = timer.schedule_with_delay(chrono::Duration::seconds(1), move || {
            s.send("Busqueda"); // Cada medio minuto busca
        });
          
        windInt.end();
        windInt.show();
    

        seleccionPaquete.set_callback(move |_|{

            s.send("Seleccion");
        });

        //Las usaremos en la busqueda
        let mut interfazseleccionada = NetworkInterface {
            name: "".to_string(),
            description: "".to_string(),
            index: 0,
            mac: None,
            ips: Vec::new(),
            flags: 0,
        };
        

        let mut helpVersion: Listener<_>  = Button::default().into();
        let mut helpIHL : Listener<_> = Button::default().into();
        let mut helpTOS : Listener<_>  = Button::default().into();
        let mut helpECN : Listener<_>  = Button::default().into();
        let mut helpLen : Listener<_>  = Button::default().into();
        let mut helpID : Listener<_>  = Button::default().into();
        let mut helpFlags : Listener<_>  = Button::default().into();
        let mut helpOffSet : Listener<_>  = Button::default().into();
        let mut helpTTL : Listener<_>  = Button::default().into();
        let mut helpPro : Listener<_>  = Button::default().into();
        let mut helpChecksum: Listener<_>  = Button::default().into();
        let mut helpSource : Listener<_>  = Button::default().into();
        let mut helpDest: Listener<_> = Button::default().into();
        let mut hexaPayload :Input = Input::default();
        let mut helphexPay: Listener<_>  = Button::default().into() ;
        let mut paylo: Listener<_>  = Button::default().into();
        let mut helpPay: Listener<_>   = Button::default().into();
        while appInt.wait(){
           //Mientras espera, tenemos el contador

          
          
            match r.recv(){
                Some(recuperado)=>{
                    let helpS = recuperado;
                    let stringAux =  self.paqueteIP.field_Help(helpS.to_string());
                    let strHelp = stringAux.as_str();

                    
                    match recuperado{
                        "Busqueda"=>{
                            //Limpiamos la lista para una nueva busqueda
                        
                            //Procedemos a obtener la interfaz de red desde la que buscar
                            let aux = selectorInterfaces.choice().clone().unwrap();
                            let seleccion = aux.as_str();
                            for val in &self.listaInterfaces { //Buscamos en la lista de interfaces cual hemos elegido
                                let texto = val.name.as_str();
                                match seleccion {
                                    
                                    texto => {
                                        let nameAux = texto.to_string();
                                        let descAux = val.description.clone();
                                        let indexAux = val.index;
                                        let macAux = val.mac;
                                        let ipsAux = val.ips.clone();
                                        let flagsAux = val.flags;

                                        interfazseleccionada = NetworkInterface { //Obtendremos la interfaz elegida con todos sus datos
                                            name: nameAux,
                                            description: descAux,
                                            index: indexAux,
                                            mac: macAux,
                                            ips: ipsAux,
                                            flags: flagsAux,
                                        };
                                    }
                                    _ => {}
                                }
                                //end of For de Interfaz
                            }

                            let mut frameStr = String::from("Buscando paquetes en la interfaz ");
                            frameStr.push_str(seleccion);
                            frameEspera.set_label(frameStr.as_str()); //Introducimos este texto para que el usuario vea que la busqueda ha empezado
                            
                            //Buscamos paquetes una vez tiene interfaz
                            let config = Default::default(); //configuracion por defecto

                            //Obtenemos el canal de envio y recepcion de paquetes a partir de una interfaz, con una configuracion
                            let  (tx, mut sx) = match channel(&interfazseleccionada, config) {
                                Ok(Ethernet(tx, sx)) => (tx, sx),
                                _ => panic!(),
                            };
 
                            let tiempoTotal = Instant::now();
                            let minuto = Duration::from_secs(5); //Busca durante 5 seg y entonces para
                            println!("Buscando en interfaz {}... ", seleccion);
                            while tiempoTotal.elapsed() <= minuto  { //Se hayan introducido 5 paquetes en la lista o no haya pasado un minuto ( para no esperar infinitamente )
                                match sx.next() { //Recibimos en sx los paquetes que han llegado a esa interfaz
                                    Ok(packet) => { 
                                        //Recibido el paquete, lo transformamos en un paquete Ethernet
                                        //Suponemos que no habrá errores pues si está en la red es un paquete correcto
                                        let packetEther = EthernetPacket::new(packet).unwrap();
                                        //Del paquete Ethernet obtenemos su payload, buscando si es IP, ICMP, et
                                        if packetEther.get_ethertype() == EtherType(0x0800){ //Si es tipo IPv4
                                            //Si el paquete es IP, lo obtenemos para tratar con él
                                            let payloadIP = packetEther.payload();
                                            let paqueteAuxiliar = Ipv4Packet::new(payloadIP).unwrap(); 
                                            
                                            let octetosOrigen = paqueteAuxiliar.get_source().octets(); //Obtenemos el XXX.XXX.XX.XX de la direccion IP para añadirla a la lista
                                            let octetosDestino = paqueteAuxiliar.get_destination().octets();
                                            let mut stringAux = String::from("Paquete de Interfaz: ");
                                            let iStr = seleccion.to_string();
                                            stringAux.push_str(iStr.as_str());
                                            stringAux.push_str("; ");
                                            stringAux.push_str("Origen:");
                                            
                                            let octetoOrigenUno = octetosOrigen[0].to_string(); //Lo convertimos a Str para poder introducirlos en la cadena
                                            let octetoOrigenDos = octetosOrigen[1].to_string();
                                            let octetoOrigenTres = octetosOrigen[2].to_string();
                                            let octetoOrigenCua = octetosOrigen[3].to_string();
                                            let mut stringOrigen =  String::new();

                                            stringOrigen.push_str(octetoOrigenUno.as_str()); //127
                                            stringOrigen.push_str(".");                      //.
                                            stringOrigen.push_str(octetoOrigenDos.as_str()); //0
                                            stringOrigen.push_str(".");                      //.
                                            stringOrigen.push_str(octetoOrigenTres.as_str()); //0
                                            stringOrigen.push_str(".");                      //.
                                            stringOrigen.push_str(octetoOrigenCua.as_str()); //1


                                            stringAux.push_str(octetoOrigenUno.as_str()); //127
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoOrigenDos.as_str()); //0
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoOrigenTres.as_str()); //0
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoOrigenCua.as_str()); //1
                                            stringAux.push_str("; ");
                                            stringAux.push_str("Destino:");
                                            
                                            let octetoDestUno = octetosDestino[0].to_string(); 
                                            let octetoDestDos = octetosDestino[1].to_string();
                                            let octetoDestTres = octetosDestino[2].to_string();
                                            let octetoDestCua = octetosDestino[3].to_string();
                                            let mut strDestino = String::from("");
                                            stringAux.push_str(octetoDestUno.as_str()); //127
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoDestDos.as_str()); //0
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoDestTres.as_str()); //0
                                            stringAux.push_str(".");                      //.
                                            stringAux.push_str(octetoDestCua.as_str()); //1
                                            stringAux.push_str(";");
                                            
                                            strDestino.push_str(octetoDestUno.as_str()); //127
                                            strDestino.push_str(".");                      //.
                                            strDestino.push_str(octetoDestDos.as_str()); //0
                                            strDestino.push_str(".");                      //.
                                            strDestino.push_str(octetoDestTres.as_str()); //0
                                            strDestino.push_str(".");                      //.
                                            strDestino.push_str(octetoDestCua.as_str()); //1
                                            
                                            //Introducimos paquetes en la lista 
                                            listaPaquetes.add(stringAux.as_str()); //Paquete 1; Origen: 127.0.0.1; Destino: 127.0.0.1
                                            
                                            //... y en las estructuras de datos para acceder a los paquetes posteriormente
                                            
                                            //Primero convertimos el &[u8] en Vec<u8> para que tenga permanencia 
                                            let vecPaquete = payloadIP.to_owned();
                                            
                                            //El arbol tiene como clave las dos IP para encontrar el paquete
                                            let par = (stringOrigen, strDestino);
                                            arbolPaquetes.insert(par,vecPaquete);
                                            //Aunque no haya pasado 1 min si hay mas de 5 se sale. Está aqui en vez de en el while porque si estaba en el while no funcionaba correctamente
                                            
                                            //End of IF Paquete =IP
                                        }
                                        
                                    //End of Ok de recepcion de paquetes en SX
                                    }
                                    Error=>{
                                        //No se ha encontrado ningun paquete
                                        listaPaquetes.clear();
                                        listaPaquetes.add("No se pudo encontrar paquetes");
                                    },
                                   
                                }
                                
                                //end of While
                                
                            }    
                            listaPaquetes.add("\n");
                            frameEspera.set_label("Paquetes encontrados.\nEn 30 segundos se realizará otra búsqueda...");
                            _guard = timer.schedule_with_delay(chrono::Duration::seconds(30), move || {
                                s.send("Busqueda"); // Cada medio minuto busca
                            });

                            //end of Busqueda
                        },
                        "Seleccion"=>{
                        
                            
                            //Selecciona el Protocolo del que vamos a sacar la interfaz
                            let auxP = selectorProtocolo.choice().clone().unwrap(); //IP,TCP,UDP,...
                            let protocoloSeleccionado = auxP.as_str();
                            //Primero obtenemos cual valor de la lista ha sido
                            if listaPaquetes.size() != 0{

                            
                                let seleccionado =  listaPaquetes.selected_text(); //Paquete 1; Origen: 127.0.0.1; Destino: 127.0.0.1
                                match seleccionado{
                                    Some(correcto)=>{
                                        if correcto != "\n" {
                                            let vectorSplitSeleccionado :Vec<&str> = correcto.split(";").collect(); //vec[0] = Paquete..., vec[1]=Origen, vec[2]=Destino

                                            let vectorOrigen : Vec<&str> = vectorSplitSeleccionado[1].split(":").collect(); //vec[2] = 127.0.0.1
                                            let vectorDest : Vec<&str> = vectorSplitSeleccionado[2].split(":").collect(); //vec[2] = 127.0.0.1
                                            
                                            let ipOrigen = vectorOrigen[1];
                                            let ipDestino = vectorDest[1];
                                           
                                            let parIP = (ipOrigen.to_string(),ipDestino.to_string());
                                            //Con el Par de ambas IP origen y destino se hace obtiene el dato del arbol
                                            let paqueteAObtener :Vec<u8> =  arbolPaquetes.get(&parIP).unwrap().to_vec();
                                      
                                            
                                            //Una vez obtenido el paquete, se manda a la funcion de creacion de interfaces
                                            //Dependiendo del protocolo , hará una cosa u otra 
                                            match protocoloSeleccionado{ //Dependiendo de la interfaz que nos pida, tendremos que mostrar mas o menos
                                                "IP"=>{
                                    
                                                    let pac = Ipv4Packet::new(&paqueteAObtener).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                                                    let vectorIp = self.getIPData(pac); //A partir de el, obtenemos los datos para la interfaz

                                                    let mut windIP = Window::default()
                                                    .with_size(900, 350) //Ancho, Alto
                                                    .center_screen()
                                                    .with_label("Paquete de Capa 3");
                                                    //Definimos la interfaz
                                                    let titulo = Frame::new(0, 0, 900, 50, "IP");
                                            
                                                    let mut inputVersion = Input::new(65,50,75,25,"Version"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputVersion.set_value(vectorIp[0].as_str());
                                                    inputVersion.set_readonly(true); 
                                                        helpVersion = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpVersion.set_color(Color::Yellow);
                                            
                                                    let mut inputIHL = Input::new(215,50,75,25,"IHL"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputIHL.set_value(vectorIp[1].as_str());
                                                    inputIHL.set_readonly(true); 
                                                        helpIHL= Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpIHL.set_color(Color::Yellow);
                                            
                                                    let mut inputTypeOfServ = Input::new(435,50,75,25,"TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputTypeOfServ.set_value(vectorIp[2].as_str());
                                                    inputTypeOfServ.set_readonly(true); 
                                                        helpTOS = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpTOS.set_color(Color::Yellow);
                                            
                                                    let mut inputECN = Input::new(595,50,75,25,"ECN"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputECN.set_value(vectorIp[3].as_str());
                                                    inputECN.set_readonly(true); 
                                                        helpECN  = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpECN.set_color(Color::Yellow);
                                            
                                                    let mut inputLength = Input::new(775,50,75,25,"Length"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputLength.set_value(vectorIp[4].as_str());
                                                    inputLength.set_readonly(true); 
                                                        helpLen  = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpLen.set_color(Color::Yellow);
                                            
                                            
                                                    let mut inputID = Input::new(65,100,175,25,"ID"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputID.set_value(vectorIp[5].as_str());
                                                    inputID.set_readonly(true);
                                                        helpID = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpID.set_color(Color::Yellow);
                                            
                                                    let mut inputFlags =  Input::new(355,100,50,25,"Flags"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputFlags.set_value(vectorIp[6].as_str());
                                                    inputFlags.set_readonly(true);
                                                        helpFlags = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpFlags.set_color(Color::Yellow);
                                            
                                            
                                                    let mut inputFragmentOff = Input::new(685,100,130,25,"FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputFragmentOff.set_value(vectorIp[7].as_str());
                                                    inputFragmentOff.set_readonly(true); 
                                                        helpOffSet = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpOffSet.set_color(Color::Yellow);
                                            
                                            
                                                    let mut inputTTL = Input::new(65,150,100,25,"TTL"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputTTL.set_value(vectorIp[8].as_str());
                                                    inputTTL.set_readonly(true); 
                                                        helpTTL  = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpTTL.set_color(Color::Yellow);
                                            
                                                    let mut inputProtocol = Input::new(345,150,100,25,"Protocol"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputProtocol.set_value(vectorIp[9].to_uppercase().as_str());
                                                    inputProtocol.set_readonly(true); 
                                                        helpPro = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpPro.set_color(Color::Yellow);
                                            
                                                    let mut inputHeaderChecksum = Input::new(630,150,100,25,"Checksum"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputHeaderChecksum.set_value(vectorIp[10].as_str());
                                                    inputHeaderChecksum.set_readonly(true); 
                                                        helpChecksum = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpChecksum.set_color(Color::Yellow);
                                                    
                                            
                                                    let mut inputSourceAdd = Input::new(165,205,200,25,"Source Address"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputSourceAdd.set_value(vectorIp[11].as_str());
                                                    inputSourceAdd.set_readonly(true); 
                                                        helpSource  = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpSource.set_color(Color::Yellow);
                                            
                                            
                                                    let mut inputDestAdd = Input::new(630,205,200,25,"Destination Address"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputDestAdd.set_value(vectorIp[12].as_str());
                                                    inputDestAdd.set_readonly(true); 
                                                        helpDest  = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpDest.set_color(Color::Yellow);
                                            
                                            
                                                    hexaPayload = Input::new(250,255,400,25,"Payload"); //X,Y,ANCHO, ALTO, TITULO
                                                    hexaPayload.set_value(vectorIp[13].as_str());
                                                    hexaPayload.set_readonly(true); 
                                                        helphexPay = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helphexPay.set_color(Color::Yellow);
                                            
                                            
                                                    paylo= Button::new(50, 265, 80, 40, "Payload").into();
                                                        helpPay  = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                        helpPay.set_color(Color::Yellow);
                                                    
                                                    
                                            
                                                    helpVersion.set_callback(move |b|{
                                                        s.send("Version"); 
                                                    });
                                            
                                                    helpIHL.on_click(move |b|{
                                                        s.send("IHL");
                                                    });
                                                   
                                            
                                                    helpTOS.on_click(move |b|{
                                                        s.send("TypeOfService");
                                                    });
                                            
                                                    helpECN.on_click(move |b|{
                                                        s.send("ECN");
                                                    });
                                            
                                                    helpLen.on_click(move |b|{
                                                        s.send("Length");
                                                    });
                                            
                                            
                                                    helpID.on_click(move |b|{
                                                        s.send("ID");
                                                    });
                                            
                                            
                                                    helpFlags.on_click(move |b|{
                                                        s.send("Flags");
                                                    });
                                            
                                                    helpOffSet.on_click(move |b|{
                                                        s.send("FragmentOffset");
                                                    });
                                            
                                                    helpTTL.on_click(move |b|{
                                                        s.send("TTL");
                                                    });
                                            
                                            
                                                    helpPro.on_click(move |b|{
                                                        s.send("Protocol");
                                                    });
                                            
                                            
                                                    helpChecksum.on_click(move |b|{
                                                        s.send("HeaderChecksum");
                                                    });
                                                    
                                                    
                                                    helpSource.on_click(move |b|{
                                                        s.send("Source");
                                                    });
                                            
                                            
                                                    helpDest.on_click(move |b|{
                                                        s.send("Destination");
                                                    });
                                            
                                                    helphexPay.on_click(move |b|{
                                                        s.send("HexadecimalPayload");
                                                    });
                                            
                                                    helpPay.on_click(move |b|{
                                                        s.send("Payload");
                                                    });
                                                   
                                                    paylo.set_callback(move |b|{
                                                        s.send("ButonPayload");
                                                    }); 
                                            
                                                    
                                                    windIP.end();
                                                    windIP.show();
                                                    
                                                    //End of Interfaz IP
                                                },


                                                "ICMP"=>{
                                    
                                                },
                                                "TCP"=>{
                                    
                                                },
                                                "UDP"=>{
                                    
                                                },
                                                "HTTP"=>{
                                    
                                                },
                                                "FTP"=>{
                                    
                                                },
                                                _=>(),
                                            }
                                            
                                            //end if 
                                        } else{
                                            //POPUP: "Busque de nuevo, en la busqueda anterior no se encontraron paquetes"
                                            
                                            let frase = "Por favor, seleccione un paquete primero\n";
                                            let arc = Arc::new(Mutex::new(frase));
                                            let tituloVentana = String::from("ERROR");
                                            let p = self.clone();
                                            let mut wi = p.popup_Mensaje(tituloVentana, arc);
                                            wi.show();
                                            
                                        }
                                        //end Some Correcto 
                                    }

                                    _=>(),
                                }
                                
                            } else{ //No hay nada

                                //POPUP : Primero busque antes de seleccionar
                                let frase = "No hay paquetes disponibles.\nPor favor, busque antes de seleccionar";
                                let arc = Arc::new(Mutex::new(frase));
                                let tituloVentana = String::from("ERROR");
                                let p = self.clone();
                                let mut wi = p.popup_Mensaje(tituloVentana, arc);
                                wi.show();
                            }
                            
                            windInt.redraw();     
                            //End of Seleccion
                        },
                        //Llamamos para solicitar los datos de la interfazIP
                        "Version"=>{
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                        },
                        "IHL"=>{
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                        },
                        "TypeOfService"=> {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                        },
                        "ECN"=>{
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                        },
                        "Length"=>{
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                        },
                        "ID"=>{
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                        },
                        "Flags"=>{
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                        },
                        "FragmentOffset"=>{
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                        },
                        "TTL"=>{
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                        },
                        "Protocol"=>{
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                        },
                        "HeaderChecksum"=>{
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                        },
                        "Source"=>{
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                        },
                        "Destination"=>{
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                        },
                        "HexadecimalPayload"=>{
                            helphexPay.set_tooltip(strHelp);
                            helphexPay.tooltip();
                        },
                        "Payload"=>{
                            helpPay.set_tooltip(strHelp);
                            helpPay.tooltip();
                        },

                        "ButonPayload" =>{
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();
                        
                            if copiaPayloadHexadecimal != "" {
                                let separador : Vec<&str> = copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"
                                
                                //Teniendo ese vector, iteramos sobre el
                                for var in separador{ //  0x95
                                    if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }
                        _=>{print!("Error");},
                        //End of Match recuperado
                    }
                    //end of Ok
                },
                None=>{

                    
                },
                _=>(),
                //end of  r.recv()
            }
        //end of while appwait        
        windInt.redraw();
        appInt.redraw();
        }
      
       //appInt.run().unwrap();
    }

    /**
     * @in: Paquete IPv4
     * @out: Vector de String: Vector que contiene en cada casilla un dato del paquete IP para mostrarlo en la interfaz, 
     *          de la forma [4,5,..] SIendo Version 4, IHL=5, etc
     * @function: Esta funcion obtendrá los datos del paquete IPv4  y los abstraerá a un vector de string para poder mostrar dichos datos por la interfaz
     *              De la misma forma, generará un paquete IP en caso de que necesitemos usar sus funciones (como fieldHelp)
     */
    pub fn getIPData(&mut self, paquete : Ipv4Packet ) -> Vec<String>{
        let mut vectorResultado = Vec::new();

        let version =  paquete.get_version().to_string();
        let IHL = paquete.get_header_length().to_string();
        let  dcsp = paquete.get_dscp().to_string();
        let ecn = paquete.get_ecn().to_string();
        let length = paquete.get_total_length().to_string();
        let id =  paquete.get_identification().to_string();
        let flags = paquete.get_flags().to_string();
        let offset = paquete.get_fragment_offset().to_string();
        let tetele = paquete.get_ttl().to_string();
        let protocol = paquete.get_next_level_protocol().to_string();
        let checksum = paquete.get_checksum().to_string();
        let source = paquete.get_source().to_string();
        let dest = paquete.get_destination().to_string();
        let payload = paquete.payload();

        let mut stringPayl = String::new();

        self.paqueteIP = IP::ip_From_Data(paquete.get_version(),paquete.get_header_length(),paquete.get_dscp(),paquete.get_ecn(),paquete.get_total_length(),
                                paquete.get_identification(),paquete.get_flags(),paquete.get_fragment_offset(),paquete.get_ttl(), paquete.get_next_level_protocol(),paquete.get_checksum(),
                                paquete.get_source(),paquete.get_destination(),paquete.payload().to_vec());


        let stringNumerosHexadecimales = hex::encode_upper(payload); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        let mut auxilStr =String::from("");
        let mut contadorAuxiliar :u16=0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();
        
        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() { 
            
            contadorAuxiliar=contadorAuxiliar+1;
            auxilStr.push(c);

            if contadorAuxiliar>=2 && contadorAuxiliar%2 == 0 { //Corta de 2 en 2
                stringPayl.push_str("0x"); 
                stringPayl.push_str(auxilStr.as_str());
                stringPayl.push_str(";"); //0x95;
                auxilStr =String::from(""); //reseteamos auxilStr
            }
        }
        /*for val in payload{ 
            let num = *val;
            let auxiliarStr = num.to_string();
            stringPayl.push_str("0x");
            stringPayl.push_str(auxiliarStr.as_str());
            stringPayl.push_str(";");
        }*/
        vectorResultado.push( version);
        vectorResultado.push( IHL);
        vectorResultado.push( dcsp);
        vectorResultado.push( ecn);
        vectorResultado.push( length);
        vectorResultado.push( id);
        vectorResultado.push( flags);
        vectorResultado.push( offset);
        vectorResultado.push(tetele);
        vectorResultado.push( protocol);
        vectorResultado.push( checksum);
        vectorResultado.push( source);
        vectorResultado.push( dest);
        vectorResultado.push( stringPayl);
        
        return vectorResultado;
    }


    /*
    
    //Por si soluciono los problemas de self en la interfaz

    /**
     * @in: Vec<u8> que representa un paquete en cadena de bytes
     *      String con el protocolo seleccionado por el usuario
     * @out: 
     * @function: La función obtiene el paquete de datos recibido y seleccionado, lo transforma a un paquete de la capa
     *              seleccionada y lo abre en dicha capa para su visionado
     */
    pub fn muestraPaqueteInterfaz(&mut self, vectorDatos : Vec<u8>, protocoloSeleccionado : &str){

        match protocoloSeleccionado{ //Dependiendo de la interfaz que nos pida, tendremos que mostrar mas o menos
            "IP"=>{

                let pac = Ipv4Packet::new(&vectorDatos).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                let vectorIp = self.getIPData(pac); //A partir de el, obtenemos los datos para la interfaz
                
                let hiloApoyo = self.clone();

                hiloApoyo.generaInterfazIP(vectorIp); //Mandamos dichos datos a la interfaz
                
                
            },
            "ICMP"=>{

            },
            "TCP"=>{

            },
            "UDP"=>{

            },
            "HTTP"=>{

            },
            "FTP"=>{

            },
            _=>(),
        }

    }

    */

/* Por si en el futuro consigo hacer que las interfaces no se bloqueen
    /**
     * @in : vector de string con las variables que componen el protocolo IP
     * @out:
     * @function: A partir de los datos, correctamente transformados a String para su facil visionado,
     *              esta función abre una interfaz de IP de solo lectura para observar completamente el paquete
     */
    pub fn generaInterfazIP(&self, vectorDatos : Vec<String>){
        let appIP = app::App::default();
        let mut windIP = Window::default()
        .with_size(900, 350) //Ancho, Alto
        .center_screen()
        .with_label("Paquete de Capa 3");
        //Definimos la interfaz
        let titulo = Frame::new(0, 0, 900, 50, "IP");

        let mut inputVersion = Input::new(65,50,75,25,"Version"); //X,Y,ANCHO, ALTO, TITULO
        inputVersion.set_value(vectorDatos[0].as_str());
        inputVersion.set_readonly(true); 
            let mut helpVersion: Listener<_>  = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpVersion.set_color(Color::Yellow);

        let mut inputIHL = Input::new(215,50,75,25,"IHL"); //X,Y,ANCHO, ALTO, TITULO
        inputIHL.set_value(vectorDatos[1].as_str());
        inputIHL.set_readonly(true); 
            let mut helpIHL : Listener<_> = Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpIHL.set_color(Color::Yellow);

        let mut inputTypeOfServ = Input::new(435,50,75,25,"TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
        inputTypeOfServ.set_value(vectorDatos[2].as_str());
        inputTypeOfServ.set_readonly(true); 
            let mut helpTOS : Listener<_> = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpTOS.set_color(Color::Yellow);

        let mut inputECN = Input::new(595,50,75,25,"ECN"); //X,Y,ANCHO, ALTO, TITULO
        inputECN.set_value(vectorDatos[3].as_str());
        inputECN.set_readonly(true); 
            let mut helpECN : Listener<_> = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpECN.set_color(Color::Yellow);

        let mut inputLength = Input::new(775,50,75,25,"Length"); //X,Y,ANCHO, ALTO, TITULO
        inputLength.set_value(vectorDatos[4].as_str());
        inputLength.set_readonly(true); 
            let mut helpLen : Listener<_> = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpLen.set_color(Color::Yellow);


        let mut inputID = Input::new(65,100,175,25,"ID"); //X,Y,ANCHO, ALTO, TITULO
        inputID.set_value(vectorDatos[5].as_str());
        inputID.set_readonly(true);
            let mut helpID : Listener<_> = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpID.set_color(Color::Yellow);

        let mut inputFlags =  Input::new(355,100,50,25,"Flags"); //X,Y,ANCHO, ALTO, TITULO
        inputFlags.set_value(vectorDatos[6].as_str());
        inputFlags.set_readonly(true);
            let mut helpFlags : Listener<_> = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpFlags.set_color(Color::Yellow);


        let mut inputFragmentOff = Input::new(685,100,130,25,"FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputFragmentOff.set_value(vectorDatos[7].as_str());
        inputFragmentOff.set_readonly(true); 
            let mut helpOffSet : Listener<_> = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpOffSet.set_color(Color::Yellow);


        let mut inputTTL = Input::new(65,150,100,25,"TTL"); //X,Y,ANCHO, ALTO, TITULO
        inputTTL.set_value(vectorDatos[8].as_str());
        inputTTL.set_readonly(true); 
            let mut helpTTL : Listener<_> = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpTTL.set_color(Color::Yellow);

        let mut inputProtocol = Input::new(345,150,100,25,"Protocol"); //X,Y,ANCHO, ALTO, TITULO
        inputProtocol.set_value(vectorDatos[9].to_uppercase().as_str());
        inputProtocol.set_readonly(true); 
            let mut helpPro : Listener<_> = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpPro.set_color(Color::Yellow);

        let mut inputHeaderChecksum = Input::new(630,150,100,25,"Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputHeaderChecksum.set_value(vectorDatos[10].as_str());
        inputHeaderChecksum.set_readonly(true); 
            let mut helpChecksum: Listener<_>  = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpChecksum.set_color(Color::Yellow);
        

        let mut inputSourceAdd = Input::new(165,205,200,25,"Source Address"); //X,Y,ANCHO, ALTO, TITULO
        inputSourceAdd.set_value(vectorDatos[11].as_str());
        inputSourceAdd.set_readonly(true); 
            let mut helpSource : Listener<_> = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpSource.set_color(Color::Yellow);


        let mut inputDestAdd = Input::new(630,205,200,25,"Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(vectorDatos[12].as_str());
        inputDestAdd.set_readonly(true); 
            let mut helpDest: Listener<_>  = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpDest.set_color(Color::Yellow);


        let mut hexaPayload = Input::new(250,255,400,25,"Payload"); //X,Y,ANCHO, ALTO, TITULO
        hexaPayload.set_value(vectorDatos[13].as_str());
        hexaPayload.set_readonly(true); 
            let mut helphexPay: Listener<_>  = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helphexPay.set_color(Color::Yellow);


        let mut paylo: Listener<_> = Button::new(50, 265, 80, 40, "Payload").into();
            let mut helpPay: Listener<_>  = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpPay.set_color(Color::Yellow);
        
        windIP.end();
        windIP.show();
        

        let (s, r) =  app::channel::<&str>();

        helpVersion.set_callback(move |b|{
            s.send("Version"); 
        });

        helpIHL.on_click(move |b|{
            s.send("IHL");
        });
       

        helpTOS.on_click(move |b|{
            s.send("TypeOfService");
        });

        helpECN.on_click(move |b|{
            s.send("ECN");
        });

        helpLen.on_click(move |b|{
            s.send("Length");
        });


        helpID.on_click(move |b|{
            s.send("ID");
        });


        helpFlags.on_click(move |b|{
            s.send("Flags");
        });

        helpOffSet.on_click(move |b|{
            s.send("FragmentOffset");
        });

        helpTTL.on_click(move |b|{
            s.send("TTL");
        });


        helpPro.on_click(move |b|{
            s.send("Protocol");
        });


        helpChecksum.on_click(move |b|{
            s.send("HeaderChecksum");
        });
        
        
        helpSource.on_click(move |b|{
            s.send("Source");
        });


        helpDest.on_click(move |b|{
            s.send("Destination");
        });

        helphexPay.on_click(move |b|{
            s.send("HexadecimalPayload");
        });

        helpPay.on_click(move |b|{
            s.send("Payload");
        });
       
        paylo.set_callback(move |b|{
            s.send("ButonPayload");
        }); 

        while appIP.wait(){
            match r.recv(){
                Some(variableAux)=>{
                    let helpS = variableAux;
                    let stringAux =  self.paqueteIP.field_Help(helpS.to_string());
                    let strHelp = stringAux.as_str();

                    match helpS {
                        "Version"=>{
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                        },
                        "IHL"=>{
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                        },
                        "TypeOfService"=> {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                        },
                        "ECN"=>{
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                        },
                        "Length"=>{
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                        },
                        "ID"=>{
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                        },
                        "Flags"=>{
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                        },
                        "FragmentOffset"=>{
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                        },
                        "TTL"=>{
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                        },
                        "Protocol"=>{
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                        },
                        "HeaderChecksum"=>{
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                        },
                        "Source"=>{
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                        },
                        "Destination"=>{
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                        },
                        "HexadecimalPayload"=>{
                            helphexPay.set_tooltip(strHelp);
                            helphexPay.tooltip();
                        },
                        "Payload"=>{
                            helpPay.set_tooltip(strHelp);
                            helpPay.tooltip();
                        },

                        "ButonPayload" =>{
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();
                        
                            if copiaPayloadHexadecimal != "" {
                                let separador : Vec<&str> = copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"
                                
                                //Teniendo ese vector, iteramos sobre el
                                for var in separador{ //  0x95
                                    if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }

                        _=>(), 
                    }
                }
                None=>(),
            }
        }
    }
*/
    pub fn muestraPayloadIP(self, vectorMiembros : Vec<&str>)  -> Window{

        let mut vectorMiembrosPayload = vectorMiembros.clone();
        let mut windPayl = Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                           
        let clonGrid =  self.clone();
        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input : HashMap<(i32,i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input> 
        let mut map_input : HashMap<(i32,i32), String> =  HashMap::new(); 
        
        let mut flex = Flex::new(0,0,70,700,None).column();
        {
            let mut grid = Grid::default_fill();
            grid.debug(false);
            grid.set_layout(25, 1);

            //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
            let mut labelOffset = Frame::default(); labelOffset.set_label("Offset");
            grid.insert(&mut labelOffset, 0, 0); // Titulo Offset
            
            let mut i: i8=0;
            let mut varCol = 1;
            for mut i in 1..11
            {
                let mut  a = String::from("00");
                let mut ar : u8= i*10;
                let arg = ar.to_string();
                a.push_str(arg.as_str());
                let mut offsetCol = Frame::default(); offsetCol.set_label(a.as_str());
                grid.insert(&mut offsetCol, varCol, 0); // Titulo Offset

                varCol+=1;
            }
            
            
            flex.end();
        }
        let mut gridMedio :Grid ;
        let mut columnaMedio = Flex::new(70,0,430,700,None).column();
        {
            let mut tablaMitad  =  Flex::default_fill().row();
            {
               gridMedio = Grid::default_fill();
                gridMedio.debug(false);
                gridMedio.set_layout(25, 17);
                let mut labelHexadecimal = Frame::default(); labelHexadecimal.set_label("Texto Hexadecimal");
                gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 1); // Titulo Hexadecimal


                for y in 1..25 { //10 columnas * 20 de distancia
                    for x in 0..17 { // 16 filas * 20 de distancia entre cada una
                        if x != 8{

                            let mut valorInputHexa = "";
                            
                            //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload
                            
                            if !vectorMiembrosPayload.is_empty(){ //Hay elementos aun en el vector
                                let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                valorInputHexa=valorAux; 
                            }else{ //Ya se ha gastado el vector
                               valorInputHexa="00"; //0 como valor por defecto
                            }
                            // Primero creamos el input
                            let mut inputHexa =  Input::default(); inputHexa.set_value(valorInputHexa);
                            //Por ultimo lo metemos en la interfaz
                            gridMedio.insert(&mut inputHexa, y, x);
                            //Despues lo añadimos a la lista de inputs
                            but_input.insert((y, x) , inputHexa );
                            
                            &(map_input).insert((y, x), String::from(valorInputHexa));
                            
                            
                            
                            
                        }
                    }
                }

                
                
                tablaMitad.end();
            }
           
            columnaMedio.end();
        }


        let mut casillaSignificado  =  TextEditor::default(); //Fuera para poder transformarla


        let mut columnaFinal = Flex::new(500,0,200,700,None).column();
        {
            let mut gridFinal = Grid::default_fill();
            gridFinal.debug(false);
            gridFinal.set_layout(25, 5);

            //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
            let mut labelSignificado = Frame::default(); labelSignificado.set_label("Significado");
            gridFinal.insert_ext(&mut labelSignificado, 0, 0,5,1); // Titulo Offset
                                                
            
            let wrap = WrapMode::AtBounds;
            let mut buff = TextBuffer::default();
            casillaSignificado.wrap_mode(wrap,0); //El limite de texto es 100, es decir, el limite de columna
            let string = clonGrid.actualizaLenguajeNatural(map_input.clone());
                                
            buff.set_text(string.as_str());
            casillaSignificado.set_buffer(buff);
            gridFinal.insert_ext(&mut casillaSignificado, 1, 0, 5, 21); //  Label de transformacion

            columnaFinal.end();
        }
       
        windPayl.end();

        return windPayl;
    }
    /**
     * @in : but_input: Mapa con todos los resultados hexadecimales de la interfaz
     * @out : String que devolverá el valor en lenguaje natural de ese conjunto de string hexadecimales
     * @function : La funcion transforma el conjunto completo de texto hexadecimal "00409A..." a un texto 
     * que el usuario puede entender y que le dice que significa todo el payload
     */ 
    pub fn actualizaLenguajeNatural(self, but_input : HashMap<(i32,i32),String> ) -> String{ //Map de Button, hay que coger los valores

        let mut concatenacion= "";
        let mut auxiStr = String::new();
    
        let iterador =  but_input.iter().sorted(); 
        //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
        for (key,val) in iterador {
            auxiStr.push_str(val.as_str()); 
        }	
        concatenacion = auxiStr.as_str();
        // Aqui concatenacion debe tener el string de todos losvalores hexadecimales 
        let mut vecHexa = Vec::new();
        vecHexa = match hex::decode(concatenacion){ //Vec<u8>
                                Ok(res) => {res},
                                Err(e) => return String::from(format!("Error al transformar.\n Siga editando el hexadecimal\n para obtener los datos correctos")),
                            };//Lo transformamos a Vec<u8>
                        
        //Vec u8 lo pasamos al string, ahora resuelto
                         
        let s = String::from_utf8_lossy(&vecHexa).into_owned(); // turns invalid UTF-8 bytes into � and so no error handling is required. 
                        
        return s;
    }

    /**
     * @in: String que nos indica si es un mensaje tipo "ERROR" o tipo "INFORMACION"
     *      String mensaje que nos indica el mensaje , del tipo anterior
     * @out: 
     * @function: Esta funcion abre una interfaz a modo de PopUp que nos indica si hay algun error en nuestro uso
     *              o si debemos tener algo en cuenta para usar la interfaz principal correctamente
     *                  (buscar datos antes de intentar seleccionarlos, por ejemplo)
     */
    pub fn popup_Mensaje<'a>(&self, tipoMensaje : String, mensajeErr : Arc<Mutex<&str>>)-> Window {
        
        let mensajeTitulo = tipoMensaje.as_str();
        let mensajeError = mensajeErr.to_owned();
        let varAux = mensajeError.lock().unwrap();
        let auxi = varAux;

        let mut windo = Window::default().with_size(400, 300).center_screen().with_label(mensajeTitulo);
        let mut frame = Frame::new(170, 75, 50, 100, "");
        frame.set_label(&auxi); //La solucion era pasarlo como label editado en lugar de en la creación 

        // mut ok = Button::new(170, 200, 50, 50, "OK"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        windo.end();
        return windo;
    }

    
}