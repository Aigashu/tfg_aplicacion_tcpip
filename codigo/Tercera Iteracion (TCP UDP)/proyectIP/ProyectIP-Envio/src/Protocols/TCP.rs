#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]


extern crate pnet;
extern crate std;


use crate::Interfaces::Protocols::IP::IP;
//use std::alloc::Global;
use std::io::Error;
use std::net::Ipv4Addr;
use pnet::packet::ipv4::*;
use pnet::packet::*;
use pnet::packet::tcp::*;
use pnet::packet::tcp::MutableTcpPacket;
use pnet::packet::tcp::TcpOptionNumber;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;

#[derive(Clone,Debug)]
pub struct TCP {
    sourcePort : u16,
    destinationPort: u16,
    sequenceNum : u32,
    acknowledgementNum : u32,
    dataOffset : u8,
    reserved : u8,
    flagsTCP : u16,
    tamWindow : u16,
    checksum : u16,
    urgentPointer: u16,
    optionsTCP :Vec<TcpOption>,
    payload : Vec<u8>,
    ipPacket : IP,
}

/**
 * @in : ipv4Pac: Paquete TCP creado a partir de la interfaz
 *       buffer_IP : vector de u8 que  necesitaremos para crear un paquete Ipv4
 *       direccionOrigen : direccion Origen del paquete IP , para calcular el cheksum de TCP
 *       direccionDestino: direccion Destino del paquete IP , para calcular el cheksum de TCP
 * @out : MutableTcpPacket : Paquete TCP , osea, un objeto Tcp en forma de paquete
 * @function : Esta funcion transformara un objeto Tcp en un PacketTcp, objeto que nos permite enviar paquetes Tcp por la red
 *              y  nos devolverá ese objeto y su checksum, en caso de necesitarlo 
 */
pub fn create_tcp_packet<'a>(buffer_ip: &'a mut [u8],ipv4Pac : pnet::packet::tcp::Tcp, checksu : u16,
             direccionOrigen : Ipv4Addr, direccionDestino : Ipv4Addr) -> (MutableTcpPacket,u16) {

    let mut ipv4_packet = MutableTcpPacket::new(buffer_ip).unwrap();
    let ipv4_aux = ipv4Pac;
     /* Populate crea un Ipv4Packet (objeto que se manda)
                 a partir de un objeto Ipv4 */
    ipv4_packet.populate(&ipv4_aux);    
    let mut num = 0;
    let mut inmutable = ipv4_packet.to_immutable();
    // Generamos el checksum correcto, ya que es un calculo
    let checks = pnet::packet::tcp::ipv4_checksum(&inmutable,&direccionOrigen,&direccionDestino); 
    num = checks;
    ipv4_packet.set_checksum(checks);
    
    
    return (ipv4_packet,num);
}

impl TCP{

    /**
     * @in:
     * @out: Objeto de la clase TCP
     * @function: Constructor por defecto de TCP
     */
    pub fn new() -> TCP{ 

        let checks = 0;
        let seq = 0;
        let offs = 0;
        let vecOption = Vec::new();
        let vecPayl = Vec::new();
        let mut ipAux = IP::new();
        ipAux.set_Protocol(IpNextHeaderProtocol::new(6)); //Es importante que indiquemos que el paquete es TCP
        TCP{
            sourcePort : 80, //PAQUETE SYN obtenido desde Wireshark
            destinationPort : 57888,
            sequenceNum : 1360020059,
            acknowledgementNum : 1438363754,
            dataOffset :40 ,
            reserved : 0,
            flagsTCP : 34, //34 = 010010 => SYN
            tamWindow : 65535,
            checksum : 0,
            urgentPointer: 0,
            optionsTCP : vecOption,
            payload : vecPayl,
            ipPacket : ipAux,
        }
    }

    /**
     * @in: Datos de creación del paquete TCP
     * @out: Paquete TCP creado
     * @function: La funcion crea un paquete TCP a partir de los datos obtenidos
     */
    pub fn tcp_From_Data( sPort : u16, destPort: u16, seqNum : u32, ackNum : u32, offse : u8,
                reserva: u8, flTCP : u16,tWind : u16, checksu : u16,
                urgPtr: u16, optTCP :Vec<TcpOption>, payl : Vec<u8>,
                ipPc : IP )-> TCP{
        
        let copiaOption = optTCP.clone();
        let copiaPayl = payl.clone();
        let ipClone = ipPc.clone();

        let paquete =  TCP{
            sourcePort : sPort,
            destinationPort : destPort,
            sequenceNum : seqNum,
            acknowledgementNum : ackNum,
            dataOffset :offse ,
            reserved : reserva,
            flagsTCP : flTCP,
            tamWindow : tWind,
            checksum : checksu,
            urgentPointer: urgPtr,
            optionsTCP : copiaOption,
            payload : copiaPayl,
            ipPacket : ipClone,
        };

        return paquete;

    }

    /**
     * @in: string "opti" : Lista de valores ["0x95", "0x50" , ...]
     * @out: String : String con el valor a convertir en hexadecimal por la funcion "hex"
     * @function: Esta función nos permitirá obtener a partir de los valores escritos en el input de Payload (0x95;0x50;...)
     * un string que se pueda transformar en hexadecimal : "9550..."
     */
    pub fn transform_Hexadecimal(self, separador : Vec<&str>) -> String{
        let mut strAux = String::new();

        for var in separador{ //  0x95
            if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                strAux.push_str(val[1]);
            }
            
        }
        let ret = strAux;
        return ret;
    }
    
    /**
     * @in: vecStrOption : Vector con los numeros que identifican a cada TCPOption
     * @out: vecOptions : vector de TCPOptions
     * @function: Esta funcion obtendrá todos los valores que identifiquen cada TcpOption y los transformará en su TCPOption correspondiente
     */

    pub fn obtain_TCPOption(&self, vecStrOption : Vec<&str>) -> Vec<TcpOption>{
        
        let mut vecOptions  =Vec::new();
       

        for opcion in vecStrOption{
            let mut nuevaOpcion = TcpOption::nop();

            match opcion {
                "0"=>{ // End Of Operation List
                    let mut packet = [0u8; 3];
                    let mut tcp_options =
                                MutableTcpOptionPacket::new(&mut packet[..]).unwrap();
                    tcp_options.set_number(TcpOptionNumber(0));
                    tcp_options.set_length(&vec!(16));
                    tcp_options.set_data(&vec!(16));

                    nuevaOpcion = tcp_options.from_packet();
                },
                "1"=>{ //No Operation
                    nuevaOpcion = TcpOption::nop();
                },
                "2"=>{ //Maximum Segment Size
                    nuevaOpcion = TcpOption::mss(500);
                },
                "3"=>{ // Window Scale
                    nuevaOpcion = TcpOption::wscale(100);
                },
                "4"=>{ //SACK Permited
                    nuevaOpcion = TcpOption::sack_perm();
                },
                "5"=>{ //SACK Permited
                    nuevaOpcion = TcpOption::sack_perm();
                },
                "8"=>{ //Timestamps
                    nuevaOpcion = TcpOption::timestamp(10, 10);
                },
                _=>{}
            }
            vecOptions.push(nuevaOpcion);
        }

        return vecOptions;
    }

    /**
     * @in: vecOption : Vector con  cada TCPOption
     * @out: vecOptions : String que representa en la interfaz a cada TCPOption 
     * @function: Esta funcion obtiene los valores numericos de los TCPOptions para poder mostrarlos en la Interfaz 
     */

    pub fn from_TCPOption(&self, vecOption : Vec<TcpOption>) -> String{
        
        let mut stringOpciones = String::new();

        for opcion in vecOption{

            let mut tamPaquete = MutableTcpOptionPacket::packet_size(&opcion);
            let mut packet = [0u8; 10];
                    let mut tcp_options =
                                MutableTcpOptionPacket::new(&mut packet[..]).unwrap();
                                tcp_options.populate(&opcion);

            match tcp_options.get_number() {
                TcpOptionNumber(0)=>{ // End Of Operation List
                    
                    stringOpciones.push_str("0;");
                },
                TcpOptionNumber(1)=>{ //No Operation
                    stringOpciones.push_str("1;");
                },
                TcpOptionNumber(2)=>{ //Maximum Segment Size
                    stringOpciones.push_str("2;");
                },
                TcpOptionNumber(3)=>{ // Window Scale
                    stringOpciones.push_str("3;");
                },
                TcpOptionNumber(4)=>{ //SACK Permited
                    stringOpciones.push_str("4;");
                },
                TcpOptionNumber(5)=>{ //SACK Permited
                    stringOpciones.push_str("5;");
                },
                TcpOptionNumber(8)=>{ //Timestamps
                    stringOpciones.push_str("8;");
                },
                _=>{}
            }
        }

        return stringOpciones;
    }
    /**
     * @in: String que contiene todos los datos de la clase TCP codificados en Texto
     * @out: Paquete TCP
     * @function: Esta funcion transforma un string en un paquete TCP completo, a partir de los datos que este string contiene
     */

    pub fn tcp_From_String(&self, stringTCP : String) -> Result<TCP,String> { //Devuelve IP pero cambialo cuando tengas uno decente
        
        /*Definimos las variables con las que crearemos el paquete TCP nuevo
        * Ponemos las que teniamos por defecto ya. Esto servirá para crear un paquete
         y para ver a la vez si el paquete se creado correctamente (si tiene datos por defecto
        sabremos que algo ha fallado)
        */

        let mut sPort : u16 = self.sourcePort; 
        let mut destPort: u16 = self.destinationPort;
        let mut seqNum : u32  = self.sequenceNum;
        let mut ackNum : u32 = self.acknowledgementNum;
        let mut offse : u8 = self.dataOffset;
        let mut reserva : u8 = self.reserved;
        let mut flTCP : u16 = self.flagsTCP;
        let mut tWind : u16 = self.tamWindow;
        let mut checksu : u16 = self.checksum;
        let mut urgPtr: u16 = self.urgentPointer;
        let mut optTCP :Vec<TcpOption> = self.optionsTCP.clone();
        let mut paylTCP : Vec<u8> = self.payload.clone();
        
        
        //Primero separamos en las distintas variables con split (iterador) y convertimos en vector con collect

        let vecSplit: Vec<&str>  =  stringTCP.split("#").collect();

        //Una vez tenemos el vector iteramos sobre él y obtenemos sus valores

        for val in vecSplit{
            // Como el dato de val es "Val:Valor" separamos en ":" y sabemos que split[0] será "Val" y split[1] será "Valor"

            let vectorCampos: Vec<&str>  = val.split(":").collect();
            let valorCampo = vectorCampos[0];

            match valorCampo{
                "PuertoOrigen"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    sPort = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "PuertoDestino"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    destPort = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Sequence"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u32, String>= match aux.parse::<u32>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    seqNum = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "ACK"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u32, String>= match aux.parse::<u32>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    ackNum = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Offset"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    offse = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                    
                },
                "Reserved"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    reserva = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Flags"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match u16::from_str_radix(aux, 2){ //Transformamos de Binario a Decimal
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                
                    flTCP = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Window"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    tWind = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Checksum"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    checksu = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Pointer"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    urgPtr = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Options"=>{
                    let mut vecHexa = Vec::new();
                    let aux = vectorCampos[1]; // "1;..;..;"
                    let vectorOpciones: Vec<&str>  = aux.split(";").collect();
                    let p = self.clone();
                    vecHexa = p.obtain_TCPOption(vectorOpciones);

                    optTCP = vecHexa.clone();
                },
                "Payload"=> { //Provisional. Falta saber como pasar string a vector, no se la estructura del vector
                    let aux = vectorCampos[1]; // "0x95;0xff;...;0x43"
                    let mut vecHexa = Vec::new();
                    if aux != "" {
                        let mut separador : Vec<&str> = aux.split(";").collect(); //Cada elemento es "0x95"
                        //Una vez tenemos el string entero lo convertimos a u8
                        let clon = self.clone();
                        let numHexa = clon.transform_Hexadecimal(separador); //Obtenemos un string
                        let numHexaStr = numHexa.as_str();
                        let vecHexa = match hex::decode(numHexaStr){
                            Ok(res) => {vecHexa = res},
                            Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                            asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),
                        };//Lo transformamos a Vec<u8>
                    }
                    paylTCP = vecHexa;  //Si payload viene  vacio así irá el payload. Si tiene datos los codificaremos
                }
                _=>{},
            }
        }

        let ipPc = self.ipPacket.clone();
        Ok(TCP{
            sourcePort : sPort,
            destinationPort : destPort,
            sequenceNum : seqNum,
            acknowledgementNum : ackNum,
            dataOffset :offse ,
            reserved : reserva,
            flagsTCP : flTCP,
            tamWindow : tWind,
            checksum : checksu,
            urgentPointer: urgPtr,
            optionsTCP : optTCP,
            payload : paylTCP,
            ipPacket : ipPc,
        })

    }


     /**
     * @in : Paquete de clase TCP
     * @out: Vector de bytes 
     * @function : Esta funcion transformará el paquete TCP que hemos creado en un vector de bytes para que sea
     *              el payload del paquete IP que hay inmediatamente debajo. Devolverá tambien el tamaño de este 
     *              payload, para que pueda sumarse al paquete total de TCP
     */
    pub fn tcpToPayload(&self, sour : Ipv4Addr, dest : Ipv4Addr) -> (Vec<u8>,usize) {

        let mut vectorRetorno = Vec::new();
        // Convertimos el objeto actual en un paquete ICMP
        let pac = 
        pnet::packet::tcp::Tcp{
            source: self.sourcePort,
            destination: self.destinationPort ,
            sequence: self.sequenceNum,
            acknowledgement: self.acknowledgementNum,
            data_offset: self.dataOffset,
            reserved: self.reserved,
            flags: self.flagsTCP,
            window: self.tamWindow,
            checksum: self.checksum,
            urgent_ptr: self.urgentPointer,
            options: self.optionsTCP.clone(),
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 400];
       
        // Llamamos a la funcion que nos dará un MutableICMPPacket
        let mut resultado = create_tcp_packet(&mut int_slice, pac, self.checksum,sour,dest);
        let mut paquete = resultado.0;
        let mut nuevoChecksum = resultado.1;

        //Una vez tenemos el MutableICMPPacket , lo transformamos a &[u8]
        let tamBytes = paquete.packet_size();
        let bytesV = paquete.packet_mut();
        
        //Y de &[u8] a Vec<u8>
        vectorRetorno = bytesV.to_vec();

        return (vectorRetorno,tamBytes);
    }

    /** 
     * @in: Int "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, que devolverá a la Interfaz
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    pub fn field_Help(&self, campo : String) -> String{
        let mut ayuda : String = String::new();
        let campoStr = campo.as_str();
        match campoStr{
            "PuertoOrigen" => {
                let helpe = "El campo 'SourcePort' define el puerto de origen\n desde el que se envia la información.\n
                El puerto del que provenga indicará\n también que tipo de paquete estamos enviado. Por ejemplo:\n
                7: echo \n
                80  : HTTP\n
                8080 : Pruebas de HTTP\n
                21: FTP\n
                22: SFTP y SSH\n
                25: SMTP \n
                53:  DNS\n
                443 : HTTPS\n
                33434 : traceroute \n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "PuertoDestino" => {
                let helpe = "El campo 'DestinationPort' define el puerto de destino\n al que irá a parar la información.\n
                Este puerto también indica\n que tipo de paquete estamos enviado. Por ejemplo:\n
                7: echo \n
                80  : HTTP\n
                8080 : Pruebas de HTTP\n
                21: FTP\n
                22: SFTP y SSH\n
                25: SMTP \n
                53:  DNS\n
                443 : HTTPS\n
                33434 : traceroute \n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "Sequence" => {
                let helpe = "El campo Sequence/Secuencia indicará el numero de secuencia\n donde está el primer octeto de datos en el segmento.\n
                Si el flag SYN está activado, este campo será el inicio de la secuencia\n y el primer octeto de datos será el valor del campo+1\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "ACK" => {
                let helpe = "Si el flag ACK = 1, este campo contiene el valor\n del siguiente numero de secuencia que se espera recibir.\n
                Este valor se envia siempre una vez que se ha establecido la conexión\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "DataOffset" => {
                let helpe = "El Data Offset indica el número de palabras de 32 bits\n de las que está compuesta la cabecera TCP. 
                Es decir,\n el valor que nos indica donde acaba la cabecera TCP y empiezan los datos. \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "Reserved" => {
                let helpe = "Este campo se usará para TCP en un futuro,\n por el momento será siempre 0.\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "TCPFlags" => {
                let helpe = "El campo Flags está formado 6 valores que\n modificarán como se trata el paquete\n en la conversación de paquetes TCP: \n
                URG : Si URG(Urgente) = 1, el paquete es urgente\n y debe ser procesado antes que el resto de los paquetes recibidos.\n
                Desde 2011 se desaconseja su uso, pero puede seguir usandose.\n
                ACK : Si ACK(Acknowledgment/Reconocimiento ) = 1,\nsirve para dar respuesta al paquete TCP anterior\n e indicar con el campo del mismo nombre cual\n
                es el paquete siguiente, a partir del primer SYN\n
                PSH : Si PSH(PUSH)= 1, el paquete forzará al sistema\n a procesar los paquetes recibidos hasta el momento, en lugar de\n esperar al mínimo como haría normalmente\n
                RST : Si RST (Reset) = 1 , se reiniciará la conversación de paquetes TCP desde el principio.\n Suele usarse en caso de errores.\n
                SYN : Si SYN (Synchronization /Sincronización) = 1,\n se inicia la conversación TCP y se establece la conexión\n
                FIN : Si FIN = 1, se termina la conversación TCP correctamente.\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "TCPWindow" => {
                let helpe = "Este campo indica el tamaño de la ventana de recepción de segmentos. Es decir,\n
                este campo indica cual es el tamaño maximo de paquete que el receptor\n puede recibir sin segmentar (y asi no necesitar ACK)\n\n
                Para mas informacion:\n'https://www.rfc-editor.org/rfc/rfc793.html'.\n";
                ayuda = helpe.to_string();
            },
            
            "TCPChecksum" => {
                let helpe = "Este valor es una suma de comprobacion\nque indica que el paquete no fue modificado.\n
                                Se calcula en el envio pero puedes\n pre calcularlo con el boton de la izquierda\n\n
                                Para mas informacion:\n'https://www.rfc-editor.org/rfc/rfc793.html'.\n";
                ayuda = helpe.to_string();
            },
            "UrgPointer" => {
                let helpe = "En caso de estar el flag URG=1, este campo indica\n la posición del paquete desde la que empieza el paquete urgente.\n
                Si dicho flag está desactivado, este campo se descarta.\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'";
                ayuda = helpe.to_string();
            },
            "TCPOptions" => {
                let helpe = "Las Opciones de TCP nos dan información extra sobre el paquete TCP.\nPuedes añadir hasta 7 opciones, separadas por ';'.Entre ellas estan:\n
                0 -> End of Option List : Se añade cuando terminas la lista de opciones.\n
                1 -> No Operation : Se añade cuando no quieres incluir opciones\n
                2 -> Maximum Segment Size: Amplia el maximo de tamaño del segmento en el paquete.\n
                3 -> Window Scale : Escala el tamaño de la Ventana de TCP entre 65535 bytes y 1Gigabyte\n
                4 -> SACK Permited : Selective Acknowledgement/ Reconocimiento Selectivo :\n Permite reconocer paquetes discontinuos en la comunicación de paquetes TCP\n 
                5 -> SACK : Esta opcion, una vez incluida la anterior,\n especifica cada cuantos bloques puede reconocer paquetes discontinuos.\n
                8 -> Timestamps/Marca de fecha: Esta opcion hace que se muestre la fecha del paquete\n que se envia, con el fin de facilitar la ordenación del tráfico TCP a la llegada al destino\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc793.html'\n y 'https://www.iana.org/assignments/tcp-parameters/tcp-parameters.xhtml'";
                ayuda = helpe.to_string();
            },
            "TCPPayload" => {
                let helpe = "El campo de Payload (32 bits) servirá para\n
                añadir mediante codigos hexadecimales\n los campos de protocolos de capa superiores a TCP,\n
                sin necesidad de crear una nueva capa\n en el paquete para la edición\nde dicho protocolo.\n\n
                Generalmente, al ser TCP por encima hay un protocolo de capa 5.\n
                Puedes introducir los datos mediante el boton de tu derecha\n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc792.html'. ";
                ayuda = helpe.to_string();
            },
            "TCPBotonPayload" => {
                let helpe = "Con este boton podras modificar\n comodamente el payload de capa superior a TCP\n";
                ayuda = helpe.to_string();
            },
            _=>{},
        }
        return ayuda;
    }

    /**
     * @in: Paquete TCP
     * @out: checksum del paquete TCP
     * @function: La funcion genera un checksum a partir de los datos que hay en la interfaz cuando esta es llamada 
    */

    pub fn generaChecksum(&self, sour : Ipv4Addr, dest : Ipv4Addr) -> u16 {
        //Generamos el paquete ICMP a partir de los datos que tenemos actualmente
        let pac = 
        pnet::packet::tcp::Tcp{
            source: self.sourcePort,
            destination: self.destinationPort ,
            sequence: self.sequenceNum,
            acknowledgement: self.acknowledgementNum,
            data_offset: self.dataOffset,
            reserved: self.reserved,
            flags: self.flagsTCP,
            window: self.tamWindow,
            checksum: self.checksum,
            urgent_ptr: self.urgentPointer,
            options: self.optionsTCP.clone(),
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 400];
       
        // Llamamos a la funcion checksum
        let resultado = create_tcp_packet(&mut int_slice, pac, self.checksum, sour, dest).1;

        return resultado;
    }

    /**
     * @in: IP de Origen y Destino, Tamaño total del paquete IP+TCP
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion será llamada por la interfaz y permitirá mandar un mensaje IP con su payload TCP
     */
    pub fn sendPackage(&mut self, tamanioModificado : u16, sour : Ipv4Addr, dest : Ipv4Addr) ->  Result< u16, String>{

        //Transformamos en payload el paquete
        let mut paquete = self.tcpToPayload(sour, dest);
        let paylICMP = paquete.0;
        let tamPayload = paquete.1;

        let envio = self.send_TCPPackage(paylICMP,tamPayload, tamanioModificado);
        return envio;
    }

/**
     * @in: nuevoPayload: Paquete TCP convertido en Payload de IP
     *      tamPayload : Tamaño del payload TCP, que habrá que sumarle al paquete IP para un envio correcto
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion se encargará de enviar correctamente un  paquete TCP , usando la cabecera IP
     */
    fn send_TCPPackage(&mut self, nuevoPayload : Vec<u8>, tamPayload : usize, tamanioModificado : u16 )->  Result< u16, String>{

        let mut tamReal = (tamPayload as u16);
        let mut tamAnterior = self.ipPacket.get_Length();
        let mut nuevoTam = 0;
        
       
        
            nuevoTam = 500;
        
        
        //Actualizamos datos del paquete IP
        &self.ipPacket.set_HexPayload(nuevoPayload);
        &self.ipPacket.set_Length( nuevoTam);
        let envio =  self.ipPacket.send_IP_Package();

        return envio;

    }
    //Getter y Setter

    pub fn get_SourcePort(&self) -> u16 {
        return self.sourcePort;
    }
    pub fn set_SourcePort (&mut self, sourp : u16) -> () {
        self.sourcePort = sourp;
    }

    pub fn get_DestinationPort(&self) -> u16 {
        return self.destinationPort;
    }
    pub fn set_DestinationPort (&mut self, destP : u16) -> () {
        self.destinationPort = destP;
    }

    pub fn get_Sequence(&self) -> u32 {
        return self.sequenceNum;
    }
    pub fn set_Sequence (&mut self, seq : u32) -> () {
        self.sequenceNum = seq;
    }

    pub fn get_ACK(&self) -> u32 {
        return self.acknowledgementNum;
    }
    pub fn set_ACK (&mut self, ackk : u32) -> () {
        self.acknowledgementNum = ackk;
    }

    pub fn get_DataOffset(&self) -> u8 {
        return self.dataOffset;
    }
    pub fn set_DataOffset (&mut self, doff : u8) -> () {
        self.dataOffset = doff;
    }

    pub fn get_Reserved(&self) -> u8 {
        return self.reserved;
    }
    pub fn set_Reserved (&mut self, ress : u8) -> () {
        self.reserved = ress;
    }

    pub fn get_Flags(&self) -> u16 {
        return self.flagsTCP;
    }
    pub fn set_Flags (&mut self, flaa : u16) -> () {
        self.flagsTCP = flaa;
    }

    pub fn get_Window(&self) -> u16 {
        return self.tamWindow;
    }
    pub fn set_Window (&mut self, vent : u16) -> () {
        self.tamWindow = vent;
    }

    pub fn get_Checksum(&self) -> u16 {
        return self.checksum;
    }
    pub fn set_Checksum (&mut self, checksu : u16) -> () {
        self.checksum = checksu;
    }

    pub fn get_UrgentPointer(&self) -> u16 {
        return self.urgentPointer;
    }
    pub fn set_UrgentPointer (&mut self, urgp : u16) -> () {
        self.urgentPointer = urgp;
    }

    pub fn get_Options(&self) -> Vec<TcpOption> {
        return self.optionsTCP.clone();
    }
    pub fn set_Options (&mut self, optio : Vec<TcpOption>) -> () {
        self.optionsTCP = optio.clone();
    }

    pub fn get_Payload(&self) -> Vec<u8> {
        return self.payload.clone();
    }

    pub fn set_Payload (&mut self, payl : Vec<u8>) -> () {
        self.payload = payl.clone();
    }

    pub fn get_IPPacket(&self) -> IP {
        return self.ipPacket.clone();
    }

    pub fn set_IPPacket (&mut self, pac : IP) -> () {
        self.ipPacket = pac;
    }

}