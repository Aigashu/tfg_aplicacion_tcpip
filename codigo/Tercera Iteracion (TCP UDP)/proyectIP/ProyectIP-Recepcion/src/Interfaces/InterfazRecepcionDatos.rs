#![allow(dead_code)]
#![allow(unused_variables)]
use crossbeam::channel::unbounded;
use fltk::{
    app, browser::HoldBrowser, button::Button, enums::Color, frame::Frame, input::Input,
    prelude::*, window::Window,
};
use fltk::{button::RadioRoundButton, group::*, menu::*, prelude::*, table::*, text::*};
use fltk::{prelude::*, *};
use fltk_evented::Listener;
use fltk_grid::Grid;

use std::fmt::Write;
use std::time::{Duration, Instant};
use std::{thread, time};
extern crate chrono;
extern crate timer; //Temporizador
                    //use crossbeam_utils::thread as other_thread;
use itertools::Itertools;
use pnet::datalink::Channel::Ethernet;
use pnet::datalink::*;
use pnet::packet::ethernet::EthernetPacket;
use pnet::packet::ethernet::{EtherType, EtherTypes};
use rayon as other_thread;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::{Arc, Mutex};

use crate::Interfaces::Protocols::ICMP::ICMP;
use crate::Interfaces::Protocols::IP::IP;
use crate::Interfaces::Protocols::TCP::TCP;
use crate::Interfaces::Protocols::UDP::UDP;
use pnet::packet::icmp::MutableIcmpPacket;
use pnet::packet::icmp::{IcmpPacket, IcmpType};
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::Ipv4Packet;
use pnet::packet::ipv4::MutableIpv4Packet;
use pnet::packet::tcp::TcpPacket;
use pnet::packet::udp::UdpPacket;

use pnet::packet::*;
use std::net::Ipv4Addr;

//La interfaz de recepción tendrá los mismos datos que la de envío
#[derive(Clone)]
pub struct InterfazDatos {
    campoVersion: String,
    campoIHL: String,
    campoTypeOfService: String,
    campoECN: String,
    campoLength: String,
    campoID: String,
    campoFlags: String,
    campoFragmentOffSet: String,
    campoTTL: String,
    campoProtocol: String,
    campoHeaderChecksum: String,
    campoSourceAddress: String,
    campoDestinationAddress: String,
    campoOptions: String,
    campoPadding: String,
    campoHexadecimalPayload: String,
    listaInterfaces: Vec<NetworkInterface>,
    listaPaquetes: HoldBrowser,
    paqueteIP: IP,
    paqueteICMP: ICMP,
    paqueteTCP: TCP,
    paqueteUDP: UDP,
}

//Constructor por defecto
pub fn new() -> InterfazDatos {
    let listaInter = interfaces(); //Vec<NetworkInterfaces>
    let listaPaq = HoldBrowser::default();
    let pIP = IP::new();
    let pICMP = ICMP::new();
    let pTCP: TCP = TCP::new();
    let pUDP: UDP = UDP::new();
    InterfazDatos {
        campoVersion: "".to_string(),
        campoIHL: "".to_string(),
        campoTypeOfService: "".to_string(),
        campoECN: "".to_string(),
        campoLength: "".to_string(),
        campoID: "".to_string(),
        campoFlags: "".to_string(),
        campoFragmentOffSet: "".to_string(),
        campoTTL: "".to_string(),
        campoProtocol: "".to_string(),
        campoHeaderChecksum: "".to_string(),
        campoSourceAddress: "127.0.0.1".to_string(),
        campoDestinationAddress: "".to_string(),
        campoOptions: "".to_string(),
        campoPadding: "".to_string(),
        campoHexadecimalPayload: "".to_string(),
        listaInterfaces: listaInter,
        listaPaquetes: listaPaq,
        paqueteIP: pIP,
        paqueteICMP: pICMP,
        paqueteTCP: pTCP,
        paqueteUDP: pUDP,
    }
}

impl InterfazDatos {
    /**
     * @in: String con la IP en formato: "XXX.XXX.XX.XX"
     * @out: IP en formato Ipv4Addr
     * @function: La función transforma de manera general un string con la IP en una IP en formato Ipv4Addr, en caso de necesitarla
     */
    pub fn convertstring_to_IPV4(&self, ipString: String) -> Ipv4Addr {
        let vecIP: Vec<&str> = ipString.split(".").collect();
        //Conversion con cobertura de errores
        let bit1: Result<u8, String> = match vecIP[0].parse::<u8>() {
            Ok(bit1) => Ok(bit1),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
            Err(e) => Err(String::from("Error al convertir el bit")),
        };
        let bit2: Result<u8, String> = match vecIP[1].parse::<u8>() {
            Ok(bit2) => Ok(bit2),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
            Err(e) => Err(String::from("Error al convertir el bit")),
        };
        let bit3: Result<u8, String> = match vecIP[2].parse::<u8>() {
            Ok(bit3) => Ok(bit3),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
            Err(e) => Err(String::from("Error al convertir el bit")),
        };
        let bit4: Result<u8, String> = match vecIP[3].parse::<u8>() {
            Ok(bit4) => Ok(bit4),
            //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
            Err(e) => Err(String::from("Error al convertir el bit")),
        };
        let valA = bit1.unwrap();
        let valB = bit2.unwrap();
        let valC = bit3.unwrap();
        let valD = bit4.unwrap();

        let ret = Ipv4Addr::new(valA, valB, valC, valD);

        return ret;
    }

    /**
     * @in: tipo : ICMPType
     * @out stringRes : String que indica el tipo de paquete icmp que es. P.e. : 0 : Echo Reply
     * @function : Esta funcion dará el texto a la interfaz de que tipo de paquete ICMP es cada uno
     */
    pub fn convertTipoICMPtoText(&self, tipo: IcmpType) -> String {
        let mut stringRes = String::new();

        match tipo {
            IcmpType(0) => {
                stringRes = String::from(" (Echo Reply) ");
            }
            IcmpType(3) => {
                stringRes = String::from(" (Destination Unreachable) ");
            }
            IcmpType(5) => {
                stringRes = String::from(" (Redirect) ");
            }
            IcmpType(7) => {
                stringRes = String::from(" (Unassigned) ");
            }
            IcmpType(8) => {
                stringRes = String::from(" (Echo Request) ");
            }
            IcmpType(9) => {
                stringRes = String::from(" (Router Advertisement) ");
            }
            IcmpType(10) => {
                stringRes = String::from(" (Router Solicitation) ");
            }
            IcmpType(11) => {
                stringRes = String::from(" (Time Exceeded) ");
            }
            IcmpType(12) => {
                stringRes = String::from(" (Parameter Problem) ");
            }
            IcmpType(13) => {
                stringRes = String::from(" (Timestamp) ");
            }
            IcmpType(14) => {
                stringRes = String::from(" (Timestamp Reply) ");
            }
            IcmpType(15) => {
                stringRes = String::from(" (Information Request) ");
            }
            IcmpType(16) => {
                stringRes = String::from(" (Information Reply) ");
            }
            IcmpType(30) => {
                stringRes = String::from(" (Traceroute) ");
            }
            _ => (),
        }

        return stringRes;
    }
    /**
     * @in:
     * @out:
     * @function: La funcion crea la interfaz de Búsqueda y Recepción de paquetes, permitiendo abrirlos en la capa que el usuario elija
     */
    pub fn createInterface(&mut self) {
        let appInt = app::App::default();
        let mut windInt = Window::new(800, 100, 500, 650, "Busqueda de paquetes"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        let frameInterfaz = Frame::new(275, 15, 75, 50, "Interfaz de red:");
        let frameBusqueda = Frame::new(25, 35, 75, 50, "Busqueda:");
        let mut borrarPaquetes: Listener<_> = Button::new(155, 15, 75, 50, "Borrar\nPaquetes").into();
        let mut frameEspera = Frame::new(185, 65, 75, 50, "Para pausar la busqueda pulse el boton\n a su derecha. Pulselo de nuevo para reiniciarla.");
        let frameSeleccion = Frame::new(25, 100, 75, 50, "Seleccion: ");
        let frameProtocolo =
            Frame::new(60, 575, 100, 50, "Protocolo con el que abrir\nel paquete:");

        let mut selectorProtocolo = menu::Choice::new(210, 590, 100, 30, None);
        selectorProtocolo.add_choice("IP");
        selectorProtocolo.add_choice("ICMP");
        selectorProtocolo.add_choice("TCP");
        selectorProtocolo.add_choice("UDP");
        selectorProtocolo.add_choice("HTTP");
        selectorProtocolo.add_choice("FTP");
        selectorProtocolo.set_value(0); //Posicion 0 de la lista : IP por defecto

        //Chooser con interfaces
        let mut vectorInterfaces = Vec::new(); //Guarda los nombres de las interfaces
        let mut numValue = 0; //Seleccionaremos la fila donde está eth0/enps0x/wpsf0x
        let mut nombreInterfaz = "".to_string();
        let mut selectorInterfaces = menu::Choice::new(375, 25, 100, 30, None);
        for val in &self.listaInterfaces {
            // No sabemos que interfaces tenemos pero tenemos el vector de las obtenidas
            //Introducimos las opciones encontradas en el selector
            vectorInterfaces.push(val.name.clone());
            selectorInterfaces.add_choice(val.name.as_str());
        }
        //Buscaremos la primera interfaz que empiece por e(eth0/enps03) o w (wpf0x)
        for val in vectorInterfaces {
            match val.chars().next() {
                Some('e') => {
                    //Si empieza por e la interfaz:
                    nombreInterfaz = val.clone();
                    break;
                }
                Some('w') => {
                    nombreInterfaz = val.clone();
                    break;
                }
                _ => (),
            }
        }
        //Por defecto se pone la primera interfaz que se detecte. Se entenderá que si detecta antes wpf0x será que obtiene red por wifi
        numValue = selectorInterfaces.find_index(&nombreInterfaz.as_str());
        selectorInterfaces.set_value(numValue);

        let mut seleccionPaquete: Listener<_> = Button::new(400, 585, 80, 40, "Abrir\nPaquete").into();
        //Funcion de busqueda de paquetes
        //Recogida en lista de dichos paquetes
        let mut listaPaquetes = HoldBrowser::new(25, 140, 450, 400, "Paquetes encontrados"); //x,y,ancho, alto, titulo

        let mut paraBusqueda: Listener<_> = Button::new(405, 75, 80, 40, "Busqueda").into();
        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self

        //Para guardar los paquetes IP que recibamos . Tree para que sea mas rapida su busqueda
        let mut arbolPaquetes: BTreeMap<(String, String), Vec<u8>> = BTreeMap::new();

        //let (mut p, mut x)= unbounded::<&str>();
        let (mut s, mut r) = app::channel::<&str>();
        let sDos = s.clone();
        let (mut sendPac, mut recPac) = app::channel::<Vec<Vec<u8>>>();
        let timer = timer::Timer::new();
        let mut variableControl = 1; //Busqueda = 0; 1 = Stop
        windInt.make_resizable(true);
        windInt.end();

        let mut contadorPaquetes = 0;
        paraBusqueda.on_click(move |_| {
            s.send("Busqueda");
        });

        seleccionPaquete.set_callback(move |_| {
            s.send("Seleccion");
        });

        borrarPaquetes.set_callback(move |_| {
            s.send("Borrado");
        });

        //Las usaremos en la busqueda
        let mut interfazseleccionada = NetworkInterface {
            name: "".to_string(),
            description: "".to_string(),
            index: 0,
            mac: None,
            ips: Vec::new(),
            flags: 0,
        };

        let mut aux = selectorInterfaces.choice().clone().unwrap();
        let seleccion = aux.as_str();
        for val in &self.listaInterfaces {
            //Buscamos en la lista de interfaces cual hemos elegido
            let texto = val.name.as_str();
            match seleccion {
                texto => {
                    let nameAux = texto.to_string();
                    let descAux = val.description.clone();
                    let indexAux = val.index;
                    let macAux = val.mac;
                    let ipsAux = val.ips.clone();
                    let flagsAux = val.flags;

                    interfazseleccionada = NetworkInterface {
                        //Obtendremos la interfaz elegida con todos sus datos
                        name: nameAux,
                        description: descAux,
                        index: indexAux,
                        mac: macAux,
                        ips: ipsAux,
                        flags: flagsAux,
                    };
                }
                _ => {}
            }
            //end of For de Interfaz
        }

        let mut firstime = 1;
        aux = selectorInterfaces.choice().clone().unwrap();
        let seleccion = aux.as_str();
        let interface_names_match = |iface: &NetworkInterface| iface.name == seleccion;
        let interfazseleccionada = self
            .listaInterfaces
            .clone()
            .into_iter()
            .filter(interface_names_match)
            .next()
            .unwrap_or_else(|| panic!("No such network interface: {}", seleccion));

        //Buscamos paquetes una vez tiene interfaz

        let mut helpVersion: Listener<_> = Button::default().into();
        let mut helpIHL: Listener<_> = Button::default().into();
        let mut helpTOS: Listener<_> = Button::default().into();
        let mut helpECN: Listener<_> = Button::default().into();
        let mut helpLen: Listener<_> = Button::default().into();
        let mut helpID: Listener<_> = Button::default().into();
        let mut helpFlags: Listener<_> = Button::default().into();
        let mut helpOffSet: Listener<_> = Button::default().into();
        let mut helpTTL: Listener<_> = Button::default().into();
        let mut helpPro: Listener<_> = Button::default().into();
        let mut helpChecksum: Listener<_> = Button::default().into();
        let mut helpSource: Listener<_> = Button::default().into();
        let mut helpDest: Listener<_> = Button::default().into();
        let mut hexaPayload: Input = Input::default();
        let mut helphexPay: Listener<_> = Button::default().into();
        let mut paylo: Listener<_> = Button::default().into();
        let mut helpPay: Listener<_> = Button::default().into();

        let mut inputICMPType = Input::default();
        let mut inputICMPCode = Input::default();
        let mut inputICMPChecksum = Input::default();
        let mut inputICMPPayload = Input::default();
        let mut helpType: Listener<_> = Button::default().into();
        let mut helpCode: Listener<_> = Button::default().into();
        let mut helpICMPChecksum: Listener<_> = Button::default().into();
        let mut helpICMPPayl: Listener<_> = Button::default().into();
        let mut helpICMPBotonPayl: Listener<_> = Button::default().into();

        let mut inputSourcePort = Input::default();
        let mut helpSourcePort: Listener<_> = Button::default().into();
        let mut inputDestPort = Input::default();
        let mut helpDestPort: Listener<_> = Button::default().into();
        let mut inputTCPSequence = Input::default();
        let mut helpSequence: Listener<_> = Button::default().into();
        let mut inputTCPAck = Input::default();
        let mut helpTCPAck: Listener<_> = Button::default().into();
        let mut inputDataOffset = Input::default();
        let mut helpTCPOffset: Listener<_> = Button::default().into();
        let mut inputReserved = Input::default();
        let mut helpTCPReserved: Listener<_> = Button::default().into();
        let mut inputFlagsTCP = Frame::default();
        let mut tituloFlagsURG = Frame::default();
        let mut tituloFlagsACK = Frame::default();
        let mut tituloFlagsPSH = Frame::default();
        let mut tituloFlagsRST = Frame::default();
        let mut tituloFlagsSYN = Frame::default();
        let mut tituloFlagsFIN = Frame::default();
        let mut helpTCPFlags: Listener<_> = Button::default().into();
        let mut inputTCPWindow = Input::default();
        let mut helpTCPWindow: Listener<_> = Button::default().into();
        let mut inputTCPChecksum = Input::default();
        let mut helpTCPChecksum: Listener<_> = Button::default().into();
        let mut inputTCPUrgentPointer = Input::default();
        let mut helpTCPUrgPointer: Listener<_> = Button::default().into();
        let mut inputTCPOptions = Input::default();
        let mut helpTCPOpt: Listener<_> = Button::default().into();
        let mut inputTCPPayload = Input::default();
        let mut helpTCPPayl: Listener<_> = Button::default().into();
        let mut helpPayTCP: Listener<_> = Button::default().into();

        let mut inputUDPSourcePort = Input::default();
        let mut helpUDPSourcePort: Listener<_> = Button::default().into();
        let mut inputUDPDestPort = Input::default();
        let mut helpUDPDestPort: Listener<_> = Button::default().into();
        let mut inputUDPLength = Input::default();
        let mut helpUDPLen: Listener<_> = Button::default().into();
        let mut inputUDPChecksum = Input::default();
        let mut helpUDPChecksum: Listener<_> = Button::default().into();
        let mut inputUDPPayload = Input::default();
        let mut helpUDPPayl: Listener<_> = Button::default().into();
        let mut buttonPaylUDP: Listener<_> = Button::default().into();
        let mut helpPayUDP: Listener<_> = Button::default().into();

        let mut vectorUdp = Vec::new();
        let mut vectorTcp = Vec::new();
        let mut vectorIcmp = Vec::new();
        let mut vectorPacket: Vec<Vec<u8>> = Vec::new();
        let mut activado = 0;
        let mut activacion = 0;
        let mut windEmergente = Window::new(800, 100, 400, 150, "Espere, por favor"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        let frameTitulo = Frame::new(50, 0, 300, 150, "Se estan buscando paquetes.\nEsta ventana se cerrara automaticamente\ncuando se encuentre alguno. Por favor, espere.");
        windEmergente.make_resizable(true);
        windEmergente.end();
        windEmergente.show();

        while appInt.wait() {
            let mut vecAuxC = Vec::new();
            aux = selectorInterfaces.choice().clone().unwrap();
            let auxClone = aux.clone();
            let listaClone = self.listaInterfaces.clone();
            let mut copiaListaPaquetes = listaPaquetes.clone();
            //Mientras espera, tenemos el contador

            appInt.redraw();

            match r.recv() {
                Some(recuperado) => {
                    let helpS = recuperado;
                    let stringAux = self.paqueteIP.field_Help(helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let icmpAux = self.paqueteICMP.field_Help(helpS.to_string());
                    let strHelpICMP = icmpAux.as_str();

                    let tcpAux = self.paqueteTCP.field_Help(helpS.to_string());
                    let strHelpTCP = tcpAux.as_str();

                    let udpAux = self.paqueteUDP.field_Help(helpS.to_string());
                    let strHelpUDP = udpAux.as_str();

                    match recuperado {
                        "Busqueda" => {
                            if variableControl == 0 {
                                variableControl = 1; //Para
                                activado = 0;
                                paraBusqueda.set_label("Busqueda");
                            } else {
                                //Se pone a buscar
                                paraBusqueda.set_label("Parar");
                                activado = 1;
                                variableControl = 0;

                                s.send("BusquedaWhile");
                            }

                            //end of Busqueda
                        }
                        "Borrado" => {
                            arbolPaquetes.clear();
                            listaPaquetes.clear();
                            //end of Borrado
                        }
                        "ContinuaBusqueda" => {
                            if activado == 1 {
                                let p = self.clone();
                                let recu = recPac.clone();
                                
                                    match recu.recv() {
                                        Some(pacIP) => {
                                            vectorPacket = pacIP.clone();
                                        }
                                        None => {}
                                    }
    
                                    if !vectorPacket.is_empty() {
                                        for valIP in &vectorPacket {
                                            
                                            let packetEther = valIP.as_slice();
                                            let paqueteAuxiliar = Ipv4Packet::new(packetEther).unwrap();
                                            let mut vecPaquete = paqueteAuxiliar.packet().to_owned();
                                            let prot = paqueteAuxiliar.get_next_level_protocol();
                                            //println!("Protocolo del Paquete : {}", prot);
                                            let octetosOrigen = paqueteAuxiliar.get_source().octets(); //Obtenemos el XXX.XXX.XX.XX de la direccion IP para añadirla a la lista
                                            let octetosDestino =
                                                paqueteAuxiliar.get_destination().octets();
                                            let mut stringAux = String::from("Paquete de Protocolo: ");
                                            let mut iStr = prot.to_string().to_uppercase();
                                            if iStr == "ICMP" {
                                                let aux = iStr;
                                                let paylIPP = paqueteAuxiliar.payload();
                                                let icmpPac = IcmpPacket::new(paylIPP).unwrap();
                                                let tipoTexto =
                                                p.convertTipoICMPtoText(icmpPac.get_icmp_type());
    
                                                let mut stringCopia = String::new();
                                                stringCopia.push_str(aux.as_str());
                                                stringCopia.push_str(tipoTexto.as_str());
                                                iStr = stringCopia.clone();
                                            }
                                            stringAux.push_str(iStr.as_str());
                                            stringAux.push_str("; ");
                                            stringAux.push_str("Origen:");
    
                                            let octetoOrigenUno = octetosOrigen[0].to_string(); //Lo convertimos a Str para poder introducirlos en la cadena
                                            let octetoOrigenDos = octetosOrigen[1].to_string();
                                            let octetoOrigenTres = octetosOrigen[2].to_string();
                                            let octetoOrigenCua = octetosOrigen[3].to_string();
                                            let mut stringOrigen = String::new();
    
                                            stringOrigen.push_str(octetoOrigenUno.as_str()); //127
                                            stringOrigen.push_str("."); //.
                                            stringOrigen.push_str(octetoOrigenDos.as_str()); //0
                                            stringOrigen.push_str("."); //.
                                            stringOrigen.push_str(octetoOrigenTres.as_str()); //0
                                            stringOrigen.push_str("."); //.
                                            stringOrigen.push_str(octetoOrigenCua.as_str()); //1
    
                                            stringAux.push_str(octetoOrigenUno.as_str()); //127
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoOrigenDos.as_str()); //0
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoOrigenTres.as_str()); //0
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoOrigenCua.as_str()); //1
                                            stringAux.push_str("; ");
                                            stringAux.push_str("Destino:");
    
                                            let octetoDestUno = octetosDestino[0].to_string();
                                            let octetoDestDos = octetosDestino[1].to_string();
                                            let octetoDestTres = octetosDestino[2].to_string();
                                            let octetoDestCua = octetosDestino[3].to_string();
                                            let mut strDestino = String::from("");
                                            stringAux.push_str(octetoDestUno.as_str()); //127
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoDestDos.as_str()); //0
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoDestTres.as_str()); //0
                                            stringAux.push_str("."); //.
                                            stringAux.push_str(octetoDestCua.as_str()); //1
                                            stringAux.push_str(";");
    
                                            strDestino.push_str(octetoDestUno.as_str()); //127
                                            strDestino.push_str("."); //.
                                            strDestino.push_str(octetoDestDos.as_str()); //0
                                            strDestino.push_str("."); //.
                                            strDestino.push_str(octetoDestTres.as_str()); //0
                                            strDestino.push_str("."); //.
                                            strDestino.push_str(octetoDestCua.as_str()); //1
    
                                            //Introducimos paquetes en la lista
                                            listaPaquetes.add(stringAux.as_str()); //Paquete 1; Origen: 127.0.0.1; Destino: 127.0.0.1
                                                                                   //... y en las estructuras de datos para acceder a los paquetes posteriormente
                                                                                   //Primero convertimos el &[u8] en Vec<u8> para que tenga permanencia
    
                                            //El arbol tiene como clave las dos IP para encontrar el paquete
                                            let par = (stringOrigen, strDestino);
                                            arbolPaquetes.insert(par, vecPaquete);
                                        }
                                        
                                    }
                                    vectorPacket = Vec::new();
                                

                                if activacion == 0 {
                                    activacion = 1;
                                    //println!("Entro a activar interfaz");
                                    windEmergente.hide();
                                    windInt.show();
                                }
                                s.send("BusquedaWhile");
                            }
                        }
                        "BusquedaWhile" => {
                            let sendPoc = sendPac.clone();
                            other_thread::spawn(move || {
                                while variableControl == 0 {
                                    let seleccion = auxClone.as_str();
                                    let interface_names_match =
                                        |iface: &NetworkInterface| iface.name == seleccion;
                                    let interfazseleccionada = listaClone
                                        .clone()
                                        .into_iter()
                                        .filter(interface_names_match)
                                        .next()
                                        .unwrap_or_else(|| {
                                            panic!("No such network interface: {}", seleccion)
                                        });

                                    //Buscamos paquetes una vez tiene interfaz
                                    let mut config = Default::default(); //configuracion por defecto
                                                                         //Obtenemos el canal de envio y recepcion de paquetes a partir de una interfaz, con una configuracion
                                    let (mut tx, mut sx) =
                                        match channel(&interfazseleccionada, config) {
                                            Ok(Ethernet(tx, sx)) => (tx, sx),
                                            _ => panic!(),
                                        };
                                    while vecAuxC.len() < 2 {
                                        match sx.next() {
                                            //Recibimos en sx los paquetes que han llegado a esa interfaz
                                            Ok(packet) => {
                                                vecAuxC.push(packet.to_vec().clone());
                                            }
                                            Error => {}
                                        }
                                    }

                                    if vecAuxC.len() >= 1 {
                                        let vecTake = vecAuxC.clone();
                                        let mut vecEnvio = Vec::new();
                                        for valorVec in &vecTake {
                                            if !valorVec.is_empty() {
                                                let veCl: Vec<u8> = valorVec.clone();
                                                let paqueteAgain = veCl.as_slice();

                                                //Recibido el paquete, lo transformamos en un paquete Ethernet
                                                //Suponemos que no habrá errores pues si está en la red es un paquete correcto
                                                let packetEther =
                                                    EthernetPacket::new(paqueteAgain).unwrap();
                                                //Del paquete Ethernet obtenemos su payload, buscando si es IP, ICMP, et
                                                if packetEther.get_ethertype() == EtherTypes::Ipv4 {
                                                    //Si es tipo IPv4
                                                    //Si el paquete es IP, lo obtenemos para tratar con él
                                                    let vecPaquete =
                                                        packetEther.payload().to_owned();

                                                    vecEnvio.push(vecPaquete); //Enviamos un vector de vectores
                                                }
                                            }
                                        }

                                        vecAuxC = Vec::new();

                                        sDos.send("ContinuaBusqueda");
                                        sendPoc.send(vecEnvio);
                                        vecEnvio = Vec::new();
                                        break;
                                    }
                                    app::redraw();
                                }
                                app::redraw();
                            });
                        }
                        "Seleccion" => {
                            //Selecciona el Protocolo del que vamos a sacar la interfaz
                            let auxP = selectorProtocolo.choice().clone().unwrap(); //IP,TCP,UDP,...
                            let protocoloSeleccionado = auxP.as_str();
                            //Primero obtenemos cual valor de la lista ha sido
                            if listaPaquetes.size() != 0 {
                                let seleccionado = listaPaquetes.selected_text(); //Paquete 1; Origen: 127.0.0.1; Destino: 127.0.0.1
                                match seleccionado {
                                    Some(correcto) => {
                                        if correcto != "\n" {
                                            let vectorSplitSeleccionado: Vec<&str> =
                                                correcto.split(";").collect(); //vec[0] = Paquete..., vec[1]=Origen, vec[2]=Destino

                                            let vectorOrigen: Vec<&str> =
                                                vectorSplitSeleccionado[1].split(":").collect(); //vec[2] = 127.0.0.1
                                            let vectorDest: Vec<&str> =
                                                vectorSplitSeleccionado[2].split(":").collect(); //vec[2] = 127.0.0.1

                                            let ipOrigen = vectorOrigen[1];
                                            let ipDestino = vectorDest[1];

                                            let parIP =
                                                (ipOrigen.to_string(), ipDestino.to_string());
                                            //Con el Par de ambas IP origen y destino se hace obtiene el dato del arbol
                                            let paqueteAObtener: Vec<u8> =
                                                arbolPaquetes.get(&parIP).unwrap().to_vec();

                                            //Una vez obtenido el paquete, se manda a la funcion de creacion de interfaces
                                            //Dependiendo del protocolo , hará una cosa u otra
                                            match protocoloSeleccionado {
                                                //Dependiendo de la interfaz que nos pida, tendremos que mostrar mas o menos
                                                "IP" => {
                                                    let pac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                                                    let vectorIp = self.getIPData(pac); //A partir de el, obtenemos los datos para la interfaz

                                                    let mut windIP = Window::default()
                                                        .with_size(900, 350) //Ancho, Alto
                                                        .center_screen()
                                                        .with_label("Paquete de Capa 3");
                                                    //Definimos la interfaz
                                                    let titulo = Frame::new(0, 0, 900, 50, "IP");

                                                    let mut inputVersion =
                                                        Input::new(65, 50, 75, 25, "Version"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputVersion.set_value(vectorIp[0].as_str());
                                                    inputVersion.set_readonly(true);
                                                    helpVersion =
                                                        Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpVersion.set_color(Color::Yellow);

                                                    let mut inputIHL =
                                                        Input::new(215, 50, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputIHL.set_value(vectorIp[1].as_str());
                                                    inputIHL.set_readonly(true);
                                                    helpIHL =
                                                        Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpIHL.set_color(Color::Yellow);

                                                    let mut inputTypeOfServ = Input::new(
                                                        435,
                                                        50,
                                                        75,
                                                        25,
                                                        "TypeOfService",
                                                    ); //X,Y,ANCHO, ALTO, TITULO
                                                    inputTypeOfServ.set_value(vectorIp[2].as_str());
                                                    inputTypeOfServ.set_readonly(true);
                                                    helpTOS =
                                                        Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpTOS.set_color(Color::Yellow);

                                                    let mut inputECN =
                                                        Input::new(595, 50, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputECN.set_value(vectorIp[3].as_str());
                                                    inputECN.set_readonly(true);
                                                    helpECN =
                                                        Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpECN.set_color(Color::Yellow);

                                                    let mut inputLength =
                                                        Input::new(775, 50, 75, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputLength.set_value(vectorIp[4].as_str());
                                                    inputLength.set_readonly(true);
                                                    helpLen =
                                                        Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpLen.set_color(Color::Yellow);

                                                    let mut inputID =
                                                        Input::new(65, 100, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputID.set_value(vectorIp[5].as_str());
                                                    inputID.set_readonly(true);
                                                    helpID =
                                                        Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpID.set_color(Color::Yellow);

                                                    let mut inputFlags =
                                                        Input::new(355, 100, 50, 25, "Flags"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputFlags.set_value(vectorIp[6].as_str());
                                                    inputFlags.set_readonly(true);
                                                    helpFlags =
                                                        Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpFlags.set_color(Color::Yellow);

                                                    let mut inputFragmentOff = Input::new(
                                                        685,
                                                        100,
                                                        130,
                                                        25,
                                                        "FragmentOffset",
                                                    ); //X,Y,ANCHO, ALTO, TITULO
                                                    inputFragmentOff
                                                        .set_value(vectorIp[7].as_str());
                                                    inputFragmentOff.set_readonly(true);
                                                    helpOffSet =
                                                        Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpOffSet.set_color(Color::Yellow);

                                                    let mut inputTTL =
                                                        Input::new(65, 150, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputTTL.set_value(vectorIp[8].as_str());
                                                    inputTTL.set_readonly(true);
                                                    helpTTL =
                                                        Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpTTL.set_color(Color::Yellow);

                                                    let mut inputProtocol =
                                                        Input::new(345, 150, 100, 25, "Protocol"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputProtocol.set_value(
                                                        vectorIp[9].to_uppercase().as_str(),
                                                    );
                                                    inputProtocol.set_readonly(true);
                                                    helpPro =
                                                        Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpPro.set_color(Color::Yellow);

                                                    let mut inputHeaderChecksum =
                                                        Input::new(630, 150, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
                                                    inputHeaderChecksum
                                                        .set_value(vectorIp[10].as_str());
                                                    inputHeaderChecksum.set_readonly(true);
                                                    helpChecksum =
                                                        Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpChecksum.set_color(Color::Yellow);

                                                    let mut inputSourceAdd = Input::new(
                                                        165,
                                                        205,
                                                        200,
                                                        25,
                                                        "Source Address",
                                                    ); //X,Y,ANCHO, ALTO, TITULO
                                                    inputSourceAdd.set_value(vectorIp[11].as_str());
                                                    inputSourceAdd.set_readonly(true);
                                                    helpSource =
                                                        Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpSource.set_color(Color::Yellow);

                                                    let mut inputDestAdd = Input::new(
                                                        630,
                                                        205,
                                                        200,
                                                        25,
                                                        "Destination Address",
                                                    ); //X,Y,ANCHO, ALTO, TITULO
                                                    inputDestAdd.set_value(vectorIp[12].as_str());
                                                    inputDestAdd.set_readonly(true);
                                                    helpDest =
                                                        Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpDest.set_color(Color::Yellow);

                                                    hexaPayload =
                                                        Input::new(250, 255, 400, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
                                                    hexaPayload.set_value(vectorIp[13].as_str());
                                                    hexaPayload.set_readonly(true);
                                                    helphexPay =
                                                        Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helphexPay.set_color(Color::Yellow);

                                                    paylo = Button::new(50, 265, 80, 40, "Payload")
                                                        .into();
                                                    helpPay =
                                                        Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                    helpPay.set_color(Color::Yellow);

                                                    helpVersion.set_callback(move |b| {
                                                        s.send("Version");
                                                    });

                                                    helpIHL.on_click(move |b| {
                                                        s.send("IHL");
                                                    });

                                                    helpTOS.on_click(move |b| {
                                                        s.send("TypeOfService");
                                                    });

                                                    helpECN.on_click(move |b| {
                                                        s.send("ECN");
                                                    });

                                                    helpLen.on_click(move |b| {
                                                        s.send("Length");
                                                    });

                                                    helpID.on_click(move |b| {
                                                        s.send("ID");
                                                    });

                                                    helpFlags.on_click(move |b| {
                                                        s.send("Flags");
                                                    });

                                                    helpOffSet.on_click(move |b| {
                                                        s.send("FragmentOffset");
                                                    });

                                                    helpTTL.on_click(move |b| {
                                                        s.send("TTL");
                                                    });

                                                    helpPro.on_click(move |b| {
                                                        s.send("Protocol");
                                                    });

                                                    helpChecksum.on_click(move |b| {
                                                        s.send("HeaderChecksum");
                                                    });

                                                    helpSource.on_click(move |b| {
                                                        s.send("Source");
                                                    });

                                                    helpDest.on_click(move |b| {
                                                        s.send("Destination");
                                                    });

                                                    helphexPay.on_click(move |b| {
                                                        s.send("HexadecimalPayload");
                                                    });

                                                    helpPay.on_click(move |b| {
                                                        s.send("Payload");
                                                    });

                                                    paylo.set_callback(move |b| {
                                                        s.send("ButonPayload");
                                                    });
                                                    windIP.make_resizable(true);
                                                    windIP.end();
                                                    windIP.show();

                                                    //End of Interfaz IP
                                                }

                                                "ICMP" => {
                                                    let pac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                                                    let otropac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Una copia para poder transmitir su pertenencia a ICMP
                                                    let payloadICMP = otropac.payload();
                                                    let icmpPaquete = IcmpPacket::new(payloadICMP); //convertimos este paquete en ICMP
                                                    match icmpPaquete {
                                                        Some(pacICMP) => {
                                                            //El paquete es de tipo ICMP

                                                            vectorIcmp =
                                                                self.getICMPData(pac, pacICMP); //A partir de el, obtenemos los datos para la interfaz
                                                            let mut windIP = Window::default()
                                                                .with_size(900, 350) //Ancho, Alto
                                                                .center_screen()
                                                                .with_label("Paquete de Capa 3");
                                                            //Definimos la interfaz
                                                            let titulo = Frame::new(
                                                                0, 0, 900, 50, "ICMP-IP",
                                                            );

                                                            let mut inputVersion = Input::new(
                                                                65, 50, 75, 25, "Version",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputVersion
                                                                .set_value(vectorIcmp[0].as_str());
                                                            inputVersion.set_readonly(true);
                                                            helpVersion =
                                                                Button::new(150, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpVersion.set_color(Color::Yellow);

                                                            let mut inputIHL =
                                                                Input::new(215, 50, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputIHL
                                                                .set_value(vectorIcmp[1].as_str());
                                                            inputIHL.set_readonly(true);
                                                            helpIHL =
                                                                Button::new(300, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpIHL.set_color(Color::Yellow);

                                                            let mut inputTypeOfServ = Input::new(
                                                                435,
                                                                50,
                                                                75,
                                                                25,
                                                                "TypeOfService",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTypeOfServ
                                                                .set_value(vectorIcmp[2].as_str());
                                                            inputTypeOfServ.set_readonly(true);
                                                            helpTOS =
                                                                Button::new(520, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTOS.set_color(Color::Yellow);

                                                            let mut inputECN =
                                                                Input::new(595, 50, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputECN
                                                                .set_value(vectorIcmp[3].as_str());
                                                            inputECN.set_readonly(true);
                                                            helpECN =
                                                                Button::new(680, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpECN.set_color(Color::Yellow);

                                                            let mut inputLength = Input::new(
                                                                775, 50, 75, 25, "Length",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputLength
                                                                .set_value(vectorIcmp[4].as_str());
                                                            inputLength.set_readonly(true);
                                                            helpLen =
                                                                Button::new(865, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpLen.set_color(Color::Yellow);

                                                            let mut inputID =
                                                                Input::new(65, 100, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputID
                                                                .set_value(vectorIcmp[5].as_str());
                                                            inputID.set_readonly(true);
                                                            helpID =
                                                                Button::new(265, 100, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpID.set_color(Color::Yellow);

                                                            let mut inputFlags = Input::new(
                                                                355, 100, 50, 25, "Flags",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputFlags
                                                                .set_value(vectorIcmp[6].as_str());
                                                            inputFlags.set_readonly(true);
                                                            helpFlags =
                                                                Button::new(520, 100, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpFlags.set_color(Color::Yellow);

                                                            let mut inputFragmentOff = Input::new(
                                                                685,
                                                                100,
                                                                130,
                                                                25,
                                                                "FragmentOffset",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputFragmentOff
                                                                .set_value(vectorIcmp[7].as_str());
                                                            inputFragmentOff.set_readonly(true);
                                                            helpOffSet =
                                                                Button::new(830, 100, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpOffSet.set_color(Color::Yellow);

                                                            let mut inputTTL =
                                                                Input::new(65, 150, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTTL
                                                                .set_value(vectorIcmp[8].as_str());
                                                            inputTTL.set_readonly(true);
                                                            helpTTL =
                                                                Button::new(175, 150, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTTL.set_color(Color::Yellow);

                                                            let mut inputProtocol = Input::new(
                                                                345, 150, 100, 25, "Protocol",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputProtocol.set_value(
                                                                vectorIcmp[9]
                                                                    .to_uppercase()
                                                                    .as_str(),
                                                            );
                                                            inputProtocol.set_readonly(true);
                                                            helpPro =
                                                                Button::new(460, 150, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpPro.set_color(Color::Yellow);

                                                            let mut inputHeaderChecksum =
                                                                Input::new(
                                                                    630, 150, 100, 25, "Checksum",
                                                                ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputHeaderChecksum
                                                                .set_value(vectorIcmp[10].as_str());
                                                            inputHeaderChecksum.set_readonly(true);
                                                            helpChecksum =
                                                                Button::new(740, 150, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpChecksum.set_color(Color::Yellow);

                                                            let mut inputSourceAdd = Input::new(
                                                                165,
                                                                205,
                                                                200,
                                                                25,
                                                                "Source Address",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputSourceAdd
                                                                .set_value(vectorIcmp[11].as_str());
                                                            inputSourceAdd.set_readonly(true);
                                                            helpSource =
                                                                Button::new(385, 205, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpSource.set_color(Color::Yellow);

                                                            let mut inputDestAdd = Input::new(
                                                                630,
                                                                205,
                                                                200,
                                                                25,
                                                                "Destination Address",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputDestAdd
                                                                .set_value(vectorIcmp[12].as_str());
                                                            inputDestAdd.set_readonly(true);
                                                            helpDest =
                                                                Button::new(840, 205, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpDest.set_color(Color::Yellow);

                                                            hexaPayload = Input::new(
                                                                250, 255, 400, 25, "Payload",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            hexaPayload
                                                                .set_value(vectorIcmp[13].as_str());
                                                            hexaPayload.set_readonly(true);
                                                            helphexPay =
                                                                Button::new(670, 255, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helphexPay.set_color(Color::Yellow);

                                                            paylo = Button::new(
                                                                50, 265, 80, 40, "Payload",
                                                            )
                                                            .into();
                                                            helpPay =
                                                                Button::new(145, 265, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpPay.set_color(Color::Yellow);

                                                            helpVersion.set_callback(move |b| {
                                                                s.send("Version");
                                                            });

                                                            helpIHL.on_click(move |b| {
                                                                s.send("IHL");
                                                            });

                                                            helpTOS.on_click(move |b| {
                                                                s.send("TypeOfService");
                                                            });

                                                            helpECN.on_click(move |b| {
                                                                s.send("ECN");
                                                            });

                                                            helpLen.on_click(move |b| {
                                                                s.send("Length");
                                                            });

                                                            helpID.on_click(move |b| {
                                                                s.send("ID");
                                                            });

                                                            helpFlags.on_click(move |b| {
                                                                s.send("Flags");
                                                            });

                                                            helpOffSet.on_click(move |b| {
                                                                s.send("FragmentOffset");
                                                            });

                                                            helpTTL.on_click(move |b| {
                                                                s.send("TTL");
                                                            });

                                                            helpPro.on_click(move |b| {
                                                                s.send("Protocol");
                                                            });

                                                            helpChecksum.on_click(move |b| {
                                                                s.send("HeaderChecksum");
                                                            });

                                                            helpSource.on_click(move |b| {
                                                                s.send("Source");
                                                            });

                                                            helpDest.on_click(move |b| {
                                                                s.send("Destination");
                                                            });

                                                            helphexPay.on_click(move |b| {
                                                                s.send("HexadecimalPayload");
                                                            });

                                                            helpPay.on_click(move |b| {
                                                                s.send("Payload");
                                                            });

                                                            paylo.set_callback(move |b| {
                                                                s.send("PayloadICMP");
                                                            });
                                                            windIP.make_resizable(true);
                                                            windIP.end();
                                                            windIP.show();

                                                            //End of Interfaz ICMP
                                                        }
                                                        None => {
                                                            //No se puede traducir este payload a ICMP
                                                            let frase = "Este paquete no es ICMP\nPor favor, seleccione otro";
                                                            let arc = Arc::new(Mutex::new(frase));
                                                            let tituloVentana =
                                                                String::from("ERROR");
                                                            let p = self.clone();
                                                            let mut wi =
                                                                p.popup_Mensaje(tituloVentana, arc);
                                                            wi.show();
                                                        }
                                                    }
                                                    //End of ICMP
                                                }
                                                "TCP" => {
                                                    let pac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                                                    let otropac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Una copia para poder transmitir su pertenencia a ICMP
                                                    let payloadTCP = otropac.payload();
                                                    let tcpPaquete = TcpPacket::new(payloadTCP); //convertimos este paquete en ICMP
                                                    match tcpPaquete {
                                                        Some(pacTCP) => {
                                                            vectorTcp =
                                                                self.getTCPData(pac, pacTCP); //A partir de el, obtenemos los datos para la interfaz
                                                            let mut windTCP = Window::default()
                                                                .with_size(900, 550) //Ancho, Alto
                                                                .center_screen()
                                                                .with_label("Paquete de Capa 4");
                                                            //Definimos la interfaz
                                                            let titulo =
                                                                Frame::new(0, 0, 900, 50, "TCP");

                                                            //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
                                                            //Los hacemos mutable para cambiar sus valores
                                                            //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
                                                            //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

                                                            //TCP
                                                            let mut inputSourcePort = Input::new(
                                                                250,
                                                                50,
                                                                75,
                                                                25,
                                                                "Source Port",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputSourcePort
                                                                .set_value(vectorTcp[13].as_str());
                                                            helpSourcePort =
                                                                Button::new(335, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpSourcePort.set_color(Color::Yellow);

                                                            let mut inputDestPort = Input::new(
                                                                620,
                                                                50,
                                                                75,
                                                                25,
                                                                "Destination Port",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputDestPort
                                                                .set_value(vectorTcp[14].as_str());
                                                            helpDestPort =
                                                                Button::new(705, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpDestPort.set_color(Color::Yellow);

                                                            let mut inputTCPSequence = Input::new(
                                                                135, 95, 200, 25, "Sequence",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPSequence
                                                                .set_value(vectorTcp[15].as_str());
                                                            helpSequence =
                                                                Button::new(345, 95, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpSequence.set_color(Color::Yellow);

                                                            let mut inputTCPAck = Input::new(
                                                                600,
                                                                95,
                                                                200,
                                                                25,
                                                                "Acknowledgement",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPAck
                                                                .set_value(vectorTcp[16].as_str());
                                                            helpTCPAck =
                                                                Button::new(810, 95, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPAck.set_color(Color::Yellow);

                                                            let mut inputDataOffset = Input::new(
                                                                90,
                                                                140,
                                                                75,
                                                                25,
                                                                "DataOffset",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputDataOffset
                                                                .set_value(vectorTcp[17].as_str());
                                                            helpTCPOffset =
                                                                Button::new(170, 140, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPOffset.set_color(Color::Yellow);

                                                            let mut inputReserved = Input::new(
                                                                270, 140, 25, 25, "Reserved",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputReserved
                                                                .set_value(vectorTcp[18].as_str());
                                                            inputReserved.set_readonly(true);
                                                            helpTCPReserved =
                                                                Button::new(300, 140, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPReserved
                                                                .set_color(Color::Yellow);

                                                            let tituloFlags = Frame::new(330, 140, 50, 25, "Flags:"); //X,Y,ANCHO, ALTO, TITULO
                                                            let mut flagsbinarias = vectorTcp[19].as_str();
                                                            let mut valorURG = "0";
                                                            let mut valorACK = "0";
                                                            let mut valorPSH = "0";
                                                            let mut valorRST = "0";
                                                            let mut valorSYN = "0";
                                                            let mut valorFIN = "0";
                                                            let mut contadorFlag = 1;
                                                            let mut auxiliarValor = String::new();
                                                            let mut auxiliarValorA = String::new();
                                                            let mut auxiliarValorB = String::new();
                                                            let mut auxiliarValorC = String::new();
                                                            let mut auxiliarValorD = String::new();
                                                            let mut auxiliarValorE = String::new();
                                                            for value in flagsbinarias.chars().rev() { //Rev hace que vaya del final al principio
                                                                match contadorFlag{
                                                                    1=>{
                                                                        auxiliarValor = value.to_string();
                                                                        let mut auxVal = auxiliarValor.as_str();
                                                                        valorFIN = auxVal;
                                                                        contadorFlag +=1;
                                                                    },
                                                                    2=>{
                                                                        auxiliarValorA = value.to_string();
                                                                        let mut auxVal = auxiliarValorA.as_str();
                                                                        valorSYN = auxVal;
                                                                        contadorFlag +=1;
                                                                    },
                                                                    3=>{
                                                                        auxiliarValorB = value.to_string();
                                                                        let mut auxVal = auxiliarValorB.as_str();
                                                                        valorRST = auxVal;
                                                                        contadorFlag +=1; 
                                                                    },
                                                                    4=>{
                                                                        auxiliarValorC = value.to_string();
                                                                        let mut auxVal = auxiliarValorC.as_str();
                                                                        valorPSH = auxVal;
                                                                        contadorFlag +=1;
                                                                    },
                                                                    5=>{
                                                                        auxiliarValorD = value.to_string();
                                                                        let mut auxVal = auxiliarValorD.as_str();
                                                                        valorACK = auxVal;
                                                                        contadorFlag +=1;
                                                                    },
                                                                    6=>{
                                                                        auxiliarValorE = value.to_string();
                                                                        let mut auxVal = auxiliarValorE.as_str();
                                                                        valorURG = auxVal;
                                                                        contadorFlag +=1;
                                                                    },
                                                                    _=>{},
                                                                }
                                                            }

                                                            let mut urg = Input::new(380, 140, 40, 25, None); //1
                                                            urg.set_value(valorURG); //Posicion 1 de la lista
                                                            urg.set_readonly(true);
                                                            let tituloFlagsURG =
                                                                Frame::new(370, 165, 50, 25, "URG");
                                                            
                                                            let mut ack = Input::new(420, 140, 40, 25, None); //1
                                                            ack.set_value(valorACK); //Posicion 1 de la lista
                                                            ack.set_readonly(true);
                                                            let tituloFlagsACK =
                                                                Frame::new(410, 165, 50, 25, "ACK");
                                                            let mut psh = Input::new(460, 140, 40, 25, None); //1
                                                            psh.set_value(valorPSH); //Posicion 1 de la lista
                                                            psh.set_readonly(true);
                                                            let tituloFlagsPSH =
                                                                Frame::new(450, 165, 50, 25, "PSH");
                                                            let mut rst = Input::new(500, 140, 40, 25, None); //1
                                                            rst.set_value(valorRST); //Posicion 1 de la lista
                                                            rst.set_readonly(true);
                                                            let tituloFlagsRST =
                                                                Frame::new(490, 165, 50, 25, "RST");
                                                            let mut syn = Input::new(540, 140, 40, 25, None); //1
                                                            syn.set_value(valorSYN); //Posicion 1 de la lista
                                                            syn.set_readonly(true);
                                                            let tituloFlagsSYN =
                                                                Frame::new(530, 165, 50, 25, "SYN");
                                                            let mut fin = Input::new(580, 140, 40, 25, None); //0
                                                                fin.set_value(valorFIN); //Posicion 0
                                                                fin.set_readonly(true);
                                                            let tituloFlagsFIN =
                                                                Frame::new(570, 165, 50, 25, "FIN");
                                                            helpTCPFlags =
                                                                Button::new(630, 140, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPFlags.set_color(Color::Yellow);

                                                            let mut inputTCPWindow = Input::new(
                                                                730, 140, 100, 25, "Window",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPWindow
                                                                .set_value(vectorTcp[20].as_str());
                                                            helpTCPWindow =
                                                                Button::new(840, 140, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPWindow.set_color(Color::Yellow);

                                                            let mut inputTCPChecksum = Input::new(
                                                                220, 200, 100, 25, "Checksum",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPChecksum
                                                                .set_value(vectorTcp[21].as_str());
                                                            helpTCPChecksum =
                                                                Button::new(330, 200, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPChecksum
                                                                .set_color(Color::Yellow);

                                                            let mut inputTCPUrgentPointer =
                                                                Input::new(
                                                                    630,
                                                                    200,
                                                                    100,
                                                                    25,
                                                                    "Urgent Pointer",
                                                                ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPUrgentPointer
                                                                .set_value(vectorTcp[22].as_str());
                                                            helpTCPUrgPointer =
                                                                Button::new(740, 200, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPUrgPointer
                                                                .set_color(Color::Yellow);

                                                            let mut inputTCPOptions = Input::new(
                                                                100, 250, 200, 25, "Options",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPOptions
                                                                .set_value(vectorTcp[23].as_str());
                                                            helpTCPOpt =
                                                                Button::new(320, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPOpt.set_color(Color::Yellow);

                                                            inputTCPPayload = Input::new(
                                                                500, 250, 200, 25, "Payload",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTCPPayload
                                                                .set_value(vectorTcp[24].as_str());
                                                            inputTCPPayload.set_readonly(true);
                                                            helpTCPPayl =
                                                                Button::new(710, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTCPPayl.set_color(Color::Yellow);

                                                            let mut buttonPaylTCP: Listener<_> =
                                                                Button::new(
                                                                    760, 235, 80, 40, "Payload",
                                                                )
                                                                .into();
                                                            helpPayTCP =
                                                                Button::new(850, 245, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpPayTCP.set_color(Color::Yellow);

                                                            //IP
                                                            let subtitulo =
                                                                Frame::new(0, 300, 900, 50, "IP");
                                                            let mut inputVersion = Input::new(
                                                                65, 350, 75, 25, "Version",
                                                            );
                                                            inputVersion
                                                                .set_value(vectorTcp[0].as_str());
                                                            inputVersion.set_readonly(true);
                                                            helpVersion =
                                                                Button::new(150, 350, 20, 20, "?")
                                                                    .into();
                                                            helpVersion.set_color(Color::Yellow);

                                                            let mut inputIHL =
                                                                Input::new(215, 350, 75, 25, "IHL");
                                                            inputIHL
                                                                .set_value(vectorTcp[1].as_str());
                                                            inputIHL.set_readonly(true);
                                                            helpIHL =
                                                                Button::new(300, 350, 20, 20, "?")
                                                                    .into();
                                                            helpIHL.set_color(Color::Yellow);

                                                            let mut inputTypeOfServ = Input::new(
                                                                435,
                                                                350,
                                                                75,
                                                                25,
                                                                "TypeOfService",
                                                            );
                                                            inputTypeOfServ
                                                                .set_value(vectorTcp[2].as_str());
                                                            inputTypeOfServ.set_readonly(true);
                                                            helpTOS =
                                                                Button::new(520, 350, 20, 20, "?")
                                                                    .into();
                                                            helpTOS.set_color(Color::Yellow);

                                                            let mut inputECN =
                                                                Input::new(595, 350, 75, 25, "ECN");
                                                            inputECN
                                                                .set_value(vectorTcp[3].as_str());
                                                            inputECN.set_readonly(true);
                                                            helpECN =
                                                                Button::new(680, 350, 20, 20, "?")
                                                                    .into();
                                                            helpECN.set_color(Color::Yellow);

                                                            let mut inputLength = Input::new(
                                                                775, 350, 75, 25, "Length",
                                                            );
                                                            inputLength
                                                                .set_value(vectorTcp[4].as_str());
                                                            inputLength.set_readonly(true);
                                                            helpLen =
                                                                Button::new(865, 350, 20, 20, "?")
                                                                    .into();
                                                            helpLen.set_color(Color::Yellow);

                                                            let mut inputID =
                                                                Input::new(65, 400, 175, 25, "ID");
                                                            inputID
                                                                .set_value(vectorTcp[5].as_str());
                                                            inputID.set_readonly(true);
                                                            helpID =
                                                                Button::new(265, 400, 20, 20, "?")
                                                                    .into();
                                                            helpID.set_color(Color::Yellow);

                                                            let mut inputFlags = Input::new(
                                                                355, 400, 100, 25, "Flags",
                                                            );
                                                            inputFlags
                                                                .set_value(vectorTcp[6].as_str());
                                                            inputFlags.set_readonly(true);
                                                            helpFlags =
                                                                Button::new(520, 400, 20, 20, "?")
                                                                    .into();
                                                            helpFlags.set_color(Color::Yellow);

                                                            let mut inputFragmentOff = Input::new(
                                                                685,
                                                                400,
                                                                130,
                                                                25,
                                                                "FragmentOffset",
                                                            );
                                                            inputFragmentOff
                                                                .set_value(vectorTcp[7].as_str());
                                                            inputFragmentOff.set_readonly(true);
                                                            helpOffSet =
                                                                Button::new(830, 400, 20, 20, "?")
                                                                    .into();
                                                            helpOffSet.set_color(Color::Yellow);

                                                            let mut inputTTL =
                                                                Input::new(65, 450, 100, 25, "TTL");
                                                            inputTTL
                                                                .set_value(vectorTcp[8].as_str());
                                                            inputTTL.set_readonly(true);
                                                            helpTTL =
                                                                Button::new(175, 450, 20, 20, "?")
                                                                    .into();
                                                            helpTTL.set_color(Color::Yellow);

                                                            let mut inputProtocol = Input::new(
                                                                345, 450, 100, 25, "Protocol",
                                                            );
                                                            inputProtocol.set_value(
                                                                vectorTcp[9]
                                                                    .to_uppercase()
                                                                    .as_str(),
                                                            );
                                                            inputProtocol.set_readonly(true);
                                                            helpPro =
                                                                Button::new(460, 450, 20, 20, "?")
                                                                    .into();
                                                            helpPro.set_color(Color::Yellow);

                                                            let mut inputHeaderChecksum =
                                                                Input::new(
                                                                    630, 450, 100, 25, "Checksum",
                                                                );
                                                            inputHeaderChecksum
                                                                .set_value(vectorTcp[10].as_str());
                                                            inputHeaderChecksum.set_readonly(true);
                                                            helpChecksum =
                                                                Button::new(740, 450, 20, 20, "?")
                                                                    .into();
                                                            helpChecksum.set_color(Color::Yellow);

                                                            let mut inputSourceAdd = Input::new(
                                                                125,
                                                                505,
                                                                200,
                                                                25,
                                                                "Source Address",
                                                            );
                                                            inputSourceAdd
                                                                .set_value(vectorTcp[11].as_str());
                                                            inputSourceAdd.set_readonly(true);
                                                            helpSource =
                                                                Button::new(340, 505, 20, 20, "?")
                                                                    .into();
                                                            helpSource.set_color(Color::Yellow);

                                                            let mut inputDestAdd = Input::new(
                                                                550,
                                                                505,
                                                                200,
                                                                25,
                                                                "Destination Address",
                                                            );
                                                            inputDestAdd
                                                                .set_value(vectorTcp[12].as_str());
                                                            inputDestAdd.set_readonly(true);
                                                            helpDest =
                                                                Button::new(760, 505, 20, 20, "?")
                                                                    .into();
                                                            helpDest.set_color(Color::Yellow);

                                                            let (s, r) = app::channel::<&str>();

                                                            // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
                                                            // Cuando la app lo recoge , lo muestra
                                                            helpVersion.on_click(move |b| {
                                                                s.send("Version");
                                                            });

                                                            helpIHL.on_click(move |b| {
                                                                s.send("IHL");
                                                            });

                                                            helpTOS.on_click(move |b| {
                                                                s.send("TypeOfService");
                                                            });

                                                            helpECN.on_click(move |b| {
                                                                s.send("ECN");
                                                            });

                                                            helpLen.on_click(move |b| {
                                                                s.send("Length");
                                                            });

                                                            helpID.on_click(move |b| {
                                                                s.send("ID");
                                                            });

                                                            helpFlags.on_click(move |b| {
                                                                s.send("Flags");
                                                            });

                                                            helpOffSet.on_click(move |b| {
                                                                s.send("FragmentOffset");
                                                            });

                                                            helpTTL.on_click(move |b| {
                                                                s.send("TTL");
                                                            });

                                                            helpPro.on_click(move |b| {
                                                                s.send("Protocol");
                                                            });

                                                            helpChecksum.on_click(move |b| {
                                                                s.send("HeaderChecksum");
                                                            });

                                                            helpSource.on_click(move |b| {
                                                                s.send("Source");
                                                            });

                                                            helpDest.on_click(move |b| {
                                                                s.send("Destination");
                                                            });

                                                            helpSourcePort.on_click(move |b| {
                                                                s.send("PuertoOrigen");
                                                            });
                                                            helpDestPort.on_click(move |b| {
                                                                s.send("PuertoDestino");
                                                            });
                                                            helpSequence.on_click(move |b| {
                                                                s.send("Sequence");
                                                            });
                                                            helpTCPAck.on_click(move |b| {
                                                                s.send("ACK");
                                                            });
                                                            helpTCPOffset.on_click(move |b| {
                                                                s.send("DataOffset");
                                                            });
                                                            helpTCPReserved.on_click(move |b| {
                                                                s.send("Reserved");
                                                            });
                                                            helpTCPFlags.on_click(move |b| {
                                                                s.send("TCPFlags");
                                                            });
                                                            helpTCPWindow.on_click(move |b| {
                                                                s.send("TCPWindow");
                                                            });
                                                            helpTCPChecksum.on_click(move |b| {
                                                                s.send("TCPChecksum");
                                                            });

                                                            helpTCPUrgPointer.on_click(move |b| {
                                                                s.send("UrgPointer");
                                                            });
                                                            helpTCPOpt.on_click(move |b| {
                                                                s.send("TCPOptions");
                                                            });
                                                            helpTCPPayl.on_click(move |b| {
                                                                s.send("TCPPayload");
                                                            });
                                                            buttonPaylTCP.on_click(move |b| {
                                                                //Boton Payload
                                                                s.send("ButonPayloadTCP");
                                                            });

                                                            helpPayTCP.on_click(move |b| {
                                                                //ayuda boton payload
                                                                s.send("TCPBotonPayload");
                                                            });
                                                            windTCP.make_resizable(true);
                                                            windTCP.end();
                                                            windTCP.show();
                                                        }
                                                        None => {
                                                            //No se puede traducir este payload a ICMP
                                                            let frase = "Este paquete no es TCP\nPor favor, seleccione otro";
                                                            let arc = Arc::new(Mutex::new(frase));
                                                            let tituloVentana =
                                                                String::from("ERROR");
                                                            let p = self.clone();
                                                            let mut wi =
                                                                p.popup_Mensaje(tituloVentana, arc);
                                                            wi.show();
                                                        }
                                                    }
                                                }
                                                "UDP" => {
                                                    let pac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                                                    let otropac =
                                                        Ipv4Packet::new(&paqueteAObtener).unwrap(); //Una copia para poder transmitir su pertenencia a ICMP
                                                    let payloadUDP = otropac.payload();
                                                    let tcpPaquete = UdpPacket::new(payloadUDP); //convertimos este paquete en ICMP
                                                    match tcpPaquete {
                                                        Some(pacUDP) => {
                                                            vectorUdp =
                                                                self.getUDPData(pac, pacUDP); //A partir de el, obtenemos los datos para la interfaz

                                                            let mut windUDP = Window::default()
                                                                .with_size(900, 450) //Ancho, Alto
                                                                .center_screen()
                                                                .with_label("Paquete de Capa 4");
                                                            //Definimos la interfaz
                                                            let titulo =
                                                                Frame::new(0, 0, 900, 50, "UDP");

                                                            //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
                                                            //Los hacemos mutable para cambiar sus valores
                                                            //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
                                                            //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

                                                            //UDP
                                                            inputUDPSourcePort = Input::new(
                                                                250,
                                                                50,
                                                                75,
                                                                25,
                                                                "Source Port",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputUDPSourcePort
                                                                .set_value(vectorUdp[13].as_str());
                                                            helpUDPSourcePort =
                                                                Button::new(335, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpUDPSourcePort
                                                                .set_color(Color::Yellow);

                                                            inputUDPDestPort = Input::new(
                                                                620,
                                                                50,
                                                                75,
                                                                25,
                                                                "Destination Port",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputUDPDestPort
                                                                .set_value(vectorUdp[14].as_str());
                                                            helpUDPDestPort =
                                                                Button::new(705, 50, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpUDPDestPort
                                                                .set_color(Color::Yellow);

                                                            inputUDPLength = Input::new(
                                                                250, 95, 100, 25, "Length",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputUDPLength
                                                                .set_value(vectorUdp[15].as_str());
                                                            helpUDPLen =
                                                                Button::new(360, 95, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpUDPLen.set_color(Color::Yellow);

                                                            inputUDPChecksum = Input::new(
                                                                620, 95, 100, 25, "Checksum",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputUDPChecksum
                                                                .set_value(vectorUdp[16].as_str());
                                                            helpUDPChecksum =
                                                                Button::new(740, 95, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpUDPChecksum
                                                                .set_color(Color::Yellow);

                                                            inputUDPPayload = Input::new(
                                                                350, 150, 200, 25, "Payload",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputUDPPayload
                                                                .set_value(vectorUdp[17].as_str());
                                                            inputUDPPayload.set_readonly(true);
                                                            helpUDPPayl =
                                                                Button::new(560, 150, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpUDPPayl.set_color(Color::Yellow);

                                                            buttonPaylUDP = Button::new(
                                                                610, 145, 80, 40, "Payload",
                                                            )
                                                            .into();
                                                            helpPayUDP =
                                                                Button::new(700, 145, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpPayUDP.set_color(Color::Yellow);
                                                            //set_draw_color(Color::White);
                                                            //draw_line(0, 250, 900, 250);
                                                            //IP
                                                            let subtitulo =
                                                                Frame::new(0, 200, 900, 50, "IP");
                                                            let mut inputVersion = Input::new(
                                                                65, 250, 75, 25, "Version",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputVersion
                                                                .set_value(vectorUdp[0].as_str());
                                                            helpVersion =
                                                                Button::new(150, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpVersion.set_color(Color::Yellow);

                                                            let mut inputIHL =
                                                                Input::new(215, 250, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputIHL
                                                                .set_value(vectorUdp[1].as_str());
                                                            helpIHL =
                                                                Button::new(300, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpIHL.set_color(Color::Yellow);

                                                            let mut inputTypeOfServ = Input::new(
                                                                435,
                                                                250,
                                                                75,
                                                                25,
                                                                "TypeOfService",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTypeOfServ
                                                                .set_value(vectorUdp[2].as_str());
                                                            helpTOS =
                                                                Button::new(520, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTOS.set_color(Color::Yellow);

                                                            let mut inputECN =
                                                                Input::new(595, 250, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputECN
                                                                .set_value(vectorUdp[3].as_str());
                                                            helpECN =
                                                                Button::new(680, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpECN.set_color(Color::Yellow);

                                                            let mut inputLength = Input::new(
                                                                775, 250, 75, 25, "Length",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputLength
                                                                .set_value(vectorUdp[4].as_str());
                                                            helpLen =
                                                                Button::new(865, 250, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpLen.set_color(Color::Yellow);

                                                            let mut inputID =
                                                                Input::new(65, 300, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputID
                                                                .set_value(vectorUdp[5].as_str());
                                                            helpID =
                                                                Button::new(265, 300, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpID.set_color(Color::Yellow);

                                                            let mut inputFlags = Input::new(
                                                                355, 300, 100, 25, "Flags",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputFlags
                                                                .set_value(vectorUdp[6].as_str());
                                                            inputFlags.set_readonly(true);
                                                            helpFlags =
                                                                Button::new(520, 300, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpFlags.set_color(Color::Yellow);

                                                            let mut inputFragmentOff = Input::new(
                                                                685,
                                                                300,
                                                                130,
                                                                25,
                                                                "FragmentOffset",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputFragmentOff
                                                                .set_value(vectorUdp[7].as_str());
                                                            helpOffSet =
                                                                Button::new(830, 300, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpOffSet.set_color(Color::Yellow);

                                                            let mut inputTTL =
                                                                Input::new(65, 350, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
                                                            inputTTL
                                                                .set_value(vectorUdp[8].as_str());
                                                            helpTTL =
                                                                Button::new(175, 350, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpTTL.set_color(Color::Yellow);

                                                            let mut inputProtocol = Input::new(
                                                                345, 350, 100, 25, "Protocol",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputProtocol.set_value(
                                                                vectorUdp[9]
                                                                    .to_uppercase()
                                                                    .as_str(),
                                                            );
                                                            helpPro =
                                                                Button::new(460, 350, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpPro.set_color(Color::Yellow);

                                                            let mut inputHeaderChecksum =
                                                                Input::new(
                                                                    630, 350, 100, 25, "Checksum",
                                                                ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputHeaderChecksum
                                                                .set_value(vectorUdp[10].as_str());
                                                            helpChecksum =
                                                                Button::new(740, 350, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpChecksum.set_color(Color::Yellow);

                                                            let mut inputSourceAdd = Input::new(
                                                                125,
                                                                405,
                                                                200,
                                                                25,
                                                                "Source Address",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputSourceAdd
                                                                .set_value(vectorUdp[11].as_str());
                                                            helpSource =
                                                                Button::new(340, 405, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpSource.set_color(Color::Yellow);

                                                            let mut inputDestAdd = Input::new(
                                                                550,
                                                                405,
                                                                200,
                                                                25,
                                                                "Destination Address",
                                                            ); //X,Y,ANCHO, ALTO, TITULO
                                                            inputDestAdd
                                                                .set_value(vectorUdp[12].as_str());
                                                            helpDest =
                                                                Button::new(760, 405, 20, 20, "?")
                                                                    .into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                                            helpDest.set_color(Color::Yellow);

                                                            let (s, r) = app::channel::<&str>();

                                                            // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
                                                            // Cuando la app lo recoge , lo muestra
                                                            helpVersion.on_click(move |b| {
                                                                s.send("Version");
                                                            });

                                                            helpIHL.on_click(move |b| {
                                                                s.send("IHL");
                                                            });

                                                            helpTOS.on_click(move |b| {
                                                                s.send("TypeOfService");
                                                            });

                                                            helpECN.on_click(move |b| {
                                                                s.send("ECN");
                                                            });

                                                            helpLen.on_click(move |b| {
                                                                s.send("Length");
                                                            });

                                                            helpID.on_click(move |b| {
                                                                s.send("ID");
                                                            });

                                                            helpFlags.on_click(move |b| {
                                                                s.send("Flags");
                                                            });

                                                            helpOffSet.on_click(move |b| {
                                                                s.send("FragmentOffset");
                                                            });

                                                            helpTTL.on_click(move |b| {
                                                                s.send("TTL");
                                                            });

                                                            helpPro.on_click(move |b| {
                                                                s.send("Protocol");
                                                            });

                                                            helpChecksum.on_click(move |b| {
                                                                s.send("HeaderChecksum");
                                                            });

                                                            helpSource.on_click(move |b| {
                                                                s.send("Source");
                                                            });

                                                            helpDest.on_click(move |b| {
                                                                s.send("Destination");
                                                            });

                                                            helpUDPSourcePort.on_click(move |b| {
                                                                s.send("PuertoOrigenUDP");
                                                            });
                                                            helpUDPDestPort.on_click(move |b| {
                                                                s.send("PuertoDestinoUDP");
                                                            });
                                                            helpUDPLen.on_click(move |b| {
                                                                s.send("UDPLength");
                                                            });

                                                            helpUDPChecksum.on_click(move |b| {
                                                                s.send("UDPChecksum");
                                                            });

                                                            helpUDPPayl.on_click(move |b| {
                                                                s.send("UDPPayload");
                                                            });
                                                            buttonPaylUDP.on_click(move |b| {
                                                                //Boton Payload
                                                                s.send("ButonPayloadUDP");
                                                            });

                                                            helpPayUDP.on_click(move |b| {
                                                                //ayuda boton payload
                                                                s.send("UDPBotonPayload");
                                                            });
                                                            windUDP.make_resizable(true);
                                                            windUDP.end();
                                                            windUDP.show();
                                                        }
                                                        None => {
                                                            //No se puede traducir este payload a UDP
                                                            let frase = "Este paquete no es UDP\nPor favor, seleccione otro";
                                                            let arc = Arc::new(Mutex::new(frase));
                                                            let tituloVentana =
                                                                String::from("ERROR");
                                                            let p = self.clone();
                                                            let mut wi =
                                                                p.popup_Mensaje(tituloVentana, arc);
                                                            wi.show();
                                                        }
                                                    }
                                                }
                                                "HTTP" => {}
                                                "FTP" => {}
                                                _ => (),
                                            }

                                            //end if
                                        } else {
                                            //POPUP: "Busque de nuevo, en la busqueda anterior no se encontraron paquetes"

                                            let frase =
                                                "Por favor, seleccione un paquete primero\n";
                                            let arc = Arc::new(Mutex::new(frase));
                                            let tituloVentana = String::from("ERROR");
                                            let p = self.clone();
                                            let mut wi = p.popup_Mensaje(tituloVentana, arc);
                                            wi.show();
                                        }
                                        //end Some Correcto
                                    }

                                    _ => (),
                                }
                            } else {
                                //No hay nada

                                //POPUP : Primero busque antes de seleccionar
                                let frase = "No hay paquetes disponibles.\nPor favor, busque antes de seleccionar";
                                let arc = Arc::new(Mutex::new(frase));
                                let tituloVentana = String::from("ERROR");
                                let p = self.clone();
                                let mut wi = p.popup_Mensaje(tituloVentana, arc);
                                wi.show();
                            }

                            windInt.redraw();
                            //End of Seleccion
                        }
                        //Llamamos para solicitar los datos de la interfazIP
                        "Version" => {
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                        }
                        "IHL" => {
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                        }
                        "TypeOfService" => {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                        }
                        "ECN" => {
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                        }
                        "Length" => {
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                        }
                        "ID" => {
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                        }
                        "Flags" => {
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                        }
                        "FragmentOffset" => {
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                        }
                        "TTL" => {
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                        }
                        "Protocol" => {
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                        }
                        "HeaderChecksum" => {
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                        }
                        "Source" => {
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                        }
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                        }
                        "HexadecimalPayload" => {
                            helphexPay.set_tooltip(strHelp);
                            helphexPay.tooltip();
                        }
                        "Payload" => {
                            helpPay.set_tooltip(strHelp);
                            helpPay.tooltip();
                        }
                        //Metodos de ayuda de ICMP
                        "ICMPType" => {
                            helpType.set_tooltip(strHelpICMP);
                            helpType.tooltip();
                        }
                        "ICMPCode" => {
                            helpCode.set_tooltip(strHelpICMP);
                            helpCode.tooltip();
                        }
                        "ICMPChecksum" => {
                            helpICMPChecksum.set_tooltip(strHelpICMP);
                            helpICMPChecksum.tooltip();
                        }
                        "ICMPPayload" => {
                            helpICMPPayl.set_tooltip(strHelpICMP);
                            helpICMPPayl.tooltip();
                        }
                        "ICMPBotonPayload" => {
                            helpICMPBotonPayl.set_tooltip(strHelpICMP);
                            helpICMPBotonPayl.tooltip();
                        }
                        //Metodos de ayuda de TCP
                        "PuertoOrigen" => {
                            helpSourcePort.set_tooltip(strHelpTCP);
                            helpSourcePort.tooltip();
                        }
                        "PuertoDestino" => {
                            helpDestPort.set_tooltip(strHelpTCP);
                            helpDestPort.tooltip();
                        }
                        "Sequence" => {
                            helpSequence.set_tooltip(strHelpTCP);
                            helpSequence.tooltip();
                        }
                        "ACK" => {
                            helpTCPAck.set_tooltip(strHelpTCP);
                            helpTCPAck.tooltip();
                        }
                        "DataOffset" => {
                            helpTCPOffset.set_tooltip(strHelpTCP);
                            helpTCPOffset.tooltip();
                        }
                        "Reserved" => {
                            helpTCPReserved.set_tooltip(strHelpTCP);
                            helpTCPReserved.tooltip();
                        }
                        "TCPFlags" => {
                            helpTCPFlags.set_tooltip(strHelpTCP);
                            helpTCPFlags.tooltip();
                        }
                        "TCPWindow" => {
                            helpTCPWindow.set_tooltip(strHelpTCP);
                            helpTCPWindow.tooltip();
                        }
                        "TCPChecksum" => {
                            helpTCPChecksum.set_tooltip(strHelpTCP);
                            helpTCPChecksum.tooltip();
                        }
                        "UrgPointer" => {
                            helpTCPUrgPointer.set_tooltip(strHelpTCP);
                            helpTCPUrgPointer.tooltip();
                        }
                        "TCPOptions" => {
                            helpTCPOpt.set_tooltip(strHelpTCP);
                            helpTCPOpt.tooltip();
                        }
                        "TCPPayload" => {
                            helpTCPPayl.set_tooltip(strHelpTCP);
                            helpTCPPayl.tooltip();
                        }
                        "TCPBotonPayload" => {
                            helpPayTCP.set_tooltip(strHelpTCP);
                            helpPayTCP.tooltip();
                        }
                        "PuertoOrigenUDP" => {
                            helpUDPSourcePort.set_tooltip(strHelpUDP);
                            helpUDPSourcePort.tooltip();
                        }
                        "PuertoDestinoUDP" => {
                            helpUDPDestPort.set_tooltip(strHelpUDP);
                            helpUDPDestPort.tooltip();
                        }
                        "UDPLength" => {
                            helpUDPLen.set_tooltip(strHelpUDP);
                            helpUDPLen.tooltip();
                        }
                        "UDPChecksum" => {
                            helpUDPChecksum.set_tooltip(strHelpUDP);
                            helpUDPChecksum.tooltip();
                        }
                        "UDPPayload" => {
                            helpUDPPayl.set_tooltip(strHelpUDP);
                            helpUDPPayl.tooltip();
                        }
                        "UDPBotonPayload" => {
                            helpPayUDP.set_tooltip(strHelpUDP);
                            helpPayUDP.tooltip();
                        }
                        "PayloadICMP" => {
                            let mut windo = Window::default()
                                .with_size(550, 180)
                                .center_screen()
                                .with_label("Payload de IP: ICMP");

                            inputICMPType = Input::new(50, 25, 50, 25, "Type"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPType.set_value(vectorIcmp[14].as_str());
                            inputICMPType.set_readonly(true);
                            helpType = Button::new(105, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpType.set_color(Color::Yellow);
                            inputICMPCode = Input::new(180, 25, 50, 25, "Code"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPCode.set_value(vectorIcmp[15].as_str());
                            inputICMPCode.set_readonly(true);
                            helpCode = Button::new(235, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpCode.set_color(Color::Yellow);

                            inputICMPChecksum = Input::new(350, 25, 75, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPChecksum.set_value(vectorIcmp[16].as_str());
                            inputICMPChecksum.set_readonly(true);
                            helpICMPChecksum = Button::new(430, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPChecksum.set_color(Color::Yellow);

                            inputICMPPayload = Input::new(100, 85, 350, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPPayload.set_value(vectorIcmp[17].as_str());
                            inputICMPPayload.set_readonly(true);
                            helpICMPPayl = Button::new(460, 85, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPPayl.set_color(Color::Yellow);
                            let mut botonPayloadICMP: Listener<_> =
                                Button::new(20, 125, 75, 30, "Payload").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPBotonPayl = Button::new(460, 85, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPBotonPayl.set_color(Color::Yellow);
                            windo.make_resizable(true);
                            windo.end();
                            windo.show();

                            helpType.on_click(move |b| {
                                s.send("ICMPType");
                            });
                            helpCode.on_click(move |b| {
                                s.send("ICMPCode");
                            });
                            helpICMPChecksum.on_click(move |b| {
                                s.send("ICMPChecksum");
                            });
                            helpICMPPayl.on_click(move |b| {
                                s.send("ICMPPayload");
                            });
                            helpICMPBotonPayl.on_click(move |b| {
                                s.send("ICMPBotonPayload");
                            });
                            botonPayloadICMP.on_click(move |b| {
                                s.send("ButonPayloadICMP");
                            });
                        }
                        "ButonPayload" => {
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }
                        "ButonPayloadICMP" => {
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputICMPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }
                        "ButonPayloadTCP" => {
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputTCPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }

                        "ButonPayloadUDP" => {
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputUDPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }
                            let p = self.clone();
                            let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                            windPayl.show();
                        }
                        _ => {
                            print!("Error");
                        } //End of Match recuperado
                    }
                    windInt.redraw();
                    appInt.redraw();
                    //end of Ok
                }
                None => {}
                _ => (),
                //end of  r.recv()
            }

            if firstime == 1 {
                firstime = 0;
                thread::sleep_ms(1000);
                s.send("Busqueda");
            }

            //end of while appwait
        }

        //appInt.run().unwrap();
    }

    /**
     * @in: Paquete IPv4
     * @out: Vector de String: Vector que contiene en cada casilla un dato del paquete IP para mostrarlo en la interfaz,
     *          de la forma [4,5,..] SIendo Version 4, IHL=5, etc
     * @function: Esta funcion obtendrá los datos del paquete IPv4  y los abstraerá a un vector de string para poder mostrar dichos datos por la interfaz
     *              De la misma forma, generará un paquete IP en caso de que necesitemos usar sus funciones (como fieldHelp)
     */
    pub fn getIPData(&mut self, paquete: Ipv4Packet) -> Vec<String> {
        let mut vectorResultado = Vec::new();

        let version = paquete.get_version().to_string();
        let IHL = paquete.get_header_length().to_string();
        let dcsp = paquete.get_dscp().to_string();
        let ecn = paquete.get_ecn().to_string();
        let length = paquete.get_total_length().to_string();
        let id = paquete.get_identification().to_string();
        let flags = paquete.get_flags();
        let offset = paquete.get_fragment_offset().to_string();
        let tetele = paquete.get_ttl().to_string();
        let protocol = paquete.get_next_level_protocol().to_string();
        let checksum = paquete.get_checksum().to_string();
        let source = paquete.get_source().to_string();
        let dest = paquete.get_destination().to_string();
        let payload = paquete.payload();

        let mut stringPayl = String::new();
        let flagsBinarioIP = format!("{:b}", flags);

        self.paqueteIP = IP::ip_From_Data(
            paquete.get_version(),
            paquete.get_header_length(),
            paquete.get_dscp(),
            paquete.get_ecn(),
            paquete.get_total_length(),
            paquete.get_identification(),
            paquete.get_flags(),
            paquete.get_fragment_offset(),
            paquete.get_ttl(),
            paquete.get_next_level_protocol(),
            paquete.get_checksum(),
            paquete.get_source(),
            paquete.get_destination(),
            paquete.payload().to_vec(),
        );

        let stringNumerosHexadecimales = hex::encode_upper(payload); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        let mut auxilStr = String::from("");
        let mut contadorAuxiliar: u16 = 0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();

        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() {
            contadorAuxiliar = contadorAuxiliar + 1;
            auxilStr.push(c);

            if contadorAuxiliar >= 2 && contadorAuxiliar % 2 == 0 {
                //Corta de 2 en 2
                stringPayl.push_str("0x");
                stringPayl.push_str(auxilStr.as_str());
                stringPayl.push_str(";"); //0x95;
                auxilStr = String::from(""); //reseteamos auxilStr
            }
        }
       
        vectorResultado.push(version);
        vectorResultado.push(IHL);
        vectorResultado.push(dcsp);
        vectorResultado.push(ecn);
        vectorResultado.push(length);
        vectorResultado.push(id);
        vectorResultado.push(flagsBinarioIP);
        vectorResultado.push(offset);
        vectorResultado.push(tetele);
        vectorResultado.push(protocol);
        vectorResultado.push(checksum);
        vectorResultado.push(source);
        vectorResultado.push(dest);
        vectorResultado.push(stringPayl);

        return vectorResultado;
    }

    /**
     * @in: Paquete Ipv4
     * @out: Vector de String: Vector que contiene en cada casilla un dato del paquete IP para mostrarlo en la interfaz,
     *          de la forma [4,5,..] SIendo Version 4, IHL=5, etc
     * @function: Esta funcion obtendrá los datos del paquete IPv4  y los abstraerá a un vector de string para poder mostrar dichos datos por la interfaz ICMP
     *              De la misma forma, generará un paquete IP en caso de que necesitemos usar sus funciones (como fieldHelp)
     */
    pub fn getICMPData(&mut self, paquete: Ipv4Packet, icmpPaquete: IcmpPacket) -> Vec<String> {
        let mut vectorResultado = Vec::new();

        let version = paquete.get_version().to_string();
        let IHL = paquete.get_header_length().to_string();
        let dcsp = paquete.get_dscp().to_string();
        let ecn = paquete.get_ecn().to_string();
        let length = paquete.get_total_length().to_string();
        let id = paquete.get_identification().to_string();
        let flags = paquete.get_flags();
        let offset = paquete.get_fragment_offset().to_string();
        let tetele = paquete.get_ttl().to_string();
        let protocol = paquete.get_next_level_protocol().to_string();
        let checksum = paquete.get_checksum().to_string();
        let source = paquete.get_source().to_string();
        let dest = paquete.get_destination().to_string();
        let payload = paquete.payload();

        self.paqueteIP = IP::ip_From_Data(
            paquete.get_version(),
            paquete.get_header_length(),
            paquete.get_dscp(),
            paquete.get_ecn(),
            paquete.get_total_length(),
            paquete.get_identification(),
            paquete.get_flags(),
            paquete.get_fragment_offset(),
            paquete.get_ttl(),
            paquete.get_next_level_protocol(),
            paquete.get_checksum(),
            paquete.get_source(),
            paquete.get_destination(),
            paquete.payload().to_vec(),
        );

        let tipoICMP = icmpPaquete.get_icmp_type();
        let codeICMP = icmpPaquete.get_icmp_code();
        let payloICMP = icmpPaquete.payload();
        let auxIP = self.paqueteIP.clone();

        self.paqueteICMP = ICMP::icmp_From_Data(
            icmpPaquete.get_icmp_type(),
            icmpPaquete.get_icmp_code(),
            icmpPaquete.get_checksum(),
            icmpPaquete.payload().to_vec(),
            auxIP,
        );

        let flagsBinarioIP = format!("{:b}", flags);
        let tipoICMPStr = self.paqueteICMP.transformaTipo(tipoICMP).to_string();
        let codeICMPStr = self.paqueteICMP.transformaCode(codeICMP).to_string();
        let checksuICMP = icmpPaquete.get_checksum().to_string();

        //Payload IP a Texto
        let mut stringPayl = String::new();
        let stringNumerosHexadecimales = hex::encode_upper(payload); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        let mut auxilStr = String::from("");
        let mut contadorAuxiliar: u16 = 0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();

        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() {
            contadorAuxiliar = contadorAuxiliar + 1;
            auxilStr.push(c);

            if contadorAuxiliar >= 2 && contadorAuxiliar % 2 == 0 {
                //Corta de 2 en 2
                stringPayl.push_str("0x");
                stringPayl.push_str(auxilStr.as_str());
                stringPayl.push_str(";"); //0x95;
                auxilStr = String::from(""); //reseteamos auxilStr
            }
        }

        //Payload ICMP a Texto
        let mut stringPaylICMP = String::new();
        let stringNumerosHexadecimales = hex::encode_upper(payloICMP); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        auxilStr = String::from("");
        contadorAuxiliar = 0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();

        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() {
            contadorAuxiliar = contadorAuxiliar + 1;
            auxilStr.push(c);

            if contadorAuxiliar >= 2 && contadorAuxiliar % 2 == 0 {
                //Corta de 2 en 2
                stringPaylICMP.push_str("0x");
                stringPaylICMP.push_str(auxilStr.as_str());
                stringPaylICMP.push_str(";"); //0x95;
                auxilStr = String::from(""); //reseteamos auxilStr
            }
        }
        vectorResultado.push(version); //IP
        vectorResultado.push(IHL);
        vectorResultado.push(dcsp);
        vectorResultado.push(ecn);
        vectorResultado.push(length);
        vectorResultado.push(id);
        vectorResultado.push(flagsBinarioIP);
        vectorResultado.push(offset);
        vectorResultado.push(tetele);
        vectorResultado.push(protocol);
        vectorResultado.push(checksum);
        vectorResultado.push(source);
        vectorResultado.push(dest);
        vectorResultado.push(stringPayl);

        vectorResultado.push(tipoICMPStr); //ICMP
        vectorResultado.push(codeICMPStr);
        vectorResultado.push(checksuICMP);
        vectorResultado.push(stringPaylICMP);

        return vectorResultado;
    }

    /**
     * @in: Paquete Ipv4
     * @out: Vector de String: Vector que contiene en cada casilla un dato del paquete IP para mostrarlo en la interfaz,
     *          de la forma [4,5,..] SIendo Version 4, IHL=5, etc
     * @function: Esta funcion obtendrá los datos del paquete IPv4  y los abstraerá a un vector de string para poder mostrar dichos datos por la interfaz TCP
     *              De la misma forma, generará un paquete IP en caso de que necesitemos usar sus funciones (como fieldHelp)
     */
    pub fn getTCPData(&mut self, paquete: Ipv4Packet, tcpPaquete: TcpPacket) -> Vec<String> {
        let mut vectorResultado = Vec::new();

        let version = paquete.get_version().to_string();
        let IHL = paquete.get_header_length().to_string();
        let dcsp = paquete.get_dscp().to_string();
        let ecn = paquete.get_ecn().to_string();
        let length = paquete.get_total_length().to_string();
        let id = paquete.get_identification().to_string();
        let flags = paquete.get_flags();
        let offset = paquete.get_fragment_offset().to_string();
        let tetele = paquete.get_ttl().to_string();
        let protocol = paquete.get_next_level_protocol().to_string();
        let checksum = paquete.get_checksum().to_string();
        let source = paquete.get_source().to_string();
        let dest = paquete.get_destination().to_string();
        let payload = paquete.payload();

        self.paqueteIP = IP::ip_From_Data(
            paquete.get_version(),
            paquete.get_header_length(),
            paquete.get_dscp(),
            paquete.get_ecn(),
            paquete.get_total_length(),
            paquete.get_identification(),
            paquete.get_flags(),
            paquete.get_fragment_offset(),
            paquete.get_ttl(),
            paquete.get_next_level_protocol(),
            paquete.get_checksum(),
            paquete.get_source(),
            paquete.get_destination(),
            paquete.payload().to_vec(),
        );

        let sourcePort = tcpPaquete.get_source().to_string();
        let destinationPort = tcpPaquete.get_destination().to_string();
        let sequenceNum = tcpPaquete.get_sequence().to_string();
        let acknowledgementNum = tcpPaquete.get_acknowledgement().to_string();
        let dataOffset = tcpPaquete.get_data_offset().to_string();
        let reserved = tcpPaquete.get_reserved().to_string();
        let mut flagsTCP = tcpPaquete.get_flags();
        let tamWindow = tcpPaquete.get_window().to_string();
        let checksumTCP = tcpPaquete.get_checksum().to_string();
        let urgentPointer = tcpPaquete.get_urgent_ptr().to_string();
        let optionsTCP = tcpPaquete.get_options();
        let payloadTCP = tcpPaquete.payload();
        let auxIP = self.paqueteIP.clone();

        self.paqueteTCP = TCP::tcp_From_Data(
            tcpPaquete.get_source(),
            tcpPaquete.get_destination(),
            tcpPaquete.get_sequence(),
            tcpPaquete.get_acknowledgement(),
            tcpPaquete.get_data_offset(),
            tcpPaquete.get_reserved(),
            tcpPaquete.get_flags(),
            tcpPaquete.get_window(),
            tcpPaquete.get_checksum(),
            tcpPaquete.get_urgent_ptr(),
            tcpPaquete.get_options(),
            tcpPaquete.payload().to_vec(),
            auxIP,
        );

        //Payload TCP a Texto
        let mut stringPaylTCP = String::new();
        let stringNumerosHexadecimales = hex::encode_upper(payloadTCP); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        let mut auxilStr = String::from("");
        let mut contadorAuxiliar = 0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();

        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() {
            contadorAuxiliar = contadorAuxiliar + 1;
            auxilStr.push(c);

            if contadorAuxiliar >= 2 && contadorAuxiliar % 2 == 0 {
                //Corta de 2 en 2
                stringPaylTCP.push_str("0x");
                stringPaylTCP.push_str(auxilStr.as_str());
                stringPaylTCP.push_str(";"); //0x95;
                auxilStr = String::from(""); //reseteamos auxilStr
            }
        }
        let optionsTexto = self.paqueteTCP.from_TCPOption(optionsTCP).clone();
        let flagsBinarioIP = format!("{:b}", flags);

        vectorResultado.push(version); //IP
        vectorResultado.push(IHL);
        vectorResultado.push(dcsp);
        vectorResultado.push(ecn);
        vectorResultado.push(length);
        vectorResultado.push(id);
        vectorResultado.push(flagsBinarioIP);
        vectorResultado.push(offset);
        vectorResultado.push(tetele);
        vectorResultado.push(protocol);
        vectorResultado.push(checksum);
        vectorResultado.push(source);
        vectorResultado.push(dest);

        //IP no muestra payload en TCP , el payload lo tiene TCP
        let flagsBinario = format!("{:b}", flagsTCP);

        vectorResultado.push(sourcePort); //TCP
        vectorResultado.push(destinationPort);
        vectorResultado.push(sequenceNum);
        vectorResultado.push(acknowledgementNum);
        vectorResultado.push(dataOffset);
        vectorResultado.push(reserved);
        vectorResultado.push(flagsBinario);
        vectorResultado.push(tamWindow);
        vectorResultado.push(checksumTCP);
        vectorResultado.push(urgentPointer);
        vectorResultado.push(optionsTexto);
        vectorResultado.push(stringPaylTCP);

        return vectorResultado;
    }

    /**
     * @in: Paquete Ipv4
     * @out: Vector de String: Vector que contiene en cada casilla un dato del paquete IP para mostrarlo en la interfaz,
     *          de la forma [4,5,..] SIendo Version 4, IHL=5, etc
     * @function: Esta funcion obtendrá los datos del paquete IPv4  y los abstraerá a un vector de string para poder mostrar dichos datos por la interfaz UDP
     *              De la misma forma, generará un paquete IP en caso de que necesitemos usar sus funciones (como fieldHelp)
     */
    pub fn getUDPData(&mut self, paquete: Ipv4Packet, udpPaquete: UdpPacket) -> Vec<String> {
        let mut vectorResultado = Vec::new();

        let version = paquete.get_version().to_string();
        let IHL = paquete.get_header_length().to_string();
        let dcsp = paquete.get_dscp().to_string();
        let ecn = paquete.get_ecn().to_string();
        let length = paquete.get_total_length().to_string();
        let id = paquete.get_identification().to_string();
        let flags = paquete.get_flags();
        let offset = paquete.get_fragment_offset().to_string();
        let tetele = paquete.get_ttl().to_string();
        let protocol = paquete.get_next_level_protocol().to_string();
        let checksum = paquete.get_checksum().to_string();
        let source = paquete.get_source().to_string();
        let dest = paquete.get_destination().to_string();
        let payload = paquete.payload();

        self.paqueteIP = IP::ip_From_Data(
            paquete.get_version(),
            paquete.get_header_length(),
            paquete.get_dscp(),
            paquete.get_ecn(),
            paquete.get_total_length(),
            paquete.get_identification(),
            paquete.get_flags(),
            paquete.get_fragment_offset(),
            paquete.get_ttl(),
            paquete.get_next_level_protocol(),
            paquete.get_checksum(),
            paquete.get_source(),
            paquete.get_destination(),
            paquete.payload().to_vec(),
        );

        let sourcePortUDP = udpPaquete.get_source().to_string();
        let destinationPortUDP = udpPaquete.get_destination().to_string();
        let udpLength = udpPaquete.get_length().to_string();
        let udpChecksum = udpPaquete.get_checksum().to_string();
        let payloadUDP = udpPaquete.payload();

        let auxIP = self.paqueteIP.clone();

        self.paqueteUDP = UDP::udp_From_Data(
            udpPaquete.get_source(),
            udpPaquete.get_destination(),
            udpPaquete.get_length(),
            udpPaquete.get_checksum(),
            udpPaquete.payload().to_vec(),
            auxIP,
        );

        //Payload TCP a Texto
        let mut stringPaylUDP = String::new();
        let stringNumerosHexadecimales = hex::encode_upper(payloadUDP); //Esto hará que pase de ser 195 => 93 (por ej), su representacion de Dec a Hex
        let mut auxilStr = String::from("");
        let mut contadorAuxiliar = 0;
        let strNumHexadecimales = stringNumerosHexadecimales.as_str();
        
        //Una vez convertimos los numeros decimales a hexadecimales, los metemos en el string para poder leerlos correctamente en la interfaz
        for (i, c) in strNumHexadecimales.chars().enumerate() {
            contadorAuxiliar = contadorAuxiliar + 1;
            auxilStr.push(c);

            if contadorAuxiliar >= 2 && contadorAuxiliar % 2 == 0 {
                //Corta de 2 en 2
                stringPaylUDP.push_str("0x");
                stringPaylUDP.push_str(auxilStr.as_str());
                stringPaylUDP.push_str(";"); //0x95;
                auxilStr = String::from(""); //reseteamos auxilStr
            }
        }

        let flagsBinarioIP = format!("{:b}", flags);

        vectorResultado.push(version); //IP
        vectorResultado.push(IHL);
        vectorResultado.push(dcsp);
        vectorResultado.push(ecn);
        vectorResultado.push(length);
        vectorResultado.push(id);
        vectorResultado.push(flagsBinarioIP);
        vectorResultado.push(offset);
        vectorResultado.push(tetele);
        vectorResultado.push(protocol);
        vectorResultado.push(checksum);
        vectorResultado.push(source);
        vectorResultado.push(dest);

        vectorResultado.push(sourcePortUDP); //UDP
        vectorResultado.push(destinationPortUDP);
        vectorResultado.push(udpLength);
        vectorResultado.push(udpChecksum);
        vectorResultado.push(stringPaylUDP);

        return vectorResultado;
    }

    /*

    //Por si soluciono los problemas de self en la interfaz

    /**
     * @in: Vec<u8> que representa un paquete en cadena de bytes
     *      String con el protocolo seleccionado por el usuario
     * @out:
     * @function: La función obtiene el paquete de datos recibido y seleccionado, lo transforma a un paquete de la capa
     *              seleccionada y lo abre en dicha capa para su visionado
     */
    pub fn muestraPaqueteInterfaz(&mut self, vectorDatos : Vec<u8>, protocoloSeleccionado : &str){

        match protocoloSeleccionado{ //Dependiendo de la interfaz que nos pida, tendremos que mostrar mas o menos
            "IP"=>{

                let pac = Ipv4Packet::new(&vectorDatos).unwrap(); //Convertimos el Vec<u8> a &[u8] para obtener el IPv4Packet
                let vectorIp = self.getIPData(pac); //A partir de el, obtenemos los datos para la interfaz

                let hiloApoyo = self.clone();

                hiloApoyo.generaInterfazIP(vectorIp); //Mandamos dichos datos a la interfaz


            },
            "ICMP"=>{

            },
            "TCP"=>{

            },
            "UDP"=>{

            },
            "HTTP"=>{

            },
            "FTP"=>{

            },
            _=>(),
        }

    }

    */

    /* Por si en el futuro consigo hacer que las interfaces no se bloqueen
        /**
         * @in : vector de string con las variables que componen el protocolo IP
         * @out:
         * @function: A partir de los datos, correctamente transformados a String para su facil visionado,
         *              esta función abre una interfaz de IP de solo lectura para observar completamente el paquete
         */
        pub fn generaInterfazIP(&self, vectorDatos : Vec<String>){
            let appIP = app::App::default();
            let mut windIP = Window::default()
            .with_size(900, 350) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 3");
            //Definimos la interfaz
            let titulo = Frame::new(0, 0, 900, 50, "IP");

            let mut inputVersion = Input::new(65,50,75,25,"Version"); //X,Y,ANCHO, ALTO, TITULO
            inputVersion.set_value(vectorDatos[0].as_str());
            inputVersion.set_readonly(true);
                let mut helpVersion: Listener<_>  = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpVersion.set_color(Color::Yellow);

            let mut inputIHL = Input::new(215,50,75,25,"IHL"); //X,Y,ANCHO, ALTO, TITULO
            inputIHL.set_value(vectorDatos[1].as_str());
            inputIHL.set_readonly(true);
                let mut helpIHL : Listener<_> = Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpIHL.set_color(Color::Yellow);

            let mut inputTypeOfServ = Input::new(435,50,75,25,"TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
            inputTypeOfServ.set_value(vectorDatos[2].as_str());
            inputTypeOfServ.set_readonly(true);
                let mut helpTOS : Listener<_> = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpTOS.set_color(Color::Yellow);

            let mut inputECN = Input::new(595,50,75,25,"ECN"); //X,Y,ANCHO, ALTO, TITULO
            inputECN.set_value(vectorDatos[3].as_str());
            inputECN.set_readonly(true);
                let mut helpECN : Listener<_> = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpECN.set_color(Color::Yellow);

            let mut inputLength = Input::new(775,50,75,25,"Length"); //X,Y,ANCHO, ALTO, TITULO
            inputLength.set_value(vectorDatos[4].as_str());
            inputLength.set_readonly(true);
                let mut helpLen : Listener<_> = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpLen.set_color(Color::Yellow);


            let mut inputID = Input::new(65,100,175,25,"ID"); //X,Y,ANCHO, ALTO, TITULO
            inputID.set_value(vectorDatos[5].as_str());
            inputID.set_readonly(true);
                let mut helpID : Listener<_> = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpID.set_color(Color::Yellow);

            let mut inputFlags =  Input::new(355,100,50,25,"Flags"); //X,Y,ANCHO, ALTO, TITULO
            inputFlags.set_value(vectorDatos[6].as_str());
            inputFlags.set_readonly(true);
                let mut helpFlags : Listener<_> = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpFlags.set_color(Color::Yellow);


            let mut inputFragmentOff = Input::new(685,100,130,25,"FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
            inputFragmentOff.set_value(vectorDatos[7].as_str());
            inputFragmentOff.set_readonly(true);
                let mut helpOffSet : Listener<_> = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpOffSet.set_color(Color::Yellow);


            let mut inputTTL = Input::new(65,150,100,25,"TTL"); //X,Y,ANCHO, ALTO, TITULO
            inputTTL.set_value(vectorDatos[8].as_str());
            inputTTL.set_readonly(true);
                let mut helpTTL : Listener<_> = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpTTL.set_color(Color::Yellow);

            let mut inputProtocol = Input::new(345,150,100,25,"Protocol"); //X,Y,ANCHO, ALTO, TITULO
            inputProtocol.set_value(vectorDatos[9].to_uppercase().as_str());
            inputProtocol.set_readonly(true);
                let mut helpPro : Listener<_> = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpPro.set_color(Color::Yellow);

            let mut inputHeaderChecksum = Input::new(630,150,100,25,"Checksum"); //X,Y,ANCHO, ALTO, TITULO
            inputHeaderChecksum.set_value(vectorDatos[10].as_str());
            inputHeaderChecksum.set_readonly(true);
                let mut helpChecksum: Listener<_>  = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpChecksum.set_color(Color::Yellow);


            let mut inputSourceAdd = Input::new(165,205,200,25,"Source Address"); //X,Y,ANCHO, ALTO, TITULO
            inputSourceAdd.set_value(vectorDatos[11].as_str());
            inputSourceAdd.set_readonly(true);
                let mut helpSource : Listener<_> = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpSource.set_color(Color::Yellow);


            let mut inputDestAdd = Input::new(630,205,200,25,"Destination Address"); //X,Y,ANCHO, ALTO, TITULO
            inputDestAdd.set_value(vectorDatos[12].as_str());
            inputDestAdd.set_readonly(true);
                let mut helpDest: Listener<_>  = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpDest.set_color(Color::Yellow);


            let mut hexaPayload = Input::new(250,255,400,25,"Payload"); //X,Y,ANCHO, ALTO, TITULO
            hexaPayload.set_value(vectorDatos[13].as_str());
            hexaPayload.set_readonly(true);
                let mut helphexPay: Listener<_>  = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helphexPay.set_color(Color::Yellow);


            let mut paylo: Listener<_> = Button::new(50, 265, 80, 40, "Payload").into();
                let mut helpPay: Listener<_>  = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpPay.set_color(Color::Yellow);

            windIP.end();
            windIP.show();


            let (s, r) =  app::channel::<&str>();

            helpVersion.set_callback(move |b|{
                s.send("Version");
            });

            helpIHL.on_click(move |b|{
                s.send("IHL");
            });


            helpTOS.on_click(move |b|{
                s.send("TypeOfService");
            });

            helpECN.on_click(move |b|{
                s.send("ECN");
            });

            helpLen.on_click(move |b|{
                s.send("Length");
            });


            helpID.on_click(move |b|{
                s.send("ID");
            });


            helpFlags.on_click(move |b|{
                s.send("Flags");
            });

            helpOffSet.on_click(move |b|{
                s.send("FragmentOffset");
            });

            helpTTL.on_click(move |b|{
                s.send("TTL");
            });


            helpPro.on_click(move |b|{
                s.send("Protocol");
            });


            helpChecksum.on_click(move |b|{
                s.send("HeaderChecksum");
            });


            helpSource.on_click(move |b|{
                s.send("Source");
            });


            helpDest.on_click(move |b|{
                s.send("Destination");
            });

            helphexPay.on_click(move |b|{
                s.send("HexadecimalPayload");
            });

            helpPay.on_click(move |b|{
                s.send("Payload");
            });

            paylo.set_callback(move |b|{
                s.send("ButonPayload");
            });

            while appIP.wait(){
                match r.recv(){
                    Some(variableAux)=>{
                        let helpS = variableAux;
                        let stringAux =  self.paqueteIP.field_Help(helpS.to_string());
                        let strHelp = stringAux.as_str();

                        match helpS {
                            "Version"=>{
                                helpVersion.set_tooltip(strHelp);
                                helpVersion.tooltip();
                            },
                            "IHL"=>{
                                helpIHL.set_tooltip(strHelp);
                                helpIHL.tooltip();
                            },
                            "TypeOfService"=> {
                                helpTOS.set_tooltip(strHelp);
                                helpTOS.tooltip();
                            },
                            "ECN"=>{
                                helpECN.set_tooltip(strHelp);
                                helpECN.tooltip();
                            },
                            "Length"=>{
                                helpLen.set_tooltip(strHelp);
                                helpLen.tooltip();
                            },
                            "ID"=>{
                                helpID.set_tooltip(strHelp);
                                helpID.tooltip();
                            },
                            "Flags"=>{
                                helpFlags.set_tooltip(strHelp);
                                helpFlags.tooltip();
                            },
                            "FragmentOffset"=>{
                                helpOffSet.set_tooltip(strHelp);
                                helpOffSet.tooltip();
                            },
                            "TTL"=>{
                                helpTTL.set_tooltip(strHelp);
                                helpTTL.tooltip();
                            },
                            "Protocol"=>{
                                helpPro.set_tooltip(strHelp);
                                helpPro.tooltip();
                            },
                            "HeaderChecksum"=>{
                                helpChecksum.set_tooltip(strHelp);
                                helpChecksum.tooltip();
                            },
                            "Source"=>{
                                helpSource.set_tooltip(strHelp);
                                helpSource.tooltip();
                            },
                            "Destination"=>{
                                helpDest.set_tooltip(strHelp);
                                helpDest.tooltip();
                            },
                            "HexadecimalPayload"=>{
                                helphexPay.set_tooltip(strHelp);
                                helphexPay.tooltip();
                            },
                            "Payload"=>{
                                helpPay.set_tooltip(strHelp);
                                helpPay.tooltip();
                            },

                            "ButonPayload" =>{
                                // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                                let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                                let mut vectorMiembrosPayload = Vec::new();

                                if copiaPayloadHexadecimal != "" {
                                    let separador : Vec<&str> = copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                    //Teniendo ese vector, iteramos sobre el
                                    for var in separador{ //  0x95
                                        if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                            let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                            vectorMiembrosPayload.push(val[1]);
                                        }
                                    }
                                    //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                                }
                                let p = self.clone();
                                let mut windPayl = p.muestraPayloadIP(vectorMiembrosPayload);
                                windPayl.show();
                            }

                            _=>(),
                        }
                    }
                    None=>(),
                }
            }
        }
    */
    pub fn muestraPayloadIP(self, vectorMiembros: Vec<&str>) -> Window {
        let mut vectorMiembrosPayload = vectorMiembros.clone();
        let mut windPayl = Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        let clonGrid = self.clone();
        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        let mut flex = Flex::new(0, 0, 70, 700, None).column();
        {
            let mut grid = Grid::default_fill();
            grid.debug(false);
            grid.set_layout(25, 1);

            //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
            let mut labelOffset = Frame::default();
            labelOffset.set_label("Offset");
            grid.insert(&mut labelOffset, 0, 0); // Titulo Offset

            let mut i: i8 = 0;
            let mut varCol = 1;
            for mut i in 1..11 {
                let mut a = String::from("00");
                let mut ar: u8 = i * 10;
                let arg = ar.to_string();
                a.push_str(arg.as_str());
                let mut offsetCol = Frame::default();
                offsetCol.set_label(a.as_str());
                grid.insert(&mut offsetCol, varCol, 0); // Titulo Offset

                varCol += 1;
            }

            flex.end();
        }
        let mut gridMedio: Grid;
        let mut columnaMedio = Flex::new(70, 0, 430, 700, None).column();
        {
            let mut tablaMitad = Flex::default_fill().row();
            {
                gridMedio = Grid::default_fill();
                gridMedio.debug(false);
                gridMedio.set_layout(25, 17);
                let mut labelHexadecimal = Frame::default();
                labelHexadecimal.set_label("Texto Hexadecimal");
                gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 1); // Titulo Hexadecimal

                for y in 1..25 {
                    //10 columnas * 20 de distancia
                    for x in 0..17 {
                        // 16 filas * 20 de distancia entre cada una
                        if x != 8 {
                            let mut valorInputHexa = "";

                            //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload

                            if !vectorMiembrosPayload.is_empty() {
                                //Hay elementos aun en el vector
                                let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                valorInputHexa = valorAux;
                            } else {
                                //Ya se ha gastado el vector
                                valorInputHexa = "00"; //0 como valor por defecto
                            }
                            // Primero creamos el input
                            let mut inputHexa = Input::default();
                            inputHexa.set_value(valorInputHexa);
                            //Por ultimo lo metemos en la interfaz
                            gridMedio.insert(&mut inputHexa, y, x);
                            //Despues lo añadimos a la lista de inputs
                            but_input.insert((y, x), inputHexa);

                            &(map_input).insert((y, x), String::from(valorInputHexa));
                        }
                    }
                }

                tablaMitad.end();
            }

            columnaMedio.end();
        }

        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla

        let mut columnaFinal = Flex::new(500, 0, 200, 700, None).column();
        {
            let mut gridFinal = Grid::default_fill();
            gridFinal.debug(false);
            gridFinal.set_layout(25, 5);

            //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
            let mut labelSignificado = Frame::default();
            labelSignificado.set_label("Significado");
            gridFinal.insert_ext(&mut labelSignificado, 0, 0, 5, 1); // Titulo Offset

            let wrap = WrapMode::AtBounds;
            let mut buff = TextBuffer::default();
            casillaSignificado.wrap_mode(wrap, 0); //El limite de texto es 100, es decir, el limite de columna
            let stringTextoReal = clonGrid.actualizaLenguajeNatural(map_input.clone());

            buff.set_text(stringTextoReal.as_str());
            casillaSignificado.set_buffer(buff);
            gridFinal.insert_ext(&mut casillaSignificado, 1, 0, 5, 21); //  Label de transformacion

            columnaFinal.end();
        }
        windPayl.make_resizable(true);
        windPayl.end();

        return windPayl;
    }
    /**
     * @in : but_input: Mapa con todos los resultados hexadecimales de la interfaz
     * @out : String que devolverá el valor en lenguaje natural de ese conjunto de string hexadecimales
     * @function : La funcion transforma el conjunto completo de texto hexadecimal "00409A..." a un texto
     * que el usuario puede entender y que le dice que significa todo el payload
     */
    pub fn actualizaLenguajeNatural(self, but_input: HashMap<(i32, i32), String>) -> String {
        //Map de Button, hay que coger los valores

        let mut concatenacion = "";
        let mut auxiStr = String::new();

        let iterador = but_input.iter().sorted();
        //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
        for (key, val) in iterador {
            auxiStr.push_str(val.as_str());
        }
        concatenacion = auxiStr.as_str();
        // Aqui concatenacion debe tener el string de todos losvalores hexadecimales
        let mut vecHexa = Vec::new();
        vecHexa = match hex::decode(concatenacion){ //Vec<u8>
                                Ok(res) => {res},
                                Err(e) => return String::from(format!("Error al transformar.\n Siga editando el hexadecimal\n para obtener los datos correctos")),
                            }; //Lo transformamos a Vec<u8>

        //Vec u8 lo pasamos al string, ahora resuelto

        let mut strResul = String::from_utf8_lossy(&vecHexa).into_owned(); // turns invalid UTF-8 bytes into � and so no error handling is required.

        //Se elimina el primer bit porque es 0 y es un final de String. Esto funciona asi en ICMP, cuando se lleguer al paquete entero (cabecera capa 5) deberia dejar de tener problemas
        strResul.remove(0);
        let mut stringRes = String::from("");

        write!(stringRes, "{}", strResul).unwrap(); //Se une con un write para permitir añadir caracteres, en caso de ser necesario
        return stringRes;
    }

    /**
     * @in: String que nos indica si es un mensaje tipo "ERROR" o tipo "INFORMACION"
     *      String mensaje que nos indica el mensaje , del tipo anterior
     * @out:
     * @function: Esta funcion abre una interfaz a modo de PopUp que nos indica si hay algun error en nuestro uso
     *              o si debemos tener algo en cuenta para usar la interfaz principal correctamente
     *                  (buscar datos antes de intentar seleccionarlos, por ejemplo)
     */
    pub fn popup_Mensaje<'a>(&self, tipoMensaje: String, mensajeErr: Arc<Mutex<&str>>) -> Window {
        let mensajeTitulo = tipoMensaje.as_str();
        let mensajeError = mensajeErr.to_owned();
        let varAux = mensajeError.lock().unwrap();
        let auxi = varAux;

        let mut windo = Window::default()
            .with_size(400, 300)
            .center_screen()
            .with_label(mensajeTitulo);
            windo.make_resizable(true);
        let mut frame = Frame::new(170, 75, 50, 100, "");
        frame.set_label(&auxi); //La solucion era pasarlo como label editado en lugar de en la creación

        // mut ok = Button::new(170, 200, 50, 50, "OK"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        windo.end();
        return windo;
    }
}
