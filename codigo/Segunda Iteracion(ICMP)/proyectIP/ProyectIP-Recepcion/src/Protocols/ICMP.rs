#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]


extern crate pnet;
extern crate std;


use crate::Interfaces::Protocols::IP::IP;
//use std::alloc::Global;
use std::io::Error;
use pnet::packet::ipv4::*;
use pnet::packet::icmp::*;
use pnet::packet::icmp::MutableIcmpPacket;
use pnet::packet::*;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;

#[derive(Clone,Debug)]
pub struct ICMP {
    typeICMP : IcmpType,
    codeICMP: IcmpCode,
    checksum : u16,
    payload : Vec<u8>,
    ipPacket : IP,
}

/**
 * @in : ipv4Pac: Paquete ICMP creado a partir de la interfaz
 *       buffer_IP : vector de u8 que  necesitaremos para crear un paquete Ipv4
 * @out : MutableIcmpPacket : Paquete ICMP , osea, un objeto Icmp en forma de paquete
 * @function : Esta funcion transformara un objeto Icmp en un PacketIcmp, objeto que nos permite enviar paquetes Icmp por la red
 *              y  nos devolverá ese objeto y su checksum, en caso de necesitarlo 
 */
pub fn create_icmp_packet<'a>(buffer_ip: &'a mut [u8],ipv4Pac : pnet::packet::icmp::Icmp, checksu : u16) -> (MutableIcmpPacket,u16) {

    let mut ipv4_packet = MutableIcmpPacket::new(buffer_ip).unwrap();
    let ipv4_aux = ipv4Pac;
     /* Populate crea un Ipv4Packet (objeto que se manda)
                 a partir de un objeto Ipv4 */
    ipv4_packet.populate(&ipv4_aux);    
    let mut num = 0;
    let mut inmutable = ipv4_packet.to_immutable();
    // Generamos el checksum correcto, ya que es un calculo
    
    let checks = pnet::packet::icmp::checksum(&inmutable); 
    num = checks;
    ipv4_packet.set_checksum(checks);
    
    


    return (ipv4_packet,num);
}

impl ICMP{

    /**
     * @in:
     * @out: Objeto de la clase ICMP
     * @function: Constructor por defecto de ICMP
     */
    pub fn new() -> ICMP{ 

        let tipo = IcmpType::new(8); //Por defecto será un Echo Request
        //El unico codigo/subtipo que admite Echo Request es 0 (Direccion de destino inalcanzable)
        let codigo = IcmpCode::new(0); 
        let check : u16 = 0;
        let vecCrea = Vec::new();
        let mut ipAux = IP::new();
        ipAux.set_Protocol(IpNextHeaderProtocol::new(1)); //Es importante que indiquemos que el paquete es ICMP
        ICMP{
            typeICMP : tipo,
            codeICMP : codigo,
            checksum : check,
            payload : vecCrea,
            ipPacket : ipAux,
        }
    }
    
    /**
     * @in: Datos de creación del paquete ICMP
     * @out: Paquete IP creado
     * @function: La funcion crea un paquete IP a partir de los datos obtenidos
     */
    pub fn icmp_From_Data( tipo : IcmpType, codigo : IcmpCode, check : u16 , paylICMP : Vec<u8>, paqueIP : IP )-> ICMP{
        
        let paquete =  ICMP{
            typeICMP : tipo,
            codeICMP : codigo,
            checksum : check,
            payload : paylICMP,
            ipPacket : paqueIP,
        };

        return paquete;

    }

    /**
     * @in: String que contiene todos los datos de la clase ICMP codificados en Texto
     * @out: Paquete ICMP
     * @function: Esta funcion transforma un string en un paquete ICMP completo, a partir de los datos que este string contiene
     */

    pub fn icmp_From_String(&self, stringICMP : String) -> Result<ICMP,String> { //Devuelve IP pero cambialo cuando tengas uno decente
        
        /*Definimos las variables con las que crearemos el paquete IP nuevo
        * Ponemos las que teniamos por defecto ya. Esto servirá para crear un paquete
         y para ver a la vez si el paquete se creado correctamente (si tiene datos por defecto
        sabremos que algo ha fallado)
        */

        let mut tipoICMP : IcmpType = self.typeICMP;
        let mut codigoICMP: IcmpCode = self.codeICMP;
        let mut checksu : u16 = self.checksum;
        let mut paylICMP : Vec<u8> = self.payload.clone();
        //Primero separamos en las distintas variables con split (iterador) y convertimos en vector con collect

        let vecSplit: Vec<&str>  =  stringICMP.split("#").collect();

        //Una vez tenemos el vector iteramos sobre él y obtenemos sus valores

        for val in vecSplit{
            // Como el dato de val es "Val:Valor" separamos en ":" y sabemos que split[0] será "Val" y split[1] será "Valor"

            let vectorCampos: Vec<&str>  = val.split(":").collect();
            let valorCampo = vectorCampos[0];

            match valorCampo{
                "Type"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    let numAux = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                    tipoICMP = IcmpType::new(numAux);
                },
                "Code"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    let numAux = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                    codigoICMP = IcmpCode::new(numAux);
                },
                "Checksum"=>{
                    let aux = vectorCampos[1];
                    let auxHed : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxHed) => Ok(auxHed),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    checksu = auxHed.unwrap();
                },
                "Payload"=> { //Provisional. Falta saber como pasar string a vector, no se la estructura del vector
                    let aux = vectorCampos[1]; // "0x95;0xff;...;0x43"
                    let mut vecHexa = Vec::new();
                    if aux != "" {
                        let mut separador : Vec<&str> = aux.split(";").collect(); //Cada elemento es "0x95"
                        
                        //Una vez tenemos el string entero lo convertimos a u8
                        let clon = self.clone();
                        let numHexa = clon.transform_Hexadecimal(separador); //Obtenemos un string
                        let numHexaStr = numHexa.as_str();
                        let vecHexa = match hex::decode(numHexaStr){
                            Ok(res) => {vecHexa = res},
                            Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                            asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),
                        };//Lo transformamos a Vec<u8>
                    }
                    paylICMP = vecHexa;  //Si payload viene  vacio así irá el payload. Si tiene datos los codificaremos
                }
                _=>{},
            }
        }

        let pacIP = self.ipPacket.clone();
        Ok(ICMP{
            typeICMP:tipoICMP,
            codeICMP : codigoICMP,
            checksum :checksu,
            payload :paylICMP,
            ipPacket : pacIP,
        })

    }
    /**
     * @in:
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion será llamada por la interfaz y permitirá mandar un mensaje IP con su payload ICMP
     */
    pub fn sendPackage(&mut self, tamanioModificado : u16) ->  Result< u16, String>{

        //Transformamos en payload el paquete
        let mut paquete = self.icmpToPayload();
        let paylICMP = paquete.0;
        let tamPayload = paquete.1;

        let envio = self.send_ICMPPackage(paylICMP,tamPayload, tamanioModificado);
        return envio;
    }


    /**
     * @in: nuevoPayload: Paquete ICMP convertido en Payload de IP
     *      tamPayload : Tamaño del payload ICMP, que habrá que sumarle al paquete IP para un envio correcto
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion se encargará de enviar correctamente un  paquete ICMP , usando la cabecera IP
     */
    fn send_ICMPPackage(&mut self, nuevoPayload : Vec<u8>, tamPayload : usize, tamanioModificado : u16 )->  Result< u16, String>{

        let mut tamReal = (tamPayload as u16) * 32;
        let mut tamAnterior = self.ipPacket.get_Length();
        let mut nuevoTam = 0;
        if tamanioModificado != tamAnterior{
            nuevoTam = tamReal + tamAnterior;
        } else{
            nuevoTam = tamAnterior;
        }
        
        //Actualizamos datos del paquete IP
        &self.ipPacket.set_HexPayload(nuevoPayload);
        &self.ipPacket.set_Length( nuevoTam);
        let envio =  self.ipPacket.send_IP_Package();

        return envio;

    }
    /**
     * @in : Paquete de clase ICMP
     * @out: Vector de bytes 
     * @function : Esta funcion transformará el paquete ICMP que hemos creado en un vector de bytes para que sea
     *              el payload del paquete IP que hay inmediatamente debajo. Devolverá tambien el tamaño de este 
     *              payload, para que pueda sumarse al paquete total de IP
     */
    pub fn icmpToPayload(&self) -> (Vec<u8>,usize) {

        let mut vectorRetorno = Vec::new();
        // Convertimos el objeto actual en un paquete ICMP
        let pac = 
        pnet::packet::icmp::Icmp{
            icmp_type: self.typeICMP,
            icmp_code: self.codeICMP,
            checksum: self.checksum,
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 100];
       
        // Llamamos a la funcion que nos dará un MutableICMPPacket
        let mut resultado = create_icmp_packet(&mut int_slice, pac, self.checksum);
        let mut paquete = resultado.0;
        let mut nuevoChecksum = resultado.1;

        //Una vez tenemos el MutableICMPPacket , lo transformamos a &[u8]
        let tamBytes = paquete.packet_size();
        let bytesV = paquete.packet_mut();
        
        //Y de &[u8] a Vec<u8>
        vectorRetorno = bytesV.to_vec();

        return (vectorRetorno,tamBytes);
    }

    /**
     * @in: string "opti" : Lista de valores ["0x95", "0x50" , ...]
     * @out: String : String con el valor a convertir en hexadecimal por la funcion "hex"
     * @function: Esta función nos permitirá obtener a partir de los valores escritos en el input de Payload (0x95;0x50;...)
     * un string que se pueda transformar en hexadecimal : "9550..."
     */
    pub fn transform_Hexadecimal(self, separador : Vec<&str>) -> String{
        let mut strAux = String::new();

        for var in separador{ //  0x95
            if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                strAux.push_str(val[1]);
            }
            
        }
        let ret = strAux;
        return ret;
    }

    /** 
     * @in: Int "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, que devolverá a la Interfaz
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    pub fn field_Help(&self, campo : String) -> String{
        let mut ayuda : String = String::new();
        let campoStr = campo.as_str();
        match campoStr{
            "ICMPType" => {
                let helpe = "El campo 'Tipo' define la clase de mensaje que el protocolo ICMP quiere dar.\n 
                Hay de diferentes tipos (Echo, Solicitud de Router, Traceroute,...)\n
                A cada tipo le corresponde un valor numerico (que debes introducir en la interfaz).
                Los mas usados son: \n\n 
                0 -> Echo Reply\n
                3 -> Destination Unreachable\n
                5 -> Redirect\n
                7 -> Unassigned\n
                8 -> Echo Request\n
                9 -> Router Advertisement\n
                10 -> Router Solicitation\n
                11 -> Time Exceeded\n
                12 -> Parameter Problem\n
                13 -> Timestamp\n
                14 -> Timestamp Reply\n
                15-> Information Request\n
                16 ->Information Reply \n
                30 -> Traceroute\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc792.html' y\n 'https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml'";
                ayuda = helpe.to_string();
            },
            "ICMPCode" => {
                let helpe = "Cada tipo de mensaje ICMP tiene una serie de codigos\n
                que añaden informacion sobre el contenido del mensaje. No todos\n los
                tipos ICMP tienen los mismos codigos, por lo que es necesario buscarlo.\n
                Por ejemplo, para un Tipo 8 (Echo) , el unico Codigo es 0(No Code),\n
                pero para el tipo 3 hay 16 posibles codigos.\n\n
                Para mas informacion:\n 'https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml' \n y 'https://www.rfc-editor.org/rfc/rfc792.html'";
                ayuda = helpe.to_string();
            },
            "ICMPChecksum" => {
                let helpe = "Checksum que hará la comprobacion\nque indica que el paquete no fue modificado.\n
                                Se calcula en el envio pero puedes\n pre calcularlo con el boton de la derecha\n\n
                                Para mas informacion:\n'https://www.rfc-editor.org/rfc/rfc792.html'.\n";
                ayuda = helpe.to_string();
            },
            "ICMPPayload" => {
                let helpe = "El campo de Payload (32 bits) servirá para\n
                añadir mediante codigos hexadecimales\n los campos de protocolos de capa superiores a ICMP,\n
                sin necesidad de crear una nueva capa\n en el paquete para la edición\nde dicho protocolo.\n\n
                Generalmente, al ser ICMP por encima hay un protocolo UDP.\n
                Puedes introducir los datos mediante el boton de tu izquierda\n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc792.html'. ";
                ayuda = helpe.to_string();
            },
            "ICMPBotonPayload" => {
                let helpe = "Con este boton se abrira una interfaz\n que permite modificar comodamente\n el payload hexadecimal de ICMP\n";
                ayuda = helpe.to_string();
            },
            _=>{},
        }
        return ayuda;
    }

    /**
     * @in: Paquete ICMP
     * @out: checksum del paquete ICMP
     * @function: La funcion genera un checksum a partir de los datos que hay en la interfaz cuando esta es llamada 
    */

    pub fn generaChecksum(&self) -> u16 {
        //Generamos el paquete ICMP a partir de los datos que tenemos actualmente
        let pac = 
        pnet::packet::icmp::Icmp{
            icmp_type: self.typeICMP,
            icmp_code: self.codeICMP,
            checksum: self.checksum,
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 40];
       
        // Llamamos a la funcion checksum
        let resultado = create_icmp_packet(&mut int_slice, pac, self.checksum).1;

        return resultado;
    }

    /**
     * @in: IcmpType 
     * @out: u8 correspondiente  a un IcmpType
     * @function : Esta funcion transforma un IcmpType en su u8 correspondiente para mayor facilidad
     *              de comprension en la interfaz
     */
    pub fn transformaTipo(&self, tipo : IcmpType) -> u8{

        let mut num : u8 =0;
        match tipo{
            IcmpTypes::EchoReply=>{num = 0;},
            IcmpTypes::DestinationUnreachable=>{num = 3;},
            IcmpTypes::SourceQuench=>{num = 4;},
            IcmpTypes::RedirectMessage=>{num = 5;},
            IcmpTypes::EchoRequest=>{num = 8;},
            IcmpTypes::RouterAdvertisement=>{num = 9;},
            IcmpTypes::RouterSolicitation=>{num = 10;},
            IcmpTypes::TimeExceeded=>{num = 11;},
            IcmpTypes::ParameterProblem=>{num = 12;},
            IcmpTypes::Timestamp=>{num = 13;},
            IcmpTypes::TimestampReply=>{num = 14;},
            IcmpTypes::InformationRequest=>{num = 15;},
            IcmpTypes::InformationReply=>{num = 16;},
            IcmpTypes::AddressMaskRequest=>{num = 17;},
            IcmpTypes::AddressMaskReply=>{num = 18;},
            IcmpTypes::Traceroute=>{num = 30;},
            _=>{},
        }

        return num;
    }


    /**
     * @in: IcmpCode 
     * @out: u8 correspondiente  a un IcmpCode
     * @function : Esta funcion transforma un IcmpCode en su u8 correspondiente para mayor facilidad
     *              de comprension en la interfaz
     */
    pub fn transformaCode(&self, codigo : IcmpCode) -> u8{

        let tipoAux = self.get_Type();
        let numType = self.transformaTipo(tipoAux);
        let mut num : u8 =0;
        match numType{ //Depende del TIpo, cada valor pertenece a un codigo u otro
            0=>{ //Echo_Reply
                num = 0; //Solo puede ser 0 ->NoCode
            },
            3=>{ //Destination Unreachable
                match codigo {
                    IcmpCode(0)=>{ //DestinationNetworkUnreachable
                        num = 0;
                    },
                    IcmpCode(1)=>{ //DestinationHostUnreachable
                        num = 1;
                    },
                    IcmpCode(2)=>{ //DestinationProtocolUnreachable
                        num = 2;
                    },
                    IcmpCode(3)=>{ //DestinationPortUnreachable
                        num = 3;
                    },
                    IcmpCode(4)=>{ //FragmentationRequiredAndDFFlagSet
                        num = 4;
                    },
                    IcmpCode(5)=>{ //SourceRouteFailed
                        num = 5;
                    },
                    IcmpCode(6)=>{ //DestinationNetworkUnknown
                        num = 6;
                    },
                    IcmpCode(7)=>{ //DestinationHostUnknown
                        num = 7;
                    },
                    IcmpCode(8)=>{ //SourceHostIsolated
                        num = 8;
                    },
                    IcmpCode(9)=>{ //NetworkAdministrativelyProhibited  
                        num = 9;
                    },
                    IcmpCode(10)=>{ //HostAdministrativelyProhibited
                        num = 10;
                    },
                    IcmpCode(11)=>{ //NetworkUnreachableForTOS
                        num = 11;
                    },
                    IcmpCode(12)=>{ //HostUnreachableForTOS
                        num = 12;
                    },
                    IcmpCode(13)=>{ //CommunicationAdministrativelyProhibited
                        num = 13;
                    },
                    IcmpCode(14)=>{ //HostPrecedenceViolation
                        num = 14;
                    },
                    IcmpCode(15)=>{ //PrecedenceCutoffInEffect
                        num = 15;
                    },

                    _=>{},
                }
            },
            4=>{ // SourceQuench
                num = 0; // Solo puede ser 0 ->NoCode
            },
            5=>{ //RedirectMessage
                match codigo{
                    IcmpCode(0)=>{ //Redirect datagrams for the Network.
                        num = 0;
                    },
                    IcmpCode(1)=>{ //Redirect datagrams for the Host.
                        num = 1;
                    },
                    IcmpCode(2)=>{ //Redirect datagrams for the Type of Service and Network.
                        num = 2;
                    },
                    IcmpCode(3)=>{ //Redirect datagrams for the Type of Service and Host
                        num = 3;
                    },
                    _=>{},
                }
            },
            8=>{ //EchoRequest
                num = 0; //Solo puede ser 0 ->NoCode
            },
            11=>{ // TimeExceeded
                match codigo{
                    IcmpCode(0)=>{ // TimeToLiveExceededInTransit
                        num = 0;
                    },
                    IcmpCode(1)=>{ //FragmentReasemblyTimeExceeded
                        num = 1;
                    },
                    _=>{},
                }
            },
            12=>{ // ParameterProblem
                num = 0; //Solo puede ser 0 ->El puntero indica cual es el error
            },
            13=>{ // Timestamp
                num = 0; //Solo puede ser 0 ->NoCode
            },
            14=>{ // TimestampReply
                num = 0; //Solo puede ser 0 ->NoCode
            },
            15=>{ // InformationRequest
                num = 0; //Solo puede ser 0 ->NoCode
            },
            16=>{ // InformationReply
                num = 0; //Solo puede ser 0 ->NoCode
            },
            _=>{},
        }

        return num;
    }
    //Getter y Setter 

    pub fn get_Type(&self) -> IcmpType{
        return self.typeICMP;
    }

    pub fn set_Type(&mut self, tip : IcmpType){
        self.typeICMP = tip;
    }

    pub fn get_Code(&self) -> IcmpCode{
        return self.codeICMP;
    }

    pub fn set_Code(&mut self, cod : IcmpCode){
        self.codeICMP = cod;
    }

    pub fn get_Checksum(&self) -> u16{
        return self.checksum;
    }

    pub fn set_Checksum(&mut self, ch : u16){
        self.checksum = ch;
    }

    pub fn get_Payload(&self) -> Vec<u8>{
        return self.payload.clone();
    }

    pub fn set_Payload(&mut self, payl : Vec<u8>){
        let copia = payl.clone();
        self.payload = copia;
    }

    pub fn get_IPPacket(&self) -> IP{
        return self.ipPacket.clone();
    }

    pub fn set_IPPacket(&mut self, ip :IP){
        self.ipPacket = ip.clone();
    }

}