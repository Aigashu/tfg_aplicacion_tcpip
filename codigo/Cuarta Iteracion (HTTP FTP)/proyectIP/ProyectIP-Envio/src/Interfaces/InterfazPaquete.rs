#![allow(dead_code)]
#![allow(unused_variables)]
use crossbeam::channel;
use fltk::{
    app, button::Button, button::RadioRoundButton, draw::draw_line, draw::set_draw_color,
    enums::Color, frame::Frame, group::Pack, group::*, input::Input, menu::*, prelude::*, table::*,
    text::*, valuator::*, window::Window,
};
use fltk_evented::Listener;
use fltk_grid::Grid;
use itertools::Itertools;
use std::cell::Cell;
use std::collections::HashMap;
use std::net::Ipv4Addr;

use crate::Interfaces::Protocols::HTTP::HTTP;
use crate::Interfaces::Protocols::ICMP::ICMP;
use crate::Interfaces::Protocols::IP::IP;
use crate::Interfaces::Protocols::TCP::TCP;
use crate::Interfaces::Protocols::UDP::UDP;
use std::cell::RefCell;
use std::rc::Rc;
use std::sync::{Arc, Mutex};
use rand::Rng;
//Struct es donde se alojan las variables
#[derive(Clone)]
pub struct InterfazPaquete {
    campoVersion: String, //Campos IP
    campoIHL: String,
    campoTypeOfService: String,
    campoECN: String,
    campoLength: String,
    campoID: String,
    campoFlags: String,
    campoFragmentOffSet: String,
    campoTTL: String,
    campoProtocol: String,
    campoHeaderChecksum: String,
    campoSourceAddress: String,
    campoDestinationAddress: String,
    campoOptions: String,
    campoPadding: String,
    campoHexadecimalPayload: String,

    campoICMPType: String, //Campos de ICMP
    campoICMPCode: String,
    campoICMPChecksum: String,
    campoICMPPayload: String,
    tamanioModificado: u16,

    campoTCPSourcePort: String, //Campos de TCP
    campoTCPDestPort: String,
    campoTCPSequenceNum: String,
    campoTCPACK: String,
    campoTCPOffset: String,
    campoTCPReserved: String,
    campoTCPFlags: String,
    campoTCPWindow: String,
    campoTCPChecksum: String,
    campoTCPUrgPtr: String,
    campoTCPOptions: String,
    campoTCPPayload: String,

    campoUDPSourcePort: String, //Campos de UDP
    campoUDPDestPort: String,
    campoUDPLength: String,
    campoUDPChecksum: String,
    campoUDPPayload: String,

    campoHTTPPetitionType: String, //Campos de HTTP
    campoHTTPHost: String,
    campoHTTPVersion: String,
    vecHTTPHeaderLines: Vec<String>, //Para tener todas las peticiones de Cabecera de HTTP
    vecHTTPEntityLines: Vec<String>, //Para tener todas las peticiones de Entidad de HTTP

    packageIP: IP, //Necesitamos el objeto IP para enviar, pedir ayuda, etc
    packageICMP: ICMP,
    packageTCP: TCP,
    packageUDP: UDP,
    packageHTTP: HTTP,
}

//Constructor por defecto
pub fn new() -> InterfazPaquete {
    let ippac = IP::new();
    let icmppac = ICMP::new();
    let tcppac: TCP = TCP::new();
    let udppac: UDP = UDP::new();
    let httppac: HTTP = HTTP::new();
    let vecA = Vec::new();
    let vecB = Vec::new();
    InterfazPaquete {
        campoVersion: "".to_string(),
        campoIHL: "".to_string(),
        campoTypeOfService: "".to_string(),
        campoECN: "".to_string(),
        campoLength: "".to_string(),
        campoID: "".to_string(),
        campoFlags: "".to_string(),
        campoFragmentOffSet: "".to_string(),
        campoTTL: "".to_string(),
        campoProtocol: "".to_string(),
        campoHeaderChecksum: "".to_string(),
        campoSourceAddress: "".to_string(),
        campoDestinationAddress: "".to_string(),
        campoOptions: "".to_string(),
        campoPadding: "".to_string(),
        campoHexadecimalPayload: "".to_string(),
        campoICMPType: "".to_string(),
        campoICMPCode: "".to_string(),
        campoICMPChecksum: "".to_string(),
        campoICMPPayload: "".to_string(),
        tamanioModificado: 100,
        campoTCPSourcePort: "".to_string(),
        campoTCPDestPort: "".to_string(),
        campoTCPSequenceNum: "".to_string(),
        campoTCPACK: "".to_string(),
        campoTCPOffset: "".to_string(),
        campoTCPReserved: "".to_string(),
        campoTCPFlags: "".to_string(),
        campoTCPWindow: "".to_string(),
        campoTCPChecksum: "".to_string(),
        campoTCPUrgPtr: "".to_string(),
        campoTCPOptions: "".to_string(),
        campoTCPPayload: "".to_string(),
        campoUDPSourcePort: "".to_string(), //Campos de UDP
        campoUDPDestPort: "".to_string(),
        campoUDPLength: "".to_string(),
        campoUDPChecksum: "".to_string(),
        campoUDPPayload: "".to_string(),
        campoHTTPPetitionType: "".to_string(), //Campos de HTTP
        campoHTTPHost: "".to_string(),
        campoHTTPVersion: "".to_string(),
        vecHTTPHeaderLines: vecA, //Para tener todas las peticiones de Cabecera de HTTP
        vecHTTPEntityLines: vecB, //Para tener todas las peticiones de Entidad de HTTP

        packageIP: ippac,
        packageICMP: icmppac,
        packageTCP: tcppac,
        packageUDP: udppac,
        packageHTTP: httppac,
    }
}

//Los métodos se ubican en el impl
impl InterfazPaquete {
    //Metodos para Interfaz

    /*
    * @function : Esta funcion creará la interfaz base de la aplicación, que nos permitirá seleccionar la creación
                    o edición del protocolo de la capa que queramos, dentro de los limites que impusimos a la aplicación
    */
    pub fn createInterface(self) {
        let app = app::App::default();
        let mut windInter = Window::new(100, 100, 300, 300, "Protocol Select"); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let frame = Frame::new(100, 2, 50, 50, "Seleccione un protocolo ");
        let mut butICMP = Button::new(90, 210, 50, 50, "ICMP");
        let mut butIP = Button::new(150, 210, 50, 50, "IP"); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let mut butTCP = Button::new(90, 140, 50, 50, "TCP");
        let mut butUDP = Button::new(150, 140, 50, 50, "UDP");
        let mut butHTTP = Button::new(120, 75, 50, 50, "HTTP");
        windInter.make_resizable(true);
        windInter.end();
        windInter.show();
        let ip = Arc::new(Mutex::new(self.clone()));
        let icmp = Arc::new(Mutex::new(self.clone()));

        let (sendProtocol, reciveProtocolo) = app::channel::<&str>();
        // the closure capture is mutable borrow to our button
        butIP.set_callback(move |b| {
            sendProtocol.send("IP");
        });
        butICMP.set_callback(move |b| {
            sendProtocol.send("ICMP");
        });
        butTCP.set_callback(move |b| {
            sendProtocol.send("TCP");
        });
        butUDP.set_callback(move |b| {
            sendProtocol.send("UDP");
        });
        butHTTP.set_callback(move |b| {
            sendProtocol.send("HTTP");
        });
        while app.wait() {
            match reciveProtocolo.recv() {
                Some(msgEnvio) => match msgEnvio {
                    "IP" => {
                        let ip = Arc::new(Mutex::new(self.clone()));
                        windInter.hide();
                        ip.lock().unwrap().clone().interfaceIP();
                        windInter.show();
                    }
                    "ICMP" => {
                        let icmp = Arc::new(Mutex::new(self.clone()));
                        windInter.hide();
                        icmp.lock().unwrap().clone().interfaceICMP();
                        windInter.show();
                    }
                    "TCP" => {
                        let tcp = Arc::new(Mutex::new(self.clone()));
                        windInter.hide();
                        tcp.lock().unwrap().clone().interfaceTCP();
                        windInter.show();
                    }
                    "UDP" => {
                        let udp = Arc::new(Mutex::new(self.clone()));
                        windInter.hide();
                        udp.lock().unwrap().clone().interfaceUDP();
                        windInter.show();
                    }
                    "HTTP" => {
                        let http = Arc::new(Mutex::new(self.clone()));
                        windInter.hide();
                        http.lock().unwrap().clone().interfaceHTTP();
                        windInter.show();
                    }
                    _ => {}
                },
                None => {}
            }
        }
    }

    /**@in :
     * @out:
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete IP
     */
    pub fn interfaceIP(mut self) {
        let app = app::App::default();
        let mut wind = Window::default()
            .with_size(900, 350) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 3");
        //Definimos la interfaz
        let titulo = Frame::new(0, 0, 900, 50, "IP");
        wind.make_resizable(true);
        //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
        //Los hacemos mutable para cambiar sus valores
        //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
        //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

        self.refresh_Interface(1);
        let mut inputVersion = Input::new(65, 50, 75, 25, "Version"); //X,Y,ANCHO, ALTO, TITULO
        inputVersion.set_value(self.campoVersion.as_str());
        let mut helpVersion: Listener<_> = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpVersion.set_color(Color::Yellow);

        let mut inputIHL = Input::new(215, 50, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
        inputIHL.set_value(self.campoIHL.as_str());
        let mut helpIHL: Listener<_> = Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpIHL.set_color(Color::Yellow);

        let mut inputTypeOfServ = Input::new(435, 50, 75, 25, "TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
        inputTypeOfServ.set_value(self.campoTypeOfService.as_str());
        let mut helpTOS: Listener<_> = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTOS.set_color(Color::Yellow);

        let mut inputECN = Input::new(595, 50, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
        inputECN.set_value(self.campoECN.as_str());
        let mut helpECN: Listener<_> = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpECN.set_color(Color::Yellow);

        let mut inputLength = Input::new(775, 50, 75, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
        inputLength.set_value(self.campoLength.as_str());
        let mut helpLen: Listener<_> = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpLen.set_color(Color::Yellow);

        let mut inputID = Input::new(65, 100, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
        inputID.set_value(self.campoID.as_str());
        let mut helpID: Listener<_> = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpID.set_color(Color::Yellow);

        let mut inputFlags = Input::new(355, 100, 30, 25, "Flags"); //X,Y,ANCHO, ALTO, TITULO
        inputFlags.set_value("0");
        inputFlags.set_readonly(true);
        let mut helpFlags: Listener<_> = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpFlags.set_color(Color::Yellow);

        let mut msb = Choice::new(400, 100, 40, 25, None); //1
        msb.add_choice("0");
        msb.add_choice("1");
        msb.set_value(1); //Posicion 1 de la lista

        let mut lsb = Choice::new(450, 100, 40, 25, None); //0
        lsb.add_choice("0");
        lsb.add_choice("1");
        lsb.set_value(0); //Posicion 0

        let mut inputFragmentOff = Input::new(685, 100, 130, 25, "FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputFragmentOff.set_value(self.campoFragmentOffSet.as_str());
        let mut helpOffSet: Listener<_> = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpOffSet.set_color(Color::Yellow);

        let mut inputTTL = Input::new(65, 150, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
        inputTTL.set_value(self.campoTTL.as_str());
        let mut helpTTL: Listener<_> = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTTL.set_color(Color::Yellow);

        let mut inputProtocol = Input::new(345, 150, 100, 25, "Protocol"); //X,Y,ANCHO, ALTO, TITULO
        inputProtocol.set_value(self.campoProtocol.to_uppercase().as_str());
        let mut helpPro: Listener<_> = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPro.set_color(Color::Yellow);

        let mut inputHeaderChecksum = Input::new(630, 150, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputHeaderChecksum.set_value(self.campoHeaderChecksum.as_str());
        let mut helpChecksum: Listener<_> = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpChecksum.set_color(Color::Yellow);

        let mut generaChecksum: Listener<_> =
            Button::new(785, 145, 80, 40, "Generar\nChecksum").into();

        let mut inputSourceAdd = Input::new(165, 205, 200, 25, "Source Address"); //X,Y,ANCHO, ALTO, TITULO
        inputSourceAdd.set_value(self.campoSourceAddress.as_str());
        let mut helpSource: Listener<_> = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSource.set_color(Color::Yellow);

        let mut inputDestAdd = Input::new(630, 205, 200, 25, "Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(self.campoDestinationAddress.as_str());
        let mut helpDest: Listener<_> = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDest.set_color(Color::Yellow);

        /*
        let mut inputOptions = Input::new(100,305,300,25,"Options"); //X,Y,ANCHO, ALTO, TITULO
        inputOptions.set_value("1;");
            let mut helpOpt : Listener<_> = Button::new(415, 305, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpOpt.set_color(Color::Yellow);

        let mut inputPadding = Input::new(530,305,300,25,"Padding"); //X,Y,ANCHO, ALTO, TITULO
        inputPadding.set_value(self.campoPadding.as_str());
        inputPadding.set_readonly(true); //Como el padding está solo por mostrar que es un campo, no se rellena
            let mut helpPadding: Listener<_>  = Button::new(845, 305, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
            helpPadding.set_color(Color::Yellow);
        */

        let mut hexaPayload = Input::new(250, 255, 400, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
        hexaPayload.set_value("0x05;0x32;0x00;0x17;0x00;0x00;0x00;0x01;0x00;0x00;0x00;0x00;0x50;0x02;0x07;0xFF;0x00;0x00;0x00;0x00;");
        hexaPayload.set_readonly(true);
        let mut helphexPay: Listener<_> = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helphexPay.set_color(Color::Yellow);

        let mut paylo: Listener<_> = Button::new(50, 265, 80, 40, "Payload").into();
        let mut helpPay: Listener<_> = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPay.set_color(Color::Yellow);

        let mut sendP: Listener<_> = Button::new(775, 265, 80, 40, "Enviar").into();
        wind.end();
        wind.show();
        let vent = wind.clone();
        //Modificamos la Interfaz con los datos que tenemos

        /*
        A la hora de pulsar un boton puede dar error porque le pasas el self y supera su tiempo de vida.
        Para solucionar esto debe pasarse Self como un Arc<Mutex<Self>>, esto es, un semaforo, que permita modificar self respetando
            tanto el tiempo de vida de los hilos como los datos de cada hilo
        */

        //BOTONES DE AYUDA
        // S : Sender y r: Receiver . Objetos que se ponen a la escucha para enviar y esperar recibir algo,
        // en este caso, un string que dirá sobre que campo buscamos información
        let (s, r) = app::channel::<&str>();

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        let clonPayload = Arc::new(Mutex::new(self.clone()));
        let clonObtencion = Arc::new(Mutex::new(self.clone()));

        //Se incluyen aqui los datos de la interfaz del Payload para crearla consistentemente ( cerrar dicha interfaz
        //  no produzca fallos en el comportamiento de la interfazcompleta )

        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
                                                                        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
        //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
        let mut flex: Flex;
        let mut gridMedio: Grid = Grid::default_fill();
        let mut columnaMedio: Flex;
        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla
        let mut columnaFinal: Flex;

        // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
        // Cuando la app lo recoge , lo muestra
        helpVersion.on_click(move |b| {
            s.send("Version");
        });

        helpIHL.on_click(move |b| {
            s.send("IHL");
        });

        helpTOS.on_click(move |b| {
            s.send("TypeOfService");
        });

        helpECN.on_click(move |b| {
            s.send("ECN");
        });

        helpLen.on_click(move |b| {
            s.send("Length");
        });

        helpID.on_click(move |b| {
            s.send("ID");
        });

        helpFlags.on_click(move |b| {
            s.send("Flags");
        });

        helpOffSet.on_click(move |b| {
            s.send("FragmentOffset");
        });

        helpTTL.on_click(move |b| {
            s.send("TTL");
        });

        helpPro.on_click(move |b| {
            s.send("Protocol");
        });

        helpChecksum.on_click(move |b| {
            s.send("HeaderChecksum");
        });

        helpSource.on_click(move |b| {
            s.send("Source");
        });

        helpDest.on_click(move |b| {
            s.send("Destination");
        });

        /*
        helpOpt.on_click(move |b|{
            b.emit(s,"Options");
        });
        helpOpt.on_hover(move |b|{
            b.emit(s,"Options");
        });
        helpPadding.on_click(move |b|{
            b.emit(s,"Padding");
        });
        helpPadding.on_hover(move |b|{
            b.emit(s,"Padding");
        });
        */

        helphexPay.on_click(move |b| {
            s.send("HexadecimalPayload");
        });

        helpPay.on_click(move |b| {
            s.send("Payload");
        });

        generaChecksum.set_callback(move |b| {
            s.send("0");
        });
        sendP.on_click(move |b| {
            s.send("1");
        });

        paylo.on_click(move |b| {
            s.send("ButonPayload");
        }); //WIP. Debe abrir una interfaz de edicion de payload

        //Esto recoge información del campo y la usa para construir el PopUp
        while app.wait() {
            match r.recv() {
                Some(variableAux) => {
                    let helpS = variableAux;
                    let stringAux = clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let p = clonObtencion.clone();
                    let mut copiaMapa = but_input.clone();

                    match helpS {
                        "Version" => {
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "IHL" => {
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TypeOfService" => {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ECN" => {
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Length" => {
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ID" => {
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Flags" => {
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "FragmentOffset" => {
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TTL" => {
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Protocol" => {
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HeaderChecksum" => {
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Source" => {
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        //"Options"=>{},
                        //"Padding"=>{},
                        "HexadecimalPayload" => {
                            helphexPay.set_tooltip(strHelp);
                            helphexPay.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Payload" => {
                            helpPay.set_tooltip(strHelp);
                            helpPay.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "0" => {
                            //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                            self.save_FieldsIP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                "0".to_string(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                hexaPayload.clone().value(),
                            );
                            //Obtenemos el json
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                "500".to_string(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                self.campoHexadecimalPayload.clone(),
                            );
                            //Procedemos a calcular checksum

                            let mut valAux: u16 = 0;
                            let mut tamLen = inputLength.clone();
                            let mut co = inputHeaderChecksum.clone();
                            let paquete = self.ip_from_json(paqueteString);

                            match paquete {
                                Ok(ref resultadoIP) => {
                                    //Si el paquete está bien, buscamos como generar su checksum

                                    self.packageIP = (*resultadoIP).clone(); //Actualizamos el paquete IP al actual
                                    valAux = self.obtener_ChecksumIP(); //Sacamos la IP con unwrap
                                    let valorChecksum = valAux;
                                    //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                    let valorStr = valorChecksum.to_string();
                                    let nuevoValorStr = valorStr.as_str();
                                    co.set_value(nuevoValorStr);

                                    let valorLe = self.packageIP.get_Length().to_string();
                                    let valorLenStr = valorLe.as_str();
                                    tamLen.set_value(valorLenStr);
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "1" => {
                            //Enviar Paquete IP
                            //Guardamos los datos
                            let clon = Arc::new(Mutex::new(self.clone()));
                            match inputLength.value().as_str().parse::<i32>() {
                                Ok(campoCorrecto) => {
                                    if campoCorrecto < 500 {
                                        inputLength.set_value("500");
                                    }
                                    self.save_FieldsIP(
                                        inputVersion.clone().value(),
                                        inputIHL.clone().value(),
                                        inputTypeOfServ.clone().value(),
                                        inputECN.clone().value(),
                                        inputLength.clone().value(),
                                        inputID.clone().value(),
                                        inputFlags.clone().value(),
                                        msb.clone().value().to_string(),
                                        lsb.clone().value().to_string(),
                                        inputFragmentOff.clone().value(),
                                        inputTTL.clone().value(),
                                        inputProtocol.clone().value(),
                                        inputHeaderChecksum.clone().value(),
                                        inputSourceAdd.clone().value(),
                                        inputDestAdd.clone().value(),
                                        hexaPayload.clone().value(),
                                    );
                                    //Obtenemos el json
                                    let paqueteString = self.json_To_IP(
                                        self.campoVersion.clone(),
                                        self.campoIHL.clone(),
                                        self.campoTypeOfService.clone(),
                                        self.campoECN.clone(),
                                        self.campoLength.clone(),
                                        self.campoID.clone(),
                                        self.campoFlags.clone(),
                                        self.campoFragmentOffSet.clone(),
                                        self.campoTTL.clone(),
                                        self.campoProtocol.clone(),
                                        self.campoHeaderChecksum.clone(),
                                        self.campoSourceAddress.clone(),
                                        self.campoDestinationAddress.clone(),
                                        self.campoOptions.clone(),
                                        self.campoHexadecimalPayload.clone(),
                                    );
                                    //Procedemos a enviar
                                    let paquete = self.ip_from_json(paqueteString);

                                    match paquete {
                                        Ok(ref resultadoIP) => {
                                            let pac = (*resultadoIP).clone();
                                            wind.redraw();
                                            app.redraw();
                                            clon.lock().unwrap().sendPackageIP(pac);
                                            //Sacamos la IP con unwrap
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText = Arc::new(Mutex::new(msg.as_str()));

                                            wind.redraw();
                                            app.redraw();
                                            clon.lock()
                                                .unwrap()
                                                .clone()
                                                .popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new("Campo Length incorrecto.\nAsegurese de introducir un numero."));

                                    wind.redraw();
                                    app.redraw();
                                    clon.lock().unwrap().clone().popup_Mensaje(tipoMen, menText);
                                }
                            }
                        }

                        "ButonPayload" => {
                            //Aqui se encuentra la creación de la interfaz de edición comoda del payload
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = hexaPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }

                            //let appPayl = app::App::default();
                            let mut windPayl =
                                Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            windPayl.make_resizable(true);
                            let mut butActualizar =
                                Button::new(510, 630, 50, 50, "Actualiza\nTexto");
                            let mut butActInput = Button::new(510, 630, 50, 50, "Actualiza\nHex");

                            flex = Flex::new(0, 0, 70, 700, None).column();
                            {
                                let mut grid = Grid::default_fill();
                                grid.debug(false);
                                grid.set_layout(25, 1);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelOffset = Frame::default();
                                labelOffset.set_label("Offset");
                                grid.insert(&mut labelOffset, 0, 0); // Titulo Offset
                                grid.insert_ext(&mut butActualizar, 22, 0, 1, 2);

                                let mut i: i8 = 0;
                                let mut varCol = 1;
                                for mut i in 1..11 {
                                    let mut a = String::from("00");
                                    let ar: u8 = i * 10;
                                    let arg = ar.to_string();
                                    a.push_str(arg.as_str());
                                    let mut offsetCol = Frame::default();
                                    offsetCol.set_label(a.as_str());
                                    grid.insert(&mut offsetCol, varCol, 0); // Titulo Offset

                                    varCol += 1;
                                }

                                flex.end();
                            }
                            columnaMedio = Flex::new(70, 0, 430, 700, None).column();
                            {
                                let mut tablaMitad = Flex::default_fill().row();
                                {
                                    gridMedio = Grid::default_fill();
                                    gridMedio.debug(false);
                                    gridMedio.set_layout(25, 17);
                                    let mut labelHexadecimal = Frame::default();
                                    labelHexadecimal.set_label("Texto Hexadecimal");
                                    gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 1); // Titulo Hexadecimal

                                    for y in 1..25 {
                                        //10 columnas * 20 de distancia
                                        for x in 0..17 {
                                            // 16 filas * 20 de distancia entre cada una
                                            if x != 8 {
                                                let mut valorInputHexa = "";

                                                //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload

                                                if !vectorMiembrosPayload.is_empty() {
                                                    //Hay elementos aun en el vector
                                                    let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                                    valorInputHexa = valorAux;
                                                } else {
                                                    //Ya se ha gastado el vector
                                                    valorInputHexa = "00"; //0 como valor por defecto
                                                }
                                                // Primero creamos el input
                                                let mut inputHexa = Input::default();
                                                inputHexa.set_value(valorInputHexa);
                                                //Por ultimo lo metemos en la interfaz
                                                gridMedio.insert(&mut inputHexa, y, x);
                                                //Despues lo añadimos a la lista de inputs
                                                but_input.insert((y, x), inputHexa);

                                                &(map_input)
                                                    .insert((y, x), String::from(valorInputHexa));
                                            }
                                        }
                                    }

                                    tablaMitad.end();
                                }

                                columnaMedio.end();
                            }

                            columnaFinal = Flex::new(500, 0, 200, 700, None).column();
                            {
                                let mut gridFinal = Grid::default_fill();
                                gridFinal.debug(false);
                                gridFinal.set_layout(25, 5);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelSignificado = Frame::default();
                                labelSignificado.set_label("Significado");
                                gridFinal.insert_ext(&mut labelSignificado, 0, 0, 5, 1); // Titulo Offset

                                let wrap = WrapMode::AtBounds;
                                let mut buff = TextBuffer::default();
                                casillaSignificado.wrap_mode(wrap, 0); //El limite de texto es 100, es decir, el limite de columna
                                buff.set_text(
                                    "Pulse actualizar para revelar el payload por defecto",
                                );
                                casillaSignificado.set_buffer(buff);
                                gridFinal.insert_ext(&mut casillaSignificado, 1, 0, 5, 21); //  Label de transformacion

                                let mut save = Button::new(510, 630, 50, 50, "Guardar"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                gridFinal.insert_ext(&mut save, 22, 3, 2, 2); //  Label de transformacion
                                gridFinal.insert_ext(&mut butActInput, 22, 0, 2, 2);

                                butActualizar.set_callback(move |_| {
                                    s.send("AC");
                                });
                                butActInput.set_callback(move |_| {
                                    s.send("TEXT");
                                });
                                save.set_callback(move |_| {
                                    s.send("SAVE");
                                });
                                columnaFinal.end();
                            }

                            windPayl.end();
                            windPayl.show();

                            // the closure capture is mutable borrow to our button

                            //appPayl.run().unwrap();
                        }
                        // Y a partir de aqui se encuentra el comportamiento de los botones de la interfaz de edición del payload:
                        "AC" => {
                            //Actualiza Hexadecimal a Texto

                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            let string = p
                                .lock()
                                .unwrap()
                                .clone()
                                .actualizaLenguajeNatural(map_input.clone());

                            let mut buff = TextBuffer::default(); //Creamos un buffer para meterlo en el texto
                            buff.set_text(string.as_str()); //Metemos texto en buffer
                            casillaSignificado.set_buffer(buff); //Metemos buffer en la casilla que ocupa
                        }
                        "SAVE" => {
                            //Guarda datos en el input de la interfaz completa
                            //Procedemos a guardar todo en el input Payload del paquete IP
                            //Para ello, guardaremos todos los datos de los inputs en un vector

                            let mut vecPayl = Vec::new();

                            //Debemos actualizar los datos en caso de que se hayan modificado
                            let copiaMapa = but_input.clone();
                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            //Una vez editados, se extraen al vector
                            let mut concatenacion = "";
                            let mut auxiStr = String::new();
                            let iterador = map_input.iter().sorted(); //Necesitamos orden
                                                                      //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                            for (key, val) in iterador {
                                vecPayl.push(val);
                            }
                            //Una vez tenemos [00,00,00,...]

                            for val in vecPayl {
                                auxiStr.push_str("0x");
                                auxiStr.push_str(val.as_str());
                                auxiStr.push_str(";");
                                //El resultado queda como "0x00;"
                            }
                            concatenacion = auxiStr.as_str();
                            //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                            //Metemos dicho valor en el hexaPayload
                            hexaPayload.set_value(concatenacion);
                        }
                        "TEXT" => {
                            //Actualiza los inputs Hexadecimales a partir del texto ASCII
                            //Obtenemos el valor del texto en el momento
                            let mut textoBuf: TextBuffer = TextBuffer::default();
                            match casillaSignificado.buffer() {
                                Some(opcion) => {
                                    textoBuf = opcion;
                                }
                                None => (),
                            };
                            let textoCasilla = textoBuf.text();
                            //Obtenido el texto, lo transformamos a hexadecimal
                            let textoHexa = textoCasilla
                                .as_str()
                                .as_bytes()
                                .iter()
                                .map(|x| format!("{:02x}", x))
                                .collect::<String>();
                            //Ese hexadecimal a vector de caracteres, para introducirlos en los inputs
                            let mut vectorChar: Vec<char> = textoHexa.chars().collect();

                            //Iteramos sobre el mapa de inputs para cambiar sus valores por los nuevos
                            let iterador = map_input.iter().sorted(); //Necesitamos orden, asi que usamos el orden del mapa de Strings

                            for (key, val) in iterador {
                                //Recorremos y modificamos
                                if !vectorChar.is_empty() {
                                    //95
                                    let mostsb = vectorChar.remove(0); //9
                                    let lestsb = vectorChar.remove(0); //5

                                    let mut duplaHexadecimal = String::from(mostsb);
                                    let copiales = String::from(lestsb);
                                    duplaHexadecimal.push_str(copiales.as_str());

                                    //Obtenido el dato, lo introducimos en la casilla correspondiente
                                    let mut inputAux = Input::default();
                                    inputAux.set_value(duplaHexadecimal.as_str());
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux); //Aunque usemos el mapa de strings nos interesan las key
                                } else {
                                    //Una vez vaciado el vector, se llenan los inputs con 0
                                    let mut inputAux = Input::default();
                                    inputAux.set_value("00");
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux);
                                }
                            }
                        }

                        _ => {}
                    }
                }
                None => (),
            }
            wind.redraw();
            app.redraw();
        }

        //app.run().unwrap();
    }

    /**@in :
     * @out:
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete ICMP
     */
    pub fn interfaceICMP(mut self) {
        let app = app::App::default();
        let mut wind = Window::default()
            .with_size(900, 350) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 3");
        //Definimos la interfaz
        let titulo = Frame::new(0, 0, 900, 50, "ICMP");
        wind.make_resizable(true);
        //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
        //Los hacemos mutable para cambiar sus valores
        //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
        //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

        self.refresh_Interface(2);
        let mut inputVersion = Input::new(65, 50, 75, 25, "Version"); //X,Y,ANCHO, ALTO, TITULO
        inputVersion.set_value(self.campoVersion.as_str());
        let mut helpVersion: Listener<_> = Button::new(150, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpVersion.set_color(Color::Yellow);

        let mut inputIHL = Input::new(215, 50, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
        inputIHL.set_value(self.campoIHL.as_str());
        let mut helpIHL: Listener<_> = Button::new(300, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpIHL.set_color(Color::Yellow);

        let mut inputTypeOfServ = Input::new(435, 50, 75, 25, "TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
        inputTypeOfServ.set_value(self.campoTypeOfService.as_str());
        let mut helpTOS: Listener<_> = Button::new(520, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTOS.set_color(Color::Yellow);

        let mut inputECN = Input::new(595, 50, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
        inputECN.set_value(self.campoECN.as_str());
        let mut helpECN: Listener<_> = Button::new(680, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpECN.set_color(Color::Yellow);

        let mut inputLength = Input::new(775, 50, 75, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
        inputLength.set_value(self.campoLength.as_str());
        let mut helpLen: Listener<_> = Button::new(865, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpLen.set_color(Color::Yellow);

        let mut inputID = Input::new(65, 100, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
        inputID.set_value(self.campoID.as_str());
        let mut helpID: Listener<_> = Button::new(265, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpID.set_color(Color::Yellow);

        let mut inputFlags = Input::new(355, 100, 30, 25, "Flags"); //X,Y,ANCHO, ALTO, TITULO
        inputFlags.set_value("0");
        inputFlags.set_readonly(true);
        let mut helpFlags: Listener<_> = Button::new(520, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpFlags.set_color(Color::Yellow);

        let mut msb = Choice::new(400, 100, 40, 25, None); //1
        msb.add_choice("0");
        msb.add_choice("1");
        msb.set_value(1); //Posicion 1 de la lista

        let mut lsb = Choice::new(450, 100, 40, 25, None); //0
        lsb.add_choice("0");
        lsb.add_choice("1");
        lsb.set_value(0); //Posicion 0

        let mut inputFragmentOff = Input::new(685, 100, 130, 25, "FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputFragmentOff.set_value(self.campoFragmentOffSet.as_str());
        let mut helpOffSet: Listener<_> = Button::new(830, 100, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpOffSet.set_color(Color::Yellow);

        let mut inputTTL = Input::new(65, 150, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
        inputTTL.set_value(self.campoTTL.as_str());
        let mut helpTTL: Listener<_> = Button::new(175, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTTL.set_color(Color::Yellow);

        let mut inputProtocol = Input::new(345, 150, 100, 25, "Protocol"); //X,Y,ANCHO, ALTO, TITULO
        inputProtocol.set_value(self.campoProtocol.to_uppercase().as_str());
        let mut helpPro: Listener<_> = Button::new(460, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPro.set_color(Color::Yellow);

        let mut inputHeaderChecksum = Input::new(630, 150, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputHeaderChecksum.set_value(self.campoHeaderChecksum.as_str());
        let mut helpChecksum: Listener<_> = Button::new(740, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpChecksum.set_color(Color::Yellow);

        let mut generaChecksum: Listener<_> =
            Button::new(785, 145, 80, 40, "Generar\nChecksum").into();

        let mut inputSourceAdd = Input::new(165, 205, 200, 25, "Source Address"); //X,Y,ANCHO, ALTO, TITULO
        inputSourceAdd.set_value(self.campoSourceAddress.as_str());
        let mut helpSource: Listener<_> = Button::new(385, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSource.set_color(Color::Yellow);

        let mut inputDestAdd = Input::new(630, 205, 200, 25, "Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(self.campoDestinationAddress.as_str());
        let mut helpDest: Listener<_> = Button::new(840, 205, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDest.set_color(Color::Yellow);

        let mut hexaPayload = Input::new(250, 255, 400, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
        hexaPayload.set_value("0x05;0x32;0x00;0x17;0x00;0x00;0x00;0x01;0x00;0x00;0x00;0x00;0x50;0x02;0x07;0xFF;0x00;0x00;0x00;0x00;");
        hexaPayload.set_readonly(true);
        let mut helphexPay: Listener<_> = Button::new(670, 255, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helphexPay.set_color(Color::Yellow);

        let mut paylo: Listener<_> = Button::new(50, 265, 80, 40, "Payload").into();
        let mut helpPay: Listener<_> = Button::new(145, 265, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPay.set_color(Color::Yellow);

        let mut sendP: Listener<_> = Button::new(775, 265, 80, 40, "Enviar").into();
        wind.end();
        wind.show();

        let (s, r) = app::channel::<&str>();

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        let clonPayload = Arc::new(Mutex::new(self.clone()));
        let clonObtencion = Arc::new(Mutex::new(self.clone()));

        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
                                                                        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
        //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
        let mut flex: Flex;
        let mut gridMedio: Grid = Grid::default_fill();
        let mut columnaMedio: Flex;
        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla
        let mut columnaFinal: Flex;
        let mut inputICMPType = Input::default();
        let mut inputICMPCode = Input::default();
        let mut inputICMPChecksum = Input::default();
        let mut inputICMPPayload = Input::default();
        let mut helpType: Listener<_> = Button::default().into();
        let mut helpCode: Listener<_> = Button::default().into();
        let mut helpICMPChecksum: Listener<_> = Button::default().into();
        let mut helpICMPPayl: Listener<_> = Button::default().into();
        let mut helpICMPBotonPayl: Listener<_> = Button::default().into();
        let mut windoAux = Window::default();
        let mut windo = Window::default();

        // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
        // Cuando la app lo recoge , lo muestra
        helpVersion.on_click(move |b| {
            s.send("Version");
        });

        helpIHL.on_click(move |b| {
            s.send("IHL");
        });

        helpTOS.on_click(move |b| {
            s.send("TypeOfService");
        });

        helpECN.on_click(move |b| {
            s.send("ECN");
        });

        helpLen.on_click(move |b| {
            s.send("Length");
        });

        helpID.on_click(move |b| {
            s.send("ID");
        });

        helpFlags.on_click(move |b| {
            s.send("Flags");
        });

        helpOffSet.on_click(move |b| {
            s.send("FragmentOffset");
        });

        helpTTL.on_click(move |b| {
            s.send("TTL");
        });

        helpPro.on_click(move |b| {
            s.send("Protocol");
        });

        helpChecksum.on_click(move |b| {
            s.send("HeaderChecksum");
        });

        helpSource.on_click(move |b| {
            s.send("Source");
        });

        helpDest.on_click(move |b| {
            s.send("Destination");
        });

        helphexPay.on_click(move |b| {
            s.send("HexadecimalPayload");
        });

        helpPay.on_click(move |b| {
            s.send("Payload");
        });

        generaChecksum.set_callback(move |b| {
            s.send("0");
        });
        sendP.on_click(move |b| {
            s.send("1");
        });

        paylo.on_click(move |b| {
            s.send("SeleccionPaqueteICMP");
        });

        //Esto recoge información del campo y la usa para construir el PopUp
        while app.wait() {
            match r.recv() {
                Some(variableAux) => {
                    let helpS = variableAux;
                    let stringAux = clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let icmpAux = clonAyuda.lock().unwrap().field_Help(2, helpS.to_string());
                    let strHelpICMP = icmpAux.as_str();

                    let p = clonObtencion.clone();
                    let mut copiaMapa = but_input.clone();

                    match helpS {
                        "Version" => {
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "IHL" => {
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TypeOfService" => {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ECN" => {
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Length" => {
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ID" => {
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Flags" => {
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "FragmentOffset" => {
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TTL" => {
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Protocol" => {
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HeaderChecksum" => {
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Source" => {
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HexadecimalPayload" => {
                            helphexPay.set_tooltip(strHelp);
                            helphexPay.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Payload" => {
                            helpPay.set_tooltip(strHelp);
                            helpPay.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        //Metodos de ayuda de ICMP
                        "ICMPType" => {
                            helpType.set_tooltip(strHelpICMP);
                            helpType.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ICMPCode" => {
                            helpCode.set_tooltip(strHelpICMP);
                            helpCode.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ICMPChecksum" => {
                            helpICMPChecksum.set_tooltip(strHelpICMP);
                            helpICMPChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ICMPPayload" => {
                            helpICMPPayl.set_tooltip(strHelpICMP);
                            helpICMPPayl.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ICMPBotonPayload" => {
                            helpICMPBotonPayl.set_tooltip(strHelpICMP);
                            helpICMPBotonPayl.tooltip();
                            wind.redraw();
                            app.redraw();
                        }

                        "SeleccionPaqueteICMP" => {
                            //Abre ventana con la que elegir  los paquetes ICMP mas comunes . Será practico para otros protocolos tambien
                            windoAux = Window::default()
                                .with_size(225, 225)
                                .with_label("Seleccion de Mensaje ICMP");
                            windoAux.make_resizable(true);
                            let mut botonPING: Listener<_> =
                                Button::new(35, 25, 70, 50, "PING").into();
                            let mut botonMaskRequest: Listener<_> =
                                Button::new(125, 25, 70, 50, "Solicitar\nMascara").into();
                            let mut botonAvisoRouter: Listener<_> =
                                Button::new(35, 95, 70, 50, "Aviso de\nRouter").into();
                            let mut botonSolRouter: Listener<_> =
                                Button::new(125, 95, 70, 50, "Solicitar\nRouter").into();
                            let mut botonCallaFuente: Listener<_> =
                                Button::new(80, 160, 70, 50, "Acallar\nFuente").into();

                            windoAux.end();
                            windoAux.show();

                            botonPING.on_click(move |b| {
                                s.send("PING");
                            });

                            botonMaskRequest.on_click(move |b| {
                                s.send("MaskRequest");
                            });

                            botonAvisoRouter.on_click(move |b| {
                                s.send("AvisoRouter");
                            });

                            botonSolRouter.on_click(move |b| {
                                s.send("SolicitudRouter");
                            });

                            botonCallaFuente.on_click(move |b| {
                                s.send("QuenchSource");
                            });
                        }
                        "0" => {
                            //Genera Checksum IP
                            //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                            self.save_FieldsICMP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                "0".to_string(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                hexaPayload.clone().value(),
                                inputICMPType.clone().value(),
                                inputICMPCode.clone().value(),
                                inputICMPChecksum.clone().value(),
                                inputICMPPayload.clone().value(),
                            );

                            let mut valAux: u16 = 0;

                            //Obtenemos el json
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                self.campoHexadecimalPayload.clone(),
                            );

                            let paqueteICMPString = self.json_To_ICMP(
                                self.campoICMPType.clone(),
                                self.campoICMPCode.clone(),
                                self.campoICMPChecksum.clone(),
                                self.campoICMPPayload.clone(),
                            );

                            let paquete = self.ip_from_json(paqueteString);
                            let paqICMP = self.icmp_from_json(paqueteICMPString);

                            let mut nuevoTam = 0;
                            let mut co = inputHeaderChecksum.clone();
                            let mut camLen = inputLength.clone();
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    //Si el paquete IP está bien, buscamos como generar su checksum
                                    let mut nuevoResultadoIP = (*resultadoIP).clone();
                                    match paqICMP {
                                        Ok(ref resultadoICMP) => {
                                            //El paquete ICMP se crea correctamente
                                            let sendPacketICMP = (*resultadoICMP).clone(); //Obtenemos el paquete ICMP
                                            let duplaICMP = sendPacketICMP.icmpToPayload();
                                            let nuevoPayloadIP = duplaICMP.0;
                                            nuevoTam = 500;
                                            self.tamanioModificado = 500;
                                            nuevoResultadoIP.set_HexPayload(nuevoPayloadIP); //Actualizamos el payload
                                            nuevoResultadoIP.set_Length(nuevoTam); //Actualizamos el tamaño total

                                            self.packageIP = nuevoResultadoIP.clone(); //Actualizamos el paquete IP al actual
                                            valAux = self.obtener_ChecksumIP(); //Sacamos la IP con unwrap
                                            let valorChecksum = valAux;
                                            //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                            let valorStr = valorChecksum.to_string();
                                            let nuevoValorStr = valorStr.as_str();
                                            co.set_value(nuevoValorStr);

                                            let nuevoTamStr = nuevoTam.to_string();
                                            let nuevoTamString = nuevoTamStr.as_str();
                                            camLen.set_value(nuevoTamString);
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                            self.clone().popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "1" => {
                            //Envia paquete ICMP
                            //Guardamos los datos
                            self.save_FieldsICMP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                hexaPayload.clone().value(),
                                inputICMPType.clone().value(),
                                inputICMPCode.clone().value(),
                                inputICMPChecksum.clone().value(),
                                inputICMPPayload.clone().value(),
                            );

                            //Obtenemos el json
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                self.campoHexadecimalPayload.clone(),
                            );

                            let paqueteICMPString = self.json_To_ICMP(
                                self.campoICMPType.clone(),
                                self.campoICMPCode.clone(),
                                self.campoICMPChecksum.clone(),
                                self.campoICMPPayload.clone(),
                            );

                            //Creamos el paquete IP
                            let paquete = self.ip_from_json(paqueteString);
                            //Obtiene un paquete ICMP con los datos que hay actualmente en la interfaz
                            let paqICMP = self.icmp_from_json(paqueteICMPString);
                            let clon = Arc::new(Mutex::new(self.clone()));
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    //El paquete IP es correcto
                                    let pac = (*resultadoIP).clone();

                                    match paqICMP {
                                        Ok(ref resultadoICMP) => {
                                            
                                            //EL paquete ICMP se crea correctamente
                                            let mut sendPacketICMP = (*resultadoICMP).clone(); //Obtenemos el paquete ICMP
                                            sendPacketICMP.set_IPPacket(pac.clone()); //Guardamos el paquete IP en el paquete ICMP
                                            wind.redraw();
                                            app.redraw();
                                            clon.lock().unwrap().sendPackageICMP(sendPacketICMP);
                                            //Lo enviamos
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText = Arc::new(Mutex::new(msg.as_str()));

                                            wind.redraw();
                                            app.redraw();
                                            clon.lock()
                                                .unwrap()
                                                .clone()
                                                .popup_Mensaje(tipoMen, menText);
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new(msg.as_str()));

                                    wind.redraw();
                                    app.redraw();
                                    clon.lock().unwrap().clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }

                        "PING" => {
                            self.campoICMPType = "8".to_string();
                            self.campoICMPCode = "0".to_string();
                            s.send("PayloadICMP"); //Una vez  que hemos actualizado datos llamamos a la verdadera interfaz
                            windoAux.hide();
                        }
                        "MaskRequest" => {
                            self.campoICMPType = "17".to_string();
                            self.campoICMPCode = "0".to_string();
                            s.send("PayloadICMP"); //Una vez  que hemos actualizado datos llamamos a la verdadera interfaz
                            windoAux.hide();
                        }
                        "AvisoRouter" => {
                            self.campoICMPType = "9".to_string();
                            self.campoICMPCode = "0".to_string();
                            s.send("PayloadICMP"); //Una vez  que hemos actualizado datos llamamos a la verdadera interfaz
                            windoAux.hide();
                        }
                        "SolicitudRouter" => {
                            self.campoICMPType = "10".to_string();
                            self.campoICMPCode = "0".to_string();
                            s.send("PayloadICMP"); //Una vez  que hemos actualizado datos llamamos a la verdadera interfaz
                            windoAux.hide();
                        }
                        "QuenchSource" => {
                            self.campoICMPType = "4".to_string();
                            self.campoICMPCode = "0".to_string();
                            s.send("PayloadICMP"); //Una vez  que hemos actualizado datos llamamos a la verdadera interfaz
                            windoAux.hide();
                        }
                        "PayloadICMP" => {
                            //Genera la interfaz de ICMP encima de IP

                            windo = Window::default()
                                .with_size(550, 180)
                                .center_screen()
                                .with_label("Payload de IP: ICMP");
                            windo.make_resizable(true);
                            inputICMPType = Input::new(50, 25, 50, 25, "Type"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPType.set_value(self.campoICMPType.as_str());
                            helpType = Button::new(105, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpType.set_color(Color::Yellow);
                            inputICMPCode = Input::new(180, 25, 50, 25, "Code"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPCode.set_value(self.campoICMPCode.as_str());
                            helpCode = Button::new(235, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpCode.set_color(Color::Yellow);

                            inputICMPChecksum = Input::new(350, 25, 75, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPChecksum.set_value(self.campoICMPChecksum.as_str());
                            helpICMPChecksum = Button::new(430, 25, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPChecksum.set_color(Color::Yellow);

                            let mut botonChecksumICMP: Listener<_> =
                                Button::new(460, 15, 75, 50, "Obtener\nChecksum").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)

                            inputICMPPayload = Input::new(100, 85, 350, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
                            inputICMPPayload.set_value(self.campoICMPPayload.as_str());
                            inputICMPPayload.set_readonly(true);
                            helpICMPPayl = Button::new(460, 85, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPPayl.set_color(Color::Yellow);
                            let mut botonPayloadICMP: Listener<_> =
                                Button::new(20, 125, 75, 30, "Payload").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPBotonPayl = Button::new(460, 85, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            helpICMPBotonPayl.set_color(Color::Yellow);

                            let mut botonSaveICMP: Listener<_> =
                                Button::new(470, 125, 70, 30, "Guardar").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)

                            windo.end();
                            windo.show();

                            helpType.on_click(move |b| {
                                s.send("ICMPType");
                            });
                            helpCode.on_click(move |b| {
                                s.send("ICMPCode");
                            });
                            helpICMPChecksum.on_click(move |b| {
                                s.send("ICMPChecksum");
                            });
                            helpICMPPayl.on_click(move |b| {
                                s.send("ICMPPayload");
                            });
                            helpICMPBotonPayl.on_click(move |b| {
                                s.send("ICMPBotonPayload");
                            });

                            botonSaveICMP.on_click(move |b| {
                                s.send("ICMPSave");
                            });

                            botonPayloadICMP.on_click(move |b| {
                                s.send("ButtonPayload");
                            });

                            botonChecksumICMP.on_click(move |b| {
                                s.send("ChecksumICMP");
                            });
                        }
                        "ICMPSave" => {
                            //ICMP se convierte en payload de IP
                            self.save_FieldsICMP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                "".to_string(),
                                inputICMPType.clone().value(),
                                inputICMPCode.clone().value(),
                                inputICMPChecksum.clone().value(),
                                inputICMPPayload.clone().value(),
                            );

                            //Obtenemos el json
                            let paqueteString = self.json_To_ICMP(
                                self.campoICMPType.clone(),
                                self.campoICMPCode.clone(),
                                self.campoICMPChecksum.clone(),
                                self.campoICMPPayload.clone(),
                            );

                            let paquete = self.icmp_from_json(paqueteString);
                            match paquete {
                                Ok(ref resultadoICMP) => {
                                    let pac = (*resultadoICMP).clone();
                                    let vecPayloadICMP = pac.icmpToPayload().0;

                                    let mut concatenacion = "";
                                    let mut auxiStr = String::new();

                                    let transformacion = hex::encode(vecPayloadICMP).to_uppercase(); //[0,1,15,2] = "00010F02"

                                    let transforChar = transformacion.chars();
                                    let mut countChar: i8 = 0;
                                    let mut auxStrFor = String::new();
                                    for val in transforChar {
                                        auxStrFor.push(val);
                                        countChar += 1;
                                        if countChar == 2 {
                                            //00,01,0f,02
                                            countChar = 0;
                                            auxiStr.push_str("0x");
                                            auxiStr.push_str(auxStrFor.as_str());
                                            auxiStr.push_str(";");
                                            auxStrFor = String::new();
                                        }

                                        //El resultado queda como "0x00;"
                                    }
                                    concatenacion = auxiStr.as_str();
                                    hexaPayload.set_value(concatenacion);
                                    windo.hide();
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new(msg.as_str()));

                                    wind.redraw();
                                    app.redraw();
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "ChecksumICMP" => {
                            //Obtener Checksum ICMP
                            self.save_FieldsICMP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                "0".to_string(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                hexaPayload.clone().value(),
                                inputICMPType.clone().value(),
                                inputICMPCode.clone().value(),
                                inputICMPChecksum.clone().value(),
                                inputICMPPayload.clone().value(),
                            );

                            //Obtenemos el json
                            let paqueteString = self.json_To_ICMP(
                                self.campoICMPType.clone(),
                                self.campoICMPCode.clone(),
                                self.campoICMPChecksum.clone(),
                                self.campoICMPPayload.clone(),
                            );

                            let mut co = inputICMPChecksum.clone();
                            let mut valAux: u16 = 0;
                            let paquete = self.icmp_from_json(paqueteString);

                            match paquete {
                                Ok(ref resultadoICMP) => {
                                    //Si el paquete está bien, buscamos como generar su checksum

                                    self.packageICMP = resultadoICMP.clone();
                                    valAux = self.obtener_ChecksumICMP(); //Sacamos la IP con unwrap

                                    let valorChecksum = valAux;
                                    //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                    let valorStr = valorChecksum.to_string();
                                    let nuevoValorStr = valorStr.as_str();
                                    co.set_value(nuevoValorStr);
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "ButtonPayload" => {
                            //Aqui se encuentra la creación de la interfaz de edición comoda del payload
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputICMPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }

                            //let appPayl = app::App::default();
                            let mut windPayl =
                                Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                            windPayl.make_resizable(true);
                            let mut butActualizar =
                                Button::new(510, 630, 50, 50, "Actualiza\nTexto");
                            let mut butActInput = Button::new(510, 630, 50, 50, "Actualiza\nHex");

                            flex = Flex::new(0, 0, 70, 700, None).column();
                            {
                                let mut grid = Grid::default_fill();
                                grid.debug(false);
                                grid.set_layout(25, 1);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelOffset = Frame::default();
                                labelOffset.set_label("Offset");
                                grid.insert(&mut labelOffset, 0, 0); // Titulo Offset
                                grid.insert_ext(&mut butActualizar, 22, 0, 1, 2);

                                let mut i: i8 = 0;
                                let mut varCol = 1;
                                for mut i in 1..11 {
                                    let mut a = String::from("00");
                                    let ar: u8 = i * 10;
                                    let arg = ar.to_string();
                                    a.push_str(arg.as_str());
                                    let mut offsetCol = Frame::default();
                                    offsetCol.set_label(a.as_str());
                                    grid.insert(&mut offsetCol, varCol + 1, 0); // Titulo Offset

                                    varCol += 1;
                                }

                                flex.end();
                            }
                            columnaMedio = Flex::new(70, 0, 430, 700, None).column();
                            {
                                let mut tablaMitad = Flex::default_fill().row();
                                {
                                    gridMedio = Grid::default_fill();
                                    gridMedio.debug(false);
                                    gridMedio.set_layout(25, 17);
                                    let mut labelHexadecimal = Frame::default();
                                    labelHexadecimal.set_label("Texto Hexadecimal");
                                    gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 2); // Titulo Hexadecimal

                                    for y in 1..24 {
                                        //10 columnas * 20 de distancia
                                        for x in 0..17 {
                                            // 16 filas * 20 de distancia entre cada una
                                            if x != 8 {
                                                let mut valorInputHexa = "";

                                                //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload

                                                if !vectorMiembrosPayload.is_empty() {
                                                    //Hay elementos aun en el vector
                                                    let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                                    valorInputHexa = valorAux;
                                                } else {
                                                    //Ya se ha gastado el vector
                                                    valorInputHexa = "00"; //0 como valor por defecto
                                                }
                                                // Primero creamos el input
                                                let mut inputHexa = Input::default();
                                                inputHexa.set_value(valorInputHexa);
                                                //Por ultimo lo metemos en la interfaz
                                                gridMedio.insert(&mut inputHexa, y + 1, x);
                                                //Despues lo añadimos a la lista de inputs
                                                but_input.insert((y + 1, x), inputHexa);

                                                &(map_input).insert(
                                                    (y + 1, x),
                                                    String::from(valorInputHexa),
                                                );
                                            }
                                        }
                                    }

                                    tablaMitad.end();
                                }

                                columnaMedio.end();
                            }

                            columnaFinal = Flex::new(500, 0, 200, 700, None).column();
                            {
                                let mut gridFinal = Grid::default_fill();
                                gridFinal.debug(false);
                                gridFinal.set_layout(25, 5);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelSignificado = Frame::default();
                                labelSignificado.set_label("Significado");
                                gridFinal.insert_ext(&mut labelSignificado, 0, 0, 5, 1); // Titulo Offset

                                let wrap = WrapMode::AtBounds;
                                let mut buff = TextBuffer::default();
                                casillaSignificado.wrap_mode(wrap, 0); //El limite de texto es 100, es decir, el limite de columna
                                buff.set_text(
                                    "Pulse actualizar para revelar el payload por defecto",
                                );
                                casillaSignificado.set_buffer(buff);
                                gridFinal.insert_ext(&mut casillaSignificado, 1, 0, 5, 21); //  Label de transformacion

                                let mut save = Button::new(510, 630, 50, 50, "Guardar"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                gridFinal.insert_ext(&mut save, 22, 3, 2, 2); //  Label de transformacion
                                gridFinal.insert_ext(&mut butActInput, 22, 0, 2, 2);

                                butActualizar.set_callback(move |_| {
                                    s.send("AC");
                                });
                                butActInput.set_callback(move |_| {
                                    s.send("TEXT");
                                });
                                save.set_callback(move |_| {
                                    s.send("SAVE");
                                });
                                columnaFinal.end();
                            }

                            windPayl.end();
                            windPayl.show();

                            // the closure capture is mutable borrow to our button

                            //appPayl.run().unwrap();
                        }
                        // Y a partir de aqui se encuentra el comportamiento de los botones de la interfaz de edición del payload:
                        "AC" => {
                            //Actualiza Hexadecimal a Texto

                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            let string = p
                                .lock()
                                .unwrap()
                                .clone()
                                .actualizaLenguajeNatural(map_input.clone());

                            let mut buff = TextBuffer::default(); //Creamos un buffer para meterlo en el texto
                            buff.set_text(string.as_str()); //Metemos texto en buffer
                            casillaSignificado.set_buffer(buff); //Metemos buffer en la casilla que ocupa
                        }
                        "SAVE" => {
                            //Guarda datos en el input de la interfaz completa
                            //Procedemos a guardar todo en el input Payload del paquete IP
                            //Para ello, guardaremos todos los datos de los inputs en un vector

                            let mut vecPayl = Vec::new();

                            //Debemos actualizar los datos en caso de que se hayan modificado
                            let copiaMapa = but_input.clone();
                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            //Una vez editados, se extraen al vector
                            let mut concatenacion = "";
                            let mut auxiStr = String::new();
                            let iterador = map_input.iter().sorted(); //Necesitamos orden
                                                                      //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                            for (key, val) in iterador {
                                vecPayl.push(val);
                            }
                            //Una vez tenemos [00,00,00,...]

                            for val in vecPayl {
                                auxiStr.push_str("0x");
                                auxiStr.push_str(val.as_str());
                                auxiStr.push_str(";");
                                //El resultado queda como "0x00;"
                            }
                            concatenacion = auxiStr.as_str();
                            //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                            //Metemos dicho valor en el hexaPayload
                            inputICMPPayload.set_value(concatenacion);
                        }
                        "TEXT" => {
                            //Actualiza los inputs Hexadecimales a partir del texto ASCII
                            //Obtenemos el valor del texto en el momento
                            let mut textoBuf: TextBuffer = TextBuffer::default();
                            match casillaSignificado.buffer() {
                                Some(opcion) => {
                                    textoBuf = opcion;
                                }
                                None => (),
                            };
                            let textoCasilla = textoBuf.text();
                            //Obtenido el texto, lo transformamos a hexadecimal
                            let textoHexa = textoCasilla
                                .as_str()
                                .as_bytes()
                                .iter()
                                .map(|x| format!("{:02x}", x))
                                .collect::<String>();
                            //Ese hexadecimal a vector de caracteres, para introducirlos en los inputs
                            let mut vectorChar: Vec<char> = textoHexa.chars().collect();

                            //Iteramos sobre el mapa de inputs para cambiar sus valores por los nuevos
                            let iterador = map_input.iter().sorted(); //Necesitamos orden, asi que usamos el orden del mapa de Strings

                            for (key, val) in iterador {
                                //Recorremos y modificamos
                                if !vectorChar.is_empty() {
                                    //95
                                    let mostsb = vectorChar.remove(0); //9
                                    let lestsb = vectorChar.remove(0); //5

                                    let mut duplaHexadecimal = String::from(mostsb);
                                    let copiales = String::from(lestsb);
                                    duplaHexadecimal.push_str(copiales.as_str());

                                    //Obtenido el dato, lo introducimos en la casilla correspondiente
                                    let mut inputAux = Input::default();
                                    inputAux.set_value(duplaHexadecimal.as_str());
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux); //Aunque usemos el mapa de strings nos interesan las key
                                } else {
                                    //Una vez vaciado el vector, se llenan los inputs con 0
                                    let mut inputAux = Input::default();
                                    inputAux.set_value("00");
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux);
                                }
                            }
                        }

                        _ => {}
                    }
                }
                None => (),
            }
        }
    }

    /**@in :
     * @out:
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete TCP
     */
    pub fn interfaceTCP(mut self) {
        let app = app::App::default();
        let mut wind = Window::default()
            .with_size(900, 550) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 4");
        //Definimos la interfaz

        let titulo = Frame::new(0, 0, 900, 50, "TCP");

        //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
        //Los hacemos mutable para cambiar sus valores
        //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
        //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

        self.refresh_Interface(3);

        //TCP
        let mut inputSourcePort = Input::new(250, 50, 75, 25, "Source Port"); //X,Y,ANCHO, ALTO, TITULO
        inputSourcePort.set_value(self.campoTCPSourcePort.as_str());
        let mut helpSourcePort: Listener<_> = Button::new(335, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSourcePort.set_color(Color::Yellow);

        let mut inputDestPort = Input::new(620, 50, 75, 25, "Destination Port"); //X,Y,ANCHO, ALTO, TITULO
        inputDestPort.set_value(self.campoTCPDestPort.as_str());
        let mut helpDestPort: Listener<_> = Button::new(705, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDestPort.set_color(Color::Yellow);

        let mut inputTCPSequence = Input::new(135, 95, 200, 25, "Sequence"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPSequence.set_value(self.campoTCPSequenceNum.as_str());
        let mut helpSequence: Listener<_> = Button::new(345, 95, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSequence.set_color(Color::Yellow);

        let mut inputTCPAck = Input::new(600, 95, 200, 25, "Acknowledgement"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPAck.set_value(self.campoTCPACK.as_str());
        let mut helpTCPAck: Listener<_> = Button::new(810, 95, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPAck.set_color(Color::Yellow);

        let mut inputDataOffset = Input::new(90, 140, 75, 25, "DataOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputDataOffset.set_value(self.campoTCPOffset.as_str());
        let mut helpTCPOffset: Listener<_> = Button::new(170, 140, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPOffset.set_color(Color::Yellow);

        let mut inputReserved = Input::new(270, 140, 25, 25, "Reserved"); //X,Y,ANCHO, ALTO, TITULO
        inputReserved.set_value(self.campoTCPReserved.as_str());
        inputReserved.set_readonly(true);
        let mut helpTCPReserved: Listener<_> = Button::new(300, 140, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPReserved.set_color(Color::Yellow);

        let tituloFlags = Frame::new(330, 140, 50, 25, "Flags:");
        let mut urg = Choice::new(380, 140, 40, 25, None); //1
        urg.add_choice("0");
        urg.add_choice("1");
        urg.set_value(0); //Posicion 1 de la lista
        let tituloFlagsURG = Frame::new(370, 165, 50, 25, "URG");

        let mut ack = Choice::new(420, 140, 40, 25, None); //0
        ack.add_choice("0");
        ack.add_choice("1");
        ack.set_value(0); //Posicion 0
        let tituloFlagsACK = Frame::new(410, 165, 50, 25, "ACK");

        let mut psh = Choice::new(460, 140, 40, 25, None); //0
        psh.add_choice("0");
        psh.add_choice("1");
        psh.set_value(0); //Posicion 0
        let tituloFlagsPSH = Frame::new(450, 165, 50, 25, "PSH");

        let mut rst = Choice::new(500, 140, 40, 25, None); //0
        rst.add_choice("0");
        rst.add_choice("1");
        rst.set_value(0); //Posicion 0
        let tituloFlagsRST = Frame::new(490, 165, 50, 25, "RST");

        let mut syn = Choice::new(540, 140, 40, 25, None); //0
        syn.add_choice("0");
        syn.add_choice("1");
        syn.set_value(1); //Posicion 0
        let tituloFlagsSYN = Frame::new(530, 165, 50, 25, "SYN");

        let mut fin = Choice::new(580, 140, 40, 25, None); //0
        fin.add_choice("0");
        fin.add_choice("1");
        fin.set_value(0); //Posicion 0
        let tituloFlagsFIN = Frame::new(570, 165, 50, 25, "FIN");
        let mut helpTCPFlags: Listener<_> = Button::new(630, 140, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPFlags.set_color(Color::Yellow);

        let mut inputTCPWindow = Input::new(730, 140, 100, 25, "Window"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPWindow.set_value(self.campoTCPWindow.as_str());
        let mut helpTCPWindow: Listener<_> = Button::new(840, 140, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPWindow.set_color(Color::Yellow);

        let mut inputTCPChecksum = Input::new(220, 200, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPChecksum.set_value(self.campoTCPChecksum.as_str());
        let mut helpTCPChecksum: Listener<_> = Button::new(330, 200, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPChecksum.set_color(Color::Yellow);

        let mut generaChecksumTCP: Listener<_> =
            Button::new(45, 190, 80, 40, "Generar\nChecksum").into();

        let mut inputTCPUrgentPointer = Input::new(630, 200, 100, 25, "Urgent Pointer"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPUrgentPointer.set_value(self.campoTCPUrgPtr.as_str());
        let mut helpTCPUrgPointer: Listener<_> = Button::new(740, 200, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPUrgPointer.set_color(Color::Yellow);

        let mut inputTCPOptions = Input::new(100, 250, 200, 25, "Options"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPOptions.set_value("1;");
        let mut helpTCPOpt: Listener<_> = Button::new(320, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPOpt.set_color(Color::Yellow);

        let mut inputTCPPayload = Input::new(500, 250, 200, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
        inputTCPPayload.set_value(self.campoTCPPayload.as_str());
        inputTCPPayload.set_readonly(true);
        let mut helpTCPPayl: Listener<_> = Button::new(710, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTCPPayl.set_color(Color::Yellow);

        let mut buttonPaylTCP: Listener<_> = Button::new(760, 235, 80, 40, "Payload").into();
        let mut helpPayTCP: Listener<_> = Button::new(850, 245, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPayTCP.set_color(Color::Yellow);
        //set_draw_color(Color::White);
        //draw_line(0, 250, 900, 250);
        //IP
        let subtitulo = Frame::new(0, 300, 900, 50, "IP");
        let mut inputVersion = Input::new(65, 350, 75, 25, "Version"); //X,Y,ANCHO, ALTO, TITULO
        inputVersion.set_value(self.campoVersion.as_str());
        let mut helpVersion: Listener<_> = Button::new(150, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpVersion.set_color(Color::Yellow);

        let mut inputIHL = Input::new(215, 350, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
        inputIHL.set_value(self.campoIHL.as_str());
        let mut helpIHL: Listener<_> = Button::new(300, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpIHL.set_color(Color::Yellow);

        let mut inputTypeOfServ = Input::new(435, 350, 75, 25, "TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
        inputTypeOfServ.set_value(self.campoTypeOfService.as_str());
        let mut helpTOS: Listener<_> = Button::new(520, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTOS.set_color(Color::Yellow);

        let mut inputECN = Input::new(595, 350, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
        inputECN.set_value(self.campoECN.as_str());
        let mut helpECN: Listener<_> = Button::new(680, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpECN.set_color(Color::Yellow);

        let mut inputLength = Input::new(775, 350, 75, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
        inputLength.set_value(self.campoLength.as_str());
        let mut helpLen: Listener<_> = Button::new(865, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpLen.set_color(Color::Yellow);

        let mut inputID = Input::new(65, 400, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
        inputID.set_value(self.campoID.as_str());
        let mut helpID: Listener<_> = Button::new(265, 400, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpID.set_color(Color::Yellow);

        let mut inputFlags = Input::new(355, 400, 30, 25, "Flags"); //X,Y,ANCHO, ALTO, TITULO
        inputFlags.set_value("0");
        inputFlags.set_readonly(true);
        let mut helpFlags: Listener<_> = Button::new(520, 400, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpFlags.set_color(Color::Yellow);

        let mut msb = Choice::new(400, 400, 40, 25, None); //1
        msb.add_choice("0");
        msb.add_choice("1");
        msb.set_value(1); //Posicion 1 de la lista

        let mut lsb = Choice::new(450, 400, 40, 25, None); //0
        lsb.add_choice("0");
        lsb.add_choice("1");
        lsb.set_value(0); //Posicion 0

        let mut inputFragmentOff = Input::new(685, 400, 130, 25, "FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputFragmentOff.set_value(self.campoFragmentOffSet.as_str());
        let mut helpOffSet: Listener<_> = Button::new(830, 400, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpOffSet.set_color(Color::Yellow);

        let mut inputTTL = Input::new(65, 450, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
        inputTTL.set_value(self.campoTTL.as_str());
        let mut helpTTL: Listener<_> = Button::new(175, 450, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTTL.set_color(Color::Yellow);

        let mut inputProtocol = Input::new(345, 450, 100, 25, "Protocol"); //X,Y,ANCHO, ALTO, TITULO
        inputProtocol.set_value(self.campoProtocol.to_uppercase().as_str());
        let mut helpPro: Listener<_> = Button::new(460, 450, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPro.set_color(Color::Yellow);

        let mut inputHeaderChecksum = Input::new(630, 450, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputHeaderChecksum.set_value(self.campoHeaderChecksum.as_str());
        let mut helpChecksum: Listener<_> = Button::new(740, 450, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpChecksum.set_color(Color::Yellow);

        let mut generaChecksum: Listener<_> =
            Button::new(785, 445, 80, 40, "Generar\nChecksum").into();

        let mut inputSourceAdd = Input::new(125, 505, 200, 25, "Source Address"); //X,Y,ANCHO, ALTO, TITULO
        inputSourceAdd.set_value(self.campoSourceAddress.as_str());
        let mut helpSource: Listener<_> = Button::new(340, 505, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSource.set_color(Color::Yellow);

        let mut inputDestAdd = Input::new(550, 505, 200, 25, "Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(self.campoDestinationAddress.as_str());
        let mut helpDest: Listener<_> = Button::new(760, 505, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDest.set_color(Color::Yellow);

        let mut sendP: Listener<_> = Button::new(810, 500, 80, 40, "Enviar").into();
        wind.make_resizable(true);
        wind.end();
        wind.show();

        let (s, r) = app::channel::<&str>();

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        let clonPayload = Arc::new(Mutex::new(self.clone()));
        let clonObtencion = Arc::new(Mutex::new(self.clone()));

        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
                                                                        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
        //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
        let mut flex: Flex;
        let mut gridMedio: Grid = Grid::default_fill();
        let mut columnaMedio: Flex;
        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla
        let mut columnaFinal: Flex;
        let mut windoAux = Window::default();
        let mut windo = Window::default();

        // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
        // Cuando la app lo recoge , lo muestra
        helpVersion.on_click(move |b| {
            s.send("Version");
        });

        helpIHL.on_click(move |b| {
            s.send("IHL");
        });

        helpTOS.on_click(move |b| {
            s.send("TypeOfService");
        });

        helpECN.on_click(move |b| {
            s.send("ECN");
        });

        helpLen.on_click(move |b| {
            s.send("Length");
        });

        helpID.on_click(move |b| {
            s.send("ID");
        });

        helpFlags.on_click(move |b| {
            s.send("Flags");
        });

        helpOffSet.on_click(move |b| {
            s.send("FragmentOffset");
        });

        helpTTL.on_click(move |b| {
            s.send("TTL");
        });

        helpPro.on_click(move |b| {
            s.send("Protocol");
        });

        helpChecksum.on_click(move |b| {
            s.send("HeaderChecksum");
        });

        helpSource.on_click(move |b| {
            s.send("Source");
        });

        helpDest.on_click(move |b| {
            s.send("Destination");
        });

        generaChecksum.set_callback(move |b| {
            s.send("0");
        });
        sendP.on_click(move |b| {
            s.send("1");
        });

        helpSourcePort.on_click(move |b| {
            s.send("PuertoOrigen");
        });
        helpDestPort.on_click(move |b| {
            s.send("PuertoDestino");
        });
        helpSequence.on_click(move |b| {
            s.send("Sequence");
        });
        helpTCPAck.on_click(move |b| {
            s.send("ACK");
        });
        helpTCPOffset.on_click(move |b| {
            s.send("DataOffset");
        });
        helpTCPReserved.on_click(move |b| {
            s.send("Reserved");
        });
        helpTCPFlags.on_click(move |b| {
            s.send("TCPFlags");
        });
        helpTCPWindow.on_click(move |b| {
            s.send("TCPWindow");
        });
        helpTCPChecksum.on_click(move |b| {
            s.send("TCPChecksum");
        });
        generaChecksumTCP.on_click(move |b| {
            s.send("ChecksumTCP");
        });
        helpTCPUrgPointer.on_click(move |b| {
            s.send("UrgPointer");
        });
        helpTCPOpt.on_click(move |b| {
            s.send("TCPOptions");
        });
        helpTCPPayl.on_click(move |b| {
            s.send("TCPPayload");
        });
        buttonPaylTCP.on_click(move |b| {
            //Boton Payload
            s.send("BotonPayloadTCP");
        });

        helpPayTCP.on_click(move |b| {
            //ayuda boton payload
            s.send("TCPBotonPayload");
        });

        while app.wait() {
            match r.recv() {
                Some(variableAux) => {
                    let helpS = variableAux;
                    let stringAux = clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let stringAuxTCP = clonAyuda.lock().unwrap().field_Help(3, helpS.to_string());
                    let strHelpTCP = stringAuxTCP.as_str();

                    let p = clonObtencion.clone();
                    let mut copiaMapa = but_input.clone();

                    match helpS {
                        "Version" => {
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "IHL" => {
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TypeOfService" => {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ECN" => {
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Length" => {
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ID" => {
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Flags" => {
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "FragmentOffset" => {
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TTL" => {
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Protocol" => {
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HeaderChecksum" => {
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Source" => {
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "PuertoOrigen" => {
                            helpSourcePort.set_tooltip(strHelpTCP);
                            helpSourcePort.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "PuertoDestino" => {
                            helpDestPort.set_tooltip(strHelpTCP);
                            helpDestPort.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Sequence" => {
                            helpSequence.set_tooltip(strHelpTCP);
                            helpSequence.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ACK" => {
                            helpTCPAck.set_tooltip(strHelpTCP);
                            helpTCPAck.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "DataOffset" => {
                            helpTCPOffset.set_tooltip(strHelpTCP);
                            helpTCPOffset.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Reserved" => {
                            helpTCPReserved.set_tooltip(strHelpTCP);
                            helpTCPReserved.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPFlags" => {
                            helpTCPFlags.set_tooltip(strHelpTCP);
                            helpTCPFlags.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPWindow" => {
                            helpTCPWindow.set_tooltip(strHelpTCP);
                            helpTCPWindow.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPChecksum" => {
                            helpTCPChecksum.set_tooltip(strHelpTCP);
                            helpTCPChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "UrgPointer" => {
                            helpTCPUrgPointer.set_tooltip(strHelpTCP);
                            helpTCPUrgPointer.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPOptions" => {
                            helpTCPOpt.set_tooltip(strHelpTCP);
                            helpTCPOpt.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPPayload" => {
                            helpTCPPayl.set_tooltip(strHelpTCP);
                            helpTCPPayl.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TCPBotonPayload" => {
                            helpPayTCP.set_tooltip(strHelpTCP);
                            helpPayTCP.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ChecksumTCP" => {
                            //Generar checksum de TCP
                            //Guardamos los datos
                            self.save_FieldsTCP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputTCPSequence.clone().value(),
                                inputTCPAck.clone().value(),
                                inputDataOffset.clone().value(),
                                inputReserved.clone().value(),
                                urg.choice().unwrap(),
                                ack.choice().unwrap(),
                                psh.choice().unwrap(),
                                rst.choice().unwrap(),
                                syn.choice().unwrap(),
                                fin.choice().unwrap(),
                                inputTCPWindow.clone().value(),
                                inputTCPChecksum.clone().value(),
                                inputTCPUrgentPointer.clone().value(),
                                inputTCPOptions.clone().value(),
                                inputTCPPayload.clone().value(),
                            );

                            //Convertimos los valores de IP en la interfaz en el momento de guardar
                            let copiaSo = self.campoSourceAddress.clone();
                            let copiaDe = self.campoDestinationAddress.clone();
                            let vecSor: Vec<&str> = copiaSo.split(".").collect();
                            let vecDes: Vec<&str> = copiaDe.split(".").collect();
                            let paqueteTCPString = self.json_To_TCP(
                                self.campoTCPSourcePort.clone(),
                                self.campoTCPDestPort.clone(),
                                self.campoTCPSequenceNum.clone(),
                                self.campoTCPACK.clone(),
                                self.campoTCPOffset.clone(),
                                self.campoTCPReserved.clone(),
                                self.campoTCPFlags.clone(),
                                self.campoTCPWindow.clone(),
                                self.campoTCPChecksum.clone(),
                                self.campoTCPUrgPtr.clone(),
                                self.campoTCPOptions.clone(),
                                self.campoTCPPayload.clone(),
                            );

                            //Creamos el paquete IP
                            if vecSor.len() == 4 && vecDes.len() == 4 {
                                //Obtiene un paquete TCP con los datos que hay actualmente en la interfaz
                                let paqTCP = self.tcp_from_json(paqueteTCPString);
                                let mut co = inputTCPChecksum.clone();
                                let mut valAux: u16 = 0;
                                let clon = Arc::new(Mutex::new(self.clone()));

                                let bit1So: Result<u8, String> = match vecSor[0].parse::<u8>() {
                                    //127
                                    Ok(bit1) => Ok(bit1),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit1So = bit1So.unwrap();

                                let bit2So: Result<u8, String> = match vecSor[1].parse::<u8>() {
                                    //0
                                    Ok(bit2) => Ok(bit2),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit2So = bit2So.unwrap();

                                let bit3So: Result<u8, String> = match vecSor[2].parse::<u8>() {
                                    //0
                                    Ok(bit3) => Ok(bit3),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit3So = bit3So.unwrap();

                                let bit4So: Result<u8, String> = match vecSor[3].parse::<u8>() {
                                    //1
                                    Ok(bit4) => Ok(bit4),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit4So = bit4So.unwrap();

                                let mut sours = Ipv4Addr::new(biit1So, biit2So, biit3So, biit4So);

                                let bit1De: Result<u8, String> = match vecDes[0].parse::<u8>() {
                                    //127
                                    Ok(bit1) => Ok(bit1),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit1De = bit1De.unwrap();

                                let bit2De: Result<u8, String> = match vecDes[1].parse::<u8>() {
                                    //0
                                    Ok(bit2) => Ok(bit2),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit2De = bit2De.unwrap();

                                let bit3De: Result<u8, String> = match vecDes[2].parse::<u8>() {
                                    //0
                                    Ok(bit3) => Ok(bit3),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit3De = bit3De.unwrap();

                                let bit4De: Result<u8, String> = match vecDes[3].parse::<u8>() {
                                    //1
                                    Ok(bit4) => Ok(bit4),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit4De = bit4De.unwrap();

                                let mut dests = Ipv4Addr::new(biit1De, biit2De, biit3De, biit4De);
                                if true {
                                    match paqTCP {
                                        Ok(ref resultadoTCP) => {
                                            //Si el paquete está bien, buscamos como generar su checksum
                                            let paqueTCP = resultadoTCP.clone();
                                            self.packageTCP = paqueTCP;
                                            valAux = self.obtener_ChecksumTCP(sours, dests); //Sacamos la IP con unwrap

                                            let valorChecksum = valAux;
                                            //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                            let valorStr = valorChecksum.to_string();
                                            let nuevoValorStr = valorStr.as_str();
                                            co.set_value(nuevoValorStr);
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                            self.clone().popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                } else {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            } else {
                                let tipoMen = "ERROR".to_string();
                                let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                self.clone().popup_Mensaje(tipoMen, menText);
                                // COn unwrap_err() sacamos el Err , osea, el string
                            }
                        }
                        "0" => {
                            //Checksum IP
                            //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                            self.save_FieldsTCP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                "0x00;".to_string(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputTCPSequence.clone().value(),
                                inputTCPAck.clone().value(),
                                inputDataOffset.clone().value(),
                                inputReserved.clone().value(),
                                urg.choice().unwrap(),
                                ack.choice().unwrap(),
                                psh.choice().unwrap(),
                                rst.choice().unwrap(),
                                syn.choice().unwrap(),
                                fin.choice().unwrap(),
                                inputTCPWindow.clone().value(),
                                inputTCPChecksum.clone().value(),
                                inputTCPUrgentPointer.clone().value(),
                                inputTCPOptions.clone().value(),
                                inputTCPPayload.clone().value(),
                            );
                            //Obtenemos el json
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                "0x00;".to_string(),
                            );

                            let paqueteTCPString = self.json_To_TCP(
                                self.campoTCPSourcePort.clone(),
                                self.campoTCPDestPort.clone(),
                                self.campoTCPSequenceNum.clone(),
                                self.campoTCPACK.clone(),
                                self.campoTCPOffset.clone(),
                                self.campoTCPReserved.clone(),
                                self.campoTCPFlags.clone(),
                                self.campoTCPWindow.clone(),
                                self.campoTCPChecksum.clone(),
                                self.campoTCPUrgPtr.clone(),
                                self.campoTCPOptions.clone(),
                                self.campoTCPPayload.clone(),
                            );

                            let paquete = self.ip_from_json(paqueteString);
                            let paqTCP = self.tcp_from_json(paqueteTCPString);

                            //Procedemos a calcular checksum

                            let mut valAux: u16 = 0;
                            let mut nuevoTam = 0;
                            let mut co = inputHeaderChecksum.clone();
                            let mut camLen = inputLength.clone();
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    //Si el paquete IP está bien, buscamos como generar su checksum
                                    let mut nuevoResultadoIP = (*resultadoIP).clone();
                                    match paqTCP {
                                        Ok(ref resultadoTCP) => {
                                            //El paquete ICMP se crea correctamente
                                            let sendPacketTCP = (*resultadoTCP).clone(); //Obtenemos el paquete ICMP
                                            let duplaTCP = sendPacketTCP.tcpToPayload(
                                                nuevoResultadoIP.get_SourceAddress(),
                                                nuevoResultadoIP.get_DestinationAddress(),
                                            );
                                            let nuevoPayloadIP = duplaTCP.0;
                                            nuevoTam = 500;
                                            self.tamanioModificado = 500;

                                            nuevoResultadoIP.set_HexPayload(nuevoPayloadIP); //Actualizamos el payload
                                            nuevoResultadoIP.set_Length(nuevoTam); //Actualizamos el tamaño total

                                            self.packageIP = nuevoResultadoIP.clone(); //Actualizamos el paquete IP al actual
                                            valAux = self.obtener_ChecksumIP(); //Sacamos la IP con unwrap
                                            let valorChecksum = valAux;
                                            //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                            let valorStr = valorChecksum.to_string();
                                            let nuevoValorStr = valorStr.as_str();
                                            co.set_value(nuevoValorStr);

                                            let nuevoTamStr = nuevoTam.to_string();
                                            let nuevoTamString = nuevoTamStr.as_str();
                                            camLen.set_value(nuevoTamString);
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                            self.clone().popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "1" => {
                            //Enviar paquete entero
                            //Guardamos los datos
                            self.save_FieldsTCP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputTCPSequence.clone().value(),
                                inputTCPAck.clone().value(),
                                inputDataOffset.clone().value(),
                                inputReserved.clone().value(),
                                urg.choice().unwrap(),
                                ack.choice().unwrap(),
                                psh.choice().unwrap(),
                                rst.choice().unwrap(),
                                syn.choice().unwrap(),
                                fin.choice().unwrap(),
                                inputTCPWindow.clone().value(),
                                inputTCPChecksum.clone().value(),
                                inputTCPUrgentPointer.clone().value(),
                                inputTCPOptions.clone().value(),
                                inputTCPPayload.clone().value(),
                            );

                            //Obtenemos el json de IP
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                "0x00;".to_string(),
                            );

                            let paqueteTCPString = self.json_To_TCP(
                                self.campoTCPSourcePort.clone(),
                                self.campoTCPDestPort.clone(),
                                self.campoTCPSequenceNum.clone(),
                                self.campoTCPACK.clone(),
                                self.campoTCPOffset.clone(),
                                self.campoTCPReserved.clone(),
                                self.campoTCPFlags.clone(),
                                self.campoTCPWindow.clone(),
                                self.campoTCPChecksum.clone(),
                                self.campoTCPUrgPtr.clone(),
                                self.campoTCPOptions.clone(),
                                self.campoTCPPayload.clone(),
                            );

                            //Creamos el paquete IP
                            let paquete = self.ip_from_json(paqueteString);
                            //Obtiene un paquete TCP con los datos que hay actualmente en la interfaz
                            let paqTCP = self.tcp_from_json(paqueteTCPString);
                            let clon = Arc::new(Mutex::new(self.clone()));
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    let pac = (*resultadoIP).clone();
                                    match paqTCP {
                                        Ok(ref resultadoTCP) => {
                                            //EL paquete TCP se crea correctamente
                                            let mut sendPacketTCP = (*resultadoTCP).clone(); //Obtenemos el paquete ICMP
                                            sendPacketTCP.set_IPPacket(pac); //Guardamos el paquete IP en el paquete ICMP
                                            wind.redraw();
                                            app.redraw();
                                            clon.lock().unwrap().sendPackageTCP(sendPacketTCP);
                                            //Lo enviamos
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText = Arc::new(Mutex::new(msg.as_str()));

                                            wind.redraw();
                                            app.redraw();
                                            clon.lock()
                                                .unwrap()
                                                .clone()
                                                .popup_Mensaje(tipoMen, menText);
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new(msg.as_str()));

                                    wind.redraw();
                                    app.redraw();
                                    clon.lock().unwrap().clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }

                        "BotonPayloadTCP" => {
                            //Generar checksum de TCP
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputTCPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }

                            //let appPayl = app::App::default();
                            let mut windPayl =
                                Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)

                            let mut butActualizar =
                                Button::new(510, 630, 50, 50, "Actualiza\nTexto");
                            let mut butActInput = Button::new(510, 630, 50, 50, "Actualiza\nHex");

                            flex = Flex::new(0, 0, 70, 700, None).column();
                            {
                                let mut grid = Grid::default_fill();
                                grid.debug(false);
                                grid.set_layout(25, 1);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelOffset = Frame::default();
                                labelOffset.set_label("Offset");
                                grid.insert_ext(&mut labelOffset, 0, 0, 1, 2); // Titulo Offset
                                grid.insert_ext(&mut butActualizar, 22, 0, 1, 2);

                                let mut i: i8 = 0;
                                let mut varCol = 1;
                                for mut i in 1..11 {
                                    let mut a = String::from("00");
                                    let ar: u8 = i * 10;
                                    let arg = ar.to_string();
                                    a.push_str(arg.as_str());
                                    let mut offsetCol = Frame::default();
                                    offsetCol.set_label(a.as_str());
                                    grid.insert(&mut offsetCol, varCol + 1, 0); // Titulo Offset

                                    varCol += 1;
                                }

                                flex.end();
                            }
                            columnaMedio = Flex::new(70, 0, 430, 700, None).column();
                            {
                                let mut tablaMitad = Flex::default_fill().row();
                                {
                                    gridMedio = Grid::default_fill();
                                    gridMedio.debug(false);
                                    gridMedio.set_layout(25, 17);
                                    let mut labelHexadecimal = Frame::default();
                                    labelHexadecimal.set_label("Texto Hexadecimal");
                                    gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 2); // Titulo Hexadecimal

                                    for y in 1..24 {
                                        //10 columnas * 20 de distancia
                                        for x in 0..17 {
                                            // 16 filas * 20 de distancia entre cada una
                                            if x != 8 {
                                                let mut valorInputHexa = "";

                                                //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload

                                                if !vectorMiembrosPayload.is_empty() {
                                                    //Hay elementos aun en el vector
                                                    let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                                    valorInputHexa = valorAux;
                                                } else {
                                                    //Ya se ha gastado el vector
                                                    valorInputHexa = "00"; //0 como valor por defecto
                                                }
                                                // Primero creamos el input
                                                let mut inputHexa = Input::default();
                                                inputHexa.set_value(valorInputHexa);
                                                //Por ultimo lo metemos en la interfaz
                                                gridMedio.insert(&mut inputHexa, y + 1, x);
                                                //Despues lo añadimos a la lista de inputs
                                                but_input.insert((y + 1, x), inputHexa);

                                                &(map_input).insert(
                                                    (y + 1, x),
                                                    String::from(valorInputHexa),
                                                );
                                            }
                                        }
                                    }

                                    tablaMitad.end();
                                }

                                columnaMedio.end();
                            }

                            columnaFinal = Flex::new(500, 0, 200, 700, None).column();
                            {
                                let mut gridFinal = Grid::default_fill();
                                gridFinal.debug(false);
                                gridFinal.set_layout(25, 5);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelSignificado = Frame::default();
                                labelSignificado.set_label("Significado");
                                gridFinal.insert_ext(&mut labelSignificado, 0, 0, 5, 2); // Titulo Offset

                                let wrap = WrapMode::AtBounds;
                                let mut buff = TextBuffer::default();
                                casillaSignificado.wrap_mode(wrap, 0); //El limite de texto es 100, es decir, el limite de columna
                                buff.set_text(
                                    "Pulse actualizar para revelar el payload por defecto",
                                );
                                casillaSignificado.set_buffer(buff);
                                gridFinal.insert_ext(&mut casillaSignificado, 2, 0, 5, 20); //  Label de transformacion

                                let mut save = Button::new(510, 630, 50, 50, "Guardar"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                gridFinal.insert_ext(&mut save, 22, 3, 2, 2); //  Label de transformacion
                                gridFinal.insert_ext(&mut butActInput, 22, 0, 2, 2);

                                butActualizar.set_callback(move |_| {
                                    s.send("AC");
                                });
                                butActInput.set_callback(move |_| {
                                    s.send("TEXT");
                                });
                                save.set_callback(move |_| {
                                    s.send("SAVE");
                                });
                                columnaFinal.end();
                            }
                            windPayl.make_resizable(true);
                            windPayl.end();
                            windPayl.show();

                            // the closure capture is mutable borrow to our button

                            //appPayl.run().unwrap();
                        }
                        // Y a partir de aqui se encuentra el comportamiento de los botones de la interfaz de edición del payload:
                        "AC" => {
                            //Actualiza Hexadecimal a Texto

                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            let string = p
                                .lock()
                                .unwrap()
                                .clone()
                                .actualizaLenguajeNatural(map_input.clone());

                            let mut buff = TextBuffer::default(); //Creamos un buffer para meterlo en el texto
                            buff.set_text(string.as_str()); //Metemos texto en buffer
                            casillaSignificado.set_buffer(buff); //Metemos buffer en la casilla que ocupa
                        }
                        "SAVE" => {
                            //Guarda datos en el input de la interfaz completa
                            //Procedemos a guardar todo en el input Payload del paquete IP
                            //Para ello, guardaremos todos los datos de los inputs en un vector

                            let mut vecPayl = Vec::new();

                            //Debemos actualizar los datos en caso de que se hayan modificado
                            let copiaMapa = but_input.clone();
                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            //Una vez editados, se extraen al vector
                            let mut concatenacion = "";
                            let mut auxiStr = String::new();
                            let iterador = map_input.iter().sorted(); //Necesitamos orden
                                                                      //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                            for (key, val) in iterador {
                                vecPayl.push(val);
                            }
                            //Una vez tenemos [00,00,00,...]

                            for val in vecPayl {
                                auxiStr.push_str("0x");
                                auxiStr.push_str(val.as_str());
                                auxiStr.push_str(";");
                                //El resultado queda como "0x00;"
                            }

                            concatenacion = auxiStr.as_str();
                            //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                            //Metemos dicho valor en el hexaPayload
                            inputTCPPayload.set_value(concatenacion);
                        }
                        "TEXT" => {
                            //Actualiza los inputs Hexadecimales a partir del texto ASCII
                            //Obtenemos el valor del texto en el momento
                            let mut textoBuf: TextBuffer = TextBuffer::default();
                            match casillaSignificado.buffer() {
                                Some(opcion) => {
                                    textoBuf = opcion;
                                }
                                None => (),
                            };
                            let textoCasilla = textoBuf.text();
                            //Obtenido el texto, lo transformamos a hexadecimal
                            let textoHexa = textoCasilla
                                .as_str()
                                .as_bytes()
                                .iter()
                                .map(|x| format!("{:02x}", x))
                                .collect::<String>();
                            //Ese hexadecimal a vector de caracteres, para introducirlos en los inputs
                            let mut vectorChar: Vec<char> = textoHexa.chars().collect();

                            //Iteramos sobre el mapa de inputs para cambiar sus valores por los nuevos
                            let iterador = map_input.iter().sorted(); //Necesitamos orden, asi que usamos el orden del mapa de Strings

                            for (key, val) in iterador {
                                //Recorremos y modificamos
                                if !vectorChar.is_empty() {
                                    //95
                                    let mostsb = vectorChar.remove(0); //9
                                    let lestsb = vectorChar.remove(0); //5

                                    let mut duplaHexadecimal = String::from(mostsb);
                                    let copiales = String::from(lestsb);
                                    duplaHexadecimal.push_str(copiales.as_str());

                                    //Obtenido el dato, lo introducimos en la casilla correspondiente
                                    let mut inputAux = Input::default();
                                    inputAux.set_value(duplaHexadecimal.as_str());
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux); //Aunque usemos el mapa de strings nos interesan las key
                                } else {
                                    //Una vez vaciado el vector, se llenan los inputs con 0
                                    let mut inputAux = Input::default();
                                    inputAux.set_value("00");
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux);
                                }
                            }
                        }
                        _ => {}
                    }
                }
                None => {}
            }
        }
    }

    /**@in :
     * @out:
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete UDP
     */
    pub fn interfaceUDP(mut self) {
        let app = app::App::default();
        let mut wind = Window::default()
            .with_size(900, 450) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 4");
        //Definimos la interfaz
        let titulo = Frame::new(0, 0, 900, 50, "UDP");

        //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
        //Los hacemos mutable para cambiar sus valores
        //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
        //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

        self.refresh_Interface(4);

        //UDP
        let mut inputSourcePort = Input::new(250, 50, 75, 25, "Source Port"); //X,Y,ANCHO, ALTO, TITULO
        inputSourcePort.set_value(self.campoUDPSourcePort.as_str());
        let mut helpSourcePort: Listener<_> = Button::new(335, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSourcePort.set_color(Color::Yellow);

        let mut inputDestPort = Input::new(620, 50, 75, 25, "Destination Port"); //X,Y,ANCHO, ALTO, TITULO
        inputDestPort.set_value(self.campoUDPDestPort.as_str());
        let mut helpDestPort: Listener<_> = Button::new(705, 50, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDestPort.set_color(Color::Yellow);

        let mut inputUDPLength = Input::new(250, 95, 100, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
        inputUDPLength.set_value(self.campoUDPLength.as_str());
        let mut helpUDPLen: Listener<_> = Button::new(360, 95, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpUDPLen.set_color(Color::Yellow);

        let mut inputUDPChecksum = Input::new(620, 95, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputUDPChecksum.set_value(self.campoUDPChecksum.as_str());
        let mut helpUDPChecksum: Listener<_> = Button::new(740, 95, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpUDPChecksum.set_color(Color::Yellow);

        let mut generaChecksumUDP: Listener<_> =
            Button::new(790, 90, 80, 40, "Generar\nChecksum").into();

        let mut inputUDPPayload = Input::new(350, 150, 200, 25, "Payload"); //X,Y,ANCHO, ALTO, TITULO
        inputUDPPayload.set_value(self.campoUDPPayload.as_str());
        inputUDPPayload.set_readonly(true);
        let mut helpUDPPayl: Listener<_> = Button::new(560, 150, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpUDPPayl.set_color(Color::Yellow);

        let mut buttonPaylUDP: Listener<_> = Button::new(610, 145, 80, 40, "Payload").into();
        let mut helpPayUDP: Listener<_> = Button::new(700, 145, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPayUDP.set_color(Color::Yellow);
        //set_draw_color(Color::White);
        //draw_line(0, 250, 900, 250);
        //IP
        let subtitulo = Frame::new(0, 200, 900, 50, "IP");
        let mut inputVersion = Input::new(65, 250, 75, 25, "Version"); //X,Y,ANCHO, ALTO, TITULO
        inputVersion.set_value(self.campoVersion.as_str());
        let mut helpVersion: Listener<_> = Button::new(150, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpVersion.set_color(Color::Yellow);

        let mut inputIHL = Input::new(215, 250, 75, 25, "IHL"); //X,Y,ANCHO, ALTO, TITULO
        inputIHL.set_value(self.campoIHL.as_str());
        let mut helpIHL: Listener<_> = Button::new(300, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpIHL.set_color(Color::Yellow);

        let mut inputTypeOfServ = Input::new(435, 250, 75, 25, "TypeOfService"); //X,Y,ANCHO, ALTO, TITULO
        inputTypeOfServ.set_value(self.campoTypeOfService.as_str());
        let mut helpTOS: Listener<_> = Button::new(520, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTOS.set_color(Color::Yellow);

        let mut inputECN = Input::new(595, 250, 75, 25, "ECN"); //X,Y,ANCHO, ALTO, TITULO
        inputECN.set_value(self.campoECN.as_str());
        let mut helpECN: Listener<_> = Button::new(680, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpECN.set_color(Color::Yellow);

        let mut inputLength = Input::new(775, 250, 75, 25, "Length"); //X,Y,ANCHO, ALTO, TITULO
        inputLength.set_value(self.campoLength.as_str());
        let mut helpLen: Listener<_> = Button::new(865, 250, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpLen.set_color(Color::Yellow);

        let mut inputID = Input::new(65, 300, 175, 25, "ID"); //X,Y,ANCHO, ALTO, TITULO
        inputID.set_value(self.campoID.as_str());
        let mut helpID: Listener<_> = Button::new(265, 300, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpID.set_color(Color::Yellow);

        let mut inputFlags = Input::new(355, 300, 30, 25, "Flags"); //X,Y,ANCHO, ALTO, TITULO
        inputFlags.set_value("0");
        inputFlags.set_readonly(true);
        let mut helpFlags: Listener<_> = Button::new(520, 300, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpFlags.set_color(Color::Yellow);

        let mut msb = Choice::new(400, 300, 40, 25, None); //1
        msb.add_choice("0");
        msb.add_choice("1");
        msb.set_value(1); //Posicion 1 de la lista

        let mut lsb = Choice::new(450, 300, 40, 25, None); //0
        lsb.add_choice("0");
        lsb.add_choice("1");
        lsb.set_value(0); //Posicion 0

        let mut inputFragmentOff = Input::new(685, 300, 130, 25, "FragmentOffset"); //X,Y,ANCHO, ALTO, TITULO
        inputFragmentOff.set_value(self.campoFragmentOffSet.as_str());
        let mut helpOffSet: Listener<_> = Button::new(830, 300, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpOffSet.set_color(Color::Yellow);

        let mut inputTTL = Input::new(65, 350, 100, 25, "TTL"); //X,Y,ANCHO, ALTO, TITULO
        inputTTL.set_value(self.campoTTL.as_str());
        let mut helpTTL: Listener<_> = Button::new(175, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpTTL.set_color(Color::Yellow);

        let mut inputProtocol = Input::new(345, 350, 100, 25, "Protocol"); //X,Y,ANCHO, ALTO, TITULO
        inputProtocol.set_value(self.campoProtocol.to_uppercase().as_str());
        let mut helpPro: Listener<_> = Button::new(460, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpPro.set_color(Color::Yellow);

        let mut inputHeaderChecksum = Input::new(630, 350, 100, 25, "Checksum"); //X,Y,ANCHO, ALTO, TITULO
        inputHeaderChecksum.set_value(self.campoHeaderChecksum.as_str());
        let mut helpChecksum: Listener<_> = Button::new(740, 350, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpChecksum.set_color(Color::Yellow);

        let mut generaChecksum: Listener<_> =
            Button::new(785, 345, 80, 40, "Generar\nChecksum").into();

        let mut inputSourceAdd = Input::new(125, 405, 200, 25, "Source Address"); //X,Y,ANCHO, ALTO, TITULO
        inputSourceAdd.set_value(self.campoSourceAddress.as_str());
        let mut helpSource: Listener<_> = Button::new(340, 405, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpSource.set_color(Color::Yellow);

        let mut inputDestAdd = Input::new(550, 405, 200, 25, "Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(self.campoDestinationAddress.as_str());
        let mut helpDest: Listener<_> = Button::new(760, 405, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDest.set_color(Color::Yellow);

        let mut sendP: Listener<_> = Button::new(810, 400, 80, 40, "Enviar").into();
        wind.make_resizable(true);
        wind.end();
        wind.show();

        let (s, r) = app::channel::<&str>();

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        let clonPayload = Arc::new(Mutex::new(self.clone()));
        let clonObtencion = Arc::new(Mutex::new(self.clone()));

        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
                                                                        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
        //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
        let mut flex: Flex;
        let mut gridMedio: Grid = Grid::default_fill();
        let mut columnaMedio: Flex;
        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla
        let mut columnaFinal: Flex;
        let mut windoAux = Window::default();
        let mut windo = Window::default();

        // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
        // Cuando la app lo recoge , lo muestra
        helpVersion.on_click(move |b| {
            s.send("Version");
        });

        helpIHL.on_click(move |b| {
            s.send("IHL");
        });

        helpTOS.on_click(move |b| {
            s.send("TypeOfService");
        });

        helpECN.on_click(move |b| {
            s.send("ECN");
        });

        helpLen.on_click(move |b| {
            s.send("Length");
        });

        helpID.on_click(move |b| {
            s.send("ID");
        });

        helpFlags.on_click(move |b| {
            s.send("Flags");
        });

        helpOffSet.on_click(move |b| {
            s.send("FragmentOffset");
        });

        helpTTL.on_click(move |b| {
            s.send("TTL");
        });

        helpPro.on_click(move |b| {
            s.send("Protocol");
        });

        helpChecksum.on_click(move |b| {
            s.send("HeaderChecksum");
        });

        helpSource.on_click(move |b| {
            s.send("Source");
        });

        helpDest.on_click(move |b| {
            s.send("Destination");
        });

        generaChecksum.set_callback(move |b| {
            s.send("0");
        });
        sendP.on_click(move |b| {
            s.send("1");
        });

        helpSourcePort.on_click(move |b| {
            s.send("PuertoOrigen");
        });
        helpDestPort.on_click(move |b| {
            s.send("PuertoDestino");
        });
        helpUDPLen.on_click(move |b| {
            s.send("UDPLength");
        });

        helpUDPChecksum.on_click(move |b| {
            s.send("UDPChecksum");
        });
        generaChecksumUDP.on_click(move |b| {
            s.send("ChecksumUDP");
        });

        helpUDPPayl.on_click(move |b| {
            s.send("UDPPayload");
        });
        buttonPaylUDP.on_click(move |b| {
            //Boton Payload
            s.send("BotonPayloadUDP");
        });

        helpPayUDP.on_click(move |b| {
            //ayuda boton payload
            s.send("UDPBotonPayload");
        });

        while app.wait() {
            match r.recv() {
                Some(variableAux) => {
                    let helpS = variableAux;
                    let stringAux = clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let stringAuxUDP = clonAyuda.lock().unwrap().field_Help(4, helpS.to_string());
                    let strHelpUDP = stringAuxUDP.as_str();

                    let p = clonObtencion.clone();
                    let mut copiaMapa = but_input.clone();

                    match helpS {
                        "Version" => {
                            helpVersion.set_tooltip(strHelp);
                            helpVersion.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "IHL" => {
                            helpIHL.set_tooltip(strHelp);
                            helpIHL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TypeOfService" => {
                            helpTOS.set_tooltip(strHelp);
                            helpTOS.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ECN" => {
                            helpECN.set_tooltip(strHelp);
                            helpECN.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Length" => {
                            helpLen.set_tooltip(strHelp);
                            helpLen.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ID" => {
                            helpID.set_tooltip(strHelp);
                            helpID.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Flags" => {
                            helpFlags.set_tooltip(strHelp);
                            helpFlags.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "FragmentOffset" => {
                            helpOffSet.set_tooltip(strHelp);
                            helpOffSet.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "TTL" => {
                            helpTTL.set_tooltip(strHelp);
                            helpTTL.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Protocol" => {
                            helpPro.set_tooltip(strHelp);
                            helpPro.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HeaderChecksum" => {
                            helpChecksum.set_tooltip(strHelp);
                            helpChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Source" => {
                            helpSource.set_tooltip(strHelp);
                            helpSource.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "PuertoOrigenUDP" => {
                            helpSourcePort.set_tooltip(strHelpUDP);
                            helpSourcePort.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "PuertoDestinoUDP" => {
                            helpDestPort.set_tooltip(strHelpUDP);
                            helpDestPort.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "UDPLength" => {
                            helpUDPLen.set_tooltip(strHelpUDP);
                            helpUDPLen.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "UDPChecksum" => {
                            helpUDPChecksum.set_tooltip(strHelpUDP);
                            helpUDPChecksum.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "UDPPayload" => {
                            helpUDPPayl.set_tooltip(strHelpUDP);
                            helpUDPPayl.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "UDPBotonPayload" => {
                            helpPayUDP.set_tooltip(strHelpUDP);
                            helpPayUDP.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "ChecksumUDP" => {
                            //Generar checksum de UDP
                            //Guardamos los datos
                            self.save_FieldsUDP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputUDPLength.clone().value(),
                                inputUDPChecksum.clone().value(),
                                inputUDPPayload.clone().value(),
                            );

                            //Convertimos los valores de IP en la interfaz en el momento de guardar
                            let copiaSo = self.campoSourceAddress.clone();
                            let copiaDe = self.campoDestinationAddress.clone();
                            let vecSor: Vec<&str> = copiaSo.split(".").collect();
                            let vecDes: Vec<&str> = copiaDe.split(".").collect();
                            let paqueteUDPString = self.json_To_UDP(
                                self.campoUDPSourcePort.clone(),
                                self.campoUDPDestPort.clone(),
                                self.campoUDPLength.clone(),
                                self.campoUDPChecksum.clone(),
                                self.campoUDPPayload.clone(),
                            );

                            //Creamos el paquete IP
                            if vecSor.len() == 4 && vecDes.len() == 4 {
                                //Obtiene un paquete TCP con los datos que hay actualmente en la interfaz
                                let paqTCP = self.udp_from_json(paqueteUDPString);
                                let mut co = inputUDPChecksum.clone();
                                let mut valAux: u16 = 0;
                                let clon = Arc::new(Mutex::new(self.clone()));

                                let bit1So: Result<u8, String> = match vecSor[0].parse::<u8>() {
                                    //127
                                    Ok(bit1) => Ok(bit1),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit1So = bit1So.unwrap();

                                let bit2So: Result<u8, String> = match vecSor[1].parse::<u8>() {
                                    //0
                                    Ok(bit2) => Ok(bit2),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit2So = bit2So.unwrap();

                                let bit3So: Result<u8, String> = match vecSor[2].parse::<u8>() {
                                    //0
                                    Ok(bit3) => Ok(bit3),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit3So = bit3So.unwrap();

                                let bit4So: Result<u8, String> = match vecSor[3].parse::<u8>() {
                                    //1
                                    Ok(bit4) => Ok(bit4),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit4So = bit4So.unwrap();

                                let mut sours = Ipv4Addr::new(biit1So, biit2So, biit3So, biit4So);

                                let bit1De: Result<u8, String> = match vecDes[0].parse::<u8>() {
                                    //127
                                    Ok(bit1) => Ok(bit1),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit1De = bit1De.unwrap();

                                let bit2De: Result<u8, String> = match vecDes[1].parse::<u8>() {
                                    //0
                                    Ok(bit2) => Ok(bit2),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit2De = bit2De.unwrap();

                                let bit3De: Result<u8, String> = match vecDes[2].parse::<u8>() {
                                    //0
                                    Ok(bit3) => Ok(bit3),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit3De = bit3De.unwrap();

                                let bit4De: Result<u8, String> = match vecDes[3].parse::<u8>() {
                                    //1
                                    Ok(bit4) => Ok(bit4),
                                    //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado
                                    Err(e) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        break;
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                };
                                let biit4De = bit4De.unwrap();

                                let mut dests = Ipv4Addr::new(biit1De, biit2De, biit3De, biit4De);

                                match paqTCP {
                                    Ok(ref resultadoUDP) => {
                                        //Si el paquete está bien, buscamos como generar su checksum
                                        let paqueUDP = resultadoUDP.clone();
                                        self.packageUDP = paqueUDP;
                                        valAux = self.obtener_ChecksumUDP(sours, dests); //Sacamos la IP con unwrap

                                        let valorChecksum = valAux;
                                        //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                        let valorStr = valorChecksum.to_string();
                                        let nuevoValorStr = valorStr.as_str();
                                        co.set_value(nuevoValorStr);
                                    }
                                    Err(msg) => {
                                        let tipoMen = "ERROR".to_string();
                                        let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                        self.clone().popup_Mensaje(tipoMen, menText);
                                        // COn unwrap_err() sacamos el Err , osea, el string
                                    }
                                }
                            } else {
                                let tipoMen = "ERROR".to_string();
                                let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                self.clone().popup_Mensaje(tipoMen, menText);
                                // COn unwrap_err() sacamos el Err , osea, el string
                            }
                        }
                        "0" => {
                            //Checksum IP
                            //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                            self.save_FieldsUDP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputUDPLength.clone().value(),
                                inputUDPChecksum.clone().value(),
                                inputUDPPayload.clone().value(),
                            );
                            //Obtenemos el json
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                "0x00;".to_string(),
                            );

                            let paqueteUDPString = self.json_To_UDP(
                                self.campoUDPSourcePort.clone(),
                                self.campoUDPDestPort.clone(),
                                self.campoUDPLength.clone(),
                                self.campoUDPChecksum.clone(),
                                self.campoUDPPayload.clone(),
                            );

                            let paquete = self.ip_from_json(paqueteString);
                            let paqUDP = self.udp_from_json(paqueteUDPString);

                            //Procedemos a calcular checksum

                            let mut valAux: u16 = 0;
                            let mut nuevoTam = 0;
                            let mut co = inputHeaderChecksum.clone();
                            let mut camLen = inputLength.clone();
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    //Si el paquete IP está bien, buscamos como generar su checksum
                                    let mut nuevoResultadoIP = (*resultadoIP).clone();
                                    match paqUDP {
                                        Ok(ref resultadoUDP) => {
                                            //El paquete ICMP se crea correctamente
                                            let sendPacketUDP = (*resultadoUDP).clone(); //Obtenemos el paquete ICMP
                                            let duplaUDP = sendPacketUDP.udpToPayload(
                                                nuevoResultadoIP.get_SourceAddress(),
                                                nuevoResultadoIP.get_DestinationAddress(),
                                            );
                                            let nuevoPayloadIP = duplaUDP.0;
                                            let tamSumar = (duplaUDP.1 as u16) * 10;
                                            let tamAntiguo = nuevoResultadoIP.get_Length();

                                            nuevoTam = 500;
                                            self.tamanioModificado = 500;

                                            nuevoResultadoIP.set_HexPayload(nuevoPayloadIP); //Actualizamos el payload
                                            nuevoResultadoIP.set_Length(nuevoTam); //Actualizamos el tamaño total

                                            self.packageIP = nuevoResultadoIP.clone(); //Actualizamos el paquete IP al actual
                                            valAux = self.obtener_ChecksumIP(); //Sacamos la IP con unwrap
                                            let valorChecksum = valAux;
                                            //println!("EL valor al obtener el checksum es : {}",valorChecksum);
                                            let valorStr = valorChecksum.to_string();
                                            let nuevoValorStr = valorStr.as_str();
                                            co.set_value(nuevoValorStr);

                                            let nuevoTamStr = nuevoTam.to_string();
                                            let nuevoTamString = nuevoTamStr.as_str();
                                            camLen.set_value(nuevoTamString);
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                            self.clone().popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText =Arc::new(Mutex::new( "Error al generar el Checksum.\nAsegurese de  introducir todos los datos"));
                                    self.clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        "1" => {
                            //Enviar paquete entero
                            //Guardamos los datos
                            self.save_FieldsUDP(
                                inputVersion.clone().value(),
                                inputIHL.clone().value(),
                                inputTypeOfServ.clone().value(),
                                inputECN.clone().value(),
                                inputLength.clone().value(),
                                inputID.clone().value(),
                                inputFlags.clone().value(),
                                msb.clone().value().to_string(),
                                lsb.clone().value().to_string(),
                                inputFragmentOff.clone().value(),
                                inputTTL.clone().value(),
                                inputProtocol.clone().value(),
                                inputHeaderChecksum.clone().value(),
                                inputSourceAdd.clone().value(),
                                inputDestAdd.clone().value(),
                                inputSourcePort.clone().value(),
                                inputDestPort.clone().value(),
                                inputUDPLength.clone().value(),
                                inputUDPChecksum.clone().value(),
                                inputUDPPayload.clone().value(),
                            );
                            //Obtenemos el json de IP
                            let paqueteString = self.json_To_IP(
                                self.campoVersion.clone(),
                                self.campoIHL.clone(),
                                self.campoTypeOfService.clone(),
                                self.campoECN.clone(),
                                self.campoLength.clone(),
                                self.campoID.clone(),
                                self.campoFlags.clone(),
                                self.campoFragmentOffSet.clone(),
                                self.campoTTL.clone(),
                                self.campoProtocol.clone(),
                                self.campoHeaderChecksum.clone(),
                                self.campoSourceAddress.clone(),
                                self.campoDestinationAddress.clone(),
                                self.campoOptions.clone(),
                                "0x00;".to_string(),
                            );

                            let paqueteUDPString = self.json_To_UDP(
                                self.campoUDPSourcePort.clone(),
                                self.campoUDPDestPort.clone(),
                                self.campoUDPLength.clone(),
                                self.campoUDPChecksum.clone(),
                                self.campoUDPPayload.clone(),
                            );

                            //Creamos el paquete IP
                            let paquete = self.ip_from_json(paqueteString);
                            //Obtiene un paquete TCP con los datos que hay actualmente en la interfaz
                            let paqUDP = self.udp_from_json(paqueteUDPString);
                            let clon = Arc::new(Mutex::new(self.clone()));
                            match paquete {
                                Ok(ref resultadoIP) => {
                                    let pac = (*resultadoIP).clone();
                                    match paqUDP {
                                        Ok(ref resultadoUDP) => {
                                            //EL paquete TCP se crea correctamente
                                            let mut sendPacketUDP = (*resultadoUDP).clone(); //Obtenemos el paquete ICMP
                                            sendPacketUDP.set_IPPacket(pac); //Guardamos el paquete IP en el paquete ICMP
                                            wind.redraw();
                                            app.redraw();
                                            clon.lock().unwrap().sendPackageUDP(sendPacketUDP);
                                            //Lo enviamos
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText = Arc::new(Mutex::new(msg.as_str()));

                                            wind.redraw();
                                            app.redraw();
                                            clon.lock()
                                                .unwrap()
                                                .clone()
                                                .popup_Mensaje(tipoMen, menText);
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new(msg.as_str()));

                                    wind.redraw();
                                    app.redraw();
                                    clon.lock().unwrap().clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }

                        "BotonPayloadUDP" => {
                            //Generar checksum de TCP
                            // Creamos una interfaz que permitirá editar el payload de forma mas comoda
                            let copiaPayloadHexadecimal = inputUDPPayload.value(); //Aqui tenemos el "0x00;..."
                            let mut vectorMiembrosPayload = Vec::new();

                            if copiaPayloadHexadecimal != "" {
                                let separador: Vec<&str> =
                                    copiaPayloadHexadecimal.split(";").collect(); //Cada elemento es "0x95"

                                //Teniendo ese vector, iteramos sobre el
                                for var in separador {
                                    //  0x95
                                    if var != "" {
                                        //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                                        let val: Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                                        vectorMiembrosPayload.push(val[1]);
                                    }
                                }
                                //Una vez acabada la iteracion , vectorMiembrosPayload tiene [00,01,9A,...]
                            }

                            //let appPayl = app::App::default();
                            let mut windPayl =
                                Window::new(100, 100, 700, 700, "Edicion del Payload"); // Lugar de aparicion(x,y) , Tamaño (x,y)

                            let mut butActualizar =
                                Button::new(510, 630, 50, 50, "Actualiza\nTexto");
                            let mut butActInput = Button::new(510, 630, 50, 50, "Actualiza\nHex");

                            flex = Flex::new(0, 0, 70, 700, None).column();
                            {
                                let mut grid = Grid::default_fill();
                                grid.debug(false);
                                grid.set_layout(25, 1);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelOffset = Frame::default();
                                labelOffset.set_label("Offset");
                                grid.insert_ext(&mut labelOffset, 0, 0, 1, 2); // Titulo Offset
                                grid.insert_ext(&mut butActualizar, 22, 0, 1, 2);

                                let mut i: i8 = 0;
                                let mut varCol = 1;
                                for mut i in 1..11 {
                                    let mut a = String::from("00");
                                    let ar: u8 = i * 10;
                                    let arg = ar.to_string();
                                    a.push_str(arg.as_str());
                                    let mut offsetCol = Frame::default();
                                    offsetCol.set_label(a.as_str());
                                    grid.insert(&mut offsetCol, varCol + 1, 0); // Titulo Offset

                                    varCol += 1;
                                }

                                flex.end();
                            }
                            columnaMedio = Flex::new(70, 0, 430, 700, None).column();
                            {
                                let mut tablaMitad = Flex::default_fill().row();
                                {
                                    gridMedio = Grid::default_fill();
                                    gridMedio.debug(false);
                                    gridMedio.set_layout(25, 17);
                                    let mut labelHexadecimal = Frame::default();
                                    labelHexadecimal.set_label("Texto Hexadecimal");
                                    gridMedio.insert_ext(&mut labelHexadecimal, 0, 1, 16, 2); // Titulo Hexadecimal

                                    for y in 1..24 {
                                        //10 columnas * 20 de distancia
                                        for x in 0..17 {
                                            // 16 filas * 20 de distancia entre cada una
                                            if x != 8 {
                                                let mut valorInputHexa = "";

                                                //A la hora de incluir los datos en el texto hexadecimal, incluiremos los datos del vector de Payload

                                                if !vectorMiembrosPayload.is_empty() {
                                                    //Hay elementos aun en el vector
                                                    let valorAux = vectorMiembrosPayload.remove(0); //Quitamos por orden
                                                    valorInputHexa = valorAux;
                                                } else {
                                                    //Ya se ha gastado el vector
                                                    valorInputHexa = "00"; //0 como valor por defecto
                                                }
                                                // Primero creamos el input
                                                let mut inputHexa = Input::default();
                                                inputHexa.set_value(valorInputHexa);
                                                //Por ultimo lo metemos en la interfaz
                                                gridMedio.insert(&mut inputHexa, y + 1, x);
                                                //Despues lo añadimos a la lista de inputs
                                                but_input.insert((y + 1, x), inputHexa);

                                                &(map_input).insert(
                                                    (y + 1, x),
                                                    String::from(valorInputHexa),
                                                );
                                            }
                                        }
                                    }

                                    tablaMitad.end();
                                }

                                columnaMedio.end();
                            }

                            columnaFinal = Flex::new(500, 0, 200, 700, None).column();
                            {
                                let mut gridFinal = Grid::default_fill();
                                gridFinal.debug(false);
                                gridFinal.set_layout(25, 5);

                                //Offset es columa 0,0 - 0,20 || Hexadecimal Payload Columna 0,1 - 2,20 || Traduccion Columna 0,3,3,20
                                let mut labelSignificado = Frame::default();
                                labelSignificado.set_label("Significado");
                                gridFinal.insert_ext(&mut labelSignificado, 0, 0, 5, 2); // Titulo Offset

                                let wrap = WrapMode::AtBounds;
                                let mut buff = TextBuffer::default();
                                casillaSignificado.wrap_mode(wrap, 0); //El limite de texto es 100, es decir, el limite de columna
                                buff.set_text(
                                    "Pulse actualizar para revelar el payload por defecto",
                                );
                                casillaSignificado.set_buffer(buff);
                                gridFinal.insert_ext(&mut casillaSignificado, 2, 0, 5, 20); //  Label de transformacion

                                let mut save = Button::new(510, 630, 50, 50, "Guardar"); // Lugar de aparicion(x,y) , Tamaño (x,y)
                                gridFinal.insert_ext(&mut save, 22, 3, 2, 2); //  Label de transformacion
                                gridFinal.insert_ext(&mut butActInput, 22, 0, 2, 2);

                                butActualizar.set_callback(move |_| {
                                    s.send("AC");
                                });
                                butActInput.set_callback(move |_| {
                                    s.send("TEXT");
                                });
                                save.set_callback(move |_| {
                                    s.send("SAVE");
                                });
                                columnaFinal.end();
                            }
                            windPayl.make_resizable(true);
                            windPayl.end();
                            windPayl.show();

                            // the closure capture is mutable borrow to our button

                            //appPayl.run().unwrap();
                        }
                        // Y a partir de aqui se encuentra el comportamiento de los botones de la interfaz de edición del payload:
                        "AC" => {
                            //Actualiza Hexadecimal a Texto

                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            let string = p
                                .lock()
                                .unwrap()
                                .clone()
                                .actualizaLenguajeNatural(map_input.clone());

                            let mut buff = TextBuffer::default(); //Creamos un buffer para meterlo en el texto
                            buff.set_text(string.as_str()); //Metemos texto en buffer
                            casillaSignificado.set_buffer(buff); //Metemos buffer en la casilla que ocupa
                        }
                        "SAVE" => {
                            //Guarda datos en el input de la interfaz completa
                            //Procedemos a guardar todo en el input Payload del paquete IP
                            //Para ello, guardaremos todos los datos de los inputs en un vector

                            let mut vecPayl = Vec::new();

                            //Debemos actualizar los datos en caso de que se hayan modificado
                            let copiaMapa = but_input.clone();
                            for (key, but) in copiaMapa {
                                let varAux: String = map_input.get_mut(&key).unwrap().to_string();
                                let auxBut = but.value();
                                if auxBut != varAux.as_str() {
                                    // Si el valor actual no es igual que el anterior, ha cambiado, se actualiza
                                    let insercion = auxBut.as_str();
                                    let mut insercion = insercion.to_string();

                                    map_input.insert(key, insercion);
                                }
                            }
                            //Una vez editados, se extraen al vector
                            let mut concatenacion = "";
                            let mut auxiStr = String::new();
                            let iterador = map_input.iter().sorted(); //Necesitamos orden
                                                                      //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                            for (key, val) in iterador {
                                vecPayl.push(val);
                            }
                            //Una vez tenemos [00,00,00,...]

                            for val in vecPayl {
                                auxiStr.push_str("0x");
                                auxiStr.push_str(val.as_str());
                                auxiStr.push_str(";");
                                //El resultado queda como "0x00;"
                            }

                            concatenacion = auxiStr.as_str();
                            //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                            //Metemos dicho valor en el hexaPayload
                            inputUDPPayload.set_value(concatenacion);
                        }
                        "TEXT" => {
                            //Actualiza los inputs Hexadecimales a partir del texto ASCII
                            //Obtenemos el valor del texto en el momento
                            let mut textoBuf: TextBuffer = TextBuffer::default();
                            match casillaSignificado.buffer() {
                                Some(opcion) => {
                                    textoBuf = opcion;
                                }
                                None => (),
                            };
                            let textoCasilla = textoBuf.text();
                            //Obtenido el texto, lo transformamos a hexadecimal
                            let textoHexa = textoCasilla
                                .as_str()
                                .as_bytes()
                                .iter()
                                .map(|x| format!("{:02x}", x))
                                .collect::<String>();
                            //Ese hexadecimal a vector de caracteres, para introducirlos en los inputs
                            let mut vectorChar: Vec<char> = textoHexa.chars().collect();

                            //Iteramos sobre el mapa de inputs para cambiar sus valores por los nuevos
                            let iterador = map_input.iter().sorted(); //Necesitamos orden, asi que usamos el orden del mapa de Strings

                            for (key, val) in iterador {
                                //Recorremos y modificamos
                                if !vectorChar.is_empty() {
                                    //95
                                    let mostsb = vectorChar.remove(0); //9
                                    let lestsb = vectorChar.remove(0); //5

                                    let mut duplaHexadecimal = String::from(mostsb);
                                    let copiales = String::from(lestsb);
                                    duplaHexadecimal.push_str(copiales.as_str());

                                    //Obtenido el dato, lo introducimos en la casilla correspondiente
                                    let mut inputAux = Input::default();
                                    inputAux.set_value(duplaHexadecimal.as_str());
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux); //Aunque usemos el mapa de strings nos interesan las key
                                } else {
                                    //Una vez vaciado el vector, se llenan los inputs con 0
                                    let mut inputAux = Input::default();
                                    inputAux.set_value("00");
                                    //Lo modificamos en el grid, para que se vea reflejado
                                    gridMedio.remove(but_input.get(key).unwrap()); //Eliminamos los inputs
                                    gridMedio.insert(&mut inputAux, key.0, key.1);
                                    but_input.insert(*key, inputAux);
                                }
                            }
                        }
                        _ => {}
                    }
                }
                None => {}
            }
        }
    }

    /**@in :
     * @out:
     * @function: Esta funcion creará la interfaz en la que crearemos y modificaremos un paquete HTTP
     */
    pub fn interfaceHTTP(mut self) {
        let app = app::App::default();
        let mut wind = Window::default()
            .with_size(900, 550) //Ancho, Alto
            .center_screen()
            .with_label("Paquete de Capa 5");
        //Definimos atributos que necesitaremos para el fiuncionamiento correcto de la interfaz HTTP

        let mut vecCabeceraHTTP = Vec::new(); //Vector de los Inputs de Cabecera
        for i in 1..100{
            let mut input = Input::default();
            vecCabeceraHTTP.push(input);
        }
        let mut vecEntidadesHTTP = Vec::new(); //Vector de los Inputs de Entidades
                                               //let mut vecAyudasCabeceraHTTP = Vec::new(); //Vector de Ayudas de los Inputs de Cabecera
                                               //let mut vecAyudasEntidadesHTTP = Vec::new(); //Vector de Ayudas de los Inputs de Entidades

        let mut helpAddPetition: Listener<_> = Button::default().into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let mut helpAddEntity: Listener<_> = Button::default().into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        let stringTextoCabecera = String::from("Peticion ");
        let stringTextoEntidad = String::from("Entidad ");

        /*
            - contadorHeader : Llevará la cuenta del numero de cabeceras que llevamos, para cuando haya que añadir
            - xRelativaHeader : Lleva la posicion de X donde hay que incluir la siguiente peticion
            - yRelativaHeader : Lleva la posicion de Y donde hay que incluir la siguiente peticion
        */
        let mut contadorHeader = 1;
        let mut xRelativaHeader = 100;
        let mut yRelativaHeader = 0;
        let mut widthInput = 200;

        /*
            - contadorEntity : Llevará la cuenta del numero de entidades que llevamos, para cuando haya que añadir
            - xRelativaEntity : Lleva la posicion de X donde hay que incluir la siguiente peticion
            - yRelativaEntity : Lleva la posicion de Y donde hay que incluir la siguiente peticion
        */
        let mut contadorEntity = 1;
        let mut xRelativaEntity = 100;
        let mut yRelativaEntity = 0;

        //Definimos la interfaz
        self.refresh_Interface(5);
        //Cada DOS espacios será una linea de campos y cada espacio un boton de ayuda
        //Los hacemos mutable para cambiar sus valores
        //Bajo cada input, hay un boton que pulsaremos para obtener ayuda de dicho campo
        //Insertamos los datos guardados en los inputs y a self,  que al principio serán vacio y cuando tengamos datos serán restaurados, lo cual es comodo

        let titulo = Frame::new(0, 0, 900, 50, "HTTP");
        let mut inputHTTPPeticion = Input::new(150, 40, 75, 25, "Petition Type"); //X,Y,ANCHO, ALTO, TITULO
        inputHTTPPeticion.set_value(self.campoHTTPPetitionType.as_str());
        let mut helpHTTPPeticion: Listener<_> = Button::new(230, 40, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpHTTPPeticion.set_color(Color::Yellow);

        let mut inputHTTPHost = Input::new(400, 40, 200, 25, "HOST"); //X,Y,ANCHO, ALTO, TITULO
        inputHTTPHost.set_value(self.campoHTTPHost.as_str());
        let mut helpHTTPHost: Listener<_> = Button::new(620, 40, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpHTTPHost.set_color(Color::Yellow);

        let mut inputVerHTTP = Input::new(770, 40, 75, 25, "Version HTTP"); //X,Y,ANCHO, ALTO, TITULO
        inputVerHTTP.set_value(self.campoHTTPVersion.as_str());
        let mut helpHTTPVer: Listener<_> = Button::new(870, 40, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpHTTPVer.set_color(Color::Yellow);

        //Hacemos un grupo para tener tantas opciones como el usuario desee/necesite
        let mut packHeader = Pack::new(0, 90, 900, 150, None);
        packHeader.set_spacing(10);

        //Scroll para que pueda haber infinitas peticiones sin que afecte al tamaño de la interfaz
        let mut scroll = Scroll::new(0, 90, 900, 125, None);
        scroll.set_scrollbar_size(7);
        scroll.set_type(ScrollType::Vertical);
        let mut scrollbar = scroll.scrollbar();
        scrollbar.set_type(ScrollbarType::VerticalNice);
        scrollbar.set_color(Color::from_u32(0x757575));
        scrollbar.set_selection_color(Color::Red);

        for val in vecCabeceraHTTP.clone().into_iter(){
            let input = &val;

        }
        for i in 1..11 {
            //Empezamos con 5 peticiones y el usuario incluirá más si es necesario
            let numeroStr = contadorHeader.to_string();
            let mut peticionCero = stringTextoCabecera.clone();
            peticionCero.push_str(numeroStr.as_str());

            if contadorHeader > 2 && contadorHeader % 2 != 0 {
                //Si ya se añadieron 2 a la columna actual, bajamos la columna y reseteamos posicion de xRelativa
                xRelativaHeader = 100;
                yRelativaHeader = yRelativaHeader + 50;
            }
            let mut inputPeticionCero =
                Input::new(xRelativaHeader, yRelativaHeader, widthInput, 25, None); //X,Y,ANCHO, ALTO, TITULO
            inputPeticionCero.set_label(peticionCero.as_str());
            if !self.vecHTTPHeaderLines.is_empty() {
                let mut val = self.vecHTTPHeaderLines.remove(0);
                inputPeticionCero.set_value(val.as_str());
            } else {
                inputPeticionCero.set_value("");
            }
            vecCabeceraHTTP.remove(contadorHeader-1);
            vecCabeceraHTTP.insert(contadorHeader-1,inputPeticionCero);
            xRelativaHeader = xRelativaHeader + widthInput + 20; //Actualizamos xRelativa para incluir la ayuda en su lugar correspondiente

            if contadorHeader == 1 {
                helpAddPetition = Button::new(xRelativaHeader, yRelativaHeader, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpAddPetition.set_color(Color::Yellow);
            }
            xRelativaHeader = xRelativaHeader + 120; //Actualizamos xRelativa para el siguiente Input

            
            contadorHeader = contadorHeader + 1;
        }

        packHeader.end();

        let mut packEntity = Pack::new(0, 250, 900, 150, None);
        packEntity.set_spacing(10);

        let mut scroll = Scroll::new(0, 90, 900, 125, None);
        scroll.set_scrollbar_size(7);
        scroll.set_type(ScrollType::Vertical);
        let mut scrollbar = scroll.scrollbar();
        scrollbar.set_type(ScrollbarType::VerticalNice);
        scrollbar.set_color(Color::from_u32(0x757575));
        scrollbar.set_selection_color(Color::Red);

        for i in 1..11 {
            //Empezamos con 5 peticiones y el usuario incluirá más si es necesario
            let numeroStr = contadorEntity.to_string();
            let mut peticionCero = stringTextoEntidad.clone();
            peticionCero.push_str(numeroStr.as_str());

            if contadorEntity > 2 && contadorEntity % 2 != 0 {
                //Si ya se añadieron 2 a la columna actual, bajamos la columna y reseteamos posicion de xRelativa
                xRelativaEntity = 100;
                yRelativaEntity = yRelativaEntity + 50;
            }
            let mut inputPeticionCero =
                Input::new(xRelativaEntity, yRelativaEntity, widthInput, 25, None); //X,Y,ANCHO, ALTO, TITULO
            inputPeticionCero.set_label(peticionCero.as_str());
            if !self.vecHTTPEntityLines.is_empty() {
                let mut val = self.vecHTTPEntityLines.remove(0);
                inputPeticionCero.set_value(val.as_str());
            } else {
                inputPeticionCero.set_value("");
            }
            vecEntidadesHTTP.push(inputPeticionCero);
            xRelativaEntity = xRelativaEntity + widthInput + 20; //Actualizamos xRelativa para incluir la ayuda en su lugar correspondiente

            if contadorEntity == 1 {
                helpAddEntity = Button::new(xRelativaEntity, yRelativaEntity, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
                helpAddEntity.set_color(Color::Yellow);
            }

            xRelativaEntity = xRelativaEntity + 120; //Actualizamos xRelativa para el siguiente Input

            
            contadorEntity = contadorEntity + 1;
        }

        packEntity.end();

        //TCP
        let titulo = Frame::new(0, 380, 900, 50, "TCP");


        let mut inputDestPort = Input::new(450, 430, 75, 25, "Destination Port"); //X,Y,ANCHO, ALTO, TITULO
        inputDestPort.set_value(self.campoTCPDestPort.as_str());
        let mut helpDestPort: Listener<_> = Button::new(530, 430, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDestPort.set_color(Color::Yellow);

        

        //set_draw_color(Color::White);
        //draw_line(0, 250, 900, 250);
        //IP
        let subtitulo = Frame::new(0, 470, 900, 50, "IP");
        
        let mut inputDestAdd = Input::new(450, 510, 200, 25, "Destination Address"); //X,Y,ANCHO, ALTO, TITULO
        inputDestAdd.set_value(self.campoDestinationAddress.as_str());
        let mut helpDest: Listener<_> = Button::new(670, 510, 20, 20, "?").into(); // Lugar de aparicion(x,y) , Tamaño (x,y)
        helpDest.set_color(Color::Yellow);

        let mut sendP: Listener<_> = Button::new(810, 500, 80, 40, "Enviar").into();
        wind.make_resizable(true);
        wind.end();
        wind.show();

        let (s, r) = app::channel::<&str>();

        let clonAyuda = Arc::new(Mutex::new(self.clone())); // Creado para poder alargar el tiempo de vida de self
        let clonPayload = Arc::new(Mutex::new(self.clone()));
        let clonObtencion = Arc::new(Mutex::new(self.clone()));

        // El map que se editará se llama "but_input" y se usa en la edición del payload
        let mut but_input: HashMap<(i32, i32), Input> = HashMap::new(); //Map <Pair<X,Y> , Input>
                                                                        //Map <Pair<X,Y> , Input> . Este mapa será un mapa de lectura de datos para la creacion de la cadena de traduccion de los hexadecimales
        let mut map_input: HashMap<(i32, i32), String> = HashMap::new();

        //Necesitamos crear a priori la estructura de las celdas y grids de la interfaz del Payload
        //  para que tenga solidez y al cerrarse no reviente la estructura de la interfaz completa (payload y cabecera IP incluidos )
        let mut flex: Flex;
        let mut gridMedio: Grid = Grid::default_fill();
        let mut columnaMedio: Flex;
        let mut casillaSignificado = TextEditor::default(); //Fuera para poder transformarla
        let mut columnaFinal: Flex;
        let mut windoAux = Window::default();
        let mut windo = Window::default();

        // Cuando el boton se pulse, se manda una señal a la app, con el string de que campo estamos pidiendo ayuda
        // Cuando la app lo recoge , lo muestra
        
        helpDest.on_click(move |b| {
            s.send("Destination");
        });

        
        sendP.on_click(move |b| {
            s.send("1");
        });

        //TCP
        
        helpDestPort.on_click(move |b| {
            s.send("PuertoDestino");
        });
       
        //HTTP

        //Ayudas

        helpHTTPPeticion.on_click(move |b| {
            s.send("HTTPPeticion");
        });

        helpHTTPHost.on_click(move |b| {
            s.send("HTTPHost");
        });

        helpHTTPVer.on_click(move |b| {
            s.send("HTTPVersion");
        });

        helpAddPetition.on_click(move |b| {
            s.send("HTTPListaCabecera");
        });

        helpAddEntity.on_click(move |b| {
            s.send("HTTPListaEntidades");
        });

        while app.wait() {
            match r.recv() {
                Some(variableAux) => {
                    let helpS = variableAux;
                    let stringAux = clonAyuda.lock().unwrap().field_Help(1, helpS.to_string());
                    let strHelp = stringAux.as_str();

                    let stringAuxTCP = clonAyuda.lock().unwrap().field_Help(3, helpS.to_string());
                    let strHelpTCP = stringAuxTCP.as_str();

                    let stringAuxHTTP = clonAyuda.lock().unwrap().field_Help(5, helpS.to_string());
                    let strHelpHTTP = stringAuxHTTP.as_str();

                    let p = clonObtencion.clone();
                    let mut copiaMapa = but_input.clone();
                    let vecClone = vecCabeceraHTTP.clone();
                    match helpS {
                        
                        "Destination" => {
                            helpDest.set_tooltip(strHelp);
                            helpDest.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        
                        "PuertoDestino" => {
                            helpDestPort.set_tooltip(strHelpTCP);
                            helpDestPort.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        
                        "HTTPPeticion" => {
                            helpHTTPPeticion.set_tooltip(strHelpHTTP);
                            helpHTTPPeticion.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HTTPHost" => {
                            helpHTTPHost.set_tooltip(strHelpHTTP);
                            helpHTTPHost.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HTTPVersion" => {
                            helpHTTPVer.set_tooltip(strHelpHTTP);
                            helpHTTPVer.tooltip();
                            wind.redraw();
                            app.redraw();
                        }
                        "HTTPListaCabecera" => {
                            helpAddPetition.set_tooltip(strHelpHTTP);
                            helpAddPetition.tooltip();
                        }
                        "HTTPListaEntidades" => {
                            helpAddEntity.set_tooltip(strHelpHTTP);
                            helpAddEntity.tooltip();
                        }
                        
                        "1" => {
                            //Enviar paquete entero
                            //Para generar los datos debemos obtener todos los datos de las peticiones de cabecera y entidad HTTP
                            let mut vecCabecera = Vec::new();
                            for val in vecCabeceraHTTP.clone() {
                                vecCabecera.push(val.value());
                            }
                            let mut vecEntidad = Vec::new();
                            for val in vecEntidadesHTTP.clone() {
                                vecEntidad.push(val.value());
                            }
                            //Guardamos los datos. Como cambiaremos el Checksum nos da igual que valor tenga ahora, asi que lo ponemos a 0
                            self.save_FieldsHTTP(
                                inputDestAdd.clone().value(),
                                inputDestPort.clone().value(),
                                
                                inputHTTPPeticion.clone().value(),
                                inputHTTPHost.clone().value(),
                                inputVerHTTP.clone().value(),
                                vecCabecera.clone(),
                                vecEntidad.clone(),
                            );

                            let clon = Arc::new(Mutex::new(self.clone()));
                            //Convertimos los valores de Source y Destination de IP en la interfaz en el momento de guardar
                            let copiaSo = self.campoSourceAddress.clone();
                            let copiaDe = self.campoDestinationAddress.clone();
                            let vecSor: Vec<&str> = copiaSo.split(".").collect();
                            let vecDes: Vec<&str> = copiaDe.split(".").collect();

                            //Obtenemos el json de HTTP, para generar el payload de TCP
                            let paqueteHTTPString = self.json_To_HTTP(
                                self.campoHTTPPetitionType.clone(),
                                self.campoHTTPHost.clone(),
                                self.campoHTTPVersion.clone(),
                                self.vecHTTPHeaderLines.clone(),
                                self.vecHTTPEntityLines.clone(),
                            );

                            let paqHTTP = self.http_from_json(paqueteHTTPString);
                            match paqHTTP {
                                Ok(ref resultadoHTTP) => {
                                    //Si el paquete HTTP es correcto, generamos el payload
                                    let mut resulHTTP = (*resultadoHTTP).clone();
                                    let payloadHTTP = resulHTTP.httpToPayload();
                                    //Lo convertimos al campo de TCP
                                    let mut auxiStr = String::new();
                                    for val in payloadHTTP {
                                        let hexaDe = format!("{:02x}", val); //Con este format convertimos en el valor 119 en hex 77
                                        let mut stringPaso = hexaDe.to_string();

                                        auxiStr.push_str("0x");
                                        auxiStr.push_str(stringPaso.as_str());
                                        auxiStr.push_str(";");
                                        //El resultado queda como "0x00;"
                                    }
                                    self.campoTCPPayload = auxiStr.clone();

                                    //Una vez guardado el nuevo valor del payload de TCP (HTTP), creamos el paquete:
                                    let paqueteTCPString = self.json_To_TCP(
                                        self.campoTCPSourcePort.clone(),
                                        self.campoTCPDestPort.clone(),
                                        self.campoTCPSequenceNum.clone(),
                                        self.campoTCPACK.clone(),
                                        self.campoTCPOffset.clone(),
                                        self.campoTCPReserved.clone(),
                                        self.campoTCPFlags.clone(),
                                        self.campoTCPWindow.clone(),
                                        self.campoTCPChecksum.clone(),
                                        self.campoTCPUrgPtr.clone(),
                                        self.campoTCPOptions.clone(),
                                        self.campoTCPPayload.clone(),
                                    );
                                    let paqTCP = self.tcp_from_json(paqueteTCPString);

                                    match paqTCP {
                                        Ok(ref resultadoTCP) => {
                                            let mut sendPackTCP = (*resultadoTCP).clone();

                                            //Una vez tenemos el paquwete TCP creamos el paquete IP:
                                            //Obtenemos el json de IP
                                            let paqueteString = self.json_To_IP(
                                                self.campoVersion.clone(),
                                                self.campoIHL.clone(),
                                                self.campoTypeOfService.clone(),
                                                self.campoECN.clone(),
                                                self.campoLength.clone(),
                                                self.campoID.clone(),
                                                self.campoFlags.clone(),
                                                self.campoFragmentOffSet.clone(),
                                                self.campoTTL.clone(),
                                                self.campoProtocol.clone(),
                                                self.campoHeaderChecksum.clone(),
                                                self.campoSourceAddress.clone(),
                                                self.campoDestinationAddress.clone(),
                                                self.campoOptions.clone(),
                                                "0x00;".to_string(),
                                            );
                                            let paqueteIP = self.ip_from_json(paqueteString);
                                            match paqueteIP {
                                                Ok(ref resultadoIP) => {
                                                    let pacIP = (*resultadoIP).clone();
                                                    sendPackTCP.set_IPPacket(pacIP.clone());
                                                    resulHTTP.set_TCPPacket(sendPackTCP.clone());

                                                    wind.redraw();
                                                    app.redraw();

                                                    clon.lock()
                                                        .unwrap()
                                                        .sendPackageHTTP(resulHTTP.clone());
                                                    //Lo enviamos
                                                }
                                                Err(msg) => {
                                                    let tipoMen = "ERROR".to_string();
                                                    let menText =
                                                        Arc::new(Mutex::new(msg.as_str()));

                                                    wind.redraw();
                                                    app.redraw();
                                                    clon.lock()
                                                        .unwrap()
                                                        .clone()
                                                        .popup_Mensaje(tipoMen, menText);
                                                    // COn unwrap_err() sacamos el Err , osea, el string
                                                }
                                            }
                                        }
                                        Err(msg) => {
                                            let tipoMen = "ERROR".to_string();
                                            let menText = Arc::new(Mutex::new(msg.as_str()));

                                            wind.redraw();
                                            app.redraw();
                                            clon.lock()
                                                .unwrap()
                                                .clone()
                                                .popup_Mensaje(tipoMen, menText);
                                            // COn unwrap_err() sacamos el Err , osea, el string
                                        }
                                    }
                                }
                                Err(msg) => {
                                    let tipoMen = "ERROR".to_string();
                                    let menText = Arc::new(Mutex::new(msg.as_str()));

                                    wind.redraw();
                                    app.redraw();
                                    clon.lock().unwrap().clone().popup_Mensaje(tipoMen, menText);
                                    // COn unwrap_err() sacamos el Err , osea, el string
                                }
                            }
                        }
                        _ => {}
                    }
                }
                None => {}
            }
            wind.redraw();
            app.redraw();
        }
    }

    /**
     * @in: String "mensajeErr" : COntiene el error o mensaje que ha producido que se genere el PopUp
     *      String "tipoMensaje": Muestra en el titulo del PopUp si es un mensaje ERROR o un mensaje "Informacion Dato"
     * @out:
     * @function: Esta funcion Muestra un mensaje PopUp que avisa de un posible error o de la ayuda solicitada
     *          
     */
    pub fn popup_Mensaje<'a>(self, tipoMensaje: String, mensajeErr: Arc<Mutex<&str>>) {
        let mensajeTitulo = tipoMensaje.as_str();
        let mensajeError = mensajeErr.to_owned();
        let varAux = mensajeError.lock().unwrap();
        let auxi = varAux;

        let mut windo = Window::default()
            .with_size(400, 300)
            .center_screen()
            .with_label(mensajeTitulo);
        windo.make_resizable(true);
        let mut frame = Frame::new(170, 75, 50, 100, "");
        frame.set_label(&auxi); //La solucion era pasarlo como label editado en lugar de en la creación

        //let mut ok = Button::new(170, 200, 50, 50, "OK"); // Lugar de aparicion(x,y) , Tamaño (x,y)

        windo.end();
        windo.show();

        /*ok.set_callback(move |b|{ //Cuando pulsamos OK se cierra el POPUP
            windo.hide();
        });*/
    }

    // Metodos para IP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: String creado en formato JSON para crear un paquete IP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete IP
     */
    fn json_To_IP(
        &mut self,
        version: String,
        IHL: String,
        typeofServ: String,
        ECN: String,
        len: String,
        id: String,
        flags: String,
        fragmOffset: String,
        ttl: String,
        protcol: String,
        headerCheck: String,
        mut source: String,
        mut destination: String,
        options: String,
        payloadHexa: String,
    ) -> String {
        //Primero miramos el valor de las IP porque al llevar "." son casos especiales. En caso de ser campos vacios se los añadimos

        if source.as_str() == "" {
            source = String::from("...");
        }
        if destination.as_str() == "" {
            destination = String::from("...");
        }

        //Como son Strings necesitamos usar .clone()

        let json: String = "Version:".to_owned()
            + &version.clone()
            + "#"
            + "IHL:"
            + &IHL.clone()
            + "#"
            + "TypeOfService:"
            + &typeofServ.clone()
            + "#"
            + "ECN:"
            + &ECN.clone()
            + "#"
            + "Length:"
            + &len.clone()
            + "#"
            + "ID:"
            + &id.clone()
            + "#"
            + "Flags:"
            + &flags.clone()
            + "#"
            + "FragmentOffset:"
            + &fragmOffset.clone()
            + "#"
            + "TTL:"
            + &ttl.clone()
            + "#"
            + "Protocol:"
            + &protcol.clone()
            + "#"
            + "HeaderCheksum:"
            + &headerCheck.clone()
            + "#"
            + "Source:"
            + &source.clone()
            + "#"
            + "Destination:"
            + &destination.clone()
            + "#"
            + "Options:"
            + &options.clone()
            + "#"
            + "Payload:"
            + &payloadHexa.clone()
            + "#";

        return json;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_FieldsIP<'a>(
        &mut self,
        inputVersion: String,
        inputIHL: String,
        inputTypeOfServ: String,
        inputECN: String,
        inputLength: String,
        inputID: String,
        inputFlags: String,
        msb: String,
        lsb: String,
        inputFragmentOff: String,
        inputTTL: String,
        inputProtocol: String,
        inputHeaderChecksum: String,
        inputSourceAdd: String,
        inputDestAdd: String,
        hexaPayload: String,
    ) {
        self.campoVersion = inputVersion;
        self.campoIHL = inputIHL;
        self.campoTypeOfService = inputTypeOfServ;
        self.campoECN = inputECN;
        self.campoLength = inputLength;
        self.campoID = inputID;
        //AQUI IRIA FLAGS
        self.campoFragmentOffSet = inputFragmentOff;
        self.campoTTL = inputTTL;
        self.campoProtocol = inputProtocol;
        self.campoHeaderChecksum = inputHeaderChecksum;
        self.campoSourceAddress = inputSourceAdd;
        self.campoDestinationAddress = inputDestAdd;
        self.campoOptions = "1;".to_string();
        self.campoPadding = "".to_string();
        self.campoHexadecimalPayload = hexaPayload;

        if msb == "0".to_string() {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("000");
            } else {
                self.campoFlags = String::from("001");
            }
        } else {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("010");
            } else {
                self.campoFlags = String::from("011");
            }
        }
    }

    fn refresh_Interface(&mut self, first_time: u8) -> () {
        //actualiza interfaz sin datos nuevos

        // Actualiza la interfaz con los valores de IP
        self.campoVersion = self.packageIP.get_Version().to_string();
        self.campoIHL = self.packageIP.get_IHL().to_string();
        self.campoTypeOfService = self.packageIP.get_TypeOfService().to_string();
        self.campoECN = self.packageIP.get_ECN().to_string();
        //Length
        self.campoID = self.packageIP.get_ID().to_string();
        self.campoFlags = self.packageIP.get_Flags().to_string();
        self.campoFragmentOffSet = self.packageIP.get_FragmentOffset().to_string();
        self.campoTTL = self.packageIP.get_TTL().to_string();
        //Protocol
        self.campoHeaderChecksum = self.packageIP.get_HeaderChecksum().to_string();
        self.campoSourceAddress = self.packageIP.get_SourceAddress().to_string();
        self.campoDestinationAddress = self.packageIP.get_DestinationAddress().to_string();
        //Options y Payload Hexadecimal no se añaden aqui ya que son vectores, se añadirán en la interfaz
        match first_time {
            1 => {
                //IP
                self.campoProtocol = self.packageIP.get_Protocol().to_string();
                self.campoLength = self.packageIP.get_Length().to_string();
            }
            2 => {
                //ICMP
                //Actualizamos los campos de IP
                let paquete = self.packageICMP.icmpToPayload();
                let tamAnterior = self.packageICMP.get_IPPacket().get_Length();
                let tamSuma = paquete.1 as u16;
                let mut tamReal = tamAnterior + tamSuma;
                self.campoProtocol = "ICMP".to_string();
                self.campoLength = tamReal.to_string();

                let mut vecPayl = Vec::new();
                //Una vez editados, se extraen al vector
                let mut concatenacion = "";
                let mut auxiStr = String::new();
                let iterador = paquete.0; //Necesitamos orden
                                          //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                for val in iterador {
                    vecPayl.push(val);
                }
                //Una vez tenemos [00,00,00,...]

                for val in vecPayl {
                    auxiStr.push_str("0x");
                    let stAux = val.to_string();
                    auxiStr.push_str(stAux.as_str());
                    auxiStr.push_str(";");
                    //El resultado queda como "0x00;"
                }
                concatenacion = auxiStr.as_str();
                //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                //Metemos dicho valor en el hexaPayload
                self.campoHexadecimalPayload = concatenacion.to_string();

                //Actualizamos los campos de ICMP
                self.campoICMPType = self
                    .packageICMP
                    .transformaTipo(self.packageICMP.get_Type())
                    .to_string();
                self.campoICMPCode = self
                    .packageICMP
                    .transformaCode(self.packageICMP.get_Code())
                    .to_string();
                self.campoICMPChecksum = self.packageICMP.get_Checksum().to_string();
                self.campoICMPPayload = "0x60;0x2B;0x97;0xD8;0x00;0x08;0x5E;0xA3;0x08;0x09;0x0A;0x0B;0x0C;0x0D;0x0E;0x0F;0x10;0x11;0x12;0x13;0x14;0x15;0x16;0x17;0x18;0x19;0x1A;0x1B;0x1C;0x1D;0x1E;0x1F;0x20;0x21;0x22;0x23;0x24;0x25;0x26;0x27;0x28;0x29;0x2A;0x2B;0x2C;0x2D;0x2E;0x2F;0x30;0x31;0x32;0x33;0x34;0x35;0x36;0x37;".to_string();
            }
            3 => {
                //TCP
                //Actualizamos los campos de IP
                let sour = self.packageIP.get_SourceAddress();
                let dest = self.packageIP.get_DestinationAddress();
                let paquete = self.packageTCP.tcpToPayload(sour, dest);
                let tamAnterior = self.packageTCP.get_IPPacket().get_Length();
                let tamSuma = paquete.1 as u16;
                let mut tamReal = tamAnterior + tamSuma;
                self.campoProtocol = "TCP".to_string();
                self.campoLength = tamReal.to_string();

                let mut vecPayl = Vec::new();
                //Una vez editados, se extraen al vector
                let mut concatenacion = "";
                let mut auxiStr = String::new();
                let iterador = paquete.0; //Necesitamos orden
                                          //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                for val in iterador {
                    vecPayl.push(val);
                }
                //Una vez tenemos [00,00,00,...]

                for val in vecPayl {
                    auxiStr.push_str("0x");
                    let stAux = val.to_string();
                    auxiStr.push_str(stAux.as_str());
                    auxiStr.push_str(";");
                    //El resultado queda como "0x00;"
                }
                concatenacion = auxiStr.as_str();
                //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                //Metemos dicho valor en el hexaPayload
                self.campoHexadecimalPayload = concatenacion.to_string();

                //Actualizamos los campos de TCP

                self.campoTCPSourcePort = self.packageTCP.get_SourcePort().to_string();
                self.campoTCPDestPort = self.packageTCP.get_DestinationPort().to_string();
                self.campoTCPSequenceNum = self.packageTCP.get_Sequence().to_string();
                self.campoTCPACK = self.packageTCP.get_ACK().to_string();
                self.campoTCPOffset = self.packageTCP.get_DataOffset().to_string();
                self.campoTCPReserved = "0".to_string();
                self.campoTCPFlags = self.packageTCP.get_Flags().to_string();
                self.campoTCPWindow = self.packageTCP.get_Window().to_string();
                self.campoTCPChecksum = self.packageTCP.get_Checksum().to_string();
                self.campoTCPUrgPtr = self.packageTCP.get_UrgentPointer().to_string();
                self.campoTCPOptions = "1;".to_string();
                self.campoTCPPayload = "0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;0x00;".to_string();
            }
            4 => {
                //UDP
                //Actualizamos los campos de IP
                let sour = self.packageIP.get_SourceAddress();
                let dest = self.packageIP.get_DestinationAddress();
                let paquete = self.packageUDP.udpToPayload(sour, dest);
                let tamAnterior = self.packageUDP.get_IPPacket().get_Length();
                let tamSuma = paquete.1 as u16;
                let mut tamReal = tamAnterior + tamSuma;
                self.campoProtocol = "UDP".to_string();
                self.campoLength = tamReal.to_string();

                let mut vecPayl = Vec::new();
                //Una vez editados, se extraen al vector
                let mut concatenacion = "";
                let mut auxiStr = String::new();
                let iterador = paquete.0; //Necesitamos orden
                                          //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                for val in iterador {
                    vecPayl.push(val);
                }
                //Una vez tenemos [00,00,00,...]

                for val in vecPayl {
                    auxiStr.push_str("0x");
                    let stAux = val.to_string();
                    auxiStr.push_str(stAux.as_str());
                    auxiStr.push_str(";");
                    //El resultado queda como "0x00;"
                }
                concatenacion = auxiStr.as_str();
                //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                //Metemos dicho valor en el hexaPayload
                self.campoHexadecimalPayload = concatenacion.to_string();

                //Actualizamos los campos de UDP

                self.campoUDPSourcePort = self.packageUDP.get_SourcePort().to_string();
                self.campoUDPDestPort = self.packageUDP.get_DestinationPort().to_string();
                self.campoUDPLength = self.packageUDP.get_Length().to_string();
                self.campoUDPChecksum = self.packageUDP.get_Checksum().to_string();
                self.campoUDPPayload = "0x60;0x2B;0x97;0xD8;0x00;0x08;0x5E;0xA3;0x08;0x09;0x0A;0x0B;0x0C;0x0D;0x0E;0x0F;0x10;0x11;0x12;0x13;0x14;0x15;0x16;0x17;0x18;0x19;0x1A;0x1B;0x1C;0x1D;0x1E;0x1F;0x20;0x21;0x22;0x23;0x24;0x25;0x26;0x27;0x28;0x29;0x2A;0x2B;0x2C;0x2D;0x2E;0x2F;0x30;0x31;0x32;0x33;0x34;0x35;0x36;0x37;".to_string();
            }
            5 => {
                //HTTP
                //Actualizamos los campos de IP
                self.campoDestinationAddress = "142.251.46.227".to_string(); //IP a Google
                let sour = self.packageIP.get_SourceAddress();
                let dest = self.packageIP.get_DestinationAddress();
                let paquete = self.packageTCP.tcpToPayload(sour, dest);
                let tamAnterior = self.packageTCP.get_IPPacket().get_Length();
                let tamSuma = paquete.1 as u16;
                let mut tamReal = tamAnterior + tamSuma;
                self.campoProtocol = "TCP".to_string();
                self.campoLength = tamReal.to_string();

                let mut vecPayl = Vec::new();
                //Una vez editados, se extraen al vector
                let mut concatenacion = "";
                let mut auxiStr = String::new();
                let iterador = paquete.0; //Necesitamos orden
                                          //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
                for val in iterador {
                    vecPayl.push(val);
                }
                //Una vez tenemos [00,00,00,...]

                for val in vecPayl {
                    auxiStr.push_str("0x");
                    let stAux = val.to_string();
                    auxiStr.push_str(stAux.as_str());
                    auxiStr.push_str(";");
                    //El resultado queda como "0x00;"
                }
                concatenacion = auxiStr.as_str();
                //Una vez tenemos que concatenacion = "0x00;0x01;0x9A;... "
                //Metemos dicho valor en el hexaPayload
                self.campoHexadecimalPayload = concatenacion.to_string();

                //Actualizamos los campos de TCP

                self.campoTCPSourcePort = (rand::thread_rng().gen::<u16>()).to_string();
                self.campoTCPDestPort = self.packageTCP.get_DestinationPort().to_string();
                self.campoTCPSequenceNum = self.packageTCP.get_Sequence().to_string();
                self.campoTCPACK = self.packageTCP.get_ACK().to_string();
                self.campoTCPOffset = self.packageTCP.get_DataOffset().to_string();
                self.campoTCPReserved = "0".to_string();
                self.campoTCPFlags = "010000".to_string();//ACK
                self.campoTCPWindow = self.packageTCP.get_Window().to_string();
                self.campoTCPChecksum = self.packageTCP.get_Checksum().to_string();
                self.campoTCPUrgPtr = self.packageTCP.get_UrgentPointer().to_string();
                self.campoTCPOptions = "1;".to_string();
                self.campoTCPPayload = "0x60;0x2B;0x97;0xD8;0x00;0x08;0x5E;0xA3;0x08;0x09;0x0A;0x0B;0x0C;0x0D;0x0E;0x0F;0x10;0x11;0x12;0x13;0x14;0x15;0x16;0x17;0x18;0x19;0x1A;0x1B;0x1C;0x1D;0x1E;0x1F;0x20;0x21;0x22;0x23;0x24;0x25;0x26;0x27;0x28;0x29;0x2A;0x2B;0x2C;0x2D;0x2E;0x2F;0x30;0x31;0x32;0x33;0x34;0x35;0x36;0x37;".to_string();

                //Actualizamos los campos de HTTP;
                self.campoHTTPPetitionType = self.packageHTTP.get_PetitionType();
                self.campoHTTPHost = self.packageHTTP.get_Host();
                self.campoHTTPVersion = self.packageHTTP.get_HTTPVer();
                self.vecHTTPHeaderLines = self.packageHTTP.get_HeaderLines();
                self.vecHTTPEntityLines = self.packageHTTP.get_EntityLines();
            }
            _ => {}
        }
    }
    // Añade el nuevo protocolo y actualiza los datos. Crea los nuevos protocolos  y llama a refresh_Interface
    fn update_Interface(&self, idProtocol: String) -> () {}

    /**
     * @in: ninguno
     * @out: Objeto de la clase IP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase IP.
     */
    pub fn ip(&self) -> IP {
        let nuevo = IP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase IP
     * @function: Esta funcion creará un paquete de la clase IP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_IP
     */
    fn ip_from_json(&self, json: String) -> Result<IP, String> {
        let ippaq = self.packageIP.ip_From_String(json);

        return ippaq;
    }

    /**
     * @in: objeto de la clase IP
     * @out: null
     * @function: Esta funcion enviará la clase IP con los datos introducidos por la Interfaz
     */
    fn sendPackageIP(&mut self, pack: IP) {
        self.packageIP = pack; // actualizamos el paquete IP para guardarlo por si es necesario
        let resultado = match self.packageIP.send_IP_Package() {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Se realizado satisfactoriamente\n 
                el envio del paquete",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
        };

        println!("{:?}", resultado);
    }

    /**
     * @in: ->Int "protocolo", que nos dirá a que protocolo (y a que clase) va a pedir información
     *                          IP = 1, ICMP = 2, TCP = 3 , UDP = 5 , HTTP = 5, FTP = 6
     *      ->String "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, para mostrarlo
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete
     *              que se está creando  y devolverá dicha información
     */
    fn field_Help(&self, protocolo: u8, campo: String) -> String {
        let mut ayuda: String = String::new();
        match protocolo {
            1 => {
                ayuda = self.packageIP.field_Help(campo);
            } //Ayuda para IP
            2 => {
                ayuda = self.packageICMP.field_Help(campo);
            } //Ayuda para ICMP
            3 => {
                ayuda = self.packageTCP.field_Help(campo);
            } //Ayuda para TCP
            4 => {
                ayuda = self.packageUDP.field_Help(campo);
            } //Ayuda para UDP
            5 => {
                ayuda = self.packageHTTP.field_Help(campo);
            } //Ayuda para HTTP. Work in Progress
            6 => {
                ayuda = "".to_string();
            } //Ayuda para FTP. Work in Progress
            _ => {} // caso base
        }

        return ayuda;
    }

    /**
     * @in :
     * @out: Numero u16  que representa el checksum del paquete construido hasta ahora
     * @function: Funcion que llama a la funcion que genera checksum de paquete IP
     */

    pub fn obtener_ChecksumIP(&self) -> u16 {
        let num = self.packageIP.generaChecksum();

        return num;
    }

    /**
     * @in : but_input: Mapa con todos los resultados hexadecimales de la interfaz
     * @out : String que devolverá el valor en lenguaje natural de ese conjunto de string hexadecimales
     * @function : La funcion transforma el conjunto completo de texto hexadecimal "00409A..." a un texto
     * que el usuario puede entender y que le dice que significa todo el payload
     */
    pub fn actualizaLenguajeNatural(self, but_input: HashMap<(i32, i32), String>) -> String {
        //Map de Button, hay que coger los valores

        let mut concatenacion = "";
        let mut auxiStr = String::new();

        let iterador = but_input.iter().sorted();
        //Necesitamos ordenarlos para que esten por orden de las casillas y asi de el texto correcto
        for (key, val) in iterador {
            auxiStr.push_str(val.as_str());
        }
        concatenacion = auxiStr.as_str();
        // Aqui concatenacion debe tener el string de todos losvalores hexadecimales
        let mut vecHexa = Vec::new();
        vecHexa = match hex::decode(concatenacion){ //Vec<u8>
                                Ok(res) => {res},
                                Err(e) => return String::from(format!("Error al transformar.\n Siga editando el hexadecimal\n para obtener los datos correctos")),
                            }; //Lo transformamos a Vec<u8>

        //Vec u8 lo pasamos al string, ahora resuelto

        let s = String::from_utf8_lossy(&vecHexa).into_owned(); // turns invalid UTF-8 bytes into   and so no error handling is required.

        return s;
    }

    // Funciones para ICMP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: String creado en formato JSON para crear un paquete IP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete IP
     */
    fn json_To_ICMP(
        &mut self,
        tipo_ICMP: String,
        codigo_ICMP: String,
        check_ICMP: String,
        payl_ICMP: String,
    ) -> String {
        //Como son Strings necesitamos usar .clone()

        let json: String = "Type:".to_owned()
            + &tipo_ICMP.clone()
            + "#"
            + "Code:"
            + &codigo_ICMP.clone()
            + "#"
            + "Checksum:"
            + &check_ICMP.clone()
            + "#"
            + "Payload:"
            + &payl_ICMP.clone()
            + "#";

        return json;
    }

    /**
     * @in: ninguno
     * @out: Objeto de la clase ICMP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase ICMP.
     */
    pub fn icmp(&self) -> ICMP {
        let nuevo = ICMP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase IP
     * @function: Esta funcion creará un paquete de la clase IP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_IP
     */
    fn icmp_from_json(&self, json: String) -> Result<ICMP, String> {
        let ippaq = self.packageICMP.icmp_From_String(json);

        return ippaq;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_FieldsICMP<'a>(
        &mut self,
        inputVersion: String,
        inputIHL: String,
        inputTypeOfServ: String,
        inputECN: String,
        inputLength: String,
        inputID: String,
        inputFlags: String,
        msb: String,
        lsb: String,
        inputFragmentOff: String,
        inputTTL: String,
        inputProtocol: String,
        inputHeaderChecksum: String,
        inputSourceAdd: String,
        inputDestAdd: String,
        hexaPayload: String,
        inputICMPType: String,
        inputICMPCode: String,
        inputICMPChecksum: String,
        inputICMPPayload: String,
    ) {
        self.campoVersion = inputVersion;
        self.campoIHL = inputIHL;
        self.campoTypeOfService = inputTypeOfServ;
        self.campoECN = inputECN;
        self.campoLength = inputLength;
        self.campoID = inputID;
        //AQUI IRIA FLAGS
        self.campoFragmentOffSet = inputFragmentOff;
        self.campoTTL = inputTTL;
        self.campoProtocol = inputProtocol;
        self.campoHeaderChecksum = inputHeaderChecksum;
        self.campoSourceAddress = inputSourceAdd;
        self.campoDestinationAddress = inputDestAdd;
        self.campoOptions = "1;".to_string();
        self.campoPadding = "".to_string();
        self.campoHexadecimalPayload = hexaPayload;
        if inputICMPType != "" {
            self.campoICMPType = inputICMPType;
        }
        if inputICMPCode != "" {
            self.campoICMPCode = inputICMPCode;
        }
        if inputICMPChecksum != "" {
            self.campoICMPChecksum = inputICMPChecksum;
        }
        if inputICMPPayload != "" {
            self.campoICMPPayload = inputICMPPayload;
        }

        if msb == "0".to_string() {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("000");
            } else {
                self.campoFlags = String::from("001");
            }
        } else {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("010");
            } else {
                self.campoFlags = String::from("011");
            }
        }
    }

    /**
     * @in :
     * @out: Numero u16  que representa el checksum del paquete construido hasta ahora
     * @function: Funcion que llama a la funcion que genera checksum de paquete ICMP
     */

    pub fn obtener_ChecksumICMP(&self) -> u16 {
        let num = self.packageICMP.generaChecksum();

        return num;
    }

    /**
     * @in: objeto de la clase ICMP
     * @out: null
     * @function: Esta funcion enviará la clase ICMP con los datos introducidos por la Interfaz
     */
    fn sendPackageICMP(&mut self, pack: ICMP) {
        self.packageICMP = pack; // actualizamos el paquete ICMP para guardarlo por si es necesario
        self.packageIP = self.packageICMP.get_IPPacket().clone();
        let tamMod = self.tamanioModificado;
        let resultado = match self.packageICMP.sendPackage(tamMod) {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Se realizado satisfactoriamente\n 
                el envio del paquete",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
        };

        println!("{:?}", resultado);
    }

    //Funciones para TCP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: String creado en formato JSON para crear un paquete TCP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete TCP
     */
    fn json_To_TCP(
        &mut self,
        sourcePort: String,
        destPort: String,
        sequence: String,
        ack: String,
        offsetDA: String,
        reserved: String,
        flagsTCP: String,
        window: String,
        checksum_tcp: String,
        urgPointeer: String,
        optionsTCP: String,
        payloadTCP: String,
    ) -> String {
        //Como son Strings necesitamos usar .clone()

        let json: String = "PuertoOrigen:".to_owned()
            + &sourcePort.clone()
            + "#"
            + "PuertoDestino:"
            + &destPort.clone()
            + "#"
            + "Sequence:"
            + &sequence.clone()
            + "#"
            + "ACK:"
            + &ack.clone()
            + "#"
            + "OffsetTCP:"
            + &offsetDA.clone()
            + "#"
            + "Reserved:"
            + &reserved.clone()
            + "#"
            + "Flags:"
            + &flagsTCP.clone()
            + "#"
            + "Window:"
            + &window.clone()
            + "#"
            + "Checksum:"
            + &checksum_tcp.clone()
            + "#"
            + "Pointer:"
            + &urgPointeer.clone()
            + "#"
            + "Options:"
            + &optionsTCP.clone()
            + "#"
            + "Payload:"
            + &payloadTCP.clone()
            + "#";

        return json;
    }

    /**
     * @in: ninguno
     * @out: Objeto de la clase TCP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase TCP.
     */
    pub fn tcp(&self) -> TCP {
        let nuevo = TCP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase TCP
     * @function: Esta funcion creará un paquete de la clase TCP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_IP
     */
    fn tcp_from_json(&self, json: String) -> Result<TCP, String> {
        let ippaq = self.packageTCP.tcp_From_String(json);

        return ippaq;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_FieldsTCP<'a>(
        &mut self,
        inputVersion: String,
        inputIHL: String,
        inputTypeOfServ: String,
        inputECN: String,
        inputLength: String,
        inputID: String,
        inputFlags: String,
        msb: String,
        lsb: String,
        inputFragmentOff: String,
        inputTTL: String,
        inputProtocol: String,
        inputHeaderChecksum: String,
        inputSourceAdd: String,
        inputDestAdd: String,
        inputSourcePort: String,
        inputDestPort: String,
        inputTCPSequence: String,
        inputTCPAck: String,
        inputDataOffset: String,
        inputReserved: String,
        urgFlag: String,
        ackFlag: String,
        pshFlag: String,
        rstFlag: String,
        synFlag: String,
        finFlag: String,
        inputTCPWindow: String,
        inputTCPChecksum: String,
        inputTCPUrgentPointer: String,
        inputTCPOptions: String,
        inputTCPPayload: String,
    ) {
        self.campoVersion = inputVersion;
        self.campoIHL = inputIHL;
        self.campoTypeOfService = inputTypeOfServ;
        self.campoECN = inputECN;
        self.campoLength = inputLength;
        self.campoID = inputID;
        //AQUI IRIA FLAGS
        self.campoFragmentOffSet = inputFragmentOff;
        self.campoTTL = inputTTL;
        self.campoProtocol = inputProtocol;
        self.campoHeaderChecksum = inputHeaderChecksum;
        self.campoSourceAddress = inputSourceAdd;
        self.campoDestinationAddress = inputDestAdd;
        self.campoOptions = "1;".to_string();
        self.campoPadding = "".to_string();

        if msb == "0".to_string() {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("000");
            } else {
                self.campoFlags = String::from("001");
            }
        } else {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("010");
            } else {
                self.campoFlags = String::from("011");
            }
        }

        self.campoTCPSourcePort = inputSourcePort;
        self.campoTCPDestPort = inputDestPort;
        self.campoTCPSequenceNum = inputTCPSequence;
        self.campoTCPACK = inputTCPAck;
        self.campoTCPOffset = inputDataOffset;
        self.campoTCPReserved = inputReserved;

        let mut stringFlags = String::new();
        stringFlags.push_str(urgFlag.as_str());
        stringFlags.push_str(ackFlag.as_str());
        stringFlags.push_str(pshFlag.as_str());
        stringFlags.push_str(rstFlag.as_str());
        stringFlags.push_str(synFlag.as_str());
        stringFlags.push_str(finFlag.as_str());

        self.campoTCPFlags = stringFlags.clone();
        self.campoTCPWindow = inputTCPWindow;
        self.campoTCPChecksum = inputTCPChecksum;
        self.campoTCPUrgPtr = inputTCPUrgentPointer;
        self.campoTCPOptions = inputTCPOptions;
        self.campoTCPPayload = inputTCPPayload;
    }

    /**
     * @in :
     * @out: Numero u16  que representa el checksum del paquete construido hasta ahora
     * @function: Funcion que llama a la funcion que genera checksum de paquete TCP
     */

    pub fn obtener_ChecksumTCP(&self, sour: Ipv4Addr, dest: Ipv4Addr) -> u16 {
        let num = self.packageTCP.generaChecksum(sour, dest);

        return num;
    }

    /**
     * @in: objeto de la clase TCP
     * @out: null
     * @function: Esta funcion enviará la clase TCP con los datos introducidos por la Interfaz
     */
    fn sendPackageTCP(&mut self, pack: TCP) {
        self.packageTCP = pack; // actualizamos el paquete ICMP para guardarlo por si es necesario
        self.packageIP = self.packageTCP.get_IPPacket().clone();
        let sou = self.packageIP.get_SourceAddress();
        let des = self.packageIP.get_DestinationAddress();
        let tamMod = self.tamanioModificado;
        let resultado = match self.packageTCP.sendPackage(tamMod, sou, des) {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Se realizado satisfactoriamente\n 
                el envio del paquete",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
        };

        println!("{:?}", resultado);
    }

    //Funciones para UDP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: String creado en formato JSON para crear un paquete UDP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete UDP
     */
    fn json_To_UDP(
        &mut self,
        sourcePort: String,
        destPort: String,
        len: String,
        checksum_udp: String,
        payloadUDP: String,
    ) -> String {
        //Como son Strings necesitamos usar .clone()

        let json: String = "PuertoOrigen:".to_owned()
            + &sourcePort.clone()
            + "#"
            + "PuertoDestino:"
            + &destPort.clone()
            + "#"
            + "Length:"
            + &len.clone()
            + "#"
            + "Checksum:"
            + &checksum_udp.clone()
            + "#"
            + "Payload:"
            + &payloadUDP.clone()
            + "#";

        return json;
    }

    /**
     * @in: ninguno
     * @out: Objeto de la clase UDP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase UDP.
     */
    pub fn udp(&self) -> UDP {
        let nuevo = UDP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase UDP
     * @function: Esta funcion creará un paquete de la clase UDP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_UDP
     */
    fn udp_from_json(&self, json: String) -> Result<UDP, String> {
        let ippaq = self.packageUDP.udp_From_String(json);

        return ippaq;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_FieldsUDP<'a>(
        &mut self,
        inputVersion: String,
        inputIHL: String,
        inputTypeOfServ: String,
        inputECN: String,
        inputLength: String,
        inputID: String,
        inputFlags: String,
        msb: String,
        lsb: String,
        inputFragmentOff: String,
        inputTTL: String,
        inputProtocol: String,
        inputHeaderChecksum: String,
        inputSourceAdd: String,
        inputDestAdd: String,
        inputSourcePort: String,
        inputDestPort: String,
        inputUDPLength: String,
        inputUDPChecksum: String,
        inputUDPPayload: String,
    ) {
        self.campoVersion = inputVersion;
        self.campoIHL = inputIHL;
        self.campoTypeOfService = inputTypeOfServ;
        self.campoECN = inputECN;
        self.campoLength = inputLength;
        self.campoID = inputID;
        //AQUI IRIA FLAGS
        self.campoFragmentOffSet = inputFragmentOff;
        self.campoTTL = inputTTL;
        self.campoProtocol = inputProtocol;
        self.campoHeaderChecksum = inputHeaderChecksum;
        self.campoSourceAddress = inputSourceAdd;
        self.campoDestinationAddress = inputDestAdd;
        self.campoOptions = "1;".to_string();
        self.campoPadding = "".to_string();

        if msb == "0".to_string() {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("000");
            } else {
                self.campoFlags = String::from("001");
            }
        } else {
            if lsb == "0".to_string() {
                self.campoFlags = String::from("010");
            } else {
                self.campoFlags = String::from("011");
            }
        }

        self.campoUDPSourcePort = inputSourcePort;
        self.campoUDPDestPort = inputDestPort;
        self.campoUDPLength = inputUDPLength;
        self.campoUDPChecksum = inputUDPChecksum;
        self.campoUDPPayload = inputUDPPayload;
    }

    /**
     * @in :
     * @out: Numero u16  que representa el checksum del paquete construido hasta ahora
     * @function: Funcion que llama a la funcion que genera checksum de paquete UDP
     */

    pub fn obtener_ChecksumUDP(&self, sour: Ipv4Addr, dest: Ipv4Addr) -> u16 {
        let num = self.packageUDP.generaChecksum(sour, dest);

        return num;
    }

    /**
     * @in: objeto de la clase UDP
     * @out: null
     * @function: Esta funcion enviará la clase TCP con los datos introducidos por la Interfaz
     */
    fn sendPackageUDP(&mut self, pack: UDP) {
        self.packageUDP = pack; // actualizamos el paquete ICMP para guardarlo por si es necesario
        self.packageIP = self.packageUDP.get_IPPacket().clone();
        let sou = self.packageIP.get_SourceAddress();
        let des = self.packageIP.get_DestinationAddress();
        let tamMod = self.tamanioModificado;
        let resultado = match self.packageUDP.sendPackage(tamMod, sou, des) {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Se realizado satisfactoriamente\n 
                el envio del paquete",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
        };

        println!("{:?}", resultado);
    }

    //Funciones para HTTP

    /**
     * @in: campos de InterfazPaquete, que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: String creado en formato JSON para crear un paquete HTTP
     * @function: Esta funcion obtendrá las variables guardadas en la clase InterfazPaquete y creará una cadena
     *              de texto con la que crearemos el paquete HTTP
     */
    fn json_To_HTTP(
        &mut self,
        peticion: String,
        host: String,
        httpver: String,
        listaCabecera: Vec<String>,
        listaEntidades: Vec<String>,
    ) -> String {
        //Como son Strings necesitamos usar .clone()

        let mut stringCabecera = String::new();
        for val in listaCabecera.clone() {
            stringCabecera.push_str(val.as_str());
            stringCabecera.push_str(";");
        }
        let mut stringEntidad = String::new();
        for val in listaEntidades.clone() {
            stringEntidad.push_str(val.as_str());
            stringEntidad.push_str(";");
        }
        let json: String = "Peticion_._".to_owned()
            + &peticion.clone()
            + "#"
            + "Host_._"
            + &host.clone()
            + "#"
            + "HTTPVer_._"
            + &httpver.clone()
            + "#"
            + "ListaCabecera_._"
            + &stringCabecera.clone()
            + "#"
            + "ListaEntidades_._"
            + &stringEntidad.clone()
            + "#";

        return json;
    }

    /**
     * @in: ninguno
     * @out: Objeto de la clase HTTP
     * @function: Constructor por defecto : Esta funcion devuelve un paquete por defecto de la clase HTTP.
     */
    pub fn http(&self) -> HTTP {
        let nuevo = HTTP::new();
        return nuevo;
    }

    /**
     * @in: campos de InterfazPaquete, en un JSON
     * @out: Objeto de la clase HTTP
     * @function: Esta funcion creará un paquete de la clase HTTP a partir de un string JSON que contendrá todas las
     *              variables necesarias para su creación. Esta funcion servirá gracias a la función json_To_IP
     */
    fn http_from_json(&self, json: String) -> Result<HTTP, String> {
        let ippaq = self.packageHTTP.http_From_String(json);

        return ippaq;
    }
    /**
     * @in: inputs de la Interfaz , que tendrán guardados los datos que habrá introducido el usuario
     *      en la Interfaz
     * @out: null
     * @function: Esta funcion obtendrá las variables escritas en la Interfaz por el usuario y las guardará en las variables
     *              de la Clase InterfazPaquete
     */
    pub fn save_FieldsHTTP<'a>(
        &mut self,
        inputDestAdd: String,
        inputDestPort: String,
        

        inputHTTPPetition: String,
        inputHTTPHost: String,
        inputHTTPVer: String,
        inputHTTPVecHeaders: Vec<String>,
        inputHTTPVecEntity: Vec<String>,
    ) {

        self.campoDestinationAddress = inputDestAdd;


        
        self.campoTCPDestPort = inputDestPort;


        self.campoHTTPPetitionType = inputHTTPPetition;
        self.campoHTTPHost = inputHTTPHost;
        self.campoHTTPVersion = inputHTTPVer;
        self.vecHTTPHeaderLines = inputHTTPVecHeaders.clone();
        self.vecHTTPEntityLines = inputHTTPVecEntity.clone();
    }

    /**
     * @in: objeto de la clase HTTP
     * @out: null
     * @function: Esta funcion enviará la clase HTTP con los datos introducidos por la Interfaz
     */
    fn sendPackageHTTP(&mut self, pack: HTTP) {
        self.packageHTTP = pack;
        self.packageTCP = self.packageHTTP.get_TCPPacket(); // actualizamos el paquete ICMP para guardarlo por si es necesario
        self.packageIP = self.packageTCP.get_IPPacket().clone();
        let sou = self.packageIP.get_SourceAddress();
        let des = self.packageIP.get_DestinationAddress();
        let tamMod = self.tamanioModificado;

        /*
        let clonAviso = self.clone();
        let tipMen = "ESPERE, POR FAVOR".to_string();
        let mensajeTexto = Arc::new(Mutex::new(
            "Se está realizando la comunicacion TCP\n 
                espere, por favor",
        ));
        clonAviso.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
        */
        let mut ress = self.packageHTTP.sendPackage(tamMod, sou, des);

        match ress {
            Ok(_) => {
                let clon = self.clone();
                let tipMen = "PAQUETE ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Se realizado satisfactoriamente\n 
                el envio del paquete",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
            Err(e) => {
                let clon = self.clone();
                let tipMen = "PAQUETE NO ENVIADO".to_string();
                let mensajeTexto = Arc::new(Mutex::new(
                    "Ha habido un error al enviar su paquete.\n
                Por favor, revise los campos",
                ));
                clon.popup_Mensaje(tipMen.clone(), mensajeTexto.clone());
            }
        };
    }
}
