#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]


extern crate pnet;
extern crate std;


//use std::alloc::Global;
use std::io::Error;
use pnet::packet::ipv4::*;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::MutableIpv4Packet;
use pnet::packet::*;
use std::net::Ipv4Addr;
use pnet::transport::TransportChannelType::*;
use pnet::transport::TransportProtocol::*;
use pnet::transport::{transport_channel, ipv4_packet_iter};



//Struct es donde se alojan las variables 
#[derive(Clone,Debug)]
pub struct IP {
    versionn : u8,
    IHL : u8,
    typeOfService : u8,
    ecn: u8,
    lengthh : u16,
    ID : u16,
    flags: u8,
    fragmentOffSet: u16,
    TTL :u8,
    protocol: IpNextHeaderProtocol,
    headerChecksum : u16,
    sourceAddress: Ipv4Addr,
    destinationAddress: Ipv4Addr,
    options : Vec<Ipv4Option>,
    // padding se presupone que la clase IPv4 lo rellena solo;
    hexadecimalPayload : Vec<u8>,
    //payloadClass  : Payload //Clase de Payload pero aun no está hecho
}

/**
 * @in : ipv4Pac: Paquete Ipv4 creado a partir de la interfaz
 *       buffer_IP : vector de u8 que  necesitaremos para crear un paquete Ipv4
 * @out : MutableIpv4Packet : Paquete Ipv4 , osea, un objeto Ipv4 en forma de paquete
 * @function : Esta funcion transformara un objeto Ipv4 en un PacketIpv4, objeto que nos permite enviar paquetes Ipv4 por la red
 */
pub fn create_ipv4_packet<'a>(buffer_ip: &'a mut [u8],ipv4Pac : pnet::packet::ipv4::Ipv4, checksu : u16) -> (MutableIpv4Packet,u16) {

    let mut ipv4_packet = MutableIpv4Packet::new(buffer_ip).unwrap();
    let ipv4_aux = ipv4Pac;
     /* Populate crea un Ipv4Packet (objeto que se manda)
                 a partir de un objeto Ipv4 */
    ipv4_packet.populate(&ipv4_aux);    
    let mut num = 0;
    let mut inmutable = ipv4_packet.to_immutable();
    // Generamos el checksum correcto, ya que es un calculo
    
    let checks = pnet::packet::ipv4::checksum(&inmutable); 
    num = checks;
    ipv4_packet.set_checksum(checks);
    

    return (ipv4_packet,num);
}
//Los métodos se ubican en el impl
impl IP {

    /**
     * @in: Ninguno
     * @out: Objeto de la Clase IP generado completamente
     * @function: Constructor por defecto de la clase IP
     */
    pub fn new()-> IP {
        
        let pro = IpNextHeaderProtocol::new(6) ; //Por defecto el siguiente es un TCP
        let sourAd = Ipv4Addr::new(127, 0, 0, 1); // Por defecto es localhost
        let destAd = Ipv4Addr::new(127, 0, 0, 1); // Por defecto es localhost
        let veectt = Vec::new();
        let vectHexaPay = Vec::new();
        IP {
            versionn: 4,
            IHL: 5,
            typeOfService: 0,
            ecn: 1,
            lengthh: 55,
            ID: 1,
            flags:2,
            fragmentOffSet:0,
            TTL:64,
            protocol: pro,
            headerChecksum:0,
            sourceAddress:sourAd,
            destinationAddress:destAd,
            options: veectt,
            hexadecimalPayload:vectHexaPay,
        }
    }
    /**
     * @in: Datos de creación del paquete IP
     * @out: Paquete IP creado
     * @function: La funcion crea un paquete IP a partir de los datos obtenidos
     */
    pub fn ip_From_Data(ver : u8, ihl : u8, tos : u8, ecn : u8, len:u16, id : u16, flag : u8, foff : u16, ttl : u8, ippro : IpNextHeaderProtocol, 
                        check : u16 , sour :Ipv4Addr, dest : Ipv4Addr, hexaPayl : Vec<u8>)-> IP{
        
        let mut packet = [0u8; 3];
        let mut ipv4_options = MutableIpv4OptionPacket::new(&mut packet[..]).unwrap();
        ipv4_options.set_copied(0);
        assert_eq!(ipv4_options . get_copied (  ) , 0);
        ipv4_options.set_class(0);
        assert_eq!(ipv4_options . get_class (  ) , 0);
        ipv4_options.set_number(Ipv4OptionNumber(1));
        assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: NOP);
        let mut opcion = ipv4_options.from_packet(); //Esto transforma MutableIpv4OptionPacket en Ipv4Option
        let mut vecOpt =  Vec::new();
        vecOpt.push(opcion);
        let paquete =  IP {
            versionn: ver,
            IHL: ihl,
            typeOfService: tos,
            ecn: ecn,
            lengthh: len,
            ID: id,
            flags:flag,
            fragmentOffSet:foff,
            TTL:ttl,
            protocol: ippro,
            headerChecksum:check,
            sourceAddress:sour,
            destinationAddress:dest,
            options: vecOpt,
            hexadecimalPayload:hexaPayl,
        } ;

        return paquete;

    }

    /**
     * @in: string "str" : Con el valor del Protocolo que vayamos a usar . Por ejemplo "TCP", "IP" , "UDP" , ...
     * @out: IPNextHeaderProtocol : objeto de esta clase que nos dirá cual es el siguiente protocolo
     * @function: Esta función nos permitirá obtener a partir del string de entrada, escrito por el usuario, 
     *            un objeto de la clase "IPNextHeaderProtocol" , que nos informará de cual es el Protocolo que usaremos
     */
    fn convertNextProtocol(self, aux : &str ) -> IpNextHeaderProtocol {
        //Creamos un valor de IPNextHeaderProtocol que no usaremos en esta aplicacion (IpNextHeader::Reserved).
        //De esta forma si obtenemos a la salida este valor es que no se han introducido datos
        let mut resul = IpNextHeaderProtocol::new(255);
        
        //Codificaremos cada string con el valor al que corresponde en la variable IpNextHeaderProtocol
        match aux {
            "TCP" => {
                resul = IpNextHeaderProtocol::new(6);
            }
            "6" =>{
                resul = IpNextHeaderProtocol::new(6);
            }
            "UDP" => {
                resul = IpNextHeaderProtocol::new(17);
            }
            "17" => {
                resul = IpNextHeaderProtocol::new(17);
            }
            "IPv4" => {
                resul = IpNextHeaderProtocol::new(4);
            }
            "4" => {
                resul = IpNextHeaderProtocol::new(4);
            }
            "IPv6" => {
                resul = IpNextHeaderProtocol::new(41);
            }
            "IP" => {
                resul = IpNextHeaderProtocol::new(4);
            }
            "GPG" => {
                resul = IpNextHeaderProtocol::new(3);
            }
            "3" => {
                resul = IpNextHeaderProtocol::new(3);
            }
            "ICMP" => {
                resul = IpNextHeaderProtocol::new(1); 
            }
            "1" => {
                resul = IpNextHeaderProtocol::new(1);
            }
            _ => {}// caso base
        }


        return resul;
    }
    /**
     * @function : Funcion que transforma binario a un int
     */
    fn binary_to_int(self, auxDSCP : &str)->  &str {
        let mut ret : &str = "";
        match auxDSCP{
            "00" => {
                ret = "0";
            }
            "01" =>{
                ret = "1";
            }
            "10" => {
                ret = "2";
            }
            "11" => {
                ret = "3";
            }
            "000" =>{
                ret = "0";
            }
            "001" => {
                ret = "1";
            }

            "010" => {
                ret = "2";
            }

            "011" => {
                ret = "3";
            }

            _ => { ret = auxDSCP; }
        }

        return ret;
    }
   /**
     * @in: string "opti" : Lista de valores ["0x95", "0x50" , ...]
     * @out: String : String con el valor a convertir en hexadecimal por la funcion "hex"
     * @function: Esta función nos permitirá obtener a partir de los valores escritos en el input de Payload (0x95;0x50;...)
     * un string que se pueda transformar en hexadecimal : "9550..."
     */
    pub fn transform_Hexadecimal(self, separador : Vec<&str>) -> String{
        let mut strAux = String::new();

        for var in separador{ //  0x95
            if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                strAux.push_str(val[1]);
            }
            
        }
        let ret = strAux;
        return ret;
    }

    /**
     * @in: string "opti" : Con el valor de la Opcion que sea necesario : 133 : Security , 0 : End Of Option List, 82 : Traceroute , etc
     * @out: IPv4Option : objeto de esta clase que nos permitirá crear un vector de Opciones para crear IP
     * @function: Esta función nos permitirá obtener a partir del string de entrada, escrito por el usuario, 
     *            un objeto de la clase "IPv4Options" , lo que nos permitirá recopilar las opciones indicadas para este paquete IP
     */

    pub fn obtain_Option( self, opti : &str)-> Ipv4Option{

        let mut packet = [0u8; 3];
        let mut ipv4_options =
                    MutableIpv4OptionPacket::new(&mut packet[..]).unwrap();
        match opti{
            "0" => { // EOOL
                
                    
                ipv4_options.set_copied(0);
                assert_eq!(ipv4_options . get_copied (  ) , 0);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(0));
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: EOL);
                
                
            }
            "1" => { //NOP
                
                
                ipv4_options.set_copied(0);
                assert_eq!(ipv4_options . get_copied (  ) , 0);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(1));
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: NOP);
            }
            "130" => { // SEC
                
                    
                ipv4_options.set_copied(1);
                assert_eq!(ipv4_options . get_copied (  ) , 1);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(2)); //SEC
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: SEC);
                ipv4_options.set_length(&vec!(11));
                assert_eq!(ipv4_options . get_length (  ) , vec ! [ 11 ]);
                ipv4_options.set_data(&vec!(16));
                
            }
            "133" => { // E-SEC
                
                    
                ipv4_options.set_copied(1);
                assert_eq!(ipv4_options . get_copied (  ) , 1);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(5)); //SEC
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: ESEC);
                ipv4_options.set_length(&vec!(16));
                assert_eq!(ipv4_options . get_length (  ) , vec ! [ 16 ]);
                ipv4_options.set_data(&vec!(16));
                
            }
            "136" => { //Stream ID
                
                   
                ipv4_options.set_copied(1);
                assert_eq!(ipv4_options . get_copied (  ) , 1);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(8)); //Stream-ID
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: SID);
                ipv4_options.set_length(&vec!(4));
                assert_eq!(ipv4_options . get_length (  ) , vec ! [ 4 ]);
                ipv4_options.set_data(&vec!(16));
                
            }
            "11" => { //MTU Probe
                
                    
                ipv4_options.set_copied(0);
                assert_eq!(ipv4_options . get_copied (  ) , 0);
                ipv4_options.set_class(0);
                assert_eq!(ipv4_options . get_class (  ) , 0);
                ipv4_options.set_number(Ipv4OptionNumber(11)); //MTUP
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: MTUP);
                ipv4_options.set_length(&vec!(15));
                assert_eq!(ipv4_options . get_length (  ) , vec ! [ 15 ]);
                ipv4_options.set_data(&vec!(16));
                
            }
            "82" => { //Traceroute
                
                ipv4_options.set_copied(0);
                assert_eq!(ipv4_options . get_copied (  ) , 0);
                ipv4_options.set_class(2);
                assert_eq!(ipv4_options . get_class (  ) , 2);
                ipv4_options.set_number(Ipv4OptionNumber(18)); //TR
                assert_eq!(ipv4_options . get_number (  ) , Ipv4OptionNumbers:: TR);
                ipv4_options.set_length(&vec!(15));
                assert_eq!(ipv4_options . get_length (  ) , vec ! [ 15 ]);
                ipv4_options.set_data(&vec!(16));
                
            }
            _ => {}
        }

        let mut ret = ipv4_options.from_packet(); //Esto transforma MutableIpv4OptionPacket en Ipv4Option
        return ret;
    }


    /**
     * @in: String "stringIP" : JSON que decodificaremos para crear un objeto de la clase IP
     * @out: Objeto de la Clase IP generado completamente
     * @function: Constructor de la clase IP a partir de un texto en formato JSON que le brinda todos los datos de sus variables
     */
    pub fn ip_From_String(&self, stringIP : String) -> Result<IP,String> { //Devuelve IP pero cambialo cuando tengas uno decente
        
        /*Definimos las variables con las que crearemos el paquete IP nuevo
        * Ponemos las que teniamos por defecto ya. Esto servirá para crear un paquete
         y para ver a la vez si el paquete se creado correctamente (si tiene datos por defecto
        sabremos que algo ha fallado)
        */
        let mut ver : u8 = self.versionn;
        let mut headerLength: u8 = self.IHL;
        let mut dscp : u8 = self.typeOfService;
        let mut ecn : u8 = self.ecn;
        let mut leng : u16 = self.lengthh;
        let mut ident : u16 = self.ID;
        let mut flagss : u8 = self.flags;
        let mut offsett : u16 = self.fragmentOffSet;
        let mut ttl : u8 = self.TTL;
        let mut nextprotocol : IpNextHeaderProtocol = self.protocol;
        let mut checksu : u16 = self.headerChecksum;
        let mut sour : Ipv4Addr= self.sourceAddress;
        let mut dest : Ipv4Addr= self.destinationAddress;
        let mut opt : Vec<Ipv4Option>= self.options.clone();
        let mut paylload : Vec<u8>=  self.hexadecimalPayload.clone();
        //Primero separamos en las distintas variables con split (iterador) y convertimos en vector con collect

        let vecSplit: Vec<&str>  =  stringIP.split("#").collect();

        //Una vez tenemos el vector iteramos sobre él y obtenemos sus valores

        for val in vecSplit{
            // Como el dato de val es "Val:Valor" separamos en ":" y sabemos que split[0] será "Val" y split[1] será "Valor"

            let vectorCampos: Vec<&str>  = val.split(":").collect();
            let valorCampo = vectorCampos[0];

            match valorCampo{
                "Version" => {
                    let aux = vectorCampos[1];
                    let auxVer : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    ver = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                }
                "IHL" => {
                    let aux = vectorCampos[1];
                    let auxIHL : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxIHL) => Ok(auxIHL),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    headerLength = auxIHL.unwrap();
                }
                "TypeOfService"=> {
                    let aux = vectorCampos[1];
                    let auxDSCP : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxDSCP) => Ok(auxDSCP),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    dscp = auxDSCP.unwrap();
                }
                "ECN"=> {
                    let aux = vectorCampos[1];
                    let clon = self.clone();
                    let auxStr = clon.binary_to_int(aux);
                    let auxECN : Result<u8, String>= match auxStr.parse::<u8>(){
                        Ok(auxECN) => Ok(auxECN),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    ecn = auxECN.unwrap();
                }
                "Length"=> {
                    let aux = vectorCampos[1];
                    let auxLeng : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxLeng) => Ok(auxLeng),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    leng = auxLeng.unwrap();
                }
                "ID"=> {
                    let aux = vectorCampos[1];
                    let auxID : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxID) => Ok(auxID),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    ident = auxID.unwrap();
                }
                "Flags"=> {
                    let aux = vectorCampos[1];
                    let clon = self.clone();
                    let auxStr = clon.binary_to_int(aux);
                    let auxFlags : Result<u8, String>= match auxStr.parse::<u8>(){
                        Ok(auxFlags) => Ok(auxFlags),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    flagss = auxFlags.unwrap();
                }
                "FragmentOffset"=> {
                    let aux = vectorCampos[1];
                    let auxOff : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxOff) => Ok(auxOff),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    offsett = auxOff.unwrap();
                }
                "TTL"=> {
                    let aux = vectorCampos[1];
                    let auxTTL : Result<u8, String>= match aux.parse::<u8>(){
                        Ok(auxTTL) => Ok(auxTTL),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    ttl = auxTTL.unwrap();
                }
                "Protocol"=> {
                    let aux = vectorCampos[1];
                    //Suponemos que IPNextProtocol es un numero (4)
                    let clon = self.clone();
                    let auxP = clon.convertNextProtocol(aux);
                    let auxPro : Result <IpNextHeaderProtocol,String> =  match auxP { 

                        // Miramos el valor que tiene auxPro. Como nos dice la funcion, si es "Reserved" , sabremos que no hay datos
                        IpNextHeaderProtocol(255) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))), 

                        IpNextHeaderProtocol(_) => Ok(auxP), //En otro caso, ha cogido un valor correcto y se acepta
                    };
                    nextprotocol = auxPro.unwrap();
                } 
                "HeaderChecksum"=> {
                    let aux = vectorCampos[1];
                    let auxHed : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxHed) => Ok(auxHed),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    checksu = auxHed.unwrap();
                }
                "Source"=> {
                    let aux = vectorCampos[1]; //127.0.0.1

                    // Como la direccion IP está separada por "." , cogeremos y decodificaremos eso a numeros

                    let mut vectorIPADDR: Vec<&str> = aux.split(".").collect();

                    //Una vez sabemos que se puede dividir  por puntos, observamos si se pueden cambiar por strings
                    let bit1 : Result<u8,String>  = match vectorIPADDR[0].parse::<u8>() { //127

                        Ok(bit1) => Ok(bit1),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))), 
                    };
                    let biit1 = bit1.unwrap();

                    let bit2 : Result<u8,String>  = match vectorIPADDR[1].parse::<u8>() { //0

                        Ok(bit2) => Ok(bit2),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))), 
                    };
                    let biit2 = bit2.unwrap();

                    let bit3 : Result<u8,String>  = match vectorIPADDR[2].parse::<u8>() { //0

                        Ok(bit3) => Ok(bit3),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    let biit3 = bit3.unwrap();
                    let bit4 : Result<u8,String>  = match vectorIPADDR[3].parse::<u8>() { //1

                        Ok(bit4) => Ok(bit4),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    }; 
                    let biit4 = bit4.unwrap();
                    
                    //Si todos son strings podemos crear una direccion IP satisfactoriamente
                    sour = Ipv4Addr::new(biit1, biit2, biit3, biit4);
                }
                "Destination"=> {
                    let aux = vectorCampos[1];
                    // Como la direccion IP está separada por "." , cogeremos y decodificaremos eso a numeros
                    let vectorIPADDR : Vec<&str>=  aux.split(".").collect();
                                        
                    //Una vez sabemos que se puede dividir  por puntos, observamos si se pueden cambiar por strings
                    let bit1 : Result<u8,String>  = match vectorIPADDR[0].parse::<u8>() { //127

                        Ok(bit1) => Ok(bit1),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),   
                    };
                    let biit1 = bit1.unwrap();

                    let bit2 : Result<u8,String>  = match vectorIPADDR[1].parse::<u8>() { //0

                        Ok(bit2) => Ok(bit2),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    let biit2 = bit2.unwrap();

                    let bit3 : Result<u8,String>  = match vectorIPADDR[2].parse::<u8>() { //0

                        Ok(bit3) => Ok(bit3),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))), 
                    }; 
                    let biit3 = bit3.unwrap();

                    let bit4 : Result<u8,String>  = match vectorIPADDR[3].parse::<u8>() { //1

                        Ok(bit4) => Ok(bit4),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))), 
                    }; 
                    let biit4 = bit4.unwrap();

                    //Si todos son strings podemos crear una direccion IP satisfactoriamente
                    dest = Ipv4Addr::new(biit1, biit2, biit3, biit4);
                }
                "Options"=> { //Provisional. Falta saber como pasar string a vector, no se la estructura del vector
                    let aux = vectorCampos[1]; // 0;130;133;1

                    // Como cada Opcion está separado por ";" , cogeremos y decodificaremos eso a numeros
                    let mut auxVer = Vec::new();
                    if headerLength>5 {
                        let mut vectorIPADDR: Vec<&str> = aux.split(";").collect();
                    
                        for var in vectorIPADDR{ // Crearemos cada opcion a partir de su variable y se la devolveremos al vector
                            if var != "" { // En caso de que al dividir quede un "" , como en Payload
                                let clon = self.clone();
                                let optionAux = clon.obtain_Option(var); 
                                auxVer.push(optionAux);
                            }
                        } 
                    }
                    
                    opt = auxVer;
                }
                "Payload"=> { //Provisional. Falta saber como pasar string a vector, no se la estructura del vector
                    let aux = vectorCampos[1]; // "0x95;0xff;...;0x43"
                    let mut vecHexa = Vec::new();
                    if aux != "" {
                        let mut separador : Vec<&str> = aux.split(";").collect(); //Cada elemento es "0x95"
                        //Una vez tenemos el string entero lo convertimos a u8
                        let clon = self.clone();
                        let numHexa = clon.transform_Hexadecimal(separador); //Obtenemos un string
                        let numHexaStr = numHexa.as_str();
                        let vecHexa = match hex::decode(numHexaStr){
                            Ok(res) => {vecHexa = res},
                            Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                            asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),
                        };//Lo transformamos a Vec<u8>
                    }
                    paylload = vecHexa;  //Si payload viene  vacio así irá el payload. Si tiene datos los codificaremos
                }
                _ => { } // caso base
            }

        }
        
        Ok(IP{versionn: ver,
            IHL: headerLength,
            typeOfService: dscp,
            ecn: ecn,
            lengthh: leng,
            ID: ident,
            flags: flagss,
            fragmentOffSet: offsett,
            TTL: ttl,
            protocol: nextprotocol,
            headerChecksum: checksu,
            sourceAddress: sour,
            destinationAddress: dest,
            options: opt,
            hexadecimalPayload: paylload,
        })  

    }

    //Payload es de clase Payload pero aun no está hecha asi que se usa String
    // Es como la funcion anterior solo que en este caso está usando Payload externo, se convierte a payload hexadecimal y es justo igual 
    pub fn ip_From_Payload(&self, ipText : String, payloadC : String )-> () {


    }
   /**
     * @in: Int "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, que devolverá a la Interfaz
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    pub fn field_Help(&self, campo : String) -> String{
        let mut ayuda : String = String::new();
        let campoStr = campo.as_str();
        match campoStr{
            "Version" => {
                let helpe = "El campo Version (4 bits) indica la versión\n de la cabecera de IP que se enviará.\nEn este caso, trabajamos
                 con IPv4,\n por lo que el valor será 4.\n\nPara mas información:\n 'https://www.rfc-editor.org/rfc/rfc791.html'";
                ayuda = helpe.to_string();
            }
            "IHL" => {
                let helpe = "Internet Header Length o IHL (4 bits)\n
                es el campo que indica la longitud\n de la cabecera del protocolo
                en palabras de 32 bits,\n por lo que sirve para apuntar al inicio de\n los datos de dicho protocolo.\n
                El valor minimo para este campo es '5'\n (5 palabras x 4 bytes/palabra = 20 bytes de cabecera),\n cuando no incluye ningun campo en Options,\n
                y el maximo valor, 15, cuando incluye los 40 bytes de Options.\n\n 
                Será importante conocer el tamaño de las opciones que vayamos a incluir,\n
                pues deberemos redondear al multiplo de 4(bytes/palabra) más cercano\n 
                para que el campo IHL se cree correctamente.\n
                Así, si añadimos el campo Security, Extended Security y EOL, el resultado será:\n
                11 + 15 + 1 = 27 -> 28 -> 28/4 = 7 \n
                IHL= 7+5= 12 \n\n
                Para mas información: 'https://www.rfc-editor.org/rfc/rfc791.html'";
                ayuda = helpe.to_string();
            }
            "TypeOfService"=> {
                let helpe = "El campo Tipo de Servicio (8 bits) ofrece\n la indicación de la Calidad de Servicio\n ( o Quality of Service,QoS)\n
                que desea que tenga cada paquete de red.\n Como se sabe,\n este campo servirá para priorizar\n una serie de paquetes frente a otros\n
                y que el control de estos sean más eficiente.\nEste campo se divide en 2:\n\nEl código DSCP ,differentiated services code point,\n que estás modificando actualmente(y los\n primeros 6bits del campo) y el siguiente, 
                que hará la función de ECN (2 bits restantes).\n\n
                Los posibles valores de DSCP pueden ser:\n Best Effort/BE = 0 (000 000, la menor prioridad),\n Assured Forwarding/AF=18 (010 010 ,\n este es uno tipico pero AF puede tomar distintos valores)
                y \nExpedited Forwarding/EF = 46\n (101 110, el que mayor prioridad tiene) \n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc791.html' y\n
                 'https://www.cisco.com/c/en/us/td/docs/switches/data\ncenter/nexus1000/sw/4_0/qos/configuration/\nguide/nexus1000v_qos/qos_6dscp_val.pdf' ";
                ayuda = helpe.to_string();
            }
            "ECN"=> {
                let helpe = "El campo de Notificacion de Congestion Explicita\n (Explicit Congestion Notification, ECN),\n 
                correspondiente a los 2 ultimos bits\n del campo 'Type Of Service' de IP,\n es una extension del campo con el mismo nombre de TCP.\n
                Este campo ayuda a saber \ncomo trabajaran emisor y receptor\n del paquete IP cuando hay congestión de datos,\n usando por ejemplo un protocolo de descongestion.\n\n Los valores posibles son:\n
                ECN=0 ('00' , O emisor o receptor no es capaz de interpretar ECN)\n
                ECN=1 ('01' , Se sabe interpretar la congestion)\n
                ECN=2 ('10' , Lo mismo que ECN = 01)\n
                ECN=3 ('11' , Se ha encon trado congestion\ny deben tomarse medidas)\n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc791.html'\n y 'https://datatracker.ietf.org/doc/html/rfc3168#section-21'";
                ayuda = helpe.to_string();
            }
            "Length"=> {
                ayuda = "El campo de Length (16 bits)\ncorresponde a la longitud del datagrama completo,\n
                 medida en octetos e incluyendo\n la cabecera de Internet y los datos.\n\n
                 El campo permite hasta 65535 octetos en bytes.\n
                 Esto quiere decir que si tu paquete\n puede ser mayor a 576 bits,\n
                 (tamaño maximo de un paquete sin fragmentar)\n
                 deberias considerar activar la fragmentacion en 'Flags'.\n
                 La longitud minima es de 21, cuando no lleva Opciones ni Payload,\n.
                 \n\nPara mas información: \n
                 'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "ID"=> {
                ayuda = "EL campo de Identificacion (16 bits)\nsirve para
                 identificar\n el paquete en la red, tanto si es\n
                  uno entero como si es\n parte del datagrama cuando
                   este\n se divide para su envio y\n
                    así ordenarlo una vez recibido el paquete entero\n\n
                    Para mas información: \n
                    'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "Flags"=> {
                ayuda = "El campo de Flags (3 bits) indica si\n el paquete debe particionarse en subpaquetes\n. Sus valores seran : \n
                Bit 0: Debe ser 0 siempre\n\n
                Bit 1 : '0' ->\nMay Fragment (MF, el paquete podrá fragmentarse)\n
                Bit 1 : '1' ->\nDont Fragment (DF, el paquete no necesita fragmentarse)\n\n
                Bit 2 : '0' ->\nLast Fragment (LF, en caso de particionarse, es la ultima parte)\n
                Bit 2 : '1' ->\nMore Fragments (MF, aun le quedan\n fragmentos por recibir para completar el paquete\n\n
                    Para mas información: \n'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "FragmentOffset"=> {
                ayuda = "El campo FragmentOffset (13 bits) indica ,\n
                 en unidades de octetos (64 bits),\n
                  donde se ubica este fragmento\nen el datagrama completo,\n en caso de estar fragmentado.\n
                   El primer fragmento tiene Offset 0.\n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "TTL"=> {
                ayuda = "El campo Time To Live (8 bits) indica\n
                 el numero de saltos que\n el paquete puede dar en internet\n
                  antes de llegar a su destino.\n Este campo decrecerá en 1 cada vez\n
                   que llegue a una unidad\n que lo procese (por ejemplo, un router).\n
                   Cuando llegue a 0,\n el paquete desaparecerá como medida\n para evitar congestión en la red.\n
                   Se recomienda poner '64' como minimo de saltos.\n\n
                   Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "Protocol"=> {
                ayuda = "El campo Protocolo (8 bits)\n indica cual es el protocolo \nde siguiente nivel que tendremos en el payload.\n
                Entre los posibles valores que puede haber tenemos :\n 1(ICMP), 6(TCP), 17(UDP), 4(IPv4)\n\n
                Para mas información: 'https://www.rfc-editor.org/rfc/rfc791.html'\n  y 'https://www.rfc-editor.org/rfc/rfc790'
                ".to_string();
            } //Siguiente protocolo. Es un codigo numerico que te dirá que es lo siguiente, si un IP, un mensaje al router, un email, etc 
            "HeaderChecksum"=> {
                ayuda = "El campo checksum (16 bits) es el campo que comprueba\n la validez del datagrama IP. Esta validez se comprueba a partir \n
                de un valor generado a partir de un cálculo con el resto de campos\n
                 de la cabecera del datagrama, por lo que este valor cambiará cada vez que cambie el TTL.\n
                  Este campo se calculará por tanto en cada router, \n
                  por lo que para la construcción del campo este tendrá valor 0\n
                  aunque tambien puedes calcularlo antes del envio.\n\nPara mas información: \n
                  'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            "Source"=> {
                ayuda = "El campo Source Address (32 bits) establece \n
                la direccion desde la que se envia el datagrama IP,\n
                 en formato 255.255.255.255 .\n\nPara mas información: 'https://www.rfc-editor.org/rfc/rfc791.html',
                 \n 'https://www.rfc-editor.org/rfc/rfc791.html#section-3.2'\n y en las clases de Redes ".to_string();
            }
            "Destination"=> {
                ayuda = "El campo Destination Address (32 bits) establece \n
                la direccion que recibirá el paquete,\n
                 en formato 255.255.255.255 .\n\nPara mas información: 'https://www.rfc-editor.org/rfc/rfc791.html',
                  \n 'https://www.rfc-editor.org/rfc/rfc791.html#section-3.2'\n y en las clases de Redes ".to_string();
            }
            "Options"=> {
                ayuda = "El campo Options (Nº Variable de Bits entre 0 y 40 bytes), añade una capa de opciones al datagrama IP,\n
                siendo cada una una palabra de 32 bits (4 bytes) y 
                 permitiendo añadir una capa de Seguridad (IPSec), 
                 hacer Traceroute, etc \n\n
                 Puedes añadir hasta 10 opciones, separadas por ';'.Entre ellas estan:\n\n
                  1 -> No Operation: Para indicar que no añades Opciones\n
                  0 -> End of Options List : Indica el fin del conjunto de opciones.\nDebe ser la ultima opcion indicada\n
                  11 -> MTU Probe: Encontrar el  minimo MTU de los paquetes enviados sin fragmentar\n
                  130-> Security : Seguridad basica con flags de autenticacion\n
                  133 -> E-Sec : Extented Security : Información de seguridad añadida a la anterior y requerida por algunas autoridades\n
                  134-> Commercial Security: Usar seguridad en la red para dominios seguros como centros comeriales\n
                  82 -> Realizar Traceroute con el envío\n\n

                  Se debe tener en cuenta la lista de campos y sus longitudes para
                  crear correctamente el campo IHL.\n De esta forma tenemos:\n\n
                  NOP y EOL = 1 byte\n
                  Security = 11 bytes\n
                  Stream ID = 4 bytes\n
                  Opciones con Longitud Variable = 15 bytes\n\n
                  Así, si añadimos el campo Security, Extended Security y EOL, el resultado será:\n
                    11 + 15 + 1 = 27 -> 28 -> 28/4 = 7 \n
                    IHL= 7+5= 12 \n\n
                  Para mas información: 'https://www.rfc-editor.org/rfc/rfc791.html',
                  \n 'https://www.rfc-editor.org/rfc/rfc791.html#section-3.2', \n
                  'https://www.iana.org/assignments/ip-parameters/ip-parameters.xhtml',\n
                  'http://www.tcpipguide.com/free/t_IPDatagramOptionsandOptionFormat.htm'\n 
                  y 'https://datatracker.ietf.org/'
                  ".to_string(); //Es variable, el resto se rellena automaticamente con Padding hasta llegar a los 32 bits
            }
            "HexadecimalPayload"=> {
                ayuda = "El campo de Payload (32 bits) servirá para\n
                 añadir mediante codigos hexadecimales\n los campos de protocolos de capa superiores a IP,\n
                 sin necesidad de crear una nueva capa\n en el paquete para la edición\nde dicho protocolo\n\n
                 Puedes introducir los datos mediante el boton de tu izquierda\n\n
                 Para mas información: 'https://www.rfc-editor.org/rfc/rfc791.html'. ".to_string();
            }
            "Payload"=> {
                ayuda = "Con este boton podrás crear y modificar\n
                 un protocolo de capa superior a IP y este se añadirá al Payload del datagrama IP,\n
                 de la misma forma que modificandolo de forma hexadecimal\n\n
                 Este boton permitirá modificar el payload de forma mas simple que\n
                 mediante texto. Cuando su edicion termine,\n
                 se guardara en el campo de texto.\n\n
                 Para mas información: 'https://www.rfc-editor.org/rfc/rfc791.html'. ".to_string();
            }

            "Padding" => {
                ayuda = "El Padding es un conjunto de bits a 0 (basura)\n colocados tras el campo 'Options',\n
                 para completar hasta 32 los bits que haya escrito dicho campo.\n\nPara mas información: \n
                 'https://www.rfc-editor.org/rfc/rfc791.html'".to_string();
            }
            _ => {} // caso base
        }
        return ayuda;
    }


    // enviar paquete con Payload. Como no hay clase Payload aun está en proceso
    pub fn send_Package(&self) -> () { // Debe ser Result<(), Error> , pero al no poder hacerse aun esta vacio y no devuelve nada
        // Que sea Result Nos permitirá obtener que ha ocurrido y mostrarlo, lo cual necesitamos

    }
    /**
     * @in: Ninguno
     * @out: Result<Ok,Error>
     * @function: A partir de un paquete de la clase IP se creará un paquete IPv4 y se enviará, recibiendo Ok() si así ha sido 
     * y Error() si ha habido algún problema. Dicho Error() habrá que tratarlo como corresponda 
     */
    pub fn send_IP_Package(&self) ->  Result< u16, String> { //Nos permitirá obtener que ha ocurrido y mostrarlo, lo cual necesitamos

        //Primero creamos el paquete IPv4 a partir de los datos que tenemos guardados en la clase IP

        let envio = 
        pnet::packet::ipv4::Ipv4{version: self.versionn,
            header_length: self.IHL,
            dscp: self.typeOfService,
            ecn: self.ecn,
            total_length: self.lengthh,
            identification: self.ID,
            flags: self.flags,
            fragment_offset: self.fragmentOffSet,
            ttl: self.TTL,
            next_level_protocol: self.protocol,
            checksum: self.headerChecksum,
            source: self.sourceAddress,
            destination: self.destinationAddress,
            options: self.options.clone(),
            payload: self.hexadecimalPayload.clone(),
        }; 
        
        //Una vez creado el paquete IP, procedemos a mandarlo
        //Para ello, debemos incluirlo en un transport_channel, que crearemos primero
        //Creamos el TransportChannelType
        let mut protocol = pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocol::new(4));
        match self.protocol {
            IpNextHeaderProtocol(4) =>{
                protocol = pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocols::Ipv4);
            }
            IpNextHeaderProtocol(41)  =>{
                protocol =pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocols::Ipv6);
            }
            IpNextHeaderProtocol(6)  =>{
                protocol =pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocols::Tcp);
            }

            IpNextHeaderProtocol(17)  =>{
                protocol =pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocols::Udp);
            }
            IpNextHeaderProtocol(1) =>{
                protocol =pnet::transport::TransportChannelType::Layer3(IpNextHeaderProtocols::Icmp);
            }

            _ =>{}

        }

        let (mut tx, mut rx) = match transport_channel(4096, protocol) { // Tamaño de 4096 bytes
            Ok((tx, rx)) => (tx, rx),
            Err(e) => return Err(String::from(format!("Error al enviar el paquete,\n 
            debido a Error : '{:?}' ",e))),
        };
        // Creamos un Ipv4Packet vacio para poder instroducirle nuestro paquete IPv4
        let mut int_slice = [0u8; 500];
       
        let mut new_packet = create_ipv4_packet(&mut int_slice, envio, self.headerChecksum).0;
        let addr = self.get_DestinationAddress();
         // Mandamos el paquete
        match tx.send_to(new_packet, std::net::IpAddr::V4(addr)) {// Conviertes Ipv4Addr en IpAddr
            Ok(n) => return Ok(n.try_into().unwrap()),
            Err(e) => return Err(String::from("Error en el envio,\n
            asegurese de haber construido\ncorrectamente el paquete")), 
        }
    }

    /**
     * @in: Paquete IP
     * @out: checksum del paquete IP
     * @function: La funcion genera un checksum a partir de los datos que hay en la interfaz cuando esta es llamada 
    */

    pub fn generaChecksum(&self) -> u16 {
        //Generamos el paquete IPv4 a partir de los datos que tenemos actualmente
        let pac = 
        pnet::packet::ipv4::Ipv4{version: self.versionn,
            header_length: self.IHL,
            dscp: self.typeOfService,
            ecn: self.ecn,
            total_length: self.lengthh,
            identification: self.ID,
            flags: self.flags,
            fragment_offset: self.fragmentOffSet,
            ttl: self.TTL,
            next_level_protocol: self.protocol,
            checksum: self.headerChecksum,
            source: self.sourceAddress,
            destination: self.destinationAddress,
            options: self.options.clone(),
            payload: self.hexadecimalPayload.clone(),
        }; 
        let mut int_slice = [0u8; 500];
       
        // Llamamos a la funcion checksum
        let resultado = create_ipv4_packet(&mut int_slice, pac, self.headerChecksum).1;

        return resultado;
    }

    // Getter y Setter

    pub fn get_Version(&self) -> u8 {
        return self.versionn;
    }

    pub fn set_Version (&mut self, verr : u8) -> () {
        self.versionn = verr;
    }

    pub fn get_IHL(&self) -> u8 {
        return self.IHL;
    }

    pub fn set_IHL (&mut self, ihl : u8) -> () {
        self.IHL = ihl;
    }


    pub fn get_TypeOfService(&self) -> u8 {
        return self.typeOfService
    }

    pub fn set_TypeOfService (&mut self, tos : u8) -> () {
        self.typeOfService = tos;
    }

    pub fn get_ECN(&self) -> u8 {
        return self.ecn
    }

    pub fn set_ECN (&mut self, eecn : u8) -> () {
        self.ecn = eecn;
    }



    pub fn get_Length(&self) -> u16 {
        return self.lengthh;
    }

    pub fn set_Length (&mut self, len : u16) -> () {
        self.lengthh = len;
    }


    pub fn get_ID(&self) -> u16 {
        return self.ID;
    }

    pub fn set_ID (&mut self, id : u16) -> () {
        self.ID = id;
    }


    pub fn get_Flags(&self) -> u8 {
        return self.flags;
    }

    pub fn set_Flags (&mut self, flag : u8) -> () {
        self.flags = flag;
    }


    pub fn get_FragmentOffset(&self) -> u16 {
        return self.fragmentOffSet;
    }

    pub fn set_FragmentOffset (&mut self, offset : u16) -> () {
        self.fragmentOffSet = offset;
    }


    pub fn get_TTL(&self) -> u8 {
        return self.TTL;
    }

    pub fn set_TTL (&mut self, ttl : u8) -> () {
        self.TTL = ttl;
    }


    pub fn get_Protocol(&self) -> IpNextHeaderProtocol {
        return self.protocol;
    }

    pub fn set_Protocol (&mut self, protcol : IpNextHeaderProtocol) -> () {
        self.protocol = protcol;
    }


    pub fn get_HeaderChecksum(&self) -> u16 {
        return self.headerChecksum;
    }

    pub fn set_HeaderChecksum (&mut self, chechsum : u16) -> () {
        self.headerChecksum = chechsum;
    }


    pub fn get_SourceAddress(&self) -> Ipv4Addr {
        return self.sourceAddress;
    }

    pub fn set_SourceAddress (&mut self, source : Ipv4Addr) -> () {
        self.sourceAddress = source;
    }


    pub fn get_DestinationAddress(&self) -> Ipv4Addr {
        return self.destinationAddress;
    }

    pub fn set_DestinationAddress (&mut self, destination : Ipv4Addr) -> () {
        self.destinationAddress =  destination;
    }
    
    pub fn get_Options(&self) -> Vec<Ipv4Option> { // Como no hay copia en este vector tenemos que crear un vector
        let v : Vec<Ipv4Option> = self.options.clone();

        return v
    }

    pub fn set_Options (&mut self, optio  : Vec<Ipv4Option>) -> () { // Como no hay copia tenemos que crear un vector
        let v : Vec<Ipv4Option> =  optio.clone();

         self.options = v;

    }
   
    pub fn get_HexPayload(&self) -> Vec<u8> {
        let v : Vec<u8> = self.hexadecimalPayload.clone();
        return v;
    }

    pub fn set_HexPayload (&mut self, payl : Vec<u8>) -> () {

        let v : Vec<u8> = payl.clone();

         self.hexadecimalPayload = v;
    }
    


}
