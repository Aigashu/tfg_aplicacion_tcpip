//#![feature(allocator_api)]
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(non_snake_case)]
#![allow(unused_imports)]

use std::thread;
use fltk::{app, menu::*,text::*, button::Button,frame::Frame, group::*, prelude::*, window::Window};

pub mod Interfaces;
pub use Interfaces::InterfazPaquete;
/*#[path = "./Protocols/mod.rs"]
mod Protocols;
use Protocols::IP as IP;*/

fn main() {

    let mut inter = InterfazPaquete::new();
    inter.createInterface();
    
}
