#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]


extern crate pnet;
extern crate std;
use std::io::{Read, Write};
use std::time::Duration;
use net2::TcpBuilder;
use std::net::{ToSocketAddrs, SocketAddr};
use std::net::SocketAddrV4;
use std::net::{Shutdown, TcpStream, IpAddr};
//use tokio::net::TcpSocket;
//use tokio::io::{self, AsyncWriteExt};
use crate::Interfaces::Protocols::TCP::TCP;
use crate::Interfaces::Protocols::IP::IP;
use crate::Interfaces::Protocols::IP::create_ipv4_packet;

//use std::alloc::Global;
use std::io::Error;
use pnet::datalink::Channel::Ethernet;
use pnet::datalink::*;
use pnet::packet::ethernet::EthernetPacket;
use pnet::packet::ethernet::{EtherType, EtherTypes};
use std::net::Ipv4Addr;
use pnet::packet::ipv4::*;
use pnet::packet::*;
use pnet::packet::tcp::*;
use pnet::packet::tcp::MutableTcpPacket;
use pnet::packet::tcp::TcpOptionNumber;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;
use pnet::packet::ipv4::*;
use pnet::transport::transport_channel;



#[derive(Clone,Debug)]
pub struct HTTP {
    petitionType : String, //GET,POST, ...
    host : String, //pagina a la que irá
    httpVer : String, //version del Protocolo
    headerLines : Vec<String>, //Vector con las lineas de cabecera que el usuario añada (Acept-language, User-Agent,...)
    entityLines : Vec<String> , //Vector con las lineas de entidad que el usuario añada (Content-Type, Content-Length, Date, ...)
    tcpPacket : TCP,
}

impl HTTP{
    /**
     * @in:
     * @out: Objeto de la clase HTTP
     * @function: Constructor por defecto de HTTP
     */
    pub fn new() -> HTTP{ 

        
        let mut vecHeader = Vec::new();
        vecHeader.push(String::from("Accept-Language: fr"));
        vecHeader.push(String::from("User-Agent: proyectIP"));
        let mut vecEntity = Vec::new();
        
        vecEntity.push(String::from("Accept: */*"));
        vecEntity.push(String::from("Content-Type:text/html"));
        let mut tcpAux = TCP::new();
        HTTP{
            petitionType : "GET /".to_string(), //PAQUETE SYN obtenido desde Wireshark
            host : "www.stackoverflow.com".to_string(),
            httpVer : "2.0".to_string(),
            headerLines : vecHeader,
            entityLines : vecEntity,
            tcpPacket : tcpAux ,
        }
    }

    /**
     * @in: Datos de creación del paquete HTTP
     * @out: Paquete HTTP creado
     * @function: La funcion crea un paquete HTTP a partir de los datos obtenidos
     */
    pub fn http_From_Data( petit : String, hostt : String, ver :String, headL : Vec<String>, 
            entLin : Vec<String>,tcpPc : TCP )-> HTTP{
        

        let tcpClone = tcpPc.clone();

        let paquete =  HTTP{
            petitionType : petit.clone(), //PAQUETE SYN obtenido desde Wireshark
            host : hostt.clone(),
            httpVer : ver.clone(),
            headerLines : headL.clone(),
            entityLines : entLin.clone(),
            tcpPacket : tcpClone ,
        };

        return paquete;

    }
    /**
     * @in: String con todas las cabeceras que el usuario ha decidido añadir al mensaje HTTP
     * @out: Vector con las mismas cabeceras pero decodificadas 
     * @function:  Esta funcion convertirá una lista de cabeceras HTTP que el usuario incluya a un vector con 
     *                cada una individual, para que sea mas facil describirlo y lo interprete la clase HTTP
     */
    pub fn decodeHeaderLines(&self, cabeceras : String) -> Vec<String>{
        let mut vectorResultado = Vec::new();
        let vecSplitHeaders: Vec<&str>  =  cabeceras.split(";").collect();
        for value in vecSplitHeaders{
            if value != ""{
                vectorResultado.push(value.to_string());
            }
        }

        return vectorResultado;
    }

    /**
     * @in: String con todas las lineas de Entidad que el usuario ha decidido añadir al mensaje HTTP
     * @out: Vector con las mismas lineas de Entidad pero decodificadas 
     * @function:  Esta funcion convertirá las lineas de Entidad HTTP que el usuario incluya a un vector con 
     *                cada una individual, para que sea mas facil describirlo y lo interprete la clase HTTP
     */
    pub fn decodeEntityLines(&self, entidades : String) -> Vec<String>{
        let mut vectorResultado = Vec::new();
        let vecSplitHeaders: Vec<&str>  =  entidades.split(";").collect();
        for value in vecSplitHeaders{
            if value != ""{
                vectorResultado.push(value.to_string());
            }
        }

        return vectorResultado;
    }
    /**
     * @in: String que contiene todos los datos de la clase HTTP codificados en Texto
     * @out: Paquete HTTP
     * @function: Esta funcion transforma un string en un paquete HTTP completo, a partir de los datos que este string contiene
     */

    pub fn http_From_String(&self, stringHTTP : String) -> Result<HTTP,String> { 
        
        /*Definimos las variables con las que crearemos el paquete HTTP nuevo
        * Ponemos las que teniamos por defecto ya. Esto servirá para crear un paquete
         y para ver a la vez si el paquete se creado correctamente (si tiene datos por defecto
        sabremos que algo ha fallado)
        */

        let mut nuevaPeticion  = self.petitionType.clone(); 
        let mut nuevoHost = self.host.clone();
        let mut nuevaVer = self.httpVer.clone();
        let mut nuevoVecHeader  = self.headerLines.clone();
        let mut nuevoVecEntity = self.entityLines.clone();
        let mut paylTCP  = self.tcpPacket.clone();
        
        
        //Primero separamos en las distintas variables con split (iterador) y convertimos en vector con collect

        let vecSplit: Vec<&str>  =  stringHTTP.split("#").collect();

        //Una vez tenemos el vector iteramos sobre él y obtenemos sus valores

        for val in vecSplit{
            // Como el dato de val es "Val:Valor" separamos en ":" y sabemos que split[0] será "Val" y split[1] será "Valor"

            let vectorCampos: Vec<&str>  = val.split("_._").collect();
            let valorCampo = vectorCampos[0];

            match valorCampo{
                "Peticion"=>{
                    nuevaPeticion = vectorCampos[1].to_string(); 
                },
                "Host"=>{
                    nuevoHost = vectorCampos[1].to_string();
                },
                "HTTPVer"=>{
                    nuevaVer = vectorCampos[1].to_string();
                },
                "ListaCabecera"=>{

                    let copiaLista = vectorCampos[1].to_string();
                    let vecAux = self.decodeHeaderLines(copiaLista);
                    nuevoVecHeader = vecAux.clone();
                },
                "ListaEntidades"=>{
                    let copiaLista = vectorCampos[1].to_string();
                    let vecAux = self.decodeEntityLines(copiaLista);
                    nuevoVecEntity = vecAux.clone();
                },
                _=>{},
            }
        }

        Ok(HTTP{
            petitionType : nuevaPeticion.clone(), //PAQUETE SYN obtenido desde Wireshark
            host : nuevoHost.clone(),
            httpVer : nuevaVer.clone(),
            headerLines : nuevoVecHeader.clone(),
            entityLines : nuevoVecEntity.clone(),
            tcpPacket : paylTCP ,
        })

    }

    /** 
     * @in: Int "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, que devolverá a la Interfaz
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    pub fn field_Help(&self, campo : String) -> String{
        let mut ayuda : String = String::new();
        let campoStr = campo.as_str();
        match campoStr{
            "HTTPPeticion" => {
                let helpe = "El campo 'Peticion'/'Solicitud' define el tipo de mensaje HTTP que queremos obtener\n y deberán acompañarse de la dirección a solicitar dentro de la pagina\n ('/' si no deseas solicitar algo en concreto)\n
                Por ejemplo: 'GET /notifications HOST: www.twitter.com' .\n  
                Este campo varía su comportamiento dependiendo del valor que le indiquemos.\n 
                Estos valores y sus comportamientos son los siguientes:\n
                GET : El método GET obtiene datos de un servidor a partir de\n incluir parámetros en la URL de la solicitud.Es el metodo más usado.\n
                HEAD : El método HEAD es igual al GET, pero solo obtiene los datos\n de la cabecera HTTP de la respuesta y no el cuerpo con el código HTML.\n
                POST : El método POST se usa para publicar en el servidor datos, por ejemplo un fichero o la respuesta a un formulario.\n
                PUT : El método PUT se usa para solicitar al servidor que guarde el cuerpo del mensaje HTTP en la dirección indicada.\n
                DELETE : El método DELETE solicita el borrado del fichero en la dirección indicada.\n
                CONNECT : El método CONNECT sirve para establecer una conexión a un servidor/web mediante HTTP.\n
                OPTIONS : El método OPTIONS solicita al servidor o a la web concreta\n
                 los métodos y opciones de cabecera que dicha web o servidor permite.\n 
                 Puedes usar '*' para solicitar que opciones permite el servidor entero.\n
                 TRACE : El método TRACE sirve para mostrar el contenido de una solicitud HTTP,\n de la misma forma que si observasemos esta solicitud con un debugger.\n  
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc2616'";
                ayuda = helpe.to_string();
            },
            "HTTPHost" => {
                let helpe = "El campo 'Host' define el enlace del servidor o web al que vamos a realizar la solicitud HTTP.
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc2616'";
                ayuda = helpe.to_string();
            },
            "HTTPVersion" => {
                let helpe = "El campo 'Version' indica la version del protocolo HTTP que se usará para el envio.\n
                Las versiones pueden ser '1.0', '1.1' y '2.0'.\n Depende de la version se permiten unas cabeceras u otras.
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc2616'";
                ayuda = helpe.to_string();
            },
            "HTTPListaCabecera" => {
                let helpe = "Los campos de cabecera son campos que añaden informacion adicional\n
                al envio HTTP, lo cual facilita la comprensión del paquete HTTP por parte del servidor.\n
                Hay muchas cabeceras distintas, pero las más usadas son:\n
                Cache-Control : Informa del tipo de sistema de caché que busca la solicitud.\n
                    Puede ser: 'no-cache' , 'no-store', 'only-if-cached', 'min-fresh = seconds', etc\n
                Connection : Da datos al servidor de como se desea que se realice la conexión.\n
                    Puede ser : 'keep-alive', 'upgrade'\n
                Accept-Language : Lista de idiomas en los que puede devolverte la solicitud de la web/servidor.\n
                    Puede ser: 'en', 'fr', 'es', 'de', ...\n
                User-Agent : Permite identificar el Sistema Operativo/Navegador que hace la solicitud HTTP.\n
                From: Indica quien está haciendo la solicitud HTTP. Normalmente suele ser un email.\n
                If-Modified-Since: Solicita la información de la solicitud si\n
                dicha información fue modificada posterior a la fecha indicada .\n
                    Por ejemplo: 'If-Modified-Since: Sat, 29 Oct 1994 19:43:31 GMT'\n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc2616'";
                ayuda = helpe.to_string();
            },
            "HTTPListaEntidades" => {
                let helpe = "Los campos de entidad son campos de cabecera especiales que definen\n
                 la información y los metadatos del cuerpo del mensaje HTTP. Suelen usarse en mensajes de respuesta.\n
                 Entre estas cabeceras se encuentran: \n
                Content-Type : Define el tipo de contenido que se envia.\n
                    Por ejemplo:  'text/html', 'image/jpeg', 'audio/mpeg', 'video/mp4', 'application/json', etc\n
                Date: Fecha en la que el mensaje fue originado. Por ejemplo: 'Date: Tue, 15 Nov 1994 08:12:31 GMT'\n
                Expires: Indica la fecha en la que los datos enviados caducarán.\n
                    Por ejemplo: 'Expires: Tue, 15 Nov 1994 08:12:31 GMT'\n
                Last-Modified : Fecha en la que se modificaron los datos por última vez.\n
                    Por ejemplo: 'Last-Modified: Tue, 15 Nov 1994'\n
                Content-Length: Define el tamaño en bytes del objeto que se envía.\n
                    Por ejemplo: 'Content-Length: 685'\n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc2616'";
                ayuda = helpe.to_string();
            },
            _=>{},
        }
        return ayuda;
    }

     /**
     * @in : Paquete HTTP
     * @out: Paquete HTTP codificado en string completo
     * @function : Esta funcion convertirá el paquete HTTP que tenemos actualmente en un texto de cabecera HTTP completo
     */

    pub fn httpToText(&self)-> String{
        let mut versionHttp = self.httpVer.as_str();
        let mut stringMensajeHTTP = self.petitionType.to_ascii_uppercase().clone();
        stringMensajeHTTP.push_str(" ");
        stringMensajeHTTP.push_str("HTTP/");
        if self.httpVer.as_str() != "1.0" && self.httpVer.as_str() != "1.1"  && self.httpVer.as_str() != "2.0"  {
            versionHttp = "1.1";
        }
        stringMensajeHTTP.push_str(versionHttp);
        stringMensajeHTTP.push_str("\r\n");
        stringMensajeHTTP.push_str("Host: ");
        stringMensajeHTTP.push_str(self.host.as_str());
        stringMensajeHTTP.push_str("\r\n");

        for val in self.headerLines.clone(){
            stringMensajeHTTP.push_str(val.as_str());
            stringMensajeHTTP.push_str(" \r\n");
        }

        for val in self.entityLines.clone(){
            stringMensajeHTTP.push_str(val.as_str());
            stringMensajeHTTP.push_str("\r\n");
        }
        stringMensajeHTTP.push_str("\r\n");
        return stringMensajeHTTP.clone();
    }
    /**
     * @in : Paquete HTTP
     * @out: Paquete HTTP codificado como vector de bytes/payload de TCP
     * @function : Esta funcion convertirá el paquete HTTP que tenemos actualmente en el payload del protocolo TCP
     */
    pub fn httpToPayload(&self) -> Vec<u8>{
       

        let mut vecPaylHTTP =Vec::new();
        let stringMensajeHTTP = self.httpToText();
        /*
            El mensaje stringMensajeHTTP deberia quedar tal que así :
            > GET / HTTP/1.1
            > Host: www.stackoverflow.com
            > User-Agent: insomnia/2022.5.0
            > Cookie: prov=bd93e9ec-cd36-4411-592e-8a5c017cce67
            > Accept-language: fr
            > Accept: /
        */
        vecPaylHTTP = stringMensajeHTTP.into_bytes();
        return vecPaylHTTP;
    }


    /**
     * @in: 
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion será llamada por la interfaz y permitirá mandar un mensaje TCP con su payload HTTP
     */
       pub fn sendPackage(&mut self, tamanio : u16 , sour : Ipv4Addr , dest : Ipv4Addr) ->  Result< u16, String>{

        //Transformamos en payload el paquete
        let mut paquete = self.httpToPayload();

        let envio = self.send_HTTPPackage(paquete, tamanio, sour, dest);
        return envio;
    }

/**
     * @in: nuevoPayload: Paquete HTTP convertido en Payload de TCP
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion se encargará de enviar correctamente un  paquete HTTP , usando la cabecera TCP
     */
    fn send_HTTPPackage(&mut self, nuevoPayload : Vec<u8>, tamanio : u16 , sour : Ipv4Addr , dest : Ipv4Addr)->  Result< u16, String>{

        let mut nuevoTam = 0;
        
            nuevoTam = 500;
        
        //Para mandar el mensaje HTTP primero creamos la conexión. Para ello, enviamos un mensaje TCP de inicio de conexio
        //Esto se hace modificando los flags de TCP. Los valores de flag de peticion de TCP : SYN
        //u8 con detalles de wireshark
       
 
        //modifica source port y destination port del TcpPacket
 
        
        let sourcePort = self.tcpPacket.get_SourcePort();
        let destPort = self.tcpPacket.get_DestinationPort();
        

      

        let listaInter = interfaces();        
        //Creamos la interfaz de recepcion, pues la respuesta al SYN llega pronto        
        let mut interfazseleccionada = NetworkInterface {
            name: "".to_string(),
            description: "".to_string(),
            index: 0,
            mac: None,
            ips: Vec::new(),
            flags: 0,
        };

        if sour == Ipv4Addr::new(127, 0, 0, 1) && dest == Ipv4Addr::new(127, 0, 0, 1){ // Si la ruta que buscamos es local
            let interf = String::from("lo");
            for val in listaInter{
                let stringVal = val.name.clone();
                match val.name{
                    interf => { //eth0 , enps03, ... <- Ethernet
                        interfazseleccionada = NetworkInterface {
                            name: stringVal.clone(),
                            description:val.description.clone(),
                            index: val.index,
                            mac: val.mac,
                            ips: val.ips.clone(),
                            flags: val.flags,
                        };
                        break;
                    }
                   
                    _ => (),
                }
            }
        }else{ //Si no lo es
            for val in listaInter{
                match val.name.chars().next(){
                    Some('e') => { //eth0 , enps03, ... <- Ethernet
                        interfazseleccionada = NetworkInterface {
                            name: val.name.clone(),
                            description:val.description.clone(),
                            index: val.index,
                            mac: val.mac,
                            ips: val.ips.clone(),
                            flags: val.flags,
                        };
                        break;
                    }
                    Some('w') => { //wpsf03 , .. <-wifi
                        interfazseleccionada = NetworkInterface {
                            name: val.name.clone(),
                            description:val.description.clone(),
                            index: val.index,
                            mac: val.mac,
                            ips: val.ips.clone(),
                            flags: val.flags,
                        };
                        break;
                    }
                    _ => (),
                }
            }
        }//Aqui teemos ya la interfaz 
        let mut config = Default::default();
        let (mut tx, mut sx) = //Creamos la recepcion de paquetes
        match channel(&interfazseleccionada, config) {
            Ok(Ethernet(tx, sx)) => (tx, sx),
            _ => panic!(),
        };
        let mut encontrado = 0; //Bool que dice si hemos encontrado respuesta
        let mut vecTCP = Vec::new();

        let mut envioTcp = TcpBuilder::new_v4().unwrap();
        envioTcp.bind(SocketAddrV4::new(sour, sourcePort));
        let nuevoStream = envioTcp.to_tcp_stream().unwrap();
        let socketCua = SocketAddrV4::new(dest, destPort);
        let mut nuevoSocket = socketCua.to_socket_addrs().unwrap();
        let val = nuevoSocket.next().unwrap();
        let mut  conexionTCP = TcpStream::connect_timeout(&val, Duration::new(30, 0)).unwrap(); //conexion durante 30 seg
        
        
        

        let mut nuevaIPV = Ipv4Addr::new(127,0,0,1);
        let mut nuevoPort = 0;
        //Esperamos a la respuesta
        while encontrado == 0{ //Abierta la conexion, esperamos el mensaje 
            match sx.next() {
                //Recibimos en sx los paquetes que han llegado a esa interfaz
                Ok(packet) => {
                    let packetEther =
                    EthernetPacket::new(packet).unwrap();
                    
                    let paqueteIP = Ipv4Packet::new(packetEther.payload()).unwrap();
                    if paqueteIP.get_source() == dest  //Si es mensaje de respuesta
                    && paqueteIP.get_next_level_protocol() == IpNextHeaderProtocol::new(6){ //Y es TCP
                        vecTCP = paqueteIP.payload().to_vec();
                        nuevaIPV = paqueteIP.get_destination();
                        encontrado = 1; //Encuentra paquete respuesta, asi que salimos
                    }
                        
                }
                Error => {}
            }
        }
        //Una vez recibimos la respuesta, comprobamos si esa afirmativa enviamos el mensaje ACK con el mensaje HTTP
        let paylIP = vecTCP.as_slice();
        let paqueteTCP = TcpPacket::new(paylIP).unwrap();
        let flagsRecepcion = paqueteTCP.get_flags();
        let numeroSecuencia = paqueteTCP.get_sequence();
        let nuevoAck = paqueteTCP.get_acknowledgement();
        nuevoPort = paqueteTCP.get_destination(); //El puerto de destino a nosotros será el origen

        if flagsRecepcion != 18{ //18 = 010010 = 16+2 = ACK +SYN =Mensaje de respuesta correcta
            return Err(String::from("No se ha confirmado la conexión TCP.\nPor favor, intentelo de nuevo."));
        }
        //Si lo aceptó Ahora si, enviamos el mensaje
        let mut envio = Ok(1);
        match conexionTCP.write(&nuevoPayload) {
        Ok(_) => {
            envio = Ok(0); //Ok
             
        },
        Err(e) => {
            envio = Err(String::from("No se pudo enviar correctamente el mensaje"));
        }
        }        

        return envio;
    }


    //Getter y Setter
    pub fn get_PetitionType(&self) -> String {
        return self.petitionType.clone();
    }
    pub fn set_PetitionType (&mut self, petTyp : String) -> () {
        self.petitionType = petTyp.clone();
    }

    pub fn get_Host(&self) -> String {
        return self.host.clone();
    }
    pub fn set_Host (&mut self, hostt : String) -> () {
        self.host = hostt.clone();
    }

    pub fn get_HTTPVer(&self) -> String {
        return self.httpVer.clone();
    }
    pub fn set_HTTPVer (&mut self, ver : String) -> () {
        self.httpVer = ver.clone();
    }

    pub fn get_HeaderLines(&self) -> Vec<String> {
        return self.headerLines.clone();
    }
    pub fn set_HeaderLines (&mut self, headLi : Vec<String>) -> () {
        self.headerLines = headLi.clone();
    }

    pub fn get_EntityLines(&self) -> Vec<String> {
        return self.entityLines.clone();
    }
    pub fn set_EntityLines (&mut self, entLin : Vec<String>) -> () {
        self.entityLines = entLin.clone();
    }

    pub fn get_TCPPacket(&self) -> TCP {
        return self.tcpPacket.clone();
    }
    pub fn set_TCPPacket (&mut self, tcpp : TCP) -> () {
        self.tcpPacket = tcpp;
    }
}