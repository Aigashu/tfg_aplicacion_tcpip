#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]


extern crate pnet;
extern crate std;


use crate::Interfaces::Protocols::IP::IP;
//use std::alloc::Global;
use std::io::Error;
use std::net::Ipv4Addr;
use pnet::packet::ipv4::*;
use pnet::packet::*;
use pnet::packet::udp::*;
use pnet::packet::udp::MutableUdpPacket;
use pnet::packet::tcp::TcpOptionNumber;
use pnet::packet::ip::IpNextHeaderProtocol;
use pnet::packet::ip::IpNextHeaderProtocols;

#[derive(Clone,Debug)]
pub struct UDP {
    sourcePort : u16,
    destinationPort: u16,
    length : u16,
    checksum : u16,
    payload : Vec<u8>,
    ipPacket : IP,
}

/**
 * @in : ipv4Pac: Paquete TCP creado a partir de la interfaz
 *       buffer_IP : vector de u8 que  necesitaremos para crear un paquete Ipv4
 *       direccionOrigen : direccion Origen del paquete IP , para calcular el cheksum de TCP
 *       direccionDestino: direccion Destino del paquete IP , para calcular el cheksum de TCP
 * @out : MutableTcpPacket : Paquete TCP , osea, un objeto Tcp en forma de paquete
 * @function : Esta funcion transformara un objeto Tcp en un PacketTcp, objeto que nos permite enviar paquetes Tcp por la red
 *              y  nos devolverá ese objeto y su checksum, en caso de necesitarlo 
 */
pub fn create_udp_packet<'a>(buffer_ip: &'a mut [u8],ipv4Pac : pnet::packet::udp::Udp, checksu : u16,
             direccionOrigen : Ipv4Addr, direccionDestino : Ipv4Addr) -> (MutableUdpPacket,u16) {

    let mut ipv4_packet = MutableUdpPacket::new(buffer_ip).unwrap();
    let ipv4_aux = ipv4Pac;
     /* Populate crea un Ipv4Packet (objeto que se manda)
                 a partir de un objeto Ipv4 */
    ipv4_packet.populate(&ipv4_aux);    
    let mut num = 0;
    let mut inmutable = ipv4_packet.to_immutable();
    // Generamos el checksum correcto, ya que es un calculo
    let checks = pnet::packet::udp::ipv4_checksum(&inmutable,&direccionOrigen,&direccionDestino); 
    num = checks;
    ipv4_packet.set_checksum(checks);
    
    
    return (ipv4_packet,num);
}

impl UDP{

    /**
     * @in:
     * @out: Objeto de la clase TCP
     * @function: Constructor por defecto de TCP
     */
    pub fn new() -> UDP{ 

        let checks = 0;
        let seq = 0;
        let offs = 0;
        let vecPayl = Vec::new();
        let mut ipAux = IP::new();
        ipAux.set_Protocol(IpNextHeaderProtocol::new(17)); //Es importante que indiquemos que el paquete es TCP
        
        UDP{
            sourcePort : 80, //PAQUETE SYN obtenido desde Wireshark
            destinationPort : 57888,
            length : 0,
            checksum : 0,
            payload : vecPayl,
            ipPacket : ipAux,
        }
    }

    /**
     * @in: Datos de creación del paquete UDP
     * @out: Paquete UDP creado
     * @function: La funcion crea un paquete UDP a partir de los datos obtenidos
     */
    pub fn udp_From_Data( sPort : u16, destPort: u16, len : u16, checksu : u16, payl : Vec<u8>, ipPc : IP )-> UDP{
        

        let copiaPayl = payl.clone();
        let ipClone = ipPc.clone();

        let paquete =  UDP{
            sourcePort : sPort,
            destinationPort : destPort,
            length : len,
            checksum : checksu,
            payload : copiaPayl,
            ipPacket : ipClone,
        };

        return paquete;

    }

    /**
     * @in: string "opti" : Lista de valores ["0x95", "0x50" , ...]
     * @out: String : String con el valor a convertir en hexadecimal por la funcion "hex"
     * @function: Esta función nos permitirá obtener a partir de los valores escritos en el input de Payload (0x95;0x50;...)
     * un string que se pueda transformar en hexadecimal : "9550..."
     */
    pub fn transform_Hexadecimal(self, separador : Vec<&str>) -> String{
        let mut strAux = String::new();

        for var in separador{ //  0x95
            if var != "" { //Este if es para controlar . Puede existir un ultimo elemento "" al final del vector
                let val : Vec<&str> = var.split("x").collect(); //vec[0] = 0 , vec[1] = 95
                strAux.push_str(val[1]);
            }
            
        }
        let ret = strAux;
        return ret;
    }
    
    /**
     * @in: String que contiene todos los datos de la clase UDP codificados en Texto
     * @out: Paquete UDP
     * @function: Esta funcion transforma un string en un paquete UDP completo, a partir de los datos que este string contiene
     */

    pub fn udp_From_String(&self, stringUDP : String) -> Result<UDP,String> { //Devuelve IP pero cambialo cuando tengas uno decente
        
        /*Definimos las variables con las que crearemos el paquete UDP nuevo
        * Ponemos las que teniamos por defecto ya. Esto servirá para crear un paquete
         y para ver a la vez si el paquete se creado correctamente (si tiene datos por defecto
        sabremos que algo ha fallado)
        */

        let mut sPort : u16 = self.sourcePort; 
        let mut destPort: u16 = self.destinationPort;
        let mut len : u16 = self.length;
        let mut checksu : u16 = self.checksum;
        let mut paylTCP : Vec<u8> = self.payload.clone();
        
        
        //Primero separamos en las distintas variables con split (iterador) y convertimos en vector con collect

        let vecSplit: Vec<&str>  =  stringUDP.split("#").collect();

        //Una vez tenemos el vector iteramos sobre él y obtenemos sus valores

        for val in vecSplit{
            // Como el dato de val es "Val:Valor" separamos en ":" y sabemos que split[0] será "Val" y split[1] será "Valor"

            let vectorCampos: Vec<&str>  = val.split(":").collect();
            let valorCampo = vectorCampos[0];

            match valorCampo{
                "PuertoOrigen"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    sPort = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "PuertoDestino"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    destPort = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Length"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    len = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Checksum"=>{
                    let aux = vectorCampos[1];
                    let auxVer : Result<u16, String>= match aux.parse::<u16>(){
                        Ok(auxVer) => Ok(auxVer),
                        //Si no podemos obtener satisfactoriamente la conversión quiere decir que algo ha pasado 
                        Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                        asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),  
                    };
                    checksu = auxVer.unwrap(); //El unwrap es para convertir de OK() al valor que tiene dentro
                },
                "Payload"=> { //Provisional. Falta saber como pasar string a vector, no se la estructura del vector
                    let aux = vectorCampos[1]; // "0x95;0xff;...;0x43"
                    let mut vecHexa = Vec::new();
                    if aux != "" {
                        let mut separador : Vec<&str> = aux.split(";").collect(); //Cada elemento es "0x95"
                        //Una vez tenemos el string entero lo convertimos a u8
                        let clon = self.clone();
                        let numHexa = clon.transform_Hexadecimal(separador); //Obtenemos un string
                        let numHexaStr = numHexa.as_str();
                        let vecHexa = match hex::decode(numHexaStr){
                            Ok(res) => {vecHexa = res},
                            Err(e) => return Err(String::from(format!("Error al obtener dato del campo\n '{:?}' ,\n 
                            asegurese de haber introducido datos \n o haberlos introducido correctamente",valorCampo))),
                        };//Lo transformamos a Vec<u8>
                    }
                    paylTCP = vecHexa;  //Si payload viene  vacio así irá el payload. Si tiene datos los codificaremos
                }
                _=>{},
            }
        }

        let ipPc = self.ipPacket.clone();
        Ok(UDP{
            sourcePort : sPort,
            destinationPort : destPort,
            length : len,
            checksum : checksu,
            payload : paylTCP,
            ipPacket : ipPc,
        })

    }


    
    /** 
     * @in: Int "campo", que nos dirá sobre que campo de dicho protocolo necesita información
     * @out: String con la información de dicho campo especifico de el protocolo especificado, que devolverá a la Interfaz
     * @function: Esta funcion solicitará, a partir de los datos de entrada, información sobre un campo del paquete 
     *              que se está creando  y devolverá dicha información
     */
    pub fn field_Help(&self, campo : String) -> String{
        let mut ayuda : String = String::new();
        let campoStr = campo.as_str();
        match campoStr{
            "PuertoOrigenUDP" => {
                let helpe = "El campo 'SourcePort' define el puerto de origen\n desde el que se envia la información.\n
                El puerto del que provenga indicará\n también que tipo de paquete estamos enviado. Por ejemplo:\n
                7: echo \n
                80  : HTTP\n
                8080 : Pruebas de HTTP\n
                21: FTP\n
                22: SFTP y SSH\n
                25: SMTP \n
                53:  DNS\n
                113 : Auth : Authentication Service\n
                123: Network Time Protocol\n
                33434 : traceroute \n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc768.html'";
                ayuda = helpe.to_string();
            },
            "PuertoDestinoUDP" => {
                let helpe = "El campo 'DestinationPort' define el puerto de destino\n al que irá a parar la información.\n
                Este puerto también indica\n que tipo de paquete estamos enviado. Por ejemplo:\n
                7: echo \n
                80  : HTTP\n
                8080 : Pruebas de HTTP\n
                21: FTP\n
                22: SFTP y SSH\n
                25: SMTP \n
                53:  DNS\n
                113 : Auth : Authentication Service\n
                123: Network Time Protocol\n
                33434 : traceroute \n
                \n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc768.html'";
                ayuda = helpe.to_string();
            },
            "UDPLength" => {
                let helpe = "El campo Sequence/Secuencia indicará el numero de secuencia\n donde está el primer octeto de datos en el segmento.\n
                Si el flag SYN está activado, este campo será el inicio de la secuencia\n y el primer octeto de datos será el valor del campo+1\n\n
                Para mas informacion: 'https://www.rfc-editor.org/rfc/rfc768.html'";
                ayuda = helpe.to_string();
            },            
            "UDPChecksum" => {
                let helpe = "Este valor es una suma de comprobacion\nque indica que el paquete no fue modificado.\n
                                Se calcula en el envio pero puedes\n pre calcularlo con el boton de la derecha\n\n
                                Para mas informacion:\n'https://www.rfc-editor.org/rfc/rfc768.html'.\n";
                ayuda = helpe.to_string();
            },
            "UDPPayload" => {
                let helpe = "El campo de Payload (32 bits) servirá para\n
                añadir mediante codigos hexadecimales\n los campos de protocolos de capa superiores a UDP,\n
                sin necesidad de crear una nueva capa\n en el paquete para la edición\nde dicho protocolo.\n\n
                Generalmente, al ser TCP por encima hay un protocolo de capa 5.\n
                Puedes introducir los datos mediante el boton de tu derecha\n\n
                Para mas información:\n 'https://www.rfc-editor.org/rfc/rfc768.html'. ";
                ayuda = helpe.to_string();
            },
            "UDPBotonPayload" => {
                let helpe = "Con este boton podras modificar\n comodamente el payload de capa superior a UDP\n";
                ayuda = helpe.to_string();
            },
            _=>{},
        }
        return ayuda;
    }



    /**
     * @in : Paquete de clase UDP
     * @out: Vector de bytes 
     * @function : Esta funcion transformará el paquete UDP que hemos creado en un vector de bytes para que sea
     *              el payload del paquete IP que hay inmediatamente debajo. Devolverá tambien el tamaño de este 
     *              payload, para que pueda sumarse al paquete total de UDP
     */
    pub fn udpToPayload(&self, sour : Ipv4Addr, dest : Ipv4Addr) -> (Vec<u8>,usize) {

        let mut vectorRetorno = Vec::new();
        // Convertimos el objeto actual en un paquete ICMP
        let pac = 
        pnet::packet::udp::Udp{
            source: self.sourcePort,
            destination: self.destinationPort ,
            length : self.length,
            checksum: self.checksum,
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 400];
       
        // Llamamos a la funcion que nos dará un MutableICMPPacket
        let mut resultado = create_udp_packet(&mut int_slice, pac, self.checksum,sour,dest);
        let mut paquete = resultado.0;
        let mut nuevoChecksum = resultado.1;

        //Una vez tenemos el MutableICMPPacket , lo transformamos a &[u8]
        let tamBytes = paquete.packet_size();
        let bytesV = paquete.packet_mut();
        
        //Y de &[u8] a Vec<u8>
        vectorRetorno = bytesV.to_vec();

        return (vectorRetorno,tamBytes);
    }

/**
     * @in: Paquete UDP
     * @out: checksum del paquete UDP
     * @function: La funcion genera un checksum a partir de los datos que hay en la interfaz cuando esta es llamada 
    */

    pub fn generaChecksum(&self, sour : Ipv4Addr, dest : Ipv4Addr) -> u16 {
        //Generamos el paquete ICMP a partir de los datos que tenemos actualmente
        let pac = 
        pnet::packet::udp::Udp{
            source: self.sourcePort,
            destination: self.destinationPort ,
            length : self.length,
            checksum: self.checksum,
            payload: self.payload.clone(),
        }; 

        let mut int_slice = [0u8; 400];
       
        // Llamamos a la funcion checksum
        let resultado = create_udp_packet(&mut int_slice, pac, self.checksum, sour, dest).1;

        return resultado;
    }

    /**
     * @in:IP de Origen y Destino, Tamaño total del paquete IP+UDP
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion será llamada por la interfaz y permitirá mandar un mensaje IP con su payload UDP
     */
    pub fn sendPackage(&mut self, tamanioModificado : u16, sour : Ipv4Addr, dest : Ipv4Addr) ->  Result< u16, String>{

        //Transformamos en payload el paquete
        let mut paquete = self.udpToPayload(sour, dest);
        let paylICMP = paquete.0;
        let tamPayload = paquete.1;

        let envio = self.send_UDPPackage(paylICMP,tamPayload, tamanioModificado);
        return envio;
    }

/**
     * @in: nuevoPayload: Paquete UDP convertido en Payload de IP
     *      tamPayload : Tamaño del payload UDP, que habrá que sumarle al paquete IP para un envio correcto
     * @out: Result que nos informará si el envío fue correcto o no
     * @function: Esta funcion se encargará de enviar correctamente un  paquete UDP , usando la cabecera IP
     */
    fn send_UDPPackage(&mut self, nuevoPayload : Vec<u8>, tamPayload : usize, tamanioModificado : u16 )->  Result< u16, String>{

        let mut tamReal = (tamPayload as u16);
        let mut tamAnterior = self.ipPacket.get_Length();
        let mut nuevoTam = 0;
        
        nuevoTam = 500;
        
        //Actualizamos datos del paquete IP
        &self.ipPacket.set_HexPayload(nuevoPayload);
        &self.ipPacket.set_Length( nuevoTam);
        let envio =  self.ipPacket.send_IP_Package();

        return envio;

    }

    //Getter y Setter

    pub fn get_SourcePort(&self) -> u16 {
        return self.sourcePort;
    }
    pub fn set_SourcePort (&mut self, sourp : u16) -> () {
        self.sourcePort = sourp;
    }

    pub fn get_DestinationPort(&self) -> u16 {
        return self.destinationPort;
    }
    pub fn set_DestinationPort (&mut self, destP : u16) -> () {
        self.destinationPort = destP;
    }

    pub fn get_Length(&self) -> u16 {
        return self.length;
    }
    pub fn set_Length (&mut self, len : u16) -> () {
        self.length = len;
    }

    pub fn get_Checksum(&self) -> u16 {
        return self.checksum;
    }
    pub fn set_Checksum (&mut self, check : u16) -> () {
        self.checksum = check;
    }

    pub fn get_Payload(&self) -> Vec<u8> {
        return self.payload.clone();
    }

    pub fn set_Payload (&mut self, payl : Vec<u8>) -> () {
        self.payload = payl.clone();
    }

    pub fn get_IPPacket(&self) -> IP {
        return self.ipPacket.clone();
    }

    pub fn set_IPPacket (&mut self, pac : IP) -> () {
        self.ipPacket = pac;
    }


}