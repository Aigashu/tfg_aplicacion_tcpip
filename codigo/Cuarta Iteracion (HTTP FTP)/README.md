## Ejecucion


Para poder ejecutar correctamente esta aplicación, debe dar permisos de creación de paquetes de bajo nivel. Para ello, ejecute:

```
$ ./proyectIP.sh
```
Este comando ejecutará un archivo BASH que ya ofrece permisos de forma automática, facilitando la obtención de estos y la apertura de ambas interfaces. La aplicación de recepción realiza la obtención de paquetes durante 5 segundos en intervalos de 30 en 30 segundos.

De no querer hacerlo de esta manera, puede ir a los ficheros correspondientes y ejecutar en su lugar: 

```
$ sudo setcap cap_net_admin,cap_net_raw=pei ./target/debug/proyectIP

$ ./target/debug/proyectIP
```

## Notas
> Se ha dividido la aplicación en dos partes : Envío y Recepción. Esto se hizo para evitar el problema de bloqueo de la aplicación que se producía al enviar paquetes o al realizar la recepción de los mismos, impidiendo hacer ambas de forma paralela.

> Esta aplicacion funciona de la misma manera que en la iteracion anterior, con la diferencia que el boton "Payload" de ICMP abre la interfaz de dicho protocolo, en lugar de un lector hexadecimal. 
