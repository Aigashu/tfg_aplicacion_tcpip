\chapter{Documentación de Requisitos}
\label{RequisitosAplicacion}
\section{Introducción}

Lo esencial para crear una buena aplicación es el cumplimiento de una serie de requisitos, que nos permitirán construir una aplicación correcta y funcional. Esto se hará a partir de entrevistas y revisiones con el cliente, que nos permitirán realizar un análisis exhaustivo sobre cómo hacer dicha aplicación como este la desea. Para empezar, definamos qué queremos que haga nuestra aplicación para posteriormente profundizar en cada uno de estos requisitos.


Nuestra aplicación debe permitir la composición y edición de paquetes de capa de red (capa 3), capa de transporte (capa 4) y capa de aplicación (capa 5). Se incluirán por tanto los protocolos indicados en la tabla \ref{Tabla.RequisitosNecesarios}.

\input{tablas/RequisitosNecesarios}

Además, si los costes de elaboración de este trabajo nos lo permiten, dispondremos de estos protocolos, mostrados en tabla \ref{Tabla.RequisitosOptativos}.

    
\textbf{Requisitos optativos}

\input{tablas/RequisitosOptativos}


La aplicación debe permitir la edición de estos paquetes de forma independiente (edición de paquete TCP) o como parte de un paquete de capa inferior (edición de un paquete TCP como parte del \textit{payload} del paquete IP), incluyendo además en la primera creación de estos campos que de forma predeterminada funcionen (por ejemplo, el campo ``\textit{tipo de servicio}'' de IP tomará un valor válido ''YYYY'' de forma predeterminada). A su vez, la interfaz debe apoyarse con una opción de ''\textit{ayuda}'', que permita al usuario entender qué significa cada campo de cada protocolo implementado para que este sepa cómo usarlo, y con una respuesta a los errores cometidos en el envío de un paquete, que permitan al usuario conocer qué campos de qué protocolo ha rellenado erróneamente y pueda editarlos para generar un envío satisfactorio. Por último, la interfaz  debe contar con un modo que permita recoger el tráfico de paquetes enviados y visualizarlos.

\section{Requisitos necesarios}

Los requisitos anteriormente descritos se han plasmado en un diagrama de casos de uso (Figura \ref{DiagramaRequisitosCU}). Estos diagramas nos ayudarán a entender cómo deberá comportarse la aplicación desde punto de vista del cliente. Estos casos de uso se desarrollarán posteriormente para tener una visión más específica de la funcionalidad que ofrecerá nuestro proyecto y que podremos implementar en código.

% Diagrama de casos de uso

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\linewidth]{imagenes/Diagrama_Requisitos.eps}
    \caption{Diagrama de casos de uso de la aplicación }
    \label{DiagramaRequisitosCU}
\end{figure}

Es importante que tengamos en cuenta que, para toda la aplicación, cuando hablemos de uso de paquetes nos referiremos a paquetes a enviar creados con los protocolos presentados en la Figura \ref{DiagramaPaqueteCapas}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{imagenes/Diagrama_PaqueteCapas.eps}
    \caption{Protocolos disponibles para la creación de paquetes }
    \label{DiagramaPaqueteCapas}
\end{figure}


\subsection{Requisitos funcionales}

Los requisitos especificados en el diagrama de casos de uso se especificarán en los diferentes casos de uso posibles, como muestra la tabla \ref{Tabla.CasosdeUso} .

% Tabla con el codigo de cada caso de Uso y a que corresponde

\input{tablas/CasosdeUso_Asignacion}

En el primer caso, mostrado en la tabla \ref{CU01}, trataremos cómo se creará y editará un paquete de un protocolo, independientemente de su capa. Este caso de uso resulta sencillo, pues a la vez que se explica cómo la aplicación funciona se va creando una idea mental de la aplicación. Es también importante recalcar la creación del paquete de protocolo ya que dependiendo de qué capa sea el protocolo se creará un paquete mayor o menor. Así, creando un paquete HTTP (protocolo perteneciente a la capa de aplicación) se generará un paquete HTTP, que hará de \textit{payload} para un paquete de la capa inferior, la capa de transporte, la cual formará a su vez el \textit{payload} del paquete IP, creando un paquete de datos completo desde la capa de red hasta la capa de aplicación a la que pertenece el protocolo HTTP. En cambio, si creamos un paquete de IP solo crearemos un paquete de protocolo de red, añadiendo el resto de información a partir del \textbf{CU-03} (tabla \ref{CU03}). Se presenta por último la casuística de la necesidad de ayuda a la comprensión de ciertos campos, nuestra aplicación solucionará esta problemática.

\input{tablas/CasosdeUso_CasoUno}

A continuación, en la tabla \ref{CU02} y mostrando el \textbf{CU-02}, nos encontraríamos el envío de dicho paquete de datos, anteriormente editado. En este caso se guardarán los datos editados y se enviarán. Este es el caso en el que encontraremos el soporte de errores, que nos avisará si algún campo es incorrecto y nos permitirá corregirlo antes de intentar enviarlo una vez más.

\input{tablas/CasosdeUso_CasoDos}


Una vez que podemos crear, editar y enviar paquetes, debemos plantear cómo crear paquetes de forma eficiente. Como hemos adelantado previamente, al crear un paquete tendremos la posibilidad de seleccionar el tipo de paquete de protocolo a crear y en función de dicha elección se creará un paquete más o menos completo. 

Pero, ¿qué ocurre en caso estar editando un paquete de protocolo y necesitar editar los niveles superiores? Para responder a esto tendremos el \textbf{CU-03} en la tabla \ref{CU03}, que nos permitirá editar el campo de \textit{payload} de cualquier paquete de protocolo. Esto será posible mediante la creación de la capa superior, que nos permitirá realizar el \textbf{CU-03} (Ver tabla \ref{CU01}) para el paquete de protocolo superior, o mediante la inserción del paquete de protocolo en el campo \textit{payload} a través de  caracteres hexadecimales.

\input{tablas/CasosdeUso_CasoTres}

Por último, en la tabla \ref{CU04}, hay que tratar la recepción de paquetes en la interfaz. Una vez que hemos enviado los paquetes, es recomendable su visualización para observar de qué manera llega al destinatario y qué información podemos obtener de ellos (qué datos tienen, si algún campo ha sido modificado en el envío, etc).

\input{tablas/CasosdeUso_CasoCuatro}

\subsection{Requisitos No Funcionales}

\begin{enumerate}
    \item El programa debe ser intuitivo. Esto es, debe obtenerse una aplicación cuyo uso sea fácil y su interfaz sea entendible en un vistazo.
    \item El programa debe mostrar fallos de manera informativa, se debe entender por qué ha ocurrido dicho error para que pueda solucionarse.
   
\end{enumerate}

\section{Modelo del dominio}

A partir de los casos de uso se puede desarrollar un modelo de dominio como el indicado en la imagen \ref{ModeloDominio} para entender cómo se trabajará y qué objetos tendremos que tener en cuenta para empezar a especificar la solución. Como se ha indicado antes, la disponibilidad de la capa y de los protocolos depende de las restricciones que hemos establecido para nuestra aplicación.

La interfaz nos permitirá crear y editar un paquete, pero también crear un paquete de protocolo de capa superior, por lo que necesitaremos tanto la posibilidad hacerlo a través de  la edición en el campo \textit{payload} como a través de una creación y edición de un paquete de protocolo, motivo por el cual están relacionados. A su vez, de forma concurrente se estará escuchando una determinada dirección a la espera de paquetes a analizar.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\linewidth]{imagenes/Diagrama_ModeloDeDominio.eps}
    \caption{Diagrama de modelo de dominio de la aplicación }
    \label{ModeloDominio}
\end{figure}


% Tras eso , apartado 4 de Diseño y en él ya los Diagramas de Secuencia 