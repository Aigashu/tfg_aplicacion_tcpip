% !TeX spellcheck = es_ES

% Cada capítulo de la memoria de TFG comienza con \chapter{TÍTULO DEL CAPÍTULO}, tal y como requiere la normativa de la EPSJ
\chapter{INTRODUCCIÓN}  



\section{Introducción}

A lo largo de la educación de todo informático y todo ingeniero informático es necesario conocer la importancia de la gestión y funcionamiento de las redes de comunicación. Con esto, todo informático entiende cómo un mensaje se manda por la red, tanto Intranet como Internet, cómo se recibe y cómo se trata. Así, todo profesional de la informática conoce IP, TCP, UDP y el resto de los protocolos que se usan para conseguir que dos dispositivos informáticos se comuniquen. El problema  surge cuando el estudiante de informática descubre dichos protocolos por primera vez, pues puede resultar difícil comprender su funcionamiento completo pese a que la totalidad de la comunicación se entienda. Esto se debe a la cantidad de campos que cada capa de cada protocolo necesita para crear correctamente un paquete de datos.


El propósito de este proyecto (y por tanto de la aplicación que se va a crear y tratar) es ayudar a comprender cómo se produce el envío y recepción de un paquete de datos, permitiendo mostrar y editar para al usuario todo el contenido de los protocolos implicados en la creación de dicho paquete. El objetivo que se persigue con el proyecto es que, en su etapa final, el usuario pueda tratar con los protocolos planteados, observar y modificar los datos de sus campos, obtener información sobre estos, realizar pruebas de envío de paquetes, analizar cómo se realiza la recepción y, finalmente, comprender en su totalidad el funcionamiento de un paquete TCP/IP (\cite{Stallings}). Teniendo en cuenta la dedicación en horas establecida para el desarrollo de un Trabajo de Fin de Grado se decidirá el número de protocolos a tratar (hablaremos de esto en el apartado correspondiente).

Para empezar, debemos definir qué consideramos ''protocolo'' y qué consideramos ''paquete''. Para ello, nos ayudaremos de la Real Academia Española, la cual nos da las siguientes definiciones.


\begin{itemize}
    \item Protocolo (\href{https://dle.rae.es/protocolo}{acepción 5}) \cite{Protocolo} 
    \begin{quote}
    \textit{Conjunto de reglas que se establecen en el proceso de comunicación entre dos sistemas.}
    \end{quote}
    \item Paquete (\href{https://dle.rae.es/paquete}{acepción 9}) \cite{Paquete} \begin{quote}
        \textit{Conjunto de programas o de datos}.\end{quote}
\end{itemize}


Esto quiere decir que a la hora de crear un envío de mensajes en la red (entendiéndose mensaje como cualquier tipo de envío, desde acceso a una web hasta ver un vídeo) usamos paquetes de datos que se transferirán mediante un conjunto de reglas ya establecidas. Estas reglas son los diferentes protocolos de los que hablaremos a continuación, que agruparán estos datos a enviar en campos específicos.

Como hemos dicho, disponemos de diversos protocolos para hacer un envío en la red. Estos protocolos se organizan, según el modelo TCP/IP (\cite{Stallings}), en 5 capas que gestionan y definen la arquitectura de un mensaje en la red. Dichas capas están ordenadas de abajo a arriba según su nivel de abstracción del hardware, siendo la menos abstracta y de más bajo nivel la capa 1 y la mas abstracta (y por tanto más fácil de tratar para el usuario) la capa 5. Estas capas son las mencionadas en la siguiente lista.

\begin{enumerate}
    \item \textbf{Capa física}: es la capa encargada conectar dos extremos a partir de un medio. En este caso el medio de transporte es el soporte por el que se realiza el envío por la red, como la fibra óptica, la información es la secuencia de bits que forma el paquete y los extremos son el origen y el destino de esa secuencia de bits. Esta capa se encarga de traducir la información a señales físicas, eléctricas y electromagnéticas.
    \item \textbf{Capa de enlace}: comunica el nivel físico con el nivel de red, trabajando con direcciones MAC (\textit{Media Access Control}). Esta capa aporta la estructura que agrupa la secuencia de bits que la capa anterior se encarga de enviar y que forma las capas superiores, formando tramas, e identificando origen y destino con las direcciones MAC, que permiten enviar paquetes en una misma red local o LAN (\textit{Local Area Network}), etc. Es la encargada de conectar, por ejemplo, distintos ordenadores de un mismo despacho.
    \item\textbf{Capa de red}: capa encargada del envío entre redes locales distintas, como por ejemplo entre lugares distintos del mundo o entre zonas cercanas con distintas redes virtuales. Sobre esta capa trabaja el protocolo IP (\textit{Internet Protocol}), usado en la mayoría de conexiones en la red.
    \item\textbf{Capa de transporte}: capa encargada de identificar el origen y destino del paquete de capa de red, discriminando en ''puertos'' según el proceso que envió el paquete y el que debe recibirlo. Para esto disponemos de un protocolo de transporte orientado a conexión, como es TCP, y uno no orientado a conexíón, como es UDP.
    \item \textbf{Capa de aplicación}: la capa con la que el usuario interactúa normalmente, a través de software, ya que como su nombre indica es la capa donde se sitúan los protocolos encargados de generar información sobre una aplicación concreta. En esta capa se sitúan protocolos como HTTP, FTP, SMTP, etc.
\end{enumerate}

% "La estructura de red podria explicarse asi: FOTO DE CAPAS DE RED

Así, en el envío de un paquete de datos podemos observar la estructura mencionada anteriormente. Como se ve en la Figura \ref{EstructuraPaquetes} (\cite{Newmarch2017}), el usuario generará en la capa de aplicación un mensaje, que se encapsulará con una cabecera del protocolo que corresponda (en el ejemplo, TFTP) para introducirse como datos de la capa de transporte, a la que acompañará una cabecera UDP, en este caso, y así consecutivamente hasta la capa física, donde se enviará al destinatario. 


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\linewidth]{imagenes/packets.png}
    \caption{Estructura de encapsulado de un paquete de datos}
    \label{EstructuraPaquetes}
\end{figure}


Este método, llamado encapsulación, será ejecutado de la misma manera por el destinatario, que realizará el cálculo inverso, eliminando cabeceras de protocolo, para obtener del conjunto de bits la capa de enlace, de ella la capa de red y así hasta llegar a los datos del envío TFTP. Esto se puede ver más claro en la Figura \ref{EstructuraEnvioPaquetes}, obtenida del material de la asignatura ''Programación y administración de redes'' de este mismo grado, donde se puede observar que el proceso de encapsulación y desencapsulación se realiza también en caso de encontrarse algún router/switch en tramos del envío, como suele ser usual.


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/EstructuraEnvio.png}
    \caption{Estructura de envío de paquetes de red}
    \label{EstructuraEnvioPaquetes}
\end{figure}

\section{Estructura de la memoria}

Para hacer más cómoda su lectura, se ha dividido esta memoria en los siguientes puntos claves.

\begin{enumerate}
    \item \textbf{Primer capítulo. \textit{Introducción}}: en este capítulo se trata sobre el proyecto que se realizará y mediante qué procesos, de forma simplificada.
    \item \textbf{Segundo capítulo. \textit{Especificación del trabajo}}: en este capitulo elaboraremos detalladamente mediante técnicas de ingeniería del software cómo se desarrollará y planificará la solución al proyecto que intentamos construir.
    \item \textbf{Tercer capítulo. \textit{Análisis de requisitos}}: en esta etapa se identifican los requisitos esenciales del proyecto y se desglosarán según su realización, a través de diferentes tipos de diagramas. 
    \item \textbf{Cuarto capítulo. \textit{Diseño del proyecto}}: en esta etapa se profundizará, a través de diagramas más concretos, en los diferentes procesos observados en la etapa anterior.
    \item \textbf{Quinto capítulo. \textit{Desarrollo del proyecto}}: se tratará, a partir de la concepción completa de la lógica del proyecto ya realizada en apartados anteriores, la implementación del proyecto a partir de la valoración de todas las opciones posibles (lenguaje de programación, desarrollo del tipo de interfaz, etc). Se incluirán optativas y justificación de la elección final de cada una de las ramas a tratar.
    \item  \textbf{Sexto capítulo. \textit{Conclusión}}: se llegará a una conclusión con respecto a cómo ha acabado siendo nuestra aplicación, finalmente desarrollada. Se podrán plantear futuros añadidos para dicha aplicación.
\end{enumerate}
