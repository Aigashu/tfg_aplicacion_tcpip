        \chapter{Diseño}
\label{Disenio}
\section{Introducción}

Todo el diseño se ha apoyado en el lenguaje unificado de modelado, conocido por sus siglas UML, que establece un estándar en la estructura de representación y documentación de proyectos informáticos, a partir de distintos modelos de diagramas. En este caso, se han creado los diagramas UML mediante \hyperlink{https://plantuml.com/es/}{PlantUML}, una herramienta sencilla y de código abierto para poder realizar diagramas de cualquier tipo en Ingeniería del Software.

En nuestro caso y como dijimos en apartados anteriores, se realizará la aplicación con una metodología de desarrollo ágil, en la que se irán planteando desarrollos del proyecto en iteraciones que ir construyendo poco a poco, culminando en el producto final construido. Cada etapa de desarrollo se dividió usando las iteraciones planteadas a continuación.

\begin{enumerate} 
    \item Elaborar y construir de forma completa la capa de red. Esto es, construir todo el paquete de protocolo IP en nuestra aplicación, incluyendo la edición manual de su \textit{payload} para poder realizar pruebas de envío y recepción sin tener construido lo demás.
    \item Introducción a lo anterior del Protocolo ICMP. Esto nos servirá tanto para solicitar una respuesta a los \textit{routers}, a través del envió de un paquete construible, como observar los campos mal construidos (a propósito o no) de otros protocolos, a través de la recepción y lectura.
    \item Una vez realizado lo anterior se construirá la capa de transporte. Es decir, la elaboración de los protocolos TCP y UDP.
    \item Por último, se deberá incorporar la capa de aplicación a partir de, al menos, el protocolo HTTP.
    \item En caso de disponer de tiempo, se construirán los paquetes de protocolos, como se indicó en la lista \ref{Tabla.RequisitosOptativos}.
\end{enumerate}

Para elaborar cada parte, se procederá a una introducción y posteriormente el progreso que se realizó en dicha iteración.


\section{Diseño de la aplicación}

El diseño de la aplicación se dividirá en dos etapas: diseño del sistema y diseño de la interfaz. Estas dos fases se realizarán una tras otra, ya que una vez entendemos el sistema nos será más fácil realizar la interfaz, aunque alguna parte del desarrollo de la interfaz podrá producirse concurrentemente mientras se está ampliando el sistema.

\subsection{Diseño del Sistema}

Para desarrollar el sistema correctamente es imprescindible analizar con un diagrama de clases qué clases necesitaremos para que nuestra aplicación funcione correctamente, así como qué elementos (variables y métodos) necesitará cada clase, qué función cumplen en todo el conjunto y la relación entre clases. 

A continuación con el siguiente diagrama de clases, mostrado en la Figura \ref{RelacionClases}, representaremos las clases que necesitaremos para el correcto funcionamiento de nuestra aplicación, junto a la relación existente entre ellas. Es importante destacar que en este diagrama se tratarán las relaciones existentes entre cada clase y posteriormente se detallará independientemente cada clase, para poder entenderlas completamente.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.47\linewidth]{imagenes/Disenio_DiagramaClases.eps}
    \caption{Clases que necesitaremos para diseñar nuestra aplicación }
    \label{RelacionClases}
\end{figure}

\subsubsection{Diseño de Clases - IP}

Empezando con la clase que controlará el protocolo IP y guiándonos de la Figura \ref{ClaseIP}, necesitaremos un atributo para guardar el valor que introduzcamos en la interfaz de edición para cada campo del protocolo. Además, debemos incluir un atributo de una clase más: la clase del \textit{payload}, que controlará la creación del \textit{payload} mediante protocolo o mediante texto hexadecimal como ya se explicó anteriormente.

Como parte de los protocolos, crearemos un paquete de protocolo IP de tres formas distintas: por defecto, mediante un texto en formato JSON(\textit{JavaScript Object Notation})  que le enviaremos al crearlo desde la interfaz y mediante un texto JSON que incluye todos los campos menos el del \textit{payload}, obtenido mediante su propia clase. De la misma forma que ocurre con los constructores, tenemos dos métodos de envío de paquete: un paquete de texto normal y uno con el \textit{payload} creado de forma independiente (mediante la creación de un paquete de protocolo diferente). A modo de ayuda al usuario se ha incluido también la función \texttt{obtenerChecksum()}, que calculará el valor de \texttt{HeaderChecksum} a partir de los datos presentes en la interfaz al momento de llamar a esta función. Por último, se ha incluido la función \texttt{fieldHelp()}, que sirve de apoyo a la interfaz de edición de paquete de protocolos y para cada campo obtendrá una cadena de texto con información sobre el campo solicitado,  además de los \texttt{getter} y \texttt{setter}.

Para terminar, se podrá enviar el paquete completo de datos gracias a los métodos \texttt{sendPackage()} y \texttt{sendIPPackage()}. Estos métodos nos permitirán tanto mandar un paquete IP como construir un paquete de red parcial o completo con paquetes de protocolo de capas superiores e incluirlo al paquete IP para así enviar varias capas de información al mismo tiempo.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_IPClass.eps}
    \caption{Diagrama UML para la clase IP }
     \label{ClaseIP}
\end{figure}

\subsubsection{Diseño de Clases - Payload de IP}

Esta clase tendrá la función de transformar un \textit{payload} obtenido a través de un texto hexadecimal en un paquete con los datos del protocolo que incluya dicho \textit{payload}. Dicho protocolo dependerá de los datos de los campos que se identifiquen.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_PayloadClass.eps}
    \caption{Diagrama UML para la clase de control del \textit{payload} }
    \label{ClasePayload}
\end{figure}

\subsubsection{Diseño de Clases - ICMP}

Esta clase, como muestra la Figura \ref{ClaseICMP}, incluirá como parámetros los atributos para crear un paquete de protocolo de clase ICMP y el paquete de protocolo IP del que parte para construir el paquete completo. De la misma forma que en la clase IP (Figura \ref{ClaseIP}), necesitaremos los tres tipos de constructores, en caso de necesidad, junto a los \texttt{getter} y \texttt{setter} de cada atributo. Introducimos también el método \texttt{fieldHelp()}, para obtener información de dichos atributos en caso de que el usuario no entienda qué tipo de datos son o qué valores deben tener, el método \texttt{icmpToPayload()}, que transformará todo el paquete ICMP en un vector de bytes (o \textit{payload}) para añadirlo al envío de la clase IP, y la función \texttt{obtain\_Checksum()}, que permitirá calcular el \texttt{checksum} de la capa ICMP. 

Finalmente, encontramos las funciones \texttt{sendPackage()} y \texttt{send\_ICMPPackage()}. La primera será la llamada por la interfaz y se encargará de llamar a la función \texttt{icmpToPayload()}, para obtener el nuevo \textit{payload} de IP, y llamar a la segunda función junto al tamaño del paquete ICMP. La segunda se encargará de realizar el envío real del paquete, ya que enviará al paquete de protocolo IP el \textit{payload} y el tamaño del paquete ICMP para que este actualice el tamaño total de envío y su \textit{payload} actual y así envíe el paquete correctamente.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\linewidth]{imagenes/Disenio_ICMPClass.eps}
    \caption{Diagrama UML para la clase del protocolo ICMP }
    \label{ClaseICMP}
\end{figure}


\subsubsection{Diseño de Clases - TCP}

Esta clase, como muestra la Figura \ref{ClaseTCP}, incluirá como parámetros los atributos para crear un paquete de protocolos TCP e IP del que parte para construir el paquete completo. Al tener comportamientos similares, observamos que la clase TCP se parece mucho a la clase ICMP (Figura \ref{ClaseICMP}), por lo que al igual que esta necesitaremos, junto a los tres tipos de constructores y los \texttt{getter} y \texttt{setter} de cada atributo, un método que nos dé información sobre los campos de TCP, como es \texttt{fieldHelp()}. También necesitaremos un método que transforme el paquete de la capa TCP en el \textit{payload} de IP para crear un paquete de red con los datos de todas las capas con las que estamos trabajando, por lo que necesitamos la función \texttt{tcpToPayload()}. En caso de que el usuario quiera, se podrá también calcular el \texttt{checksum} del protocolo TCP a partir de sus datos, de la misma forma que lo hace el \textit{router} en el envío de un paquete, mediante el método \texttt{obtain\_Checksum()}. Para finalizar, es importante incluir también los métodos \texttt{sendPackage()} y \texttt{send\_TCPPackage()}.

La primera función será la llamada por la interfaz y se encargará de llamar a la función \texttt{tcpToPayload()}, para obtener el nuevo \textit{payload} de IP, y llamar a la segunda función junto al tamaño del paquete TCP. 

La segunda se encargará de realizar el envío real del paquete, ya que enviará al paquete de protocolo IP el \textit{payload} y el tamaño del paquete TCP para que este actualice el tamaño total de envío y su \textit{payload} actual y así envíe el paquete correctamente.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_TCPClass.eps}
    \caption{Diagrama UML para la clase del protocolo TCP }
    \label{ClaseTCP}
\end{figure}

\subsubsection{Diseño de Clases - UDP}

Esta clase, como muestra la Figura \ref{ClaseUDP}, incluirá como parámetros los atributos para crear un paquete de protocolo de clase UDP y el paquete de protocolo IP del que parte para construir el paquete completo. Al tener comportamientos similares, observamos que la clase TCP se parece mucho a la clase ICMP y TCP (Figuras \ref{ClaseICMP} y \ref{ClaseTCP}), por lo tanto harán falta los tres tipos de constructores, junto a los \texttt{getter} y \texttt{setter} de cada atributo. Además, es imprescindible el convertir los datos de dicho protocolo en un \textit{payload} de la clase IP, al igual que en los casos anteriores, para enviar el paquete de datos completo por lo que se incluirá el método \texttt{udpToPayload()}. El método \texttt{obtain\_Checksum()} hará que sea la aplicación quien calcule el \texttt{checksum} del paquete UDP, en lugar del \textit{router}.  Introducimos también el método \texttt{fieldHelp()}, para obtener información de dichos atributos en caso de que el usuario no entienda qué tipo de datos son o qué valores deben tener, y las funciones \texttt{sendPackage()} y \texttt{send-UDPPackage()}. 

La primera será la llamada por la interfaz y se encargará de llamar a la función \texttt{udpToPayload()}, para obtener el nuevo \textit{payload} de IP, y llamar a la segunda función junto al tamaño del paquete UDP. 

La segunda se encargará de realizar el envío real del paquete, ya que enviará al paquete de protocolo IP el \textit{payload} y el tamaño del paquete UDP para que este actualice el tamaño total de envío y su \textit{payload} actual y así envíe el paquete correctamente.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\linewidth]{imagenes/Disenio_UDPClass.eps}
    \caption{Diagrama UML para la clase del protocolo UDP }
    \label{ClaseUDP}
\end{figure}

\subsubsection{Diseño de Clases - HTTP}

Esta clase, como muestra la Figura \ref{ClaseHTTP}, incluirá como parámetros los atributos para crear un paquete de protocolo de clase HTTP y el paquete de protocolo TCP del que parte cada paquete HTTP para construir un paquete completo. Al tener comportamientos similares, observamos que la clase HTTP se parece mucho a las clases anteriores (Figuras \ref{ClaseICMP}, \ref{ClaseTCP} y \ref{ClaseUDP}), por lo que al igual que esta necesitaremos, junto a los tres tipos de constructores y los \texttt{getter} y \texttt{setter} de cada atributo, un método que nos dé información sobre los campos de HTTP, como es \texttt{fieldHelp()}. 

Debido a que no sabemos cuántas opciones de cabecera HTTP introducirá el usuario, deberemos incluir una lista tanto en las líneas de cabecera (que indican información desde los idiomas que se pueden solicitar a la página web a desde qué aplicación se obtiene el paquete) como en las líneas de cabecera de entidad (que da información como el tipo de contenido MIME (\textit{Multipurpose Internet Mail Extensions}) que está enviando el usuario, para que la página no necesite averiguarlo y la carga sea más rápida). También necesitaremos un método que transforme el mensaje HTTP en el \textit{payload} de TCP para crear un paquete de red con los datos de la capa de aplicación, por lo que necesitamos la función \texttt{httpToPayload()}. Para finalizar, es importante incluir también los métodos \texttt{sendPackage()} y \texttt{send\_HTTPPackage()}.

La primera función será la llamada por la interfaz y se encargará de llamar a la función \texttt{httpToPayload()}, para obtener el nuevo \textit{payload} de TCP, y llamar a la segunda función junto al tamaño del paquete HTTP. 

La segunda se encargará de realizar el envío real del paquete, ya que enviará al paquete de protocolo TCP el \textit{payload} y el tamaño del paquete HTTP para que este actualice el tamaño total de envío y su \textit{payload} actual y así envíe el paquete correctamente.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_HTTPClass.eps}
    \caption{Diagrama UML para la clase del protocolo HTTP }
    \label{ClaseHTTP}
\end{figure}



\subsubsection{Diseño de Clases - Interfaz de edición}

En este caso, al ser la interfaz una capa de visionado y no del funcionamiento de la aplicación, todos sus métodos y atributos irán enfocados a enviar datos a las clases que contienen la lógica de la aplicación para el envío de paquetes de protocolo. Por tanto, como refleja la Figura \ref{ClaseInterfaz}, en esta clase tendremos campos donde guardaremos los datos que escribamos para cada campo de cada protocolo a escribir (campos de IP por ejemplo), los botones de ayuda para cada uno de los campos a editar y el botón de enviar paquete.

Los métodos de esta clase servirán de la misma manera como soporte para la aplicación, como el \texttt{jsonToIP()} que convertirá todos los campos de la interfaz de IP en un texto en formato JSON para la creación de un paquete de protocolo IP con el constructor parametrizado del que hablamos anteriormente. Además, tendremos una llamada al constructor por defecto de IP para que, como dijimos en apartados anteriores, se cree un paquete IP con datos válidos predeterminados. En caso de pulsar el botón de envío se ejecutará el método \texttt{sendPackage()} que enviará con los datos establecidos en los campos guardados en cada una de las clases de los protocolos un paquete de datos completo. 

Para trabajar con la interfaz visible usaremos el método \texttt{saveFields()} que guardará los datos escritos en los campos en los atributos, a forma de copia de seguridad o \textit{backup} en caso de necesidad y el método \texttt{updateInterface()}, con dos variantes. En la primera, el método sin atributos de cabecera, se guardará la información de los campos, al igual que el método anteriormente explicado, y recargará la interfaz. En el segundo caso, el método con el atributo  \texttt{idProtocol}, la clase generará una nueva interfaz a partir de los nuevos protocolos que se especifiquen en cabecera. Esto es, si el id de Protocolo indicado es de un protocolo de capa de aplicación y en el momento de lanzar este método solo trabajábamos con una capa de red, se generará una interfaz de edición de paquete de protocolos de red, transporte y aplicación, generando datos para sus campos de forma predeterminada, como ya se había hecho en la primera creación, y actualizará los campos que ya teníamos modificados de la misma forma que lo había hecho \texttt{updateInterface()}.

Introduciremos también dos funciones: \texttt{popupMensaje()} que nos avisará mediante una ventana \textit{PopUp} de diversa información, como si ha habido error en el envío de un paquete o ha sido un envío satisfactorio, y \texttt{obtenerChecksum()}, que de ser ejecutada por el usuario solicitará a la clase IP el cálculo del valor de \textit{HeaderChecksum} del paquete IP a partir de los datos que existan en la interfaz en el momento de llamar a esta función.

Por último, usaremos el método \texttt{fieldHelp()}, que solicitará a cada clase responsable de un protocolo, dependiendo de cual se solicite en su cabecera, información sobre el campo solicitado también en cabecera.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_InterfazClass.eps}
    \caption{Diagrama UML para la interfaz de creación y envío de paquetes de datos }
    \label{ClaseInterfaz}
\end{figure}

\subsubsection{Diseño de Clases: Interfaz de recogida de datos}

Esta clase, representada en la Figura \ref{ClaseRecogida}, se encargará de recoger paquetes de datos enviados a una interfaz de red concreta de forma concurrente con la edición y envío de paquetes de protocolos. Para ello, la interfaz tendrá un campo para la interfaz de red de la que queramos obtener datos. 

A partir de este atributo y en un hilo independiente se estará ejecutando el método \texttt{searchPackage()}, que recogerá durante unos segundos todos los paquetes obtenidos con estos datos y los almacenará en la lista \texttt{packageList}. Dicha lista se mostrará en la interfaz y pulsando en cualquiera de sus elementos se ejecutará el método \texttt{openPackage()}, que abrirá una interfaz que nos permitirá leer los datos del paquete de protocolo seleccionado. La interfaz tendrá la misma estructura que se puede observar en la interfaz de creación de paquetes de protocolo, con la salvedad de que esta es una interfaz de solo lectura.

Además, como añadido para la comodidad del usuario, se han añadido los botones \texttt{searchButton} y \texttt{cleanButton}, que ante su uso llamarán a las funciones \texttt{stopSearch()} y \texttt{cleanPackageList()} . Estas funciones cesarán la búsqueda constante y eliminarán todos los paquetes encontrados anteriormente. Esto facilitará el uso de la aplicación al usuario, al poder centrarse en algunos paquetes en concreto y analizarlos sin que la lista aumente, borrarlos cuando ya no sean de utilidad y que así no sobrepoblen la lista junto a los nuevos paquetes a analizar.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_InterfazRecogidaClass.eps}
    \caption{Diagrama UML para la clase recolectora de paquetes de datos }
    \label{ClaseRecogida}
\end{figure}

\subsection{Diagramas de Secuencia}

\subsubsection{IP - Creación y edición del paquete}

El diagrama mostrado en la Figura \ref{DiagSecCreacionPaquete} nos enseña cómo se comportará la aplicación y cómo interaccionará el usuario para realizar la actividad que deseamos, en este caso la creación y edición de un paquete de protocolo de red. Observamos cómo la aplicación a través de la interfaz permite que el usuario genere un dialogo, creando el paquete, modificando sus campos, editándolos en caso de error y por último enviándolos. En caso de que este no quiera o no sepa modificar los campos, la aplicación le brinda unos datos predeterminados para cada campo (en el caso de \textit{checksum} incluso permitiendo su cálculo automático), que permitirán una creación y envío satisfactorios del paquete.


\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecCreacionIP.eps}
    \caption{Diagrama de secuencia para la creación y edición del paquete IP}
    \label{DiagSecCreacionPaquete}
\end{figure}

\subsubsection{IP: Envío del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEnvioPaquete.eps}
    \caption{Diagrama de secuencia para el envío del paquete IP}
    \label{DiagSecEnvioPaquete}
\end{figure}

Como muestra la Figura \ref{DiagSecEnvioPaquete}, el envío del paquete de datos será una actividad sencilla pues lo único que deberá hacer será guardar los datos de cada campo en caso de que sea necesario volver a usarlos posteriormente, generar un paquete con dichos campos y enviar a la red el paquete, que llegará a su destinatario sin necesidad de añadir nada más, pues todos los datos necesarios del mismo estarán guardados en el paquete.



\subsubsection{ICMP - Creación y edición del paquete}


\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecCreacionICMP.eps}
    \caption{Diagrama de secuencia para la creación y edición del paquete ICMP}
    \label{DiagSecCreacionPaqueteICMP}
\end{figure}

El diagrama mostrado en la Figura \ref{DiagSecCreacionPaqueteICMP} nos muestra cómo se creará un paquete del protocolo ICMP. Observamos que el comportamiento es muy parecido al de la creación de un paquete IP (\ref{DiagSecCreacionPaquete}), ya que tiene muchas similitudes como el acceso a ayudas para los campos del protocolo, la generación de \textit{checksum} automática o el comportamiento de la interfaz al actualizar datos cuando el usuario modifica algún campo. Encontraremos diferencias principalmente en el envío de un paquete ICMP, pues como veremos en el apartado siguiente necesita modificar el paquete IP.




\subsubsection{ICMP - Envío del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEnvioPaqueteICMP.eps}
    \caption{Diagrama de secuencia para el envío del paquete ICMP}
    \label{DiagSecEnvioPaqueteICMP}
\end{figure}

Como muestra la Figura \ref{DiagSecEnvioPaqueteICMP}, el envío del paquete de datos ICMP es en última instancia el envío de un paquete IP, pero cuyo \textit{payload} se verá modificado pues incluirá la información del paquete ICMP. Para poder hacer esto, ante la solicitud del usuario de enviar un paquete, este deberá convertir todo el paquete del protocolo en un \textit{payload} o vector de \textit{bytes}, para posteriormente actualizar los datos del \textit{payload} y tamaños del paquete IP, añadiendo la información de este paquete ICMP y por último enviar (ahora sí) toda la información mediante el método de envío del paquete de  protocolo IP (Figura \ref{DiagSecEnvioPaquete}).


\subsubsection{TCP - Creación y edición del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_DiagSecCreacionTCP.eps}
    \caption{Diagrama de secuencia para la creación y edición del paquete TCP}
    \label{DiagSecCreacionPaqueteTCP}
\end{figure}
El diagrama mostrado en la Figura \ref{DiagSecCreacionPaqueteTCP} nos muestra cómo se creará un paquete del protocolo TCP. Observamos que es muy similar al de la creación de un paquete IP (Figura \ref{DiagSecCreacionPaquete}), ya que tiene muchas similitudes como el acceso a ayudas para los campos del protocolo, la posibilidad de generar el \textit{checksum} automáticamente o el comportamiento de la interfaz al actualizar datos cuando el usuario modifica algún campo. Las principales diferencias se encuentran en el envío de paquetes TCP, pues necesitaremos modificar el paquete IP para incluir los datos de la cabecera TCP.


\subsubsection{TCP - Envío del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEnvioPaqueteTCP.eps}
    \caption{Diagrama de secuencia para el envío del paquete TCP}
    \label{DiagSecEnvioPaqueteTCP}
\end{figure}


Como muestra la Figura \ref{DiagSecEnvioPaqueteTCP}, el envío del paquete de datos TCP es en realidad el envío de un paquete IP, pero cuyo \textit{payload} se modifica para incluir la información del paquete TCP. Para esto, ante un envío solicitado por el usuario, la aplicación convertirá todo el paquete de protocolo en un \textit{payload} o vector de \textit{bytes}, para posteriormente actualizar los datos del \textit{payload} y tamaños del paquete IP, añadiendo la información de este paquete TCP y por último enviar un paquete con toda la información mediante el método de envío del paquete de protocolo IP (Figura \ref{DiagSecEnvioPaquete}).


\subsubsection{UDP - Creación y edición del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_DiagSecCreacionUDP.eps}
    \caption{Diagrama de secuencia para la creación y edición del paquete UDP}
    \label{DiagSecCreacionPaqueteUDP}
\end{figure}

El diagrama mostrado en la Figura \ref{DiagSecCreacionPaqueteUDP} nos muestra cómo se creará un paquete del protocolo UDP. Al ser bastante similar con la creación de paquetes IP (Figura \ref{DiagSecCreacionPaquete}), su comportamiento será replicado en gran medida, incluyendo la creación de un paquete UDP el acceso a ayudas para los campos del protocolo, el cálculo automático del campo \textit{checksum} o el comportamiento de la interfaz al actualizar datos cuando el usuario modifica algún campo. Las mayores diferencias se encontrarán en el envío de paquetes UDP, pues deberemos hacer modificaciones al envío de un paquete IP.




\subsubsection{UDP - Envío del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEnvioPaqueteUDP.eps}
    \caption{Diagrama de secuencia para el envío del paquete UDP}
    \label{DiagSecEnvioPaqueteUDP}
\end{figure}

Como muestra la Figura \ref{DiagSecEnvioPaqueteTCP}, el envío del paquete de datos UDP es en última instancia el envío de un paquete IP, pero cuyo \textit{payload} se verá modificado pues incluirá la información del paquete UDP. Para poner en marcha esto, cuando se solicita el envío de un paquete, la aplicación deberá convertir todo el paquete de protocolo en un \textit{payload} o vector de \textit{bytes}, para posteriormente actualizar los datos del \textit{payload} y tamaños del paquete IP, añadiendo la información de este paquete UDP y así realizar el envío correcto con toda la información del paquete mediante el método de envío del paquete de protocolo IP (Figura \ref{DiagSecEnvioPaquete}).



\subsubsection{HTTP - Creación y edición del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.9\linewidth]{imagenes/Disenio_DiagSecCreacionHTTP.eps}
    \caption{Diagrama de secuencia para la creación y edición del paquete HTTP}
    \label{DiagSecCreacionPaqueteHTTP}
\end{figure}

El diagrama mostrado en la Figura \ref{DiagSecCreacionPaqueteHTTP} nos muestra cómo se creará un paquete del protocolo HTTP. Observamos que es muy similar al de la creación de paquetes de protocolo anterior  (Figuras \ref{DiagSecCreacionPaquete}, \ref{DiagSecCreacionPaqueteICMP}, \ref{DiagSecCreacionPaqueteTCP} y \ref{DiagSecCreacionPaqueteUDP}), ya que tiene muchas similitudes como el comportamiento de la interfaz al actualizar datos cuando el usuario modifica algún campo o las ayudas al usuario. Las principales diferencias se encuentran en el envío de paquetes HTTP, pues necesitaremos usar el envío de un paquete TCP.




\subsubsection{HTTP - Envío del paquete}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEnvioPaqueteHTTP.eps}
    \caption{Diagrama de secuencia para el envío del paquete HTTP}
    \label{DiagSecEnvioPaqueteHTTP}
\end{figure}

Como muestra la Figura \ref{DiagSecEnvioPaqueteHTTP}, el envío del paquete de datos HTTP es en realidad el envío de un paquete TCP, pero cuyo \textit{payload} se modifica para que sea la información de envío de un paquete HTTP. Para esto, ante un envío solicitado por el usuario, la aplicación deberá enviar un mensaje de inicio de conexión TCP. Una vez hecho esto, esperará el mensaje de confirmación de conexión y convertirá todo el paquete de protocolo HTTP en un \textit{payload} o vector de \textit{bytes}, para posteriormente actualizar los datos del \textit{payload} y tamaños del paquete TCP, añadiendo la información de este paquete HTTP y por último enviar un paquete ACK de confirmación con toda la información mediante el método de envío del paquete de protocolo TCP (\ref{DiagSecEnvioPaqueteTCP}).





\subsubsection{Recepción del paquete}

Continuando con lo que se observa en la Figura \ref{DiagSecRecepcionPaquete} la recepción de datos se muestra como una acción simple, necesitando solo introducir la interfaz de red para obtener una lista de paquetes a observar. Una vez tenemos dicha lista, podremos seleccionar cualquier paquete de esta y analizar los protocolos que este paquete use y qué datos tiene cada protocolo en sus campos. Además, para facilitar la lectura de la interfaz, el usuario podrá borrar los paquetes obtenidos hasta el momento o pausar la búsqueda, para reanudarla posteriormente, en caso de que suponga una molestia o incomode en la selección de paquetes. 


En el caso de parar o reanudar la búsqueda, el valor del botón cambiará para favorecer la retroalimentación interfaz-usuario. Así, si la búsqueda está pausada y se observa que el botón tiene escrito ''Buscar'' se podrá inferir que está pausada y reanudarla. Lo mismo ocurrirá cuando la interfaz busque paquetes, pues el usuario leerá ''Parar'' y entenderá que puede pausar la búsqueda cuándo lo necesite.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecRecepcionPaquete.eps}
    \caption{Diagrama de secuencia para la recepción de paquetes de red}
    \label{DiagSecRecepcionPaquete}
\end{figure}

\subsubsection{Edición del \textit{payload}}


\begin{figure}[h!]
    \centering
    \includegraphics[width=\linewidth]{imagenes/Disenio_DiagSecEdicionPayload.eps}
    \caption{Diagrama de secuencia para la edición de un payload de protocolo}
    \label{DiagSecEdicionPayload}
\end{figure}

La edición del \textit{payload}, mostrada en la Figura \ref{DiagSecEdicionPayload}, refleja las dos posibles formas de generar el paquete de datos: a través de un texto hexadecimal que incluya información sobre los datos para dicho campo o la edición del campo de \textit{payload} como un paquete de protocolo de capa superior a la que trabajamos actualmente. Se realice la edición de una forma u otra, el resultado será el mismo: un \textit{payload} completo.


